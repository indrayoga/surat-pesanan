<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	protected $title='SIM RS - Sistem Informasi Rumah Sakit';

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('utilities');
		$this->load->library('pagination');
		$this->load->model('mlaporan');
		$this->load->model('mjurnal');
		
	}
	public function index()
	{
		$no_penerimaan=$this->input->post('no_penerimaan');
		$unit_kerja=$this->input->post('unit_kerja');
		$periodeawal=$this->input->post('periodeawal');
		$periodeakhir=$this->input->post('periodeakhir');
		$posted=$this->input->post('posted');

		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'main.js','style-switcher.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'dataunitkerja'=>$this->mlaporan->ambilData('unit_kerja'),
			'items'=>$this->mlaporan->getAllPenerimaan($no_penerimaan,$unit_kerja,$periodeawal,$periodeakhir,$posted)
			);
		$this->load->view('header',$dataheader);
		$this->load->view('penerimaan/daftarpenerimaan',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function kasumumpenerimaan()
	{
		$tahun=date('Y');
		$bulan=date('m');
		$status=1;
		if($this->input->post('tahun')!=''){
			$tahun=$this->input->post('tahun');
		}
		if($this->input->post('bulan')!=''){
			$bulan=$this->input->post('bulan');
		}
		if($this->input->post('status')!=''){
			$status=$this->input->post('status');
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'items'=>$this->mlaporan->getAllKasUmumPenerimaan($tahun,$bulan,$status),
			'tahun'=>$tahun,
			'bulan'=>$bulan,
			'status'=>$status
			);

		$this->load->view('header',$dataheader);
		$this->load->view('laporan/kasumumpenerimaan',$data);
		$this->load->view('footer',$datafooter);
	}

	public function bukubesar()
	{
		$tahun=date('Y');
		$periode_awal=date('m');
		$periode_akhir=date('m');
		$akun_dari='1';
		$akun_sampai='521';

		if($this->input->post('tahun')!=''){
			$tahun=$this->input->post('tahun');
		}
		if($this->input->post('periode_awal')!=''){
			$periode_awal=$this->input->post('periode_awal');
		}
		if($this->input->post('periode_akhir')!=''){
			$periode_akhir=$this->input->post('periode_akhir');
		}

		if($this->input->post('akun_dari')!=''){
			$akun_dari=$this->input->post('akun_dari');
		}
		if($this->input->post('akun_sampai')!=''){
			$akun_sampai=$this->input->post('akun_sampai');
		}

		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'items'=>$this->mlaporan->getAllBukuBesar($akun_dari,$akun_sampai,$tahun,$periode_awal,$periode_akhir),
			'dataakun'=>$this->mlaporan->ambilData('accounts'),
			'tahun'=>$tahun,
			'periode_awal'=>$periode_awal,
			'periode_akhir'=>$periode_akhir,
			'akun_sampai'=>$akun_sampai,
			'akun_dari'=>$akun_dari
			);

		$this->load->view('header',$dataheader);
		$this->load->view('laporan/bukubesar',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function jurnal()
	{
		$tahun=date('Y');
		$bulan=date('m');
		$jurnal=array();
		$periode_awal=date('d-m-Y');
		$periode_akhir=date('d-m-Y');
		$cari=$this->input->post('cari');
		if(empty($cari))$cari=1;

		if($this->input->post('tahun')!=''){
			$tahun=$this->input->post('tahun');
		}
		if($this->input->post('bulan')!=''){
			$bulan=$this->input->post('bulan');
		}
		if($this->input->post('periode_awal')!=''){
			$periode_awal=$this->input->post('periode_awal');
		}
		if($this->input->post('periode_akhir')!=''){
			$periode_akhir=$this->input->post('periode_akhir');
		}
		$jurnal=$this->input->post('jurnal');

		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css','chosen.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/chosen.jquery.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'dataakun'=>$this->mlaporan->ambilData('accounts'),
			'jurnal'=>$this->mlaporan->ambilData('acc_journal'),
			'tahun'=>$tahun,
			'periode_awal'=>$periode_awal,
			'periode_akhir'=>$periode_akhir,
			'bulan'=>$bulan,
			'jurnal1'=>$jurnal,
			'cari'=>$cari
			);

		if($cari==1){
			$data['items']=$this->mlaporan->getAllJurnal($tahun,$bulan,'','','');			
		}
		if($cari==2){
			$data['items']=$this->mlaporan->getAllJurnal('','',$jurnal,convertDate($periode_awal),convertDate($periode_akhir));			
		}

		$this->load->view('header',$dataheader);
		$this->load->view('laporan/jurnal',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function penerimaanpenyetoran()
	{
		$tahun=date('Y');
		$bulan=date('m');
		$status=1;
		if($this->input->post('tahun')!=''){
			$tahun=$this->input->post('tahun');
		}
		if($this->input->post('bulan')!=''){
			$bulan=$this->input->post('bulan');
		}
		if($this->input->post('status')!=''){
			$status=$this->input->post('status');
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'items'=>$this->mlaporan->getAllKasUmumPenerimaan($tahun,$bulan,$status),
			'tahun'=>$tahun,
			'bulan'=>$bulan,
			'status'=>$status
			);

		$this->load->view('header',$dataheader);
		$this->load->view('laporan/penerimaanpenyetoran',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function aktivitas()
	{
		$tahun=date('Y');
		$bulan=date('m');
		$level='';
		$status=1;
		if($this->input->post('tahun')!=''){
			$tahun=$this->input->post('tahun');
		}
		if($this->input->post('bulan')!=''){
			$bulan=$this->input->post('bulan');
		}
		if($this->input->post('level')!=''){
			$level=$this->input->post('level');
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'items'=>$this->mlaporan->getAllAktifitas($tahun,$bulan,$level),
			'level'=>$level,
			'tahun'=>$tahun,
			'bulan'=>$bulan
			);

		$this->load->view('header',$dataheader);
		$this->load->view('laporan/aktifitas',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function neracasaldo()
	{
		$tahun=date('Y');
		$bulan=date('m');
		$akun_dari='1';
		$akun_sampai='521';
		if($this->input->post('tahun')!=''){
			$tahun=$this->input->post('tahun');
		}
		if($this->input->post('bulan')!=''){
			$bulan=$this->input->post('bulan');
		}
		if($this->input->post('akun_dari')!=''){
			$akun_dari=$this->input->post('akun_dari');
		}
		if($this->input->post('akun_sampai')!=''){
			$akun_sampai=$this->input->post('akun_sampai');
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'items'=>$this->mlaporan->getAllNeracaSaldo($tahun,$bulan,$akun_dari,$akun_sampai),
			'dataakun'=>$this->mlaporan->ambilData('accounts'),
			'akun_sampai'=>$akun_sampai,
			'akun_dari'=>$akun_dari,
			'tahun'=>$tahun,
			'bulan'=>$bulan
			);

		$this->load->view('header',$dataheader);
		$this->load->view('laporan/neracasaldo',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function caripenerimaanpenyetoran()
	{
		$tahun=$this->input->post('tahun');
		$bulan=$this->input->post('bulan');
		$status=$this->input->post('status');
		$submit=$this->input->post('submit');

		$msg=array();

		$msg['items']=$this->mlaporan->getAllKasUmumPenerimaan($tahun,$bulan,$status);
		$msg['status']=1;
		$msg['tahun']=$tahun;
		$msg['bulan']=$bulan;
		$msg['statuslap']=$status;
		$msg['pesan']="";
		$msg['cetak']=0;
		if($submit=="cetak"){
			$msg['cetak']=1;
		}
		echo json_encode($msg);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */