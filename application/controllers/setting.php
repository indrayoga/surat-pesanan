<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'controllers/rumahsakit.php');
class Setting extends Rumahsakit {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	protected $title='SIM RS - Sistem Informasi Rumah Sakit';
	protected $akses='109';
	protected $aksesakuntansi='107';

	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('utilities');
		$this->load->model('msetting');
		$this->load->model('mposting');
		$this->load->model('pasien/mpasien');
		$this->load->library('pagination');
		
	}
	public function index()
	{

	}

	public function restricted(){
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$this->load->view('master/header',$dataheader);
		$data=array();
		parent::view_restricted($data);
		$this->load->view('footer');
	}

	public function saldoawal($tahun="")
	{
		if(!$this->muser->isAkses("905")){
			$this->restricted();
			return false;
		}
		if(empty($tahun))$tahun=date('Y');
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js',
							'main.js','style-switcher.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'dataakun'=>$this->msetting->ambilData('accounts','type="D"','account'),
			'tahun'=>$tahun
			);
		//debugvar($data['akunkas']);
		if($this->session->userdata('akses')==$this->aksesakuntansi){
			$this->load->view('keuangan/header',$dataheader);
		}else{
			$this->load->view('master/header',$dataheader);
		}
		$this->load->view('setting/saldoawal',$data);
		$this->load->view('footer',$datafooter);

	}

	public function dokterunit($dokter="")
	{

		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'datadokter'=>$this->msetting->ambilData('apt_dokter'),
			'dataunit'=>$this->msetting->ambilData('unit_kerja','parent is not null'),
			'dok'=>$dokter
			);
			$data['datadokterunit']=array();
		if(!empty($dokter)){
			$itemsdokterunit=$this->msetting->ambilData('dokter_unit_kerja','kd_dokter = "'.$dokter.'"');
			if(!empty($itemsdokterunit)){
				foreach($itemsdokterunit as $itemdokterunit){
					$data['datadokterunit'][]=$itemdokterunit['kd_unit'];
				}
			//debugvar($data['datadokterunit']);
			}
		}	
		//debugvar($data['akunkas']);

		$this->load->view('master/header',$dataheader);
		$this->load->view('setting/dokterunit',$data);
		$this->load->view('footer',$datafooter);

	}


	public function simpansaldoawal(){
		if(!$this->muser->isAkses($this->akses) || !$this->muser->isAkses($this->aksesakuntansi)){
			redirect('/home/');
			return false;			
		}
		$akun=$this->input->post('akun');
		$debit=$this->input->post('debit');
		$kredit=$this->input->post('kredit');
		$tahun=$this->input->post('tahun');

		if(!empty($akun)){
			foreach ($akun as $key => $value) {
				# code...
				if(empty($debit[$key]) && empty($kredit[$key]))continue;
				$item=$this->msetting->ambilItemData('acc_value','years="'.$tahun.'" and account="'.$value.'"');
				if(empty($item)){
					$data=array(
						'years'=>$tahun,
						'account'=>$value,
						'DB0'=>$debit[$key],
						'CR0'=>$kredit[$key]
						);
					$this->msetting->insert('acc_value',$data);					
				}else{
					//$this->mposting->updateValue0($debit[$key],$value,$tahun,1);										
					//$this->mposting->updateValue0($kredit[$key],$value,$tahun,0);										
					$this->mposting->updateValue0versi1($debit[$key],$value,$tahun,1);										
					$this->mposting->updateValue0versi1($kredit[$key],$value,$tahun,0);										
				}
			}
			for($xyz=5;$xyz>=1;$xyz--){
				$this->mposting->updateValueDBCR0($tahun);
			}
				$this->mposting->updateValue13versi1();
		}
		
		redirect('/setting/saldoawal');
	}
	
	public function simpandokterunit(){
		if(!$this->muser->isAkses($this->akses)){
			redirect('/home/');
			return false;			
		}
		$dokter=$this->input->post('dokter');
		$unit=$this->input->post('unit');

		if(!empty($unit)){
			$this->msetting->delete("dokter_unit_kerja","kd_dokter='".$dokter."'");
			foreach($unit as $nt){
				$datadokterunit=array(
				'kd_dokter'=>$dokter,
				'kd_unit'=>$nt
				);
				$this->msetting->insert('dokter_unit_kerja',$datadokterunit);
			}
		}
		
		redirect('/setting/dokterunit');
	}
	
	public function akunkas()
	{
		if(!$this->muser->isAkses("906")){
			$this->restricted();
			return false;
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js',
							'main.js','style-switcher.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'dataakun'=>$this->msetting->ambilData('accounts','type="D"'),
			'akunkas'=>$this->msetting->ambilItemData('sys_setting','key_data="ACCOUNT_KAS_BANK"')
			);
		//debugvar($data['akunkas']);

		if($this->session->userdata('akses')==$this->aksesakuntansi){
			$this->load->view('keuangan/header',$dataheader);
		}else{
			$this->load->view('master/header',$dataheader);
		}
		$this->load->view('setting/akunkas',$data);
		$this->load->view('footer',$datafooter);

	}

	public function akunpendapatan()
	{
		if(!$this->muser->isAkses($this->akses) || !$this->muser->isAkses($this->aksesakuntansi)){
			redirect('/home/');
			return false;			
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js',
							'main.js','style-switcher.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'dataakun'=>$this->msetting->ambilData('accounts','type="D"'),
			'akunkas'=>$this->msetting->ambilItemData('sys_setting','key_data="ACCOUNT_PENDAPATAN"')
			);
		//debugvar($data['akunkas']);

		if($this->session->userdata('akses')==$this->aksesakuntansi){
			$this->load->view('keuangan/header',$dataheader);
		}else{
			$this->load->view('master/header',$dataheader);
		}
		$this->load->view('setting/akunpendapatan',$data);
		$this->load->view('footer',$datafooter);

	}

	public function aktivalancar()
	{
		if(!$this->muser->isAkses("907")){
			$this->restricted();
			return false;
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js',
							'main.js','style-switcher.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'dataakun'=>$this->msetting->ambilData('accounts','type="D"'),
			'akunkas'=>$this->msetting->ambilItemData('sys_setting','key_data="ACCOUNT_AKTIVA"')
			);
		//debugvar($data['akunkas']);

		if($this->session->userdata('akses')==$this->aksesakuntansi){
			$this->load->view('keuangan/header',$dataheader);
		}else{
			$this->load->view('master/header',$dataheader);
		}
		$this->load->view('setting/aktivalancar',$data);
		$this->load->view('footer',$datafooter);

	}

	public function pelayananrwj()
	{
		if(!$this->muser->isAkses($this->akses)){
			redirect('/home/');
			return false;			
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js',
							'main.js','style-switcher.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'datapelayanan'=>$this->mpasien->getTarifLayanan(),			
			'item'=>$this->msetting->ambilItemData('sys_setting','key_data="REGISTRASI_RWJ"')
			);
		//debugvar($data['akunkas']);

		$this->load->view('master/header',$dataheader);
		$this->load->view('setting/pelayananrwj',$data);
		$this->load->view('footer',$datafooter);

	}

	public function user()
	{
		if(!$this->muser->isAkses("900")){
			$this->restricted();
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'items'=>$this->msetting->ambilData('user')
			);
		//debugvar($data['akunkas']);

		$this->load->view('master/header',$dataheader);
		$this->load->view('setting/user',$data);
		$this->load->view('footer',$datafooter);

	}


	public function tarifunit($unit1='')
	{
		if(!$this->muser->isAkses("904")){
			$this->restricted();
			return false;
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'datapoli'=>$this->msetting->ambilData('unit_kerja'),
			'datapelayanan'=>$this->msetting->ambilData('list_pelayanan'),			
			'datapelayananunit'=>$this->msetting->getAllPelayananUnit($unit1),
			'unit'=>$unit1
			);

		$this->load->view('master/header',$dataheader);
		$this->load->view('setting/tarifunit',$data);
		$this->load->view('footer',$datafooter);

	}

	public function tarifunitpaket($unit1='',$paket='')
	{
		if(!$this->muser->isAkses("904")){
			$this->restricted();
			return false;
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'datapoli'=>$this->msetting->ambilData('unit_kerja'),
			'datapaket'=>$this->msetting->ambilData('paket_harga','kd_unit_kerja="'.$unit1.'" '),			
			'datapelayananunit'=>$this->msetting->getAllPelayananUnit($unit1),
			'datapaketpelayanan'=>$this->msetting->getAllPelayananUnitPaket($paket),
			'unit'=>$unit1,
			'paket1'=>$paket
			);
		$this->load->view('master/header',$dataheader);
		$this->load->view('setting/tarifunitpaket',$data);
		$this->load->view('footer',$datafooter);

	}

	public function akunpelayanan($account1="",$unit1='')
	{
		if(!$this->muser->isAkses($this->akses)){
			redirect('/home/');
			return false;			
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'datapoli'=>$this->msetting->ambilData('unit_kerja'),
			'dataakun'=>$this->msetting->ambilData('accounts','type="D"'),
			'datapelayanan'=>$this->msetting->getLayananByUnit($unit1),
			'datapelayananaccount'=>$this->msetting->getLayananByUnitAccount($unit1,$account1),
			'unit'=>$unit1,
			'account'=>$account1
			);

		$this->load->view('master/header',$dataheader);
		$this->load->view('setting/akunpelayanan',$data);
		$this->load->view('footer',$datafooter);

	}

	public function diagnosaunit($unit1='')
	{
		if(!$this->muser->isAkses("903")){
			$this->restricted();
			return false;
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'datapoli'=>$this->msetting->ambilData('unit_kerja'),
			'datapelayanan'=>$this->msetting->ambilData('sub_diagnosa_icd'),			
			'datapelayananunit'=>$this->msetting->getAllDiagnosaUnit($unit1),
			'unit'=>$unit1
			);

		$this->load->view('master/header',$dataheader);
		$this->load->view('setting/diagnosaunit',$data);
		$this->load->view('footer',$datafooter);

	}

	public function pelayananbmhp()
	{
		if(!$this->muser->isAkses($this->akses)){
			redirect('/home/');
			return false;			
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css','chosen.css');
		$jsfileheader=array('vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'lib/chosen.jquery.min.js',
							'spin.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'datapelayanan'=>$this->msetting->ambilData('list_pelayanan'),
			'dataobat'=>$this->msetting->ambilData('apt_obat')
			);

		$this->load->view('master/header',$dataheader);
		$this->load->view('setting/pelayananbmhp',$data);
		$this->load->view('footer',$datafooter);

	}

	public function akunaruskas($akunaruskas='')
	{

		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'dataakun'=>$this->msetting->ambilData('accounts','type="D"'),
			'dataakunaruskas'=>$this->msetting->ambilData('acc_arus_kas','type="D"'),
			'akunaruskas1'=>$akunaruskas
			);
		
		if($this->session->userdata('akses')==$this->aksesakuntansi){
			$this->load->view('keuangan/header',$dataheader);
		}else{
			$this->load->view('master/header',$dataheader);
		}
		$this->load->view('setting/akunaruskas',$data);
		$this->load->view('footer',$datafooter);

	}

	public function sapmapping($akunsap='')
	{

		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'dataakun'=>$this->msetting->ambilData('accounts','','account'),
			'dataakunsap'=>$this->msetting->ambilData('accounts_sap'),
			'akunsap1'=>$akunsap
			);

		if($this->session->userdata('akses')==$this->aksesakuntansi){
			$this->load->view('keuangan/header',$dataheader);
		}else{
			$this->load->view('master/header',$dataheader);
		}
		$this->load->view('setting/akunsap',$data);
		$this->load->view('footer',$datafooter);

	}

	public function tambahuser($aplikasi="")
	{
		if(!$this->muser->isAkses("902")){
			$this->restricted();
			return false;
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css','jstree/themes/default/style.min.css');
		$jsfileheader=array('vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'jstree.min.js',
							'spin.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'datapegawai'=>$this->msetting->ambilData('pegawai'),
			'dataaplikasi'=>$this->msetting->ambilData('aplikasi'),
			'app'=>$aplikasi,
			'dataakses'=>$this->msetting->ambilData('akses','aktif=1 and aplikasi="'.$aplikasi.'" order by urut_menu,urut_akses ')
			);

		$this->load->view('master/header',$dataheader);
		$this->load->view('setting/tambahuser',$data);
		$this->load->view('footer',$datafooter);

	}

	public function edituser($id_user,$aplikasi="",$unit="")
	{
		if(!$this->muser->isAkses("901")){
			$this->restricted();
			return false;
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css','jstree/themes/default/style.min.css');
		$jsfileheader=array('vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'jstree.min.js',
							'spin.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'datapegawai'=>$this->msetting->ambilData('pegawai'),
			'dataaplikasi'=>$this->msetting->ambilData('aplikasi'),
			'item'=>$this->msetting->ambilItemData('user','id_user="'.$id_user.'" '),
			'app'=>$aplikasi,
			'id_user'=>$id_user,
			'unit'=>$unit,
			'dataakses'=>$this->msetting->ambilData('akses','aktif=1 and aplikasi="'.$aplikasi.'" order by urut_menu,urut_akses ')
			//'dataakses'=>$this->msetting->ambilData('akses','aktif=1 and aplikasi="'.$aplikasi.'" and id_akses not in(select id_akses from user_akses where id_user ="'.$id_user.'") ')
			);
		if(!empty($unit)){
			$data['dataakses']=$this->msetting->ambilData('akses','aktif=1 and aplikasi="'.$aplikasi.'" and id_akses not in(select id_akses from akses_unit where id_user ="'.$id_user.'" and unit="'.$unit.'") ');			
		}
		$this->load->view('master/header',$dataheader);
		$this->load->view('setting/edituser',$data);
		$this->load->view('footer',$datafooter);

	}

	public function editunituser($id_user,$aplikasi="")
	{
		if(!$this->muser->isAkses("901")){
			$this->restricted();
			return false;
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'dataaplikasi'=>$this->msetting->ambilData('aplikasi'),
			'item'=>$this->msetting->ambilItemData('user','id_user="'.$id_user.'" '),
			'app'=>$aplikasi,
			'id_user'=>$id_user			
			//'dataakses'=>$this->msetting->ambilData('akses','aktif=1 and aplikasi="'.$aplikasi.'" and id_akses not in(select id_akses from user_akses where id_user ="'.$id_user.'") ')
			);

		$this->load->view('master/header',$dataheader);
		$this->load->view('setting/editunituser',$data);
		$this->load->view('footer',$datafooter);

	}



	public function approverunit($unit="")
	{
		if(!$this->muser->isAkses("901")){
			$this->restricted();
			return false;
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'datapegawai'=>$this->msetting->ambilData('pegawai'),
			'datalokasi'=>$this->msetting->ambilData('log_lokasi'),
			'datauser'=>$this->msetting->ambilDataRelasi('user','pegawai','user.id_pegawai=pegawai.id_pegawai'),
			'unit'=>$unit,
			'dataapprover'=>$this->msetting->ambilData('approval_logistik')
			//'dataakses'=>$this->msetting->ambilData('akses','aktif=1 and aplikasi="'.$aplikasi.'" and id_akses not in(select id_akses from user_akses where id_user ="'.$id_user.'") ')
			);
		//debugvar('ss');
		if(!empty($unit)){
			$data['dataakses']=$this->msetting->ambilData('approval_logistik','kd_lokasi="'.$unit.'" ');			
		}
		$this->load->view('master/header',$dataheader);
		$this->load->view('setting/approverunit',$data);
		$this->load->view('footer',$datafooter);

	}

	public function editpassword($id_user)
	{
		if(!$this->muser->isAkses("901")){
			$this->restricted();
			return false;
		}
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array(
			'datapegawai'=>$this->msetting->ambilData('pegawai'),
			'item'=>$this->msetting->ambilItemData('user','id_user="'.$id_user.'" '),
			'id_user'=>$id_user
			);

		$this->load->view('master/header',$dataheader);
		$this->load->view('setting/editpassword',$data);
		$this->load->view('footer',$datafooter);

	}

	public function periksatarifunit(){
		$msg=array();
		$kd_unit_kerja=$this->input->post('kd_unit_kerja');
		$pelayananunit=$this->input->post('pelayananunit');

		$jumlaherror=0;
		$msg['status']=1;
		
		if(empty($kd_unit_kerja)){
			$jumlaherror++;
			$msg['id'][]="kd_unit_kerja";
			$msg['pesan'][]="Pilih Unit Kerja Dulu";
		}
		


		if($jumlaherror>0){
			$msg['status']=0;
			$msg['error']=$jumlaherror;
		}
		
		echo json_encode($msg);
	}

	public function periksaakunpelayanan(){
		$msg=array();
		$kd_unit_kerja=$this->input->post('kd_unit_kerja');
		$account=$this->input->post('account');

		$jumlaherror=0;
		$msg['status']=1;
		
		if(empty($kd_unit_kerja)){
			$jumlaherror++;
			$msg['id'][]="kd_unit_kerja";
			$msg['pesan'][]="Pilih Unit Kerja Dulu";
		}
		

		if(empty($account)){
			$jumlaherror++;
			$msg['id'][]="account";
			$msg['pesan'][]="Pilih account Dulu";
		}
		


		if($jumlaherror>0){
			$msg['status']=0;
			$msg['error']=$jumlaherror;
		}
		
		echo json_encode($msg);
	}

	public function periksadiagnosaunit(){
		$msg=array();
		$kd_unit_kerja=$this->input->post('kd_unit_kerja');
		$pelayananunit=$this->input->post('pelayananunit');

		$jumlaherror=0;
		$msg['status']=1;
		
		if(empty($kd_unit_kerja)){
			$jumlaherror++;
			$msg['id'][]="kd_unit_kerja";
			$msg['pesan'][]="Pilih Unit Kerja Dulu";
		}
		


		if($jumlaherror>0){
			$msg['status']=0;
			$msg['error']=$jumlaherror;
		}
		
		echo json_encode($msg);
	}

	public function periksasapmapping(){
		$msg=array();
		$akunsap=$this->input->post('akunsap');
		$akunkas=$this->input->post('akunkas');

		$jumlaherror=0;
		$msg['status']=1;
		
		if(empty($akunsap)){
			$jumlaherror++;
			$msg['id'][]="akunsap";
			$msg['pesan'][]="Pilih Akun SAP Dulu";
		}
		


		if($jumlaherror>0){
			$msg['status']=0;
			$msg['error']=$jumlaherror;
		}
		
		echo json_encode($msg);
	}

	public function periksaakunaruskas(){
		$msg=array();
		$akunaruskas=$this->input->post('akunaruskas');
		$akunkas=$this->input->post('akunkas');

		$jumlaherror=0;
		$msg['status']=1;
		
		if(empty($akunaruskas)){
			$jumlaherror++;
			$msg['id'][]="akunaruskas";
			$msg['pesan'][]="Pilih Akun Dulu";
		}
		


		if($jumlaherror>0){
			$msg['status']=0;
			$msg['error']=$jumlaherror;
		}
		
		echo json_encode($msg);
	}

	public function periksauser(){
		$msg=array();
		$kd_aplikasi=$this->input->post('kd_aplikasi');
		$aksesuser=$this->input->post('aksesuser');

		$jumlaherror=0;
		$msg['status']=1;
		
		if(empty($kd_aplikasi)){
			$jumlaherror++;
			$msg['id'][]="kd_aplikasi";
			$msg['pesan'][]="Aplikasi Harus di isi";
		}
			

		if($jumlaherror>0){
			$msg['status']=0;
			$msg['error']=$jumlaherror;
		}
		
		echo json_encode($msg);
	}

	public function periksaapproverunit(){
		$msg=array();
		$kd_lokasi=$this->input->post('kd_lokasi');
		$id_user=$this->input->post('id_user');

		$jumlaherror=0;
		$msg['status']=1;
		
		if(empty($kd_lokasi)){
			$jumlaherror++;
			$msg['id'][]="kd_lokasi";
			$msg['pesan'][]="Lokasi Harus di isi";
		}
			
		if(empty($id_user)){
			$jumlaherror++;
			$msg['id'][]="kd_lokasi";
			$msg['pesan'][]="Pegawai Harus di isi";
		}

		if($jumlaherror>0){
			$msg['status']=0;
			$msg['error']=$jumlaherror;
		}
		
		echo json_encode($msg);
	}

	public function periksauserpassword(){
		$msg=array();
		$username=$this->input->post('username');
		$password=$this->input->post('password');

		$jumlaherror=0;
		$msg['status']=1;
		
		if(empty($username)){
			$jumlaherror++;
			$msg['id'][]="username";
			$msg['pesan'][]="Username Harus di isi";
		}
		
		if(empty($password)){
			$jumlaherror++;
			$msg['id'][]="password";
			$msg['pesan'][]="Password Harus di isi";
		}
		


		if($jumlaherror>0){
			$msg['status']=0;
			$msg['error']=$jumlaherror;
		}
		
		echo json_encode($msg);
	}

	public function simpantarifunit()
	{
		$msg=array();
		$kd_unit_kerja=$this->input->post('kd_unit_kerja');
		$pelayananunit=$this->input->post('pelayananunit');
		//debugvar($akunkas);
		$this->msetting->delete('tarif_mapping','kd_unit_kerja="'.$kd_unit_kerja.'"');
		foreach ($pelayananunit as $pelayanan) {
			# code...
			$datatarifunit=array('kd_pelayanan'=>$pelayanan,
								'kd_unit_kerja'=>$kd_unit_kerja);
			$this->msetting->insert('tarif_mapping',$datatarifunit);
		}

		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Update";

		echo json_encode($msg);
	}

	public function simpantarifunitpaket()
	{
		$msg=array();
		$kd_unit_kerja=$this->input->post('kd_unit_kerja');
		$kode_paket=$this->input->post('kode_paket');
		$pelayananunit=$this->input->post('pelayananunit');
		//debugvar($akunkas);
		$this->msetting->delete('paket_mapping','kode_paket="'.$kode_paket.'"');
		foreach ($pelayananunit as $pelayanan) {
			# code...
			$datatarifunit=array('kd_pelayanan'=>$pelayanan,
								'kode_paket'=>$kode_paket);
			$this->msetting->insert('paket_mapping',$datatarifunit);
		}

		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Update";

		echo json_encode($msg);
	}

	public function simpanakunpelayanan()
	{
		$msg=array();
		$account=$this->input->post('account');
		$kd_unit_kerja=$this->input->post('kd_unit_kerja');
		$pelayananunit=$this->input->post('pelayananunit');
		//debugvar($akunkas);
		$this->msetting->delete('accounts_pelayanan_unit','kd_unit_kerja="'.$kd_unit_kerja.'"');
		foreach ($pelayananunit as $pelayanan) {
			# code...
			$datatarifunit=array('account'=>$account,
								'kd_pelayanan'=>$pelayanan,
								'kd_unit_kerja'=>$kd_unit_kerja);
			$this->msetting->insert('accounts_pelayanan_unit',$datatarifunit);
		}

		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Update";

		echo json_encode($msg);
	}

	public function simpanpelayananbmhp()
	{
		$msg=array();
		$kd_pelayanan=$this->input->post('kd_pelayanan');
		$kd_obat=$this->input->post('kodeobat');
		$qty=$this->input->post('qty');
		//debugvar($akunkas);
		$this->msetting->delete('list_pelayanan_obat','kd_pelayanan="'.$kd_pelayanan.'"');
		foreach ($kd_obat as $key => $value) {
			# code...
			$datapelayananobat=array('kd_pelayanan'=>$kd_pelayanan,
								'kd_obat'=>$value,
								'qty'=>$qty[$key]);
			$this->msetting->insert('list_pelayanan_obat',$datapelayananobat);
		}

		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Update";

		echo json_encode($msg);
	}

	public function simpandiagnosaunit()
	{
		$msg=array();
		$kd_unit_kerja=$this->input->post('kd_unit_kerja');
		$pelayananunit=$this->input->post('pelayananunit');
		//debugvar($akunkas);
		$this->msetting->delete('diagnosa_mapping','kd_unit_kerja="'.$kd_unit_kerja.'"');
		foreach ($pelayananunit as $pelayanan) {
			# code...
			$datatarifunit=array('kd_icd'=>$pelayanan,
								'kd_unit_kerja'=>$kd_unit_kerja);
			$this->msetting->insert('diagnosa_mapping',$datatarifunit);
		}

		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Update";

		echo json_encode($msg);
	}

	public function simpanakunaruskas()
	{
		$msg=array();
		$akunaruskas=$this->input->post('akunaruskas');
		$akunkas=$this->input->post('akunkas');
		//debugvar($akunkas);
		//$this->msetting->delete('tarif_mapping','kd_unit_kerja="'.$kd_unit_kerja.'"');
		function cube($n)
		{
		    return('"'.$n.'"');
		}
		$akunkas=array_map("cube",$akunkas);
		$account=join(",",$akunkas);
		$data=array(
			'ACCOUNTS'=>$account
			);
		$this->msetting->update('acc_arus_kas',$data,'BARIS="'.$akunaruskas.'"');

		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Update";

		echo json_encode($msg);
	}

	public function simpansapmapping()
	{
		$msg=array();
		$akunsap=$this->input->post('akunsap');
		$akunkas=$this->input->post('akunkas');
		//debugvar($akunkas);
		//$this->msetting->delete('tarif_mapping','kd_unit_kerja="'.$kd_unit_kerja.'"');
		if(!empty($akunsap)){
			$this->msetting->delete('account_mapping','account_sap="'.$akunsap.'"');
			foreach($akunkas as $kas => $val){
				$data=array(
					'account_sap'=>$akunsap,
					'account_sak'=>$val
					);
				$this->msetting->insert('account_mapping',$data);							
			}
		}

		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Update";

		echo json_encode($msg);
	}

	public function simpanuser()
	{
		$msg=array();
		$kd_aplikasi=$this->input->post('kd_aplikasi');
		$id_pegawai=$this->input->post('id_pegawai');
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$aksesuser=$this->input->post('aksesuser');
		//debugvar($akunkas);
		$datauser=array('id_pegawai'=>$id_pegawai,
							'username'=>$username,
							'password'=>md5($password));
		$id_user=$this->msetting->insert('user',$datauser);
		//debugvar($akunkas);
		$this->db->trans_start();
		$this->db->query("replace into user_aplikasi(id_user,kd_aplikasi) values('".$id_user."','".$kd_aplikasi."') ");
		$this->msetting->delete('user_akses','id_user="'.$id_user.'"');
		if(!empty($aksesuser)){
			foreach ($aksesuser as $akses) {
				# code...
				$datatarifunit=array('id_user'=>$id_user,
									'id_akses'=>$akses);
				$this->msetting->insert('user_akses',$datatarifunit);
			}
		}
		$this->db->trans_complete();
		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Simpan";

		echo json_encode($msg);
	}

	public function updateuser()
	{
		$msg=array();
		$kd_aplikasi=$this->input->post('kd_aplikasi');
		$poli=$this->input->post('poli');
		$ruangan=$this->input->post('ruangan');
		$aksesuser=$this->input->post('aksesuser');
		$id_user=$this->input->post('id_user');
		//debugvar($aksesuser);
		$this->db->trans_start();
		$this->db->query("replace into user_aplikasi(id_user,kd_aplikasi) values('".$id_user."','".$kd_aplikasi."') ");
		if(!empty($aksesuser)){
			$this->msetting->delete('user_akses','id_user="'.$id_user.'" and id_akses in(select id_akses from akses where aplikasi="'.$kd_aplikasi.'")');
			foreach ($aksesuser as $key =>$akses) {
				# code...
				$datatarifunit=array('id_user'=>$id_user,
									'id_akses'=>$akses);
				$this->msetting->insert('user_akses',$datatarifunit);
			}			
		}

		if(!empty($poli)){
			$this->msetting->delete('akses_unit','id_user="'.$id_user.'" and unit="'.$poli.'" ');		
			foreach ($aksesuser as $akses) {
				# code...
				/*$datauseraksesunit=array('unit'=>$poli,'id_user'=>$id_user,
									'id_akses'=>$akses);*/
				$datauseraksesunit=array('kd_aplikasi'=>$kd_aplikasi,'unit'=>$poli,'id_user'=>$id_user);
				$this->msetting->insert('akses_unit',$datauseraksesunit);
			}							
		}

		if(!empty($ruangan)){
			$this->msetting->delete('akses_unit','id_user="'.$id_user.'" and unit="'.$ruangan.'" ');		
			foreach ($aksesuser as $akses) {
				# code...
				/*$datauseraksesunit=array('unit'=>$ruangan,'id_user'=>$id_user,
									'id_akses'=>$akses);*/
				$datauseraksesunit=array('kd_aplikasi'=>$kd_aplikasi,'unit'=>$ruangan,'id_user'=>$id_user);
				$this->msetting->insert('akses_unit',$datauseraksesunit);
			}							
		}		
		$this->db->trans_complete();
		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Simpan";

		echo json_encode($msg);
	}

	public function updateunituser()
	{
		$msg=array();
		$kd_aplikasi=$this->input->post('kd_aplikasi');
		$aksesuser=$this->input->post('aksesuser');
		$id_user=$this->input->post('id_user');
		//debugvar($akunkas);
		$this->db->trans_start();
		foreach ($aksesuser as $key => $value) {
			# code...
			$this->msetting->delete('akses_unit','id_user="'.$id_user.'" and unit="'.$value.'" and kd_aplikasi="'.$kd_aplikasi.'" ');		
			$datauseraksesunit=array('unit'=>$value,'id_user'=>$id_user,
								'kd_aplikasi'=>$kd_aplikasi);
			$this->msetting->insert('akses_unit',$datauseraksesunit);
		}							
		$this->db->trans_complete();
		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Simpan";

		echo json_encode($msg);
	}

	public function updateapproverunit()
	{
		$msg=array();
		$kd_lokasi=$this->input->post('kd_lokasi');
		$id_user=$this->input->post('id_user');
		//debugvar($akunkas);
		$this->db->trans_start();
		$this->msetting->delete('approval_logistik',' kd_lokasi="'.$kd_lokasi.'" ');		
		foreach ($id_user as $user) {
			# code...
			$datauseraksesunit=array('kd_lokasi'=>$kd_lokasi,'id_user'=>$user);
			$this->msetting->insert('approval_logistik',$datauseraksesunit);
		}							
		$this->db->trans_complete();
		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Simpan";
		echo json_encode($msg);
	}

	public function updateuserpassword()
	{
		$msg=array();
		$id_pegawai=$this->input->post('id_pegawai');
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$id_user=$this->input->post('id_user');
		//debugvar($akunkas);
		$datauser=array('id_pegawai'=>$id_pegawai,
							'username'=>$username,
							'password'=>md5($password));
		$this->msetting->update('user',$datauser,'id_user="'.$id_user.'" ');
		//debugvar($akunkas);

		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Simpan";

		echo json_encode($msg);
	}

	public function simpanakunkas()
	{
		$msg=array();
		$submit=$this->input->post('submit');
		$akunkas=$this->input->post('akunkas');
		//debugvar($akunkas);
		$dataakunkas=array(
							'setting'=>serialize($akunkas));
		$this->msetting->update('sys_setting',$dataakunkas,'key_data="ACCOUNT_KAS_BANK"');

		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Update";

		echo json_encode($msg);
	}

	public function simpanakunpendapatan()
	{
		$msg=array();
		$submit=$this->input->post('submit');
		$akunpendapatan=$this->input->post('akunpendapatan');
		//debugvar($akunkas);
		$dataakunkas=array(
							'setting'=>serialize($akunpendapatan));
		$this->msetting->update('sys_setting',$dataakunkas,'key_data="ACCOUNT_PENDAPATAN"');

		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Update";

		echo json_encode($msg);
	}

	public function simpanaktivalancar()
	{
		$msg=array();
		$submit=$this->input->post('submit');
		$aktivalancar=$this->input->post('aktivalancar');
		//debugvar($akunkas);
		$dataakunkas=array(
							'setting'=>serialize($aktivalancar));
		$this->msetting->update('sys_setting',$dataakunkas,'key_data="ACCOUNT_AKTIVA"');

		$msg['status']=1;
		$msg['pesan']="Data Berhasil Di Update";

		echo json_encode($msg);
	}

	public function hapususer($id=""){
		if(!empty($id)){
			$this->db->trans_start();
			
			$item=$this->msetting->countItemData('approval','id_user="'.$id.'"');



			$this->msetting->delete('user_aplikasi','id_user="'.$id.'"');
			$this->msetting->delete('akses_unit','id_user="'.$id.'"');
			$this->msetting->delete('user_akses','id_user="'.$id.'"');
			$this->msetting->delete('user','id_user="'.$id.'"');
			$this->db->trans_complete();
			redirect('/setting/user');
		}
	}


	public function updateaktivasi($id=""){
		if(!empty($id)){
			$item=$this->msetting->ambilItemData('user','id_user="'.$id.'" ');
			if($item['aktif']){
				$data=array('aktif'=>0);
				$this->msetting->update('user',$data,'id_user="'.$id.'" ');
			}else{
				$data=array('aktif'=>1);
				$this->msetting->update('user',$data,'id_user="'.$id.'" ');				
			}
			redirect('/setting/user');
		}
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
