<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cli extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	protected $title='SIM RS - Sistem Informasi Rumah Sakit';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('apotek/mlaporanapt');
	}

	public function mutasiobat($kd_unit_apt,$bulan,$tahun){
		$bulanInt=0;
		$bulankemarin=0;
		$tahunkemarin=0;
		$submit=$this->input->post('submit');
		$items=array();
		$msg=array();
		
		switch ($bulan) {
			case '01':
				# code...
				$bulanInt=1;
				$bulankemarin=12;
				$tahunkemarin=$tahun-1;
				break;
			
			case '02':
				# code...
				$bulanInt=2;
				$bulankemarin='01';
				$tahunkemarin=$tahun;
				break;
			
			case '03':
				# code...
				$bulanInt=3;
				$bulankemarin='02';
				$tahunkemarin=$tahun;
				break;
			
			case '04':
				# code...
				$bulanInt=4;
				$bulankemarin='03';
				$tahunkemarin=$tahun;
				break;
			
			case '05':
				# code...
				$bulanInt=5;
				$bulankemarin='04';
				$tahunkemarin=$tahun;
				break;
			
			case '06':
				# code...
				$bulanInt=6;
				$bulankemarin='05';
				$tahunkemarin=$tahun;
				break;
			
			case '07':
				# code...
				$bulanInt=7;
				$bulankemarin='06';
				$tahunkemarin=$tahun;
				break;
			
			case '08':
				# code...
				$bulanInt=8;
				$bulankemarin='07';
				$tahunkemarin=$tahun;
				break;
			
			case '09':
				# code...
				$bulanInt=9;
				$bulankemarin='08';
				$tahunkemarin=$tahun;
				break;
			
			case '10':
				# code...
				$bulanInt=10;
				$bulankemarin='09';
				$tahunkemarin=$tahun;
				break;
			
			case '11':
				# code...
				$bulanInt=11;
				$bulankemarin='10';
				$tahunkemarin=$tahun;
				break;
			
			case '12':
				# code...
				$bulanInt=12;
				$bulankemarin='11';
				$tahunkemarin=$tahun;
				break;
			
			default:
				# code...
				debugvar('There is error, script will not work');
				break;
		}
		$items=$this->mlaporanapt->ambilDataObat();
		//$this->db->trans_start();
		foreach ($items as $item) {
			# code...
			$obatmutasi=$this->mlaporanapt->getMutasiPerObat($item['kd_obat'],$bulankemarin,$tahunkemarin); //stok awal bulan ini adalah stok akhir bulan kemarin
			echo "Menghitung ".$item['kd_obat'],PHP_EOL;
			if(empty($obatmutasi)){
				$saldo_awal=0;
			}else{
				$saldo_awal=$obatmutasi['saldo_akhir'];//mengambil saldo akhir bulan kemarin dan di letakkan di saldo awal bulan ini
			}

			//mengambil data penerimaan dari pbf selama satu bulan
			$in_pbf=$this->mlaporanapt->getPenerimaanObat($item['kd_obat'],$bulan,$tahun);

			//mengambil data penerimaan dari unit lain atau gudang selama satu bulan
			$in_unit=$this->mlaporanapt->getPenerimaanDistribusiObat($item['kd_obat'],$bulan,$tahun); //in_unit --->kd_unit_tujuan

			//mengambil data pengeluaran dari penjualan selama satu bulan
			$out_jual=$this->mlaporanapt->getPenjualanObat($item['kd_obat'],$bulan,$tahun);

			//mengambil data pengeluaran ke unit lain atau gudang selama satu bulan
			$out_unit=$this->mlaporanapt->getPengeluaranDistribusiObat($item['kd_obat'],$bulan,$tahun); //in_unit --->kd_unit_tujuan

			//mengambil data retur ke pbf selama satu bulan
			$retur_pbf=$this->mlaporanapt->getReturPbfObat($item['kd_obat'],$bulan,$tahun);

			//mengambil data retur penjualan selama satu bulan
			$retur_jual=$this->mlaporanapt->getReturJualObat($item['kd_obat'],$bulan,$tahun);

			//mengambil stok opname obat selama sebulan
			$stok_opname=$this->mlaporanapt->stokopnameObat($item['kd_obat'],$bulan,$tahun);		
			
			$saldo_akhir=$saldo_awal+$stok_opname+$in_pbf+$in_unit+$retur_jual-$out_jual-$out_unit-$retur_pbf;					
			$this->db->query('replace into apt_mutasi_obat set tahun="'.$tahun.'",  
								bulan="'.$bulan.'", 
								kd_obat="'.$item['kd_obat'].'",
								kd_unit_apt="'.$kd_unit_apt.'",  
								saldo_awal="'.$saldo_awal.'",  
								in_pbf="'.$in_pbf.'",  
								in_unit="'.$in_unit.'",  
								retur_jual="'.$retur_jual.'",  
								out_jual="'.$out_jual.'",  
								out_unit="'.$out_unit.'",  
								retur_pbf="'.$retur_pbf.'",  
								saldo_akhir="'.$saldo_akhir.'",  
								stok_opname="'.$stok_opname.'"  
								');
		echo "Menghitung ".$item['kd_obat'],' Seleai',PHP_EOL;
		}
		//$this->db->trans_complete();
										

		$msg['status']=1;
		$msg['kd_unit_apt']=$kd_unit_apt;
		$msg['bulan']=$bulan;
		$msg['tahun']=$tahun;
		$msg['pesan']="";
		$msg['cetak']=0;
		//$msg['items']=$this->mlaporanapt->getMutasiObat($kd_unit_apt,$bulan,$tahun);
		$msg['items']=array();
		if($submit=="cetak"){
			$msg['cetak']=1;
		}
		echo json_encode($msg);
	}

}