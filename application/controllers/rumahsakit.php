<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rumahsakit extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	protected $title='SIM RS - Sistem Informasi Rumah Sakit';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('master/mmaster');
		//$x=$this->muser->isAkses($this->akses2);
		//debugvar($x);
	}

	public function view_tutupshift($data){
		$this->load->view('tutupshift',$data);
	}

	public function view_restricted($data){
		$this->load->view('restricted',$data);
	}

	public function view_gantipassword($data){
		$this->load->view('gantipassword',$data);
	}

	public function tutupshift(){
		
	}


	public function periksashift(){
		$shiftsebelumnya=$this->input->post('shiftsebelumnya');
		$shiftselanjutnya=$this->input->post('shiftselanjutnya');
		$unit=$this->input->post('unit');

		$jumlaherror=0;
		$msg['status']=1;
		$msg['clearform']=0;
		$msg['pesanatas']="";
		$msg['pesanlain']="";

		$query=$this->db->query("select * from apt_penjualan join apt_customers on apt_penjualan.cust_code=apt_customers.cust_code where is_lunas=0 and apt_customers.type=0 and shiftapt='".$shiftsebelumnya."' and returapt is null ");
		$item=$query->row_array();

		if(!empty($item)){
			$jumlaherror++;
			$msg['id'][]="unit";
			$msg['pesan'][]="Transaksi No ".$item['no_penjualan']." Belum Lunas, Silahkan lakukan pelunasan";
			$msg['pesanatas'][]="Transaksi No ".$item['no_penjualan']." Belum Lunas, Silahkan lakukan pelunasan";
		}
		if($jumlaherror>0){
			$msg['status']=0;
			$msg['error']=$jumlaherror;
			//$msg['pesanatas']="Terdapat beberapa kesalahan input silahkan cek inputan anda";
		}
		
		echo json_encode($msg);

	}	

	public function updateshift(){
		$shiftselanjutnya=$this->input->post('shiftselanjutnya');
		$unit=$this->input->post('unit');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$kd_user=$this->session->userdata('id_user');
		$shiftsebelumnya=$this->input->post('shiftsebelumnya');
		if($unit=='APT'){
			$data=array('kd_user'=>$kd_user,
						'tanggal_tutup'=>date('Y-m-d h:i:s'),
						'shift'=>$shiftsebelumnya);
			$this->mmaster->insert('log_tutup_shift',$data);
		}
		$data=array(
			'shift'=>$shiftselanjutnya,
			'tgl_update'=>date('Y-m-d h:i:s')
			);
		$this->mmaster->update('unit_shift',$data,'kd_unit="'.$unit.'" ');
		//redirect('/home/');
		$msg['pesan']="Data Berhasil Di Simpan";
		$msg['status']=1;
		$msg['posting']=3;

		echo json_encode($msg);
	}

	public function gantipassword(){
		$id_user=$this->input->post('id_user');
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$data=array(
			'username'=>$username,
			'password'=>md5($password)
			);
		$this->mmaster->update('user',$data,'id_user="'.$id_user.'" ');
		redirect('/home/');
	}

	public function gantipasswordapotek(){
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js','vendor/jquery-1.9.1.min.js','vendor/jquery-migrate-1.1.1.min.js','vendor/jquery-ui-1.10.0.custom.min.js','vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js','lib/jquery.dataTables.min.js','lib/DT_bootstrap.js','lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>$this->title);
		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);

		$this->load->view('headerapotek',$dataheader);
		$data=array('unit'=>'APT');
		$this->load->view('gantipassword',$data);
		$this->load->view('footer');
	}

	public function gantipasswordkasirrwj(){
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js','vendor/jquery-1.9.1.min.js','vendor/jquery-migrate-1.1.1.min.js','vendor/jquery-ui-1.10.0.custom.min.js','vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js','lib/jquery.dataTables.min.js','lib/DT_bootstrap.js','lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>$this->title);
		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);

		$this->load->view('kasir/rawatjalan/header',$dataheader);
		$data=array('unit'=>'RWJ');
		$this->load->view('gantipassword',$data);
		$this->load->view('footer');
	}

	public function tutupshiftapotek(){
		//debugvar($this->router->fetch_directory());
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js','vendor/jquery-1.9.1.min.js','vendor/jquery-migrate-1.1.1.min.js','vendor/jquery-ui-1.10.0.custom.min.js','vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js','lib/jquery.dataTables.min.js','lib/DT_bootstrap.js','lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>$this->title);
		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);

		$this->load->view('headerapotek',$dataheader);
		$query=$this->db->query('select *,date_format(tanggal_tutup,"%d-%m-%Y %h:%i:%s") as tanggal_tutup from log_tutup_shift a left join user b on a.kd_user=b.id_user order by tanggal_tutup desc');
		$data=array('unit'=>'APT','items'=>$query->result_array());
		$this->load->view('tutupshift',$data);
		$this->load->view('footer');
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
