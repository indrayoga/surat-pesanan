<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aptcustomer extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	protected $title='SIM RS - Sistem Informasi Rumah Sakit';

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('utilities');
		$this->load->library('pagination');
		$this->load->model('apotek/mcustomerapt');

	}
	public function index()
	{
		$customer=$this->input->post('customer');
		
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);
		$data=array('items'=>$this->mcustomerapt->ambilDataCustomer($customer));
		
		$this->load->view('master/header',$dataheader);
		$this->load->view('apotek/master/customer/aptcustomer',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function tambah()
	{
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','timepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-timepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array();					
		$this->load->view('master/header',$dataheader);
		$this->load->view('apotek/master/customer/tambahcustomerapt',$data);
		$this->load->view('footer',$datafooter);
	}

	public function periksa()
	{
		$msg=array();
		$mode=$this->input->post('mode');
		$submit=$this->input->post('submit');
		$cust_code=$this->input->post('cust_code');
		$customer=$this->input->post('customer');
		
		$jumlaherror=0;
		$msg['status']=1;
		$msg['clearform']=0;
		$msg['pesanatas']="";
		$msg['pesanlain']="";

		if($mode!="edit"){
			if($this->mcustomerapt->isExist('apt_customers','cust_code',$cust_code)){
				$jumlaherror++;
				$msg['id'][]="cust_code";
				$msg['pesan'][]="Customer sudah ada";
			}			
		}
		if(empty($cust_code)){
			$jumlaherror++;
			$msg['id'][]="cust_code";
			$msg['pesan'][]="Kode customer Harus di Isi";
		}			
		if(empty($customer)){
			$jumlaherror++;
			$msg['id'][]="customer";
			$msg['pesan'][]="Nama customer harus di isi";
		}
		if($jumlaherror>0){
			$msg['status']=0;
			$msg['error']=$jumlaherror;
			$msg['pesanatas']="Terdapat beberapa kesalahan input silahkan cek inputan anda";
		}
		
		echo json_encode($msg);
	}

	public function simpan(){
		//$msg=array();
		//$submit=$this->input->post('submit');
		$cust_code=$this->input->post('cust_code');
		$customer=$this->input->post('customer');
		$tipe=$this->input->post('tipe');
		$contact=$this->input->post('contact');
		$address=$this->input->post('address');
		$city=$this->input->post('city');
		$state=$this->input->post('state');
		$zip=$this->input->post('zip');
		$phone1=$this->input->post('phone1');
		$phone2=$this->input->post('phone2');
		$fax=$this->input->post('fax');
		$account=$this->input->post('account');
		$jm=$this->input->post('jm');
		$kartu=$this->input->post('kartu');
		$adm=$this->input->post('adm');
		
		$tambah=array('cust_code'=>$cust_code,
						  'customer'=>$customer,
						  'type'=>$tipe,
						  'contact'=>$contact,
						  'address'=>$address,
						  'city'=>$city,
						  'state'=>$state,
						  'zip'=>$zip,
						  'phone1'=>$phone1,
						  'phone2'=>$phone2,
						  'fax'=>$fax,
						  'account'=>$account
						  );
		$this->mcustomerapt->insert('apt_customers',$tambah);
		if(is_numeric($jm)){
			$this->db->delete('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>1));			
			$this->db->insert('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>1,'jumlah'=>$jm));
		}else{
			$this->db->delete('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>1));			
		}
		if(is_numeric($adm)){
			$this->db->delete('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>2));			
			$this->db->insert('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>2,'jumlah'=>$adm));
		}else{
			$this->db->delete('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>2));			
		}
		if(is_numeric($kartu)){
			$this->db->delete('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>3));			
			$this->db->insert('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>3,'jumlah'=>$kartu));
		}else{
			$this->db->delete('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>3));			
		}
		
		$msg['pesan']="Data Berhasil Di Simpan";
		$msg['status']=1;

		echo json_encode($msg);
	}

	public function update(){
		$cust_code=$this->input->post('cust_code');
		$customer=$this->input->post('customer');
		$tipe=$this->input->post('tipe');
		$contact=$this->input->post('contact');
		$address=$this->input->post('address');
		$city=$this->input->post('city');
		$state=$this->input->post('state');
		$zip=$this->input->post('zip');
		$phone1=$this->input->post('phone1');
		$phone2=$this->input->post('phone2');
		$fax=$this->input->post('fax');
		$account=$this->input->post('account');
		$jm=$this->input->post('jm');
		$kartu=$this->input->post('kartu');
		$adm=$this->input->post('adm');
		
		$edit=array('customer'=>$customer,
						  'type'=>$tipe,
						  'contact'=>$contact,
						  'address'=>$address,
						  'city'=>$city,
						  'state'=>$state,
						  'zip'=>$zip,
						  'phone1'=>$phone1,
						  'phone2'=>$phone2,
						  'fax'=>$fax,
						  'account'=>$account);
		$this->mcustomerapt->update('apt_customers',$edit,'cust_code="'.$cust_code.'"');
		if(is_numeric($jm)){
			$this->db->delete('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>1));			
			$this->db->insert('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>1,'jumlah'=>$jm));
		}else{
			$this->db->delete('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>1));			
		}
		if(is_numeric($adm)){
			$this->db->delete('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>2));			
			$this->db->insert('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>2,'jumlah'=>$adm));
		}else{
			$this->db->delete('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>2));			
		}
		if(is_numeric($kartu)){
			$this->db->delete('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>3));			
			$this->db->insert('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>3,'jumlah'=>$kartu));
		}else{
			$this->db->delete('jasa_medis',array('cust_code'=>$cust_code,'biaya'=>3));			
		}
		$msg['pesan']="Data Berhasil Di Edit";
		$msg['status']=1;

		echo json_encode($msg);
	}

	public function edit($id=""){
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','timepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-timepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);
		
		$data=array('items'=>$this->mcustomerapt->ambilItemData('apt_customers','cust_code="'.urldecode($id).'"'));
		
		$this->load->view('master/header',$dataheader);
		$this->load->view('apotek/master/customer/editcustomerapt',$data);
		$this->load->view('footer',$datafooter);

	}
	public function hapus($id=""){
		if(!empty($id)){
			$this->mcustomerapt->delete('apt_customers','cust_code="'.$id.'"');
			redirect('/masterapotek/aptcustomer/');
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */