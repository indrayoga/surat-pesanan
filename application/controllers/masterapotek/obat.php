<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'controllers/rumahsakit.php');
//class Obat extends CI_Controller {
class Obat extends Rumahsakit {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	protected $title='SIM RS - Sistem Informasi Rumah Sakit';

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('utilities');
		$this->load->library('pagination');
		$this->load->model('apotek/mobat');
	}
	
	public function restricted(){
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		//$this->load->view('master/header',$dataheader);
		$this->load->view('headerapotek',$dataheader);
		$data=array();
		parent::view_restricted($data);
		$this->load->view('footer');
	}
	
	public function index($nama_obat="NULL",$kd_satuan_kecil="NULL")
	{
		if(!$this->muser->isAkses("1")){
			$this->restricted();
			return false;
		}
		
		if($this->input->post('nama_obat')!='')$nama_obat=$this->input->post('nama_obat');
		if($this->input->post('kd_satuan_kecil')!='')$kd_satuan_kecil=$this->input->post('kd_satuan_kecil');
		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);
		$data=array('nama_obat'=>$nama_obat,
					'kd_satuan_kecil'=>$kd_satuan_kecil,
					'satuanobat'=>$this->mobat->ambilData('apt_satuan_kecil'),
					//'items'=>$this->mobat->ambilDataObat($nama_obat,$kd_satuan_kecil)
					);
		
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/master/obat/obat',$data);
		$this->load->view('footer',$datafooter);
	}

	public function dataobat()
	{
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$search=$this->input->get_post('sSearch');

		$this->datatables->select('a.kd_obat,a.nama_obat,e.nama_pabrik,b.satuan_kecil,a.kd_golongan,a.kd_sub,f.nama_kelas_terapi,g.nama_obat_generik,a.harga_dasar,a.harga_beli,if(a.is_aktif=1,"Aktif","Tidak Aktif") as is_aktif',false);
		$this->datatables->from("apt_obat a");
		$this->datatables->add_column('pilihan', '<a class="btn btn-info" href="'.base_url().'index.php/masterapotek/obat/edit/$1">Edit</a> <a class="btn btn-danger" href="#" onClick="xar_confirm(\''.base_url().'index.php/masterapotek/obat/hapus/$1\',\'Apakah Anda ingin menghapus data ini?\')">Hapus</a> ', 'a.kd_obat');		
		$this->datatables->edit_column('a.harga_dasar','$1','number_format("a.harga_dasar","2",",","."),');
		$this->datatables->edit_column('a.harga_beli','$1','number_format("a.harga_beli","2",",","."),');
		$this->datatables->join('apt_satuan_kecil b','a.kd_satuan_kecil=b.kd_satuan_kecil','left');
		//$this->datatables->join('apt_golongan c','a.kd_golongan=c.kd_golongan','left');
		//$this->datatables->join('apt_sub_golongan d','a.kd_sub=d.kd_sub','left');
		$this->datatables->join('apt_pabrik e','a.kd_pabrik=e.kd_pabrik','left');
		$this->datatables->join('apt_kelas_terapi f','a.kd_kelas_terapi=f.kd_kelas_terapi','left');
		$this->datatables->join('apt_obat_generik g','a.kd_obat_generik=g.kd_obat_generik','left');
		//if(!empty($nama_obat) && $nama_obat !='NULL')$this->datatables->like('a.nama_obat',$nama_obat,'both');
		//if(!empty($satuan_kecil) && $satuan_kecil !='NULL')$this->datatables->where('b.kd_satuan_kecil',$satuan_kecil);
		if(strtolower($search)=="tidak aktif"){
			$this->datatables->where("a.is_aktif=0 ",null,false);
		}elseif(strtolower($search)=="aktif"){
			$this->datatables->where("a.is_aktif=1 ",null,false);
		}else{
			$this->datatables->where("a.kd_obat LIKE '%".$search."%' OR a.nama_obat LIKE '%".$search."%' OR e.nama_pabrik LIKE '%".$search."%' OR f.nama_kelas_terapi LIKE '%".$search."%' OR g.nama_obat_generik LIKE '%".$search."%' OR b.satuan_kecil LIKE '%".$search."%' OR a.kd_golongan LIKE '%".$search."%' OR a.kd_sub LIKE '%".$search."%' OR a.harga_dasar LIKE '%".$search."%' OR a.harga_beli LIKE '%".$search."%' OR is_aktif LIKE '%".$search."%' ");
		}
		//$data['result'] = $this->datatables->generate();
		$results = $this->datatables->generate();
		//$data['aaData'] = $results['aaData']->;
		$x=json_decode($results);
		$b=$x->aaData;
		echo ($results);
	}
	
	
	public function tambah()
	{
		if(!$this->muser->isAkses("2")){
			$this->restricted();
			return false;
		}
		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','timepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-timepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array('databesar'=>$this->mobat->ambilData('apt_satuan_besar'),
					'datakecil'=>$this->mobat->ambilData('apt_satuan_kecil'),
					'datajenis'=>$this->mobat->ambilData('apt_jenis_obat'),
					'datasub'=>$this->mobat->ambilData('apt_sub_golongan'),
					'datagolongan'=>$this->mobat->ambilData('apt_golongan'),
					'datakelasterapi'=>$this->mobat->ambilData('apt_kelas_terapi'),
					'dataobatgenerik'=>$this->mobat->ambilData('apt_obat_generik'),
					'datapabrik'=>$this->mobat->ambilData('apt_pabrik'));
					
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/master/obat/tambahobat',$data);
		$this->load->view('footer',$datafooter);
	}

	public function periksa()
	{
		$msg=array();
		$mode=$this->input->post('mode');
		$submit=$this->input->post('submit');
		$kd_obat=$this->input->post('kd_obat');
		$kd_satuan_kecil=$this->input->post('kd_satuan_kecil');
		$kd_golongan=$this->input->post('kd_golongan');
		$nama_obat=$this->input->post('nama_obat');
		$ket_obat=$this->input->post('ket_obat');
		$generic=$this->input->post('generic');
		$is_aktif=$this->input->post('is_aktif');
		$jml_stok=$this->input->post('jml_stok');
		$min_stok=$this->input->post('min_stok');
		$kd_pabrik=$this->input->post('kd_pabrik');
		$jumlaherror=0;
		$msg['status']=1;
		$msg['clearform']=0;
		$msg['pesanatas']="";
		$msg['pesanlain']="";

		if($mode!="edit"){
			if($this->mobat->isExist('apt_obat','kd_obat',$kd_obat)){
				$jumlaherror++;
				$msg['id'][]="kd_obat";
				$msg['pesan'][]="Kd. Obat sudah ada";
			}			
		}
		if(empty($kd_golongan)){
			$jumlaherror++;
			$msg['id'][]="kd_golongan";
			$msg['pesan'][]="Golongan harus di pilih";
		}
		if(empty($nama_obat)){
			$jumlaherror++;
			$msg['id'][]="nama_obat";
			$msg['pesan'][]="Nama Obat Harus di Isi";
		}
		if($jumlaherror>0){
			$msg['status']=0;
			$msg['error']=$jumlaherror;
			$msg['pesanatas']="Terdapat beberapa kesalahan input silahkan cek inputan anda";
		}
		
		echo json_encode($msg);
	}

	public function simpan(){
		$kd_obat=$this->input->post('kd_obat');
		$kd_satuan_kecil=$this->input->post('kd_satuan_kecil');
		$kd_golongan=$this->input->post('kd_golongan');
		$kd_sub=$this->input->post('kd_sub');
		$kd_kelas_terapi=$this->input->post('kd_kelas_terapi');
		$kd_obat_generik=$this->input->post('kd_obat_generik');
		$nama_obat=$this->input->post('nama_obat');
		$ket_obat=$this->input->post('ket_obat');
		$generic=$this->input->post('generic');
		$is_aktif=$this->input->post('is_aktif');
		$jml_stok=$this->input->post('jml_stok');
		$min_stok=$this->input->post('min_stok');
		$max_stok=$this->input->post('max_stok');
		$harga_beli=$this->input->post('harga_beli');
		$harga_dasar=$this->input->post('harga_dasar');
		$no_batch=$this->input->post('no_batch');
		$kd_pabrik=$this->input->post('kd_pabrik');
		$persen_harga_jual=$this->input->post('persen_harga_jual');
		$harga_jual=$this->input->post('harga_jual');
		$no_minimum_stok=$this->input->post('no_minimum_stok');
		
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$kd_unit_gudang=$this->session->userdata('kd_unit_apt_gudang');
		$tanggal=$this->mobat->muncultanggal(); 
		$thn=substr($tanggal,0,4); $bln=substr($tanggal,5,2); $tgl1=substr($tanggal,8,2);
		$tgl11=($thn+5).'-'.$bln.'-'.$tgl1;
		
		$kode=$this->mobat->autoNumber();
		$kodebaru=$kode+1;
		$kodebaru=str_pad($kodebaru,8,0,STR_PAD_LEFT); 
		$kd_obat=$kodebaru;
		


		$kd_satuan=$this->input->post('kd_satuan');
		$pembanding=$this->input->post('pembanding');
		$urut=$this->input->post('urut');

		$msg['kd_obat']=$kd_obat;
		
		$tambahobat=array('kd_obat'=>$kd_obat,
						  'kd_satuan_besar'=>end($kd_satuan),
						  'kd_satuan_kecil'=>$kd_satuan[0],
						  'kd_jenis_obat'=>'139',
						  'kd_sub_jenis'=>'22',
						  'kd_golongan'=>$kd_golongan,
						  'kd_obat_generik'=>$kd_obat_generik,
						  'kd_kelas_terapi'=>$kd_kelas_terapi,
						  'nama_obat'=>$nama_obat,
						  'ket_obat'=>$ket_obat,
						  'generic'=>$generic,
						  'is_aktif'=>$is_aktif,
						  'pembanding'=>1,
						 // 'jml_stok'=>$jml_stok,
						  'harga_beli'=>$harga_beli,
						  'harga_dasar'=>$harga_dasar,
						  'kd_pabrik'=>$kd_pabrik,
						  'kd_sub'=>$kd_sub);
		$this->mobat->insert('apt_obat',$tambahobat);

		foreach ($urut as $key => $value) {
			# code...
			$datasatuan=array(
				'urut'=>$value,
				'kd_obat'=>$kd_obat,
				'kd_satuan'=>$kd_satuan[$key],
				'pembanding'=>$pembanding[$key]

			);
			$this->mobat->insert('apt_obat_satuan',$datasatuan);
		}
		$msg['pesan']="Data Berhasil Di Simpan";
		$msg['status']=1;
		$msg['posting']=3;

		echo json_encode($msg);
	}

	public function update(){
		$kd_obat=$this->input->post('kd_obat');
		$kd_satuan_kecil=$this->input->post('kd_satuan_kecil');
		$kd_golongan=$this->input->post('kd_golongan');
		$kd_sub=$this->input->post('kd_sub');
		$nama_obat=$this->input->post('nama_obat');
		$kd_kelas_terapi=$this->input->post('kd_kelas_terapi');
		$kd_obat_generik=$this->input->post('kd_obat_generik');
		$ket_obat=$this->input->post('ket_obat');
		$generic=$this->input->post('generic');
		$is_aktif=$this->input->post('is_aktif');
		$jml_stok=$this->input->post('jml_stok');
		$min_stok=$this->input->post('min_stok');
		$max_stok=$this->input->post('max_stok');
		$harga_beli=$this->input->post('harga_beli');
		$harga_dasar=$this->input->post('harga_dasar');
		$no_batch=$this->input->post('no_batch');
		$tgl=$this->input->post('tgl');
		$kd_pabrik=$this->input->post('kd_pabrik');
		$no_minimum_stok=$this->input->post('no_minimum_stok');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$kd_unit_gudang=$this->session->userdata('kd_unit_apt_gudang');

		$persen_harga_jual=$this->input->post('persen_harga_jual');
		$harga_jual=$this->input->post('harga_jual');

		$minstok=0; $maxstok=0;
		if($min_stok==''){$minstok=0;}
		else{$minstok=$min_stok;}
		
		if($max_stok==''){$maxstok=0;}
		else{$maxstok=$max_stok;}
		
		$kd_satuan=$this->input->post('kd_satuan');
		$pembanding=$this->input->post('pembanding');
		$urut=$this->input->post('urut');

		$this->db->trans_start();
		if($kd_unit_apt==$kd_unit_gudang){
			$editobat=array(
						'kd_satuan_besar'=>end($kd_satuan),
						'kd_satuan_kecil'=>$kd_satuan[0],
						'kd_golongan'=>$kd_golongan,
						'nama_obat'=>$nama_obat,
						'ket_obat'=>$ket_obat,
						'kd_obat_generik'=>$kd_obat_generik,
						'kd_kelas_terapi'=>$kd_kelas_terapi,
						'generic'=>$generic,
						'is_aktif'=>$is_aktif,
						'pembanding'=>1,
						//'jml_stok'=>$jml_stok,
						'harga_beli'=>$harga_beli,
						'harga_dasar'=>$harga_dasar,
						'no_batch'=>$no_batch,
						'kd_pabrik'=>$kd_pabrik,
						'kd_sub'=>$kd_sub);
			$this->mobat->update('apt_obat',$editobat,'kd_obat="'.$kd_obat.'"');
			

		}
				
		$this->db->delete('apt_obat_satuan',array('kd_obat'=>$kd_obat));
		$pembanding_terkecil=array();
		foreach ($urut as $key => $value) {
			# code...
			$pembanding_terkecil[]=$pembanding[$key];
			$jumlah_pembanding_terkecil=1;
			foreach ($pembanding_terkecil as $a => $b) {
				# code...
				$jumlah_pembanding_terkecil*=$b;
			}
			$datasatuan=array(
				'urut'=>$value,
				'kd_obat'=>$kd_obat,
				'kd_satuan'=>$kd_satuan[$key],
				'pembanding'=>$pembanding[$key],
				'pembanding_terkecil'=>$jumlah_pembanding_terkecil

			);
			$this->mobat->insert('apt_obat_satuan',$datasatuan);
		}

		$this->db->trans_complete();
		$msg['kd_obat']=$kd_obat;
		
		$msg['pesan']="Data Berhasil Di Edit";
		$msg['status']=1;
		$msg['posting']=3;

		echo json_encode($msg);
	}

	public function edit($id=""){
		if(!$this->muser->isAkses("3")){
			$this->restricted();
			return false;
		}
		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','timepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-timepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$data=array('databesar'=>$this->mobat->ambilData('apt_satuan_besar'),
					'datakecil'=>$this->mobat->ambilData('apt_satuan_kecil'),
					'datajenis'=>$this->mobat->ambilData('apt_jenis_obat'),
					'datasub'=>$this->mobat->ambilData("apt_sub_golongan"),
					'datagolongan'=>$this->mobat->ambilData('apt_golongan'),					
					'datakelasterapi'=>$this->mobat->ambilData('apt_kelas_terapi'),
					'dataobatgenerik'=>$this->mobat->ambilData('apt_obat_generik'),
					'dataobatsatuan'=>$this->db->order_by('urut')->get_where('apt_obat_satuan',array('kd_obat'=>urldecode($id)))->result_array(),
					'items'=>$this->mobat->ambilItemData('apt_obat','kd_obat="'.urldecode($id).'"'),
					'datapabrik'=>$this->mobat->ambilData('apt_pabrik'));
//debugvar($id);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/master/obat/editobat',$data);
		$this->load->view('footer',$datafooter);

	}
	
	public function hapus($id=""){
		if(!$this->muser->isAkses("4")){
			$this->restricted();
			return false;
		}
		
		if($this->mobat->isObatHaveChild($id)){
			echo "<script>alert('Data tidak bisa dihapus');window.location.href='".base_url()."index.php/masterapotek/obat/'</script>";
			
			return false;
		}

		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		if(!empty($id)){
			$this->db->trans_start();
			$this->mobat->delete('apt_setting_obat','kd_obat="'.$id.'" and kd_unit_apt="'.$kd_unit_apt.'"');
			$this->mobat->delete('apt_obat','kd_obat="'.$id.'"');
			$this->db->trans_complete();
			//$this->mobat->delete('apt_stok_unit','kd_obat="'.$id.'" and kd_unit_apt="U01"');
			redirect('/masterapotek/obat/');
		}
	}
	
	public function cekgolongan(){
		$q=$this->input->get('query');
		$items=$this->mobat->ambilgolongan($q);
		echo json_encode($items);
	}

	public function cetakxls(){

		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 

		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");

		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(2.14); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(10); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(40); //kode
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10); //nama obat
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(10); //satuan
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(10); //tgl expire
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(10); //stok
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(20); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(20); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(8)->setWidth(20); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(9)->setWidth(20); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(10)->setWidth(10); //harga beli
		//debugvar($kd_unit_apt);
		for($x='A';$x<='K';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}		

		$objPHPExcel->getActiveSheet()->setCellValue ('D3','DAFTAR OBAT');		
		
								
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A3','Kode Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('B3','Nama Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('C3','Pabrik');
		$objPHPExcel->getActiveSheet()->setCellValue ('D3','Satuan Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('E3','Gol');
		$objPHPExcel->getActiveSheet()->setCellValue ('F3','Sub Gol');
		$objPHPExcel->getActiveSheet()->setCellValue ('G3','Harga Dasar');
		$objPHPExcel->getActiveSheet()->setCellValue ('H3','Harga Beli');
		$objPHPExcel->getActiveSheet()->setCellValue ('I3','Margin');
		$objPHPExcel->getActiveSheet()->setCellValue ('J3','Harga Jual');
		$objPHPExcel->getActiveSheet()->setCellValue ('K3','Min Stok');
		$objPHPExcel->getActiveSheet()->setCellValue ('L3','EOQ');
		$items=array();
		$items=$this->mobat->dataobat();
		$baris=4;
		$nomor=1;
		$total=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='K';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else{
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,"'".$item['kd_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['nama_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['nama_pabrik']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['satuan_kecil']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['golongan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['sub_golongan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['harga_dasar']);
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$item['harga_beli']);
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$item['margin']);
			$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,$item['harga']);
			$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,number_format($item['min_stok'],1,'.',','));
			$objPHPExcel->getActiveSheet()->setCellValue ('L'.$baris,number_format($item['max_stok'],1,'.',','));
					
			$nomor=$nomor+1; $baris=$baris+1; 
			//$total=$total+$nilai;
		}	
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/dataobat.xls");
		header("Location: ".base_url()."download/dataobat.xls");
	}	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */