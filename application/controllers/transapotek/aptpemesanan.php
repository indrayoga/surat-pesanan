<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'controllers/rumahsakit.php');
//class Aptpemesanan extends CI_Controller {
class Aptpemesanan extends Rumahsakit {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	protected $title='SIM RS - Sistem Informasi Rumah Sakit';
	public $shift;

	public function __construct(){
		parent::__construct();
		$this->load->model('apotek/mpemesananapt');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		if(empty($kd_unit_apt)){
			redirect('/home/');
		}

        $queryunitshift=$this->db->query('select * from unit_shift where kd_unit="APT"'); 
        $unitshift=$queryunitshift->row_array();
		$this->shift=$unitshift['shift'];
	}
	
	public function restricted(){
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		//$this->load->view('master/header',$dataheader);
		$this->load->view('headerapotek',$dataheader);
		$data=array();
		parent::view_restricted($data);
		$this->load->view('footer');
	}
	
	public function index()	{
		if(!$this->muser->isAkses("37")){
			$this->restricted();
			return false;
		}
		
		/*$no_pemesanan=$this->input->post('no_pemesanan');
		$periodeawal=$this->input->post('periodeawal');
		$periodeakhir=$this->input->post('periodeakhir');*/
		
		$no_pemesanan='';
		//$no_pemesanan=$this->input->post('no_pemesanan');
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		
		if($this->input->post('no_pemesanan')!=''){
			$no_pemesanan=$this->input->post('no_pemesanan');
		}
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js','vendor/jquery-1.9.1.min.js','vendor/jquery-migrate-1.1.1.min.js','vendor/jquery-ui-1.10.0.custom.min.js','vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js','lib/jquery.dataTables.min.js','lib/DT_bootstrap.js','lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Daftar Pemesanan :: ".$this->title);
		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array('no_pemesanan'=>$no_pemesanan,
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'items'=>$this->mpemesananapt->ambilDataPemesanan($no_pemesanan,$periodeawal,$periodeakhir));
		
		//debugvar($items);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/transaksi/pemesanan/aptpemesanan',$data);
		$this->load->view('footer',$datafooter);
	}
		
	public function tambahpemesananapt(){
		if(!$this->muser->isAkses("38")){
			$this->restricted();
			return false;
		}
		$kode=""; $no_pemesanan=""; 
		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-timepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Tambah Pemesanan :: ".$this->title);
		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array('no_pemesanan'=>'',
					'datasupplier'=>$this->mpemesananapt->ambilData('apt_supplier'),
					'itemtransaksi'=>$this->mpemesananapt->ambilItemData($no_pemesanan),
					'itemsdetiltransaksi'=>$this->mpemesananapt->getAllDetailPemesanan($no_pemesanan),
					'items'=>$this->mpemesananapt->ambilDataPemesanan('','',''),
					'itemapprove'=>$this->mpemesananapt->ambilApprover(),
					'applogin'=>$this->mpemesananapt->ambilApp()
					);
		
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/transaksi/pemesanan/tambahpemesananapt',$data);
		$this->load->view('footer',$datafooter);	
	}
	
	public function ubahpemesanan($no_pemesanan=""){
		if(!$this->muser->isAkses("39")){
			$this->restricted();
			return false;
		}
		$sum="";
		if(empty($no_pemesanan))return false;
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-timepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>"Ubah Pemesanan :: ".$this->title
			);
		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);
				
		$data=array('datasupplier'=>$this->mpemesananapt->ambilData('apt_supplier'),
					'no_pemesanan'=>$no_pemesanan,
					'itemtransaksi'=>$this->mpemesananapt->ambilItemData($no_pemesanan),
					'itemsdetiltransaksi'=>$this->mpemesananapt->getAllDetailPemesanan($no_pemesanan),
					'items'=>$this->mpemesananapt->ambilDataPemesanan('','',''),
					'itemapprove'=>$this->mpemesananapt->tampilApprover($no_pemesanan),
					'applogin'=>$this->mpemesananapt->ambilApp()
					);
		
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/transaksi/pemesanan/tambahpemesananapt',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function simpanpemesanan(){
		$msg=array();
		$submit=$this->input->post('submit');
		$no_pemesanan=$this->input->post('no_pemesanan');
		$tgl_pemesanan=$this->input->post('tgl_pemesanan');
		$tgl_tempo=$this->input->post('tgl_tempo');
		$kd_supplier=$this->input->post('kd_supplier');
		$nama=$this->input->post('nama');
		$keterangan=$this->input->post('keterangan');
		
		$kd_obat=$this->input->post('kd_obat');
		$nama_obat=$this->input->post('nama_obat');
		$satuan_kecil=$this->input->post('satuan_kecil');
		$pembanding=$this->input->post('pembanding');
		$qty_box=$this->input->post('qty_box'); 
		$qty_kcl=$this->input->post('qty_kcl');
		$harga_beli=$this->input->post('harga_beli');
		$diskon=$this->input->post('diskon');
		$ppn=$this->input->post('ppn');		
		$jam_pemesanan=$this->input->post('jam_pemesanan');
		$jam_pemesanan1=$this->input->post('jam_pemesanan1');
		
		$nama_pegawai=$this->input->post('nama_pegawai');
		$status=$this->input->post('status');
		$kd_app=$this->input->post('kd_app');
		$kd_applogin=$this->input->post('kd_applogin');
		
		//$kd_user=$this->session->userdata('id_user'); 
		$msg['no_pemesanan']=$no_pemesanan;
		if($this->mpemesananapt->isNumberExist($no_pemesanan)){ //edit
			$this->db->trans_start();
			$tgl_pemesanan1=convertDate($tgl_pemesanan)." ".$jam_pemesanan1;
		    $datapemesananedit=array(
		    						'tgl_pemesanan'=>$tgl_pemesanan1,
									'keterangan'=>$keterangan,
									'tgl_tempo'=>convertDate($tgl_tempo));
				
			$this->mpemesananapt->update('apt_pemesanan',$datapemesananedit,'no_pemesanan="'.$no_pemesanan.'"');
			$urut=1;
			
			//$this->mpemesananapt->delete('apt_pemesanan_detail','no_pemesanan="'.$no_pemesanan.'"');
			
			/*if(!empty($kd_obat)){
				foreach ($kd_obat as $key => $value){
					if(empty($value))continue;					
					$datadetiledit=array('no_pemesanan'=>$no_pemesanan,'urut'=>$urut,
										'kd_obat'=>$value,'qty_box'=>$qty_box[$key],
										'qty_kcl'=>$qty_kcl[$key],'harga_beli'=>$harga_beli[$key],
										'diskon'=>$diskon[$key],'ppn'=>$ppn[$key]);
					$this->mpemesananapt->insert('apt_pemesanan_detail',$datadetiledit);										
					$urut++;
				}
			}*/
			$this->db->trans_complete();
			$msg['pesan']="Data Berhasil Di Update";
		}else { //simpan baru

		}
		$msg['status']=1;
		$msg['keluar']=0;
		if($submit=="simpankeluar"){
			$msg['keluar']=1;
		}

		$this->db->trans_complete();
		echo json_encode($msg);
	}
	
	public function hapuspemesanan($no_pemesanan=""){
		if(!$this->muser->isAkses("40")){
			$this->restricted();
			return false;
		}
		//$kd_unit_apt="";
		$msg=array();
		$error=0;
		if(empty($no_pemesanan)){
			$msg['pesan']="Pilih Transaksi yang akan di hapus";
			echo "<script>Alert('".$msg['pesan']."')</script>";
		}else{		
			$this->db->trans_start();
			$this->mpemesananapt->delete('apt_pemesanan_detail','no_pemesanan="'.$no_pemesanan.'"');	
			$this->mpemesananapt->delete('apt_pemesanan','no_pemesanan="'.$no_pemesanan.'"');
			$this->db->trans_complete();		
			redirect('/transapotek/aptpemesanan/');
		}
	}
		
	public function ambildaftarobatbynama(){
		$nama_obat=$this->input->post('nama_obat');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt_gudang');
		
		/*$this->datatables->select("apt_obat.kd_obat as kd_obat1, apt_obat.nama_obat, apt_satuan_kecil.satuan_kecil, 
								ifnull( sum(substring_index( apt_stok_unit.jml_stok, '.', 1 )) , 0 ) as jml_stok, 
								'Pilihan' as pilihan,apt_obat.pembanding,
								(select if( count(kd_obat)>0, max_stok, 0 ) from apt_setting_obat where kd_obat=kd_obat1 and kd_unit_apt='$kd_unit_apt') as max_stok,
								apt_obat.harga_beli",false);*/
		$this->datatables->select("apt_obat.kd_obat as kd_obat1, apt_obat.nama_obat, apt_satuan_kecil.satuan_kecil, 
								ifnull( sum(substring_index( apt_stok_unit.jml_stok, '.', 1 )) , 0 ) as jml_stok, 
								'Pilihan' as pilihan,apt_obat.pembanding,
								(select if( count(kd_obat)>0, max_stok, 0 ) from apt_setting_obat where kd_obat=kd_obat1 and kd_unit_apt='$kd_unit_apt') as max_stok,
								apt_obat.harga_dasar",false);
		
		$this->datatables->from("apt_obat");
		//$this->datatables->edit_column('pilihan', '<a class="btn" onclick=\'pilihobat("$1","$2","$3","$4","$5","$6","$7","$8","$9")\'>Pilih</a>','kd_obat1, apt_obat.nama_obat, apt_satuan_kecil.satuan_kecil, apt_obat.pembanding, max_stok,apt_obat.harga_beli');		
		$this->datatables->edit_column('pilihan', '<a class="btn" onclick=\'pilihobat("$1","$2","$3","$4","$5","$6","$7","$8","$9")\'>Pilih</a>','kd_obat1, apt_obat.nama_obat, apt_satuan_kecil.satuan_kecil, apt_obat.pembanding, max_stok,apt_obat.harga_dasar');		
		if(!empty($nama_obat))$this->datatables->like('apt_obat.nama_obat',$nama_obat,'both');
		
		$this->datatables->where('apt_stok_unit.kd_unit_apt',$kd_unit_apt);
		
		$this->datatables->join('apt_satuan_kecil','apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil','left');
		$this->datatables->join('apt_stok_unit','apt_obat.kd_obat=apt_stok_unit.kd_obat','left');
		$this->datatables->join('apt_unit','apt_stok_unit.kd_unit_apt=apt_unit.kd_unit_apt','left');
		$this->db->group_by("apt_stok_unit.kd_obat");
		$results = $this->datatables->generate();
		echo ($results);	
	}
	
	public function ambildaftarobatbykode(){
		$kd_obat=$this->input->post('kd_obat');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt_gudang');
		
		/*$this->datatables->select("apt_obat.kd_obat as kd_obat1, apt_obat.nama_obat, apt_satuan_kecil.satuan_kecil, 
								ifnull( sum(substring_index( apt_stok_unit.jml_stok, '.', 1 )) , 0 ) as jml_stok, 
								'Pilihan' as pilihan,apt_obat.pembanding,
								(select if( count(kd_obat)>0, max_stok, 0 ) from apt_setting_obat where kd_obat=kd_obat1 and kd_unit_apt='$kd_unit_apt') as max_stok,
								apt_obat.harga_beli",false);*/
		$this->datatables->select("apt_obat.kd_obat as kd_obat1, apt_obat.nama_obat, apt_satuan_kecil.satuan_kecil, 
								ifnull( sum(substring_index( apt_stok_unit.jml_stok, '.', 1 )) , 0 ) as jml_stok, 
								'Pilihan' as pilihan,apt_obat.pembanding,
								(select if( count(kd_obat)>0, max_stok, 0 ) from apt_setting_obat where kd_obat=kd_obat1 and kd_unit_apt='$kd_unit_apt') as max_stok,
								apt_obat.harga_dasar",false);
		
		$this->datatables->from("apt_obat");
		//$this->datatables->edit_column('pilihan', '<a class="btn" onclick=\'pilihobat("$1","$2","$3","$4","$5","$6","$7","$8","$9")\'>Pilih</a>','kd_obat1, apt_obat.nama_obat, apt_satuan_kecil.satuan_kecil, apt_obat.pembanding, max_stok,apt_obat.harga_beli');		
		$this->datatables->edit_column('pilihan', '<a class="btn" onclick=\'pilihobat("$1","$2","$3","$4","$5","$6","$7","$8","$9")\'>Pilih</a>','kd_obat1, apt_obat.nama_obat, apt_satuan_kecil.satuan_kecil, apt_obat.pembanding, max_stok,apt_obat.harga_dasar');		
		if(!empty($kd_obat))$this->datatables->like('apt_obat.kd_obat',$kd_obat,'both');
		
		$this->datatables->where('apt_stok_unit.kd_unit_apt',$kd_unit_apt);
		
		$this->datatables->join('apt_satuan_kecil','apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil','left');
		$this->datatables->join('apt_stok_unit','apt_obat.kd_obat=apt_stok_unit.kd_obat','left');
		$this->datatables->join('apt_unit','apt_stok_unit.kd_unit_apt=apt_unit.kd_unit_apt','left');
		$this->db->group_by("apt_stok_unit.kd_obat");
		$results = $this->datatables->generate();
		echo ($results);		
	}
	
	public function ambilsupplierbykode(){
		$q=$this->input->get('query');
		$items=$this->mpemesananapt->ambilData3($q);
		echo json_encode($items);
	}
	
	public function ambilsupplierbynama(){
		$q=$this->input->get('query');
		$items=$this->mpemesananapt->ambilData4($q);
		echo json_encode($items);
	}
	
	public function periksapemesanan() {
		$msg=array();
		$submit=$this->input->post('submit');
		$no_pemesanan=$this->input->post('no_pemesanan');
		$tgl_pemesanan=$this->input->post('tgl_pemesanan');
		$tgl_tempo=$this->input->post('tgl_tempo');
		$kd_supplier=$this->input->post('kd_supplier');
		$nama=$this->input->post('nama');
		$keterangan=$this->input->post('keterangan');
		
		$kd_obat=$this->input->post('kd_obat');
		$nama_obat=$this->input->post('nama_obat');
		$satuan_kecil=$this->input->post('satuan_kecil');
		$pembanding=$this->input->post('pembanding');
		$qty_box=$this->input->post('qty_box'); 
		$qty_kcl=$this->input->post('qty_kcl');
		$harga_beli=$this->input->post('harga_beli');
		$diskon=$this->input->post('diskon');
		$ppn=$this->input->post('ppn');
		
		$nama_pegawai=$this->input->post('nama_pegawai');
		$status=$this->input->post('status');
		$kd_app=$this->input->post('kd_app');
		$kd_applogin=$this->input->post('kd_applogin');
		
		$jumlaherror=0;
		$msg['status']=1;
		$msg['clearform']=0;
		$msg['pesanatas']="";
		$msg['pesanlain']="";
		
		if($submit=="hapus"){
			if(empty($no_pemesanan)){
				$msg['pesanatas']="Pilih Transaksi yang akan di hapus";
			}else{
				$this->mpemesananapt->delete('apt_pemesanan','no_pemesanan="'.$no_pemesanan.'"');
				$this->mpemesananapt->delete('apt_pemesanan_detail','no_pemesanan="'.$no_pemesanan.'"');
				$msg['pesanatas']="Data Berhasil Di Hapus";
			}
			$msg['status']=0;
			$msg['clearform']=1;
			echo json_encode($msg);
		}		
		else{
			if($jumlaherror>0){
				$msg['status']=0;
				$msg['error']=$jumlaherror;
				$msg['pesanatas']="Terdapat beberapa kesalahan input silahkan cek inputan anda";
			}
		}
		echo json_encode($msg);
	}
	
	public function ambilitem()
	{
		$q=$this->input->get('query');
		$items=$this->mpemesananapt->getPemesanan($q);

		echo json_encode($items);
	}
	
	public function ambilitems()
	{
		$q=$this->input->get('query');
		$items=$this->mpemesananapt->getAllDetailPemesanan($q);

		echo json_encode($items);
	}

	public function pemesananobatxls($periodeawal="",$periodeakhir=""){
		if($periodeawal=="NULL")$periodeawal="";
		if($periodeakhir=="NULL")$periodeakhir="";

		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 


		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");

		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(2.14); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(6); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(10); //kode
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(40); //nama obat
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(10); //satuan
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(15); //tgl expire

		$objPHPExcel->getActiveSheet()->mergeCells('A2:E2');
		$objPHPExcel->getActiveSheet()->setCellValue ('A2','DAFTAR PEMESANAN OBAT / ALKES');

		$baris=3;		
		$items=$this->mpemesananapt->ambilDataPemesanan('',$periodeawal,$periodeakhir);
		foreach ($items as $item) {
			# code...
			$baris=$baris+1;
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'Distributor ');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,' :'.$item['nama']);
			$baris=$baris+1;
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'No Pemesanan ');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,' :'.$item['no_pemesanan']);
			$baris=$baris+1;
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'No Pengajuan ');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,' :'.$item['no_pengajuan']);
			$baris=$baris+1;
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'Tgl Pemesanan ');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,' :'.$item['tgl_pesan']);
			$baris=$baris+1;

			for($x='A';$x<='E';$x++){
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			}		
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'NO');
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'KODE');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'NAMA OBAT');
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,'SATUAN');
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,'QTY');
			$baris=$baris+1;
			$rows=array();
			$rows=$this->mpemesananapt->ambilDetilDataPemesanan($item['kd_supplier'],$item['no_pemesanan'],convertDate($item['tgl_pesan']));
			$nomor=1;
			foreach ($rows as $row) {
				# code...
				for($x='A';$x<='E';$x++){
					if($x=='A'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
					}else if($x=='B'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
					}else if($x=='C'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
					}else if($x=='D'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
					}else if($x=='E'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
					}		
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
						'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
						'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => '000000')))));
				}
				$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
				$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,"'".$row['kd_obat']);
				$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$row['nama_obat']);
				$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$row['satuan_kecil']);
				$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$row['qty_kecil']);
						
				$nomor=$nomor+1; 
				$baris=$baris+1; 
				//$total=$total+$nilai;
			}
		}	
			
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/pemesananobat.xls");
		header("Location: ".base_url()."download/pemesananobat.xls");
	}

	public function suratpemesananobatxls($no_sp){
		if($periodeawal=="NULL")$periodeawal="";
		if($periodeakhir=="NULL")$periodeakhir="";

		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 


		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");

		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(2.14); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(6); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(10); //kode
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(40); //nama obat
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(10); //satuan
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(15); //tgl expire

		$objPHPExcel->getActiveSheet()->mergeCells('A2:E2');
		$objPHPExcel->getActiveSheet()->setCellValue ('A2','DAFTAR PEMESANAN OBAT / ALKES');

		$baris=3;		
		$items=$this->mpemesananapt->ambilDataPemesanan($no_sp,'','');
		foreach ($items as $item) {
			# code...
			$baris=$baris+1;
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'Distributor ');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,' :'.$item['nama']);
			$baris=$baris+1;
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'No Pemesanan ');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,' :'.$item['no_pemesanan']);
			$baris=$baris+1;
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'No Pengajuan ');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,' :'.$item['no_pengajuan']);
			$baris=$baris+1;
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':B'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'Tgl Pemesanan ');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,' :'.$item['tgl_pesan']);
			$baris=$baris+1;

			for($x='A';$x<='E';$x++){
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			}		
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'NO');
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'KODE');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'NAMA OBAT');
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,'SATUAN');
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,'QTY');
			$baris=$baris+1;
			$rows=array();
			$rows=$this->mpemesananapt->ambilDetilDataPemesanan($item['kd_supplier'],$item['no_pemesanan'],convertDate($item['tgl_pesan']));
			$nomor=1;
			foreach ($rows as $row) {
				# code...
				for($x='A';$x<='E';$x++){
					if($x=='A'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
					}else if($x=='B'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
					}else if($x=='C'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
					}else if($x=='D'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
					}else if($x=='E'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
					}		
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
						'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
						'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => '000000')))));
				}
				$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
				$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,"'".$row['kd_obat']);
				$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$row['nama_obat']);
				$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$row['satuan_kecil']);
				$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$row['qty_kecil']);
						
				$nomor=$nomor+1; 
				$baris=$baris+1; 
				//$total=$total+$nilai;
			}
		}	
			
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/pemesananobat-".$no_sp.".xls");
		header("Location: ".base_url()."download/pemesananobat-".$no_sp.".xls");
	}

	public function suratpemesananobatxls2($no_sp){

		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = IOFactory::load('template/sp-prekursor.xlsx');

		$objPHPExcel->getActiveSheet()->getStyle('A1:H30')->applyFromArray(array(
			'font'    => array('name'      => 'calibri','size'		=>10)));


		$objPHPExcel->getProperties()->setTitle("SP Prekursor")->setDescription("Surat Pesanan Prekursor");

		$objPHPExcel->getActiveSheet()->setCellValue ('A2','DAFTAR PEMESANAN OBAT / ALKES');

		$items=$this->mpemesananapt->ambilDataPemesanan($no_sp,'','');
		foreach ($items as $item) {
			# code...
			$objPHPExcel->getActiveSheet()->mergeCells('C10:D10');
			$objPHPExcel->getActiveSheet()->setCellValue ('C10',' :'.$item['nama']);
			$objPHPExcel->getActiveSheet()->mergeCells('C11:D11');
			$objPHPExcel->getActiveSheet()->setCellValue ('C11',' :'.$item['alamat']);
			$objPHPExcel->getActiveSheet()->mergeCells('C12:D12');
			$objPHPExcel->getActiveSheet()->setCellValue ('C12',' :'.$item['telp']);

			$baris=17;
			$rows=array();
			$rows=$this->mpemesananapt->ambilDetilDataPemesanan($item['kd_supplier'],$item['no_pemesanan'],convertDate($item['tgl_pesan']));
			$nomor=1;
			foreach ($rows as $row) {
				# code...
				for($x='A';$x<='H';$x++){
					if($x=='A'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
					}else if($x=='B'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
					}else if($x=='C'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
					}else if($x=='D'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
					}else if($x=='E'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
					}else if($x=='F'){
						$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
							array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
					}		
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
						'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
						'size'		=>10 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
						'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => '000000')))));
				}
				$objPHPExcel->getActiveSheet()->mergeCells('B'.$baris.':C'.$baris);
				$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
				$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$row['nama_obat']);
				$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$row['satuan_besar']);
				$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$row['qty_besar']);
						
				$nomor=$nomor+1; 
				$baris=$baris+1; 
				//$total=$total+$nilai;
			}
			
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'Obat mengandung Prekursor Farmasi tersebut akan digunakan untuk memenuhi kebutuhan :');
			$baris++;
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$item['keterangan']);

			$baris++;
			$baris++;
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':C'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'Nama Apotek / RS');

			$baris++;
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':C'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'Alamat');

			$baris++;
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':C'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'No. SIA');

			$baris++;
			$objPHPExcel->getActiveSheet()->mergeCells('F'.$baris.':H'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,'Balikpapan, '.date('d-m-Y'));
			$baris++;
			$objPHPExcel->getActiveSheet()->mergeCells('F'.$baris.':H'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,'Pemesan');

		}	
			
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/pemesananobat-".$no_sp.".xls");
		header("Location: ".base_url()."download/pemesananobat-".$no_sp.".xls");
	}

}




/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */