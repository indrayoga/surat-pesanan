<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'controllers/rumahsakit.php');
//class Laporanapt extends CI_Controller {
class Laporanapt extends Rumahsakit {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	protected $title='SIM RS - Sistem Informasi Rumah Sakit';

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('form');
		$this->load->helper('utilities');
		$this->load->library('pagination');
		$this->load->model('apotek/mlaporanapt');
		$this->load->helper('url');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		if(empty($kd_unit_apt)){
			redirect('/home/');
		}
	}
	
	public function restricted(){
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		//$this->load->view('master/header',$dataheader);
		$this->load->view('headerapotek',$dataheader);
		$data=array();
		parent::view_restricted($data);
		$this->load->view('footer');
	}
	
	public function index()
	{
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'main.js','style-switcher.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>$this->title);
		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		$data=array();
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('',$data);
		$this->load->view('footer',$datafooter);
	}

	public function penerimaanapotek(){
		if(!$this->muser->isAkses("69")){
			$this->restricted();
			return false;
		}
		
		//$shift='';
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		//$tgl_tempo='';
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		$kd_supplier='';
		$kd_pabrik='';
		
		/*if($this->input->post('shift')!=''){
			$shift=$this->input->post('shift');
		}*/
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		/*if($this->input->post('tgl_tempo')!=''){
			$tgl_tempo=$this->input->post('tgl_tempo');
		}*/
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}
		if($this->input->post('kd_supplier')!=''){
			$kd_supplier=$this->input->post('kd_supplier');
		}
		if($this->input->post('kd_pabrik')!=''){
			$kd_pabrik=$this->input->post('kd_pabrik');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Penerimaan Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array(//'items'=>$this->mlaporanapt->getAllPenerimaanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$shift,$tgl_tempo,$kd_supplier),
					//'items'=>$this->mlaporanapt->getAllPenerimaanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$tgl_tempo,$kd_supplier,$kd_pabrik),
					'items'=>$this->mlaporanapt->getAllPenerimaanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$kd_supplier,$kd_pabrik),
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'datasupplier'=>$this->mlaporanapt->ambilData("apt_supplier","is_aktif='1'"),
					'datapabrik'=>$this->mlaporanapt->ambilData('apt_pabrik'),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					//'tgl_tempo'=>$tgl_tempo,
					//'shift'=>$shift,
					'kd_pabrik'=>$kd_pabrik,
					'kd_unit_apt'=>$kd_unit_apt,
					'kd_supplier'=>$kd_supplier);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/penerimaanapotek',$data);
		$this->load->view('footer',$datafooter);
	}

	public function pareto(){
		if(!$this->muser->isAkses("69")){
			$this->restricted();
			return false;
		}
		
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		$kd_pabrik='';
		
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('kd_pabrik')!=''){
			$kd_pabrik=$this->input->post('kd_pabrik');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Penerimaan Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array('items'=>$this->mlaporanapt->pareto($periodeawal,$periodeakhir,$kd_unit_apt,$kd_pabrik),
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'datapabrik'=>$this->mlaporanapt->ambilData('apt_pabrik'),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'kd_pabrik'=>$kd_pabrik,
					'kd_unit_apt'=>$kd_unit_apt);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/paretoapotek',$data);
		$this->load->view('footer',$datafooter);
	}

	public function formularium(){
		if(!$this->muser->isAkses("69")){
			$this->restricted();
			return false;
		}
		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Formularium Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array(
			'items'=>$this->mlaporanapt->formularium()
			);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/formularium',$data);
		$this->load->view('footer',$datafooter);
	}

	public function kunjunganresep(){
		if(!$this->muser->isAkses("69")){
			$this->restricted();
			return false;
		}
		
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Penerimaan Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array('items'=>$this->mlaporanapt->kunjunganresep($periodeawal,$periodeakhir,$kd_unit_apt),
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'kd_unit_apt'=>$kd_unit_apt);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/kunjunganresep',$data);
		$this->load->view('footer',$datafooter);
	}

	public function paretoapotek(){
		if(!$this->muser->isAkses("69")){
			//$this->restricted();
			//return false;
		}
		
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		$kd_pabrik='';
		
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('kd_pabrik')!=''){
			$kd_pabrik=$this->input->post('kd_pabrik');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Penerimaan Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array('items'=>$this->mlaporanapt->paretoapotek($periodeawal,$periodeakhir,$kd_unit_apt,$kd_pabrik),
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'datapabrik'=>$this->mlaporanapt->ambilData('apt_pabrik'),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'kd_pabrik'=>$kd_pabrik,
					'kd_unit_apt'=>$kd_unit_apt);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/paretoapotekunit',$data);
		$this->load->view('footer',$datafooter);
	}

	public function summarypenerimaanapotek(){
		if(!$this->muser->isAkses("69")){
			$this->restricted();
			return false;
		}
		
		//$shift='';
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		//$tgl_tempo='';
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		$summary='';
		$submit='';
		//$kd_pabrik='';
		
		/*if($this->input->post('shift')!=''){
			$shift=$this->input->post('shift');
		}*/
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		/*if($this->input->post('tgl_tempo')!=''){
			$tgl_tempo=$this->input->post('tgl_tempo');
		}*/
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}
		if($this->input->post('summary')!=''){
			$summary=$this->input->post('summary');
		}
		if($this->input->post('submit')!=''){
			$submit=$this->input->post('submit');
		}

		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Summary Penerimaan Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array(//'items'=>$this->mlaporanapt->getAllPenerimaanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$shift,$tgl_tempo,$kd_supplier),
					//'items'=>$this->mlaporanapt->getAllPenerimaanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$tgl_tempo,$kd_supplier,$kd_pabrik),
					//'items'=>$this->mlaporanapt->getAllPenerimaanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$kd_supplier,$kd_pabrik),
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					//'datasupplier'=>$this->mlaporanapt->ambilData("apt_supplier","is_aktif='1'"),
					//'datapabrik'=>$this->mlaporanapt->ambilData('apt_pabrik'),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					//'tgl_tempo'=>$tgl_tempo,
					//'shift'=>$shift,
					//'kd_pabrik'=>$kd_pabrik,
					'kd_unit_apt'=>$kd_unit_apt,
					'summary'=>$summary);
		//debugvar($data);
		//if($submit)redirect(th);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/summarypenerimaanapotek',$data);
		$this->load->view('footer',$datafooter);
	}	
	public function returapotek(){
		if(!$this->muser->isAkses("69")){
			$this->restricted();
			return false;
		}
		
		//$shift='';
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		//$tgl_tempo='';
		$kd_unit_apt='';
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		$kd_supplier='';
		$kd_pabrik='';
		
		/*if($this->input->post('shift')!=''){
			$shift=$this->input->post('shift');
		}*/
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		/*if($this->input->post('tgl_tempo')!=''){
			$tgl_tempo=$this->input->post('tgl_tempo');
		}*/
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}
		if($this->input->post('kd_supplier')!=''){
			$kd_supplier=$this->input->post('kd_supplier');
		}
		if($this->input->post('kd_pabrik')!=''){
			$kd_pabrik=$this->input->post('kd_pabrik');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Retur Penerimaan Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array(//'items'=>$this->mlaporanapt->getAllPenerimaanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$shift,$tgl_tempo,$kd_supplier),
					//'items'=>$this->mlaporanapt->getAllPenerimaanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$tgl_tempo,$kd_supplier,$kd_pabrik),
					'items'=>$this->mlaporanapt->getAllReturApotek($periodeawal,$periodeakhir,$kd_unit_apt),
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'kd_unit_apt'=>$kd_unit_apt);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/returapotek',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function penjualanobatapotek(){
		if(!$this->muser->isAkses("77")){
			$this->restricted();
			return false;
		}
		
		$shiftapt='';
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');		
		$is_lunas='';
		
		if($this->input->post('shiftapt')!=''){
			$shiftapt=$this->input->post('shiftapt');
		}
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}
		if($this->input->post('is_lunas')!=''){
			$is_lunas=$this->input->post('is_lunas');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Penjualan Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array('items'=>$this->mlaporanapt->ambilIsiPenjualan($periodeawal,$periodeakhir,$kd_unit_apt,$shiftapt,$is_lunas),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'datashift'=>$this->mlaporanapt->ambilItemData('unit_shift','kd_unit="APT" '),
					'is_lunas'=>$is_lunas,
					'shiftapt'=>$shiftapt,
					'kd_unit_apt'=>$kd_unit_apt);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/penjualanobatapotek',$data);
		$this->load->view('footer',$datafooter);
	}

	public function penjualanobatdokter(){
		if(!$this->muser->isAkses("107")){
			$this->restricted();
			return false;
		}
		
		$kd_dokter='';
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');		
		$kd_pabrik=$this->input->post('kd_pabrik');		
		
		if($this->input->post('kd_dokter')!=''){
			$kd_dokter=$this->input->post('kd_dokter');
		}
		if($this->input->post('kd_pabrik')!=''){
			$kd_pabrik=$this->input->post('kd_pabrik');
		}
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}

		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Peresepan:: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array('items'=>$this->mlaporanapt->getResepByDokter($periodeawal,$periodeakhir,$kd_unit_apt,$kd_dokter,$kd_pabrik),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'kd_dokter'=>$kd_dokter,
					'kd_pabrik'=>$kd_pabrik,
					'datadokter'=>$this->mlaporanapt->ambilData('apt_dokter'),
					'datapabrik'=>$this->mlaporanapt->ambilData('apt_pabrik'),
					'kd_unit_apt'=>$kd_unit_apt);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/penjualanobatdokter',$data);
		$this->load->view('footer',$datafooter);
	}

	public function penjualanobatdokter2(){
		if(!$this->muser->isAkses("119")){
			$this->restricted();
			return false;
		}
		//$query=$this->db->query('call dokter_report("2015-06-01","2015-06-30")');
		//$items=$query->result_array();
		//debugvar($items);
		$kd_dokter='';
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');		
		
		if($this->input->post('kd_dokter')!=''){
			$kd_dokter=$this->input->post('kd_dokter');
		}
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}

		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Penjualan Obat Per Dokter :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array(
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'kd_dokter'=>$kd_dokter,
					'datadokter'=>$this->mlaporanapt->ambilData('apt_dokter','kd_dokter!="99"'),
					'kd_unit_apt'=>$kd_unit_apt);
		//debugvar($data['items']);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/penjualanobatdokter2',$data);
		$this->load->view('footer',$datafooter);
	}

	public function rekapkunjunganharian(){
		if(!$this->muser->isAkses("120")){
			$this->restricted();
			return false;
		}
		$bulan=date('m');
		$tahun=date('Y');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');		
		if($this->input->post('bulan')!=''){
			$bulan=$this->input->post('bulan');
		}
		if($this->input->post('tahun')!=''){
			$tahun=$this->input->post('tahun');
		}
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');

		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');

		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>$this->title);
		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		$data=array(
					'tahun'=>$tahun,
					'bulan'=>$bulan,
					'items'=>$this->mlaporanapt->rekapkunjunganharian($tahun,$bulan),
					'items2'=>$this->mlaporanapt->rekapkunjunganharianbytipe($tahun,$bulan),
					'kd_unit_apt'=>$kd_unit_apt);
		//debugvar($data['items']);

		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/rekapkunjunganharian',$data);
		$this->load->view('footer',$datafooter);
	}



	public function rekappendapatanharian(){

		if(!$this->muser->isAkses("122")){
			$this->restricted();
			return false;
		}

		$bulan=date('m');
		$tahun=date('Y');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');		
		
		if($this->input->post('bulan')!=''){
			$bulan=$this->input->post('bulan');
		}

		if($this->input->post('tahun')!=''){
			$tahun=$this->input->post('tahun');
		}

		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}


		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');

		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');

		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);

		$data=array(
					'tahun'=>$tahun,
					'bulan'=>$bulan,
					'items'=>$this->mlaporanapt->rekappendapatanharian($tahun,$bulan),
					'items2'=>$this->mlaporanapt->rekappendapatanharianbytipe($tahun,$bulan),
					'kd_unit_apt'=>$kd_unit_apt);
		//debugvar($data['items']);

		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/rekappendapatanharian',$data);
		$this->load->view('footer',$datafooter);
	}



	public function rekapkunjunganbulanan(){
		if(!$this->muser->isAkses("121")){
			$this->restricted();
			return false;
		}

		$bulan1=date('m');
		$bulan2=date('m');
		$tahun=date('Y');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');		
		
		if($this->input->post('bulan1')!=''){
			$bulan1=$this->input->post('bulan1');
		}
		if($this->input->post('bulan2')!=''){
			$bulan2=$this->input->post('bulan2');
		}

		if($this->input->post('tahun')!=''){
			$tahun=$this->input->post('tahun');
		}

		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}

		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');

		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');

		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>$this->title);

		$jsfooter=array();

		$datafooter=array('jsfile'=>$jsfooter);

		$data=array(
					'tahun'=>$tahun,
					'bulan1'=>$bulan1,
					'bulan2'=>$bulan2,
					'items'=>$this->mlaporanapt->rekapkunjunganbulanan($tahun,$bulan1,$bulan2),
					'items2'=>$this->mlaporanapt->rekapkunjunganbulananbytipe($tahun,$bulan1,$bulan2),
					'kd_unit_apt'=>$kd_unit_apt);
		//debugvar($data['items']);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/rekapkunjunganbulanan',$data);
		$this->load->view('footer',$datafooter);
	}



	public function rekappendapatanbulanan(){

		if(!$this->muser->isAkses("123")){

			$this->restricted();

			return false;

		}



		$bulan1=date('m');

		$bulan2=date('m');

		$tahun=date('Y');

		$kd_unit_apt=$this->session->userdata('kd_unit_apt');

		$nama_unit_apt=$this->input->post('nama_unit_apt');		

		

		if($this->input->post('bulan1')!=''){

			$bulan1=$this->input->post('bulan1');

		}

		if($this->input->post('bulan2')!=''){

			$bulan2=$this->input->post('bulan2');

		}

		if($this->input->post('tahun')!=''){

			$tahun=$this->input->post('tahun');

		}

		if($this->input->post('kd_unit_apt')!=''){

			$kd_unit_apt=$this->input->post('kd_unit_apt');

		}



		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');

		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',

							'vendor/jquery-1.9.1.min.js',

							'vendor/jquery-migrate-1.1.1.min.js',

							'vendor/jquery-ui-1.10.0.custom.min.js',

							'vendor/bootstrap.min.js',

							'lib/jquery.tablesorter.min.js',

							'lib/jquery.dataTables.min.js',

							'lib/DT_bootstrap.js',

							'lib/responsive-tables.js',

							'lib/bootstrap-datepicker.js',

							'lib/bootstrap-inputmask.js',

							'lib/bootstrap-modal.js',

							'spin.js',

							'main.js');

		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>$this->title);



		$jsfooter=array();

		$datafooter=array('jsfile'=>$jsfooter);

		

		$data=array(

					'tahun'=>$tahun,

					'bulan1'=>$bulan1,

					'bulan2'=>$bulan2,

					'items'=>$this->mlaporanapt->rekappendapatanbulanan($tahun,$bulan1,$bulan2),

					'items2'=>$this->mlaporanapt->rekappendapatanbulananbytipe($tahun,$bulan1,$bulan2),

					'kd_unit_apt'=>$kd_unit_apt);

		//debugvar($data['items']);

		$this->load->view('headerapotek',$dataheader);

		$this->load->view('laporanapotek/rekappendapatanbulanan',$data);

		$this->load->view('footer',$datafooter);

	}


	public function hnahargajual(){
		if(!$this->muser->isAkses("124")){
			$this->restricted();
			return false;
		}
		
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$tipe='';
		$jenis_pasien='';
		$kd_unit_apt="";
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}

		if($this->input->post('tipe')!=''){
			$tipe=$this->input->post('tipe');
		}
		if($this->input->post('jenis_pasien')!=''){
			$jenis_pasien=$this->input->post('jenis_pasien');
		}

		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array('items'=>$this->mlaporanapt->getAllHNAPPN($periodeawal,$periodeakhir,$kd_unit_apt,$jenis_pasien,$tipe),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'datacustomer'=>$this->mlaporanapt->ambilData('apt_customers'),
					'dataunit'=>$this->mlaporanapt->ambilData('apt_unit'),
					'jenis_pasien'=>$jenis_pasien,
					'tipe'=>$tipe,
					'kd_unit_apt'=>$kd_unit_apt
					);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/hnahargajual',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function penjualanapotek(){
		if(!$this->muser->isAkses("76")){
			$this->restricted();
			return false;
		}
		
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$shiftapt='';
		$is_lunas='';
		$tipe='';
		$jenis_pasien='';
		$dokter='';
		$kd_user='';
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		$resep='';
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('shiftapt')!=''){
			$shiftapt=$this->input->post('shiftapt');
		}
		if($this->input->post('is_lunas')!=''){
			$is_lunas=$this->input->post('is_lunas');
		}
		if($this->input->post('resep')!=''){
			$resep=$this->input->post('resep');
		}
		if($this->input->post('tipe')!=''){
			$tipe=$this->input->post('tipe');
		}
		if($this->input->post('jenis_pasien')!=''){
			$jenis_pasien=$this->input->post('jenis_pasien');
		}
		if($this->input->post('dokter')!=''){
			$dokter=$this->input->post('dokter');
		}
		if($this->input->post('kd_user')!=''){
			$kd_user=$this->input->post('kd_user');
		}

		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Penjualan Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		$querypegawai=$this->db->query("select * from user join pegawai on user.id_pegawai=pegawai.id_pegawai where pegawai.jenis_pegawai in (4,8) ");
		$data=array('items'=>$this->mlaporanapt->getAllPenjualanApotek($periodeawal,$periodeakhir,$shiftapt,$is_lunas,$kd_unit_apt,$resep,$jenis_pasien,$dokter,$tipe,$kd_user),					
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'shiftapt'=>$shiftapt,
					'is_lunas'=>$is_lunas,
					'datashift'=>$this->mlaporanapt->ambilItemData('unit_shift','kd_unit="APT" '),
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'datacustomer'=>$this->mlaporanapt->ambilData('apt_customers'),
					'datadokter'=>$this->mlaporanapt->ambilData('apt_dokter'),
					'kd_unit_apt'=>$kd_unit_apt,
					'jenis_pasien'=>$jenis_pasien,
					'kd_user'=>$kd_user,
					'users'=>$querypegawai->result_array(),
					'tipe'=>$tipe,
					'dktr'=>$dokter,
					'resep'=>$resep);

		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/penjualanapotek',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function penjualanapoteknontunai(){
		if(!$this->muser->isAkses("76")){
			$this->restricted();
			return false;
		}
		
		$bulan=date('m');
		$tahun=date('Y');
		$tipe='';
		$jenis_pasien='';
		$kd_user='';
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		if($this->input->post('bulan')!=''){
			$bulan=$this->input->post('bulan');
		}
		if($this->input->post('tahun')!=''){
			$tahun=$this->input->post('tahun');
		}
		if($this->input->post('tipe')!=''){
			$tipe=$this->input->post('tipe');
		}
		if($this->input->post('jenis_pasien')!=''){
			$jenis_pasien=$this->input->post('jenis_pasien');
		}

		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Penerimaan Obat Non tunai :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		$data=array('items'=>$this->mlaporanapt->getAllPenjualanApotekNonTunai($bulan,$tahun,$kd_unit_apt,$jenis_pasien,$tipe),					
					'bulan'=>$bulan,
					'tahun'=>$tahun,
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'datacustomer'=>$this->mlaporanapt->ambilData('apt_customers','type!=0'),
					'kd_unit_apt'=>$kd_unit_apt,
					'jenis_pasien'=>$jenis_pasien,
					'tipe'=>$tipe);

		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/penjualanapoteknontunai',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function returpenjualanapotek(){
		if(!$this->muser->isAkses("76")){
			$this->restricted();
			return false;
		}
		
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$shiftapt='';
		$jenis_pasien='';
		$dokter='';
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		$resep='';
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('shiftapt')!=''){
			$shiftapt=$this->input->post('shiftapt');
		}
		if($this->input->post('resep')!=''){
			$resep=$this->input->post('resep');
		}
		if($this->input->post('jenis_pasien')!=''){
			$jenis_pasien=$this->input->post('jenis_pasien');
		}
		if($this->input->post('dokter')!=''){
			$dokter=$this->input->post('dokter');
		}

		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Retur Penjualan Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array('items'=>$this->mlaporanapt->getAllReturPenjualanApotek($periodeawal,$periodeakhir,$shiftapt,$kd_unit_apt,$resep,$jenis_pasien,$dokter),					
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'shiftapt'=>$shiftapt,
					'datashift'=>$this->mlaporanapt->ambilItemData('unit_shift','kd_unit="APT" '),
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'datacustomer'=>$this->mlaporanapt->ambilData('apt_customers'),
					'datadokter'=>$this->mlaporanapt->ambilData('apt_dokter'),
					'kd_unit_apt'=>$kd_unit_apt,
					'jenis_pasien'=>$jenis_pasien,
					'dktr'=>$dokter,
					'resep'=>$resep);

		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/returpenjualanapotek',$data);
		$this->load->view('footer',$datafooter);
	}

	public function distribusiapotek(){
		if(!$this->muser->isAkses("71")){
			$this->restricted();
			return false;
		}
		
		$kd_unit_asal=$this->session->userdata('kd_unit_apt');
		$kd_unit_tujuan='';
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$kd_unit_tujuan="";
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('kd_unit_tujuan')!=''){
			$kd_unit_tujuan=$this->input->post('kd_unit_tujuan');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Distribusi Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array('items'=>$this->mlaporanapt->getAllDistribusiApotek($periodeawal,$periodeakhir,$kd_unit_asal,$kd_unit_tujuan),
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'kd_unit_tujuan'=>$kd_unit_tujuan,
					'kd_unit_asal'=>$kd_unit_asal);

		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/distribusiapotek',$data);
		$this->load->view('footer',$datafooter);
	}

	public function persediaanapotek(){
		if(!$this->muser->isAkses("72")){
			$this->restricted();
			return false;
		}
		
		//$kd_jenis_obat='';
		//$kd_sub_jenis='';
		$kd_golongan="";
		$stok='1';
		$isistok="";
		//$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$kd_unit_apt="";
		$nama_unit_apt="";
		/*if($this->input->post('kd_jenis_obat')!=''){
			$kd_jenis_obat=$this->input->post('kd_jenis_obat');
		}
		if($this->input->post('kd_sub_jenis')!=''){
			$kd_sub_jenis=$this->input->post('kd_sub_jenis');
		}*/
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}
		if($this->input->post('kd_golongan')!=''){
			$kd_golongan=$this->input->post('kd_golongan');
		}
		if($this->input->post('stok')!=''){
			$stok=$this->input->post('stok');
		}
		if($this->input->post('isistok')!=''){
			$isistok=$this->input->post('isistok');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Persediaan Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array(//'items'=>$this->mlaporanapt->getAllPersediaanApotek($stok,$kd_jenis_obat,$isistok,$kd_sub_jenis,$kd_unit_apt,$kd_golongan),
					'items'=>$this->mlaporanapt->getAllPersediaanApotek($stok,$isistok,$kd_unit_apt,$kd_golongan),
					/*'jenisobat'=>$this->mlaporanapt->ambilData('apt_jenis_obat'),
					'kd_jenis_obat'=>$kd_jenis_obat,
					'subjenis'=>$this->mlaporanapt->ambilData('apt_sub_jenis'),
					'kd_sub_jenis'=>$kd_sub_jenis,*/
					'golongan'=>$this->mlaporanapt->ambilData('apt_golongan'),
					'kd_golongan'=>$kd_golongan,
					'stok'=>$stok,
					'isistok'=>$isistok,
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'kd_unit_apt'=>$kd_unit_apt);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/persediaanapotek',$data);
		$this->load->view('footer',$datafooter);
	}

	public function kartustok(){
		if(!$this->muser->isAkses("73")){
			$this->restricted();
			return false;
		}
		
		$nama_obat='';
		$kd_obat='';
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$bulan=date('m');
		$tahun=date('Y');
		$submit1=$this->input->post('submit1');
		
		if($this->input->post('nama_obat')!=''){
			$nama_obat=$this->input->post('nama_obat');
		}
		if($this->input->post('kd_obat')!=''){
			$kd_obat=$this->input->post('kd_obat');
		}
		if($this->input->post('bulan')!=''){
			$bulan=$this->input->post('bulan');
		}
		if($this->input->post('tahun')!=''){
			$tahun=$this->input->post('tahun');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Kartu Stok Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		$data=array('items'=>$this->mlaporanapt->getKartuStok($kd_obat,$kd_unit_apt,$bulan,$tahun),
					'nama_obat'=>$nama_obat,
					'kd_obat'=>$kd_obat,
					'kd_unit_apt'=>$kd_unit_apt,
					'bulan'=>$bulan,
					'tahun'=>$tahun);
		
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/kartustok',$data);
		$this->load->view('footer',$datafooter);
	}

	/*public function ambilobatbynama(){
		$q=$this->input->get('query');
		$items=$this->mlaporanapt->ambilData4($q);
		echo json_encode($items);
	}*/
	
	public function ambilobatbynama(){
		/*$q=$this->input->get('query');
		$items=$this->mlaporanapt->ambilData4($q);
		echo json_encode($items);*/
		$nama_obat=$this->input->post('nama_obat');
		
		$this->datatables->select("kd_obat, nama_obat, 'Pilihan' as pilihan",false);
		
		$this->datatables->from("apt_obat");
		$this->datatables->edit_column('pilihan', '<a class="btn" onclick=\'pilihobat("$1","$2","$3")\'>Pilih</a>','kd_obat, nama_obat');		
		if(!empty($nama_obat))$this->datatables->like('nama_obat',$nama_obat,'both');
		
		$results = $this->datatables->generate();
		echo ($results);
	}
	
	public function ambilobatbykode(){
		$q=$this->input->get('query');
		$items=$this->mlaporanapt->ambilData5($q);
		echo json_encode($items);
	}
	

	public function mutasiobat(){
		
		if(!$this->muser->isAkses("74")){
			$this->restricted();
			return false;
		}
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$bulan=date('m');
		$tahun=date('Y');
		if($this->input->post('bulan')!=''){
			$bulan=$this->input->post('bulan');
		}
		if($this->input->post('tahun')!=''){
			$tahun=$this->input->post('tahun');
		}		
		$submit=$this->input->post('submit');
		if($submit=="cetak"){
			redirect('/transapotek/laporanapt/rl1excelmutasiobat/'.$kd_unit_apt.'/'.$bulan.'/'.$tahun);
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		//$msg['items']=$this->mlaporanapt->getMutasiObat($kd_unit_apt,$bulan,$tahun);
		$data=array('items'=>$this->mlaporanapt->getMutasiObat($kd_unit_apt,$bulan,$tahun),
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'kd_unit_apt'=>$kd_unit_apt,
					'bulan'=>$bulan,
					'tahun'=>$tahun);
		
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/mutasiobat',$data);
		$this->load->view('footer',$datafooter);
	}

	public function tutupbulan(){
		if(!$this->muser->isAkses("74")){
			$this->restricted();
			return false;
		}
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$bulan=date('m');
		$tahun=date('Y');
		if($this->input->post('bulan')!=''){
			$bulan=$this->input->post('bulan');
		}
		if($this->input->post('tahun')!=''){
			$tahun=$this->input->post('tahun');
		}		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Tutup Bulan :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		$data=array('items'=>$this->mlaporanapt->daftarmutasi(),
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'kd_unit_apt'=>$kd_unit_apt,
					'bulan'=>$bulan,
					'tahun'=>$tahun);
		
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/tutupbulan',$data);
		$this->load->view('footer',$datafooter);
	}
	

	public function carimutasiobat(){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');		
		$bulan=$this->input->post('bulan');
		$tahun=$this->input->post('tahun');
		$submit=$this->input->post('submit');
		$items=array();
		$msg=array();
		
		if($bulan=='01'){$bulan2=1;} if($bulan=='02'){$bulan2=2;} if($bulan=='03'){$bulan2=3;} if($bulan=='04'){$bulan2=4;}
		if($bulan=='05'){$bulan2=5;} if($bulan=='06'){$bulan2=6;} if($bulan=='07'){$bulan2=7;} if($bulan=='08'){$bulan2=8;}
		if($bulan=='09'){$bulan2=9;} if($bulan=='10'){$bulan2=10;} if($bulan=='11'){$bulan2=11;} if($bulan=='12'){$bulan2=12;}
		
		$a=$bulan2-1;   if($a=='0'){$a1='12';$b=$tahun-1;} //kalo dy mutasi obatnya bulan januari
						if($a=='1'){$a1='01';$b=$tahun;} if($a=='2'){$a1='02';$b=$tahun;} if($a=='3'){$a1='03';$b=$tahun;}
						if($a=='4'){$a1='04';$b=$tahun;} if($a=='5'){$a1='05';$b=$tahun;} if($a=='6'){$a1='06';$b=$tahun;}
						if($a=='7'){$a1='07';$b=$tahun;} if($a=='8'){$a1='08';$b=$tahun;} if($a=='9'){$a1='09';$b=$tahun;}
						if($a=='10'){$a1='10';$b=$tahun;} if($a=='11'){$a1='11';$b=$tahun;} //kalo dy mutasi obatnya bulan desember
		
		$in_pbf=$this->mlaporanapt->getPenerimaan($bulan,$tahun);
		if(!empty($in_pbf)){
			foreach($in_pbf as $terima){
				$kd_obat=$terima['kd_obat'];
				$qty=$terima['qty_kcl'];
				$harga=$terima['harga_beli'];
				//$harga=$this->mlaporanapt->isObatExist($kd_obat,$bulan,$tahun)
				if($this->mlaporanapt->isObatExist($kd_obat,$bulan,$tahun)){
					$b=$this->mlaporanapt->ambilin($kd_obat,$bulan,$tahun);					
					$data=array('in_pbf'=>$qty,'harga_beli'=>$harga);
					$this->mlaporanapt->update('apt_mutasi_obat',$data,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$kd_obat.'" and bulan="'.$bulan.'" and "'.$tahun.'"');
				}
				else{
					$data=array('tahun'=>$tahun,'bulan'=>$bulan,'kd_obat'=>$kd_obat,'kd_unit_apt'=>$kd_unit_apt,'saldo_awal'=>0,
								'in_pbf'=>$qty,'harga_beli'=>$harga);
					$this->mlaporanapt->insert('apt_mutasi_obat',$data);
				
				}
			}
		}
		
		$in_unit=$this->mlaporanapt->getDistribusi1($bulan,$tahun); //in_unit --->kd_unit_tujuan
		if(!empty($in_unit)){
			foreach($in_unit as $dist){
				$kd_obat=$dist['kd_obat'];
				$qtydis=$dist['qty'];
				$harga=$dist['harga_beli'];
				if($this->mlaporanapt->isObatExist($kd_obat,$bulan,$tahun)){
					$data=array('in_unit'=>$qtydis,'harga_beli'=>$harga);
					$this->mlaporanapt->update('apt_mutasi_obat',$data,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$kd_obat.'" and bulan="'.$bulan.'" and "'.$tahun.'"');
				}
				else{
					$data=array('tahun'=>$tahun,'bulan'=>$bulan,'kd_obat'=>$kd_obat,'kd_unit_apt'=>$kd_unit_apt,'saldo_awal'=>0,
								'in_unit'=>$qtydis,'harga_beli'=>$harga);
					$this->mlaporanapt->insert('apt_mutasi_obat',$data);
				}
			}
		}
		
		$out_jual=$this->mlaporanapt->getPenjualan($bulan,$tahun);
		if(!empty($out_jual)){
			foreach($out_jual as $dist){
				$kd_obat=$dist['kd_obat'];
				$qty=$dist['qty'];
				$harga=$dist['harga_jual'];
				if($this->mlaporanapt->isObatExist($kd_obat,$bulan,$tahun)){
					$data=array('out_jual'=>$qty,'harga_jual'=>$harga);
					$this->mlaporanapt->update('apt_mutasi_obat',$data,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$kd_obat.'" and bulan="'.$bulan.'" and "'.$tahun.'"');
				}
				else{
					$data=array('tahun'=>$tahun,'bulan'=>$bulan,'kd_obat'=>$kd_obat,'kd_unit_apt'=>$kd_unit_apt,'saldo_awal'=>0,
								'out_jual'=>$qty,'harga_jual'=>$harga);
					$this->mlaporanapt->insert('apt_mutasi_obat',$data);
				}
			}
		}
		
		/*$hrgbli=$this->mlaporanapt->getHargaBeli($kd_unit_apt,$bulan,$tahun);
		if(!empty($hrgbli)){
			foreach($hrgbli as $beli){
				$kd_obat=$beli['kd_obat'];
				$harga=$dist['harga_beli'];
				if($harga==''){$harga1=$this->mlaporanapt->ambilHargaBeli($kd_unit_apt,$kd_obat);}
				else{}
			}
		}*/		
		
		$out_unit=$this->mlaporanapt->getDistribusi($bulan,$tahun); //bwt yg out_unit--->distribusi ke unit lain -->kd_unit_asal
		if(!empty($out_unit)){
			foreach($out_unit as $dist){
				$kd_obat=$dist['kd_obat'];
				$qtydis=$dist['qty'];
				$harga=$dist['harga_beli'];
				if($this->mlaporanapt->isObatExist($kd_obat,$bulan,$tahun)){
					$data=array('out_unit'=>$qtydis,'harga_beli'=>$harga);
					$this->mlaporanapt->update('apt_mutasi_obat',$data,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$kd_obat.'" and bulan="'.$bulan.'" and "'.$tahun.'"');
				}
				else{
					$data=array('tahun'=>$tahun,'bulan'=>$bulan,'kd_obat'=>$kd_obat,'kd_unit_apt'=>$kd_unit_apt,'saldo_awal'=>0,
								'out_unit'=>$qtydis,'harga_beli'=>$harga);
					$this->mlaporanapt->insert('apt_mutasi_obat',$data);
				}
			}
		}
		
		$retur_pbf=$this->mlaporanapt->getReturPbf1($bulan,$tahun);
		if(!empty($retur_pbf)){
			foreach($retur_pbf as $dist){
				$kd_obat=$dist['kd_obat'];
				$qtykcl=$dist['qty_kcl'];
				if($this->mlaporanapt->isObatExist($kd_obat,$bulan,$tahun)){
					$data=array('retur_pbf'=>$qtykcl);
					$this->mlaporanapt->update('apt_mutasi_obat',$data,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$kd_obat.'" and bulan="'.$bulan.'" and "'.$tahun.'"');
				}
				else{
					$data=array('tahun'=>$tahun,'bulan'=>$bulan,'kd_obat'=>$kd_obat,'kd_unit_apt'=>$kd_unit_apt,'saldo_awal'=>0,
								'retur_pbf'=>$qtykcl);
					$this->mlaporanapt->insert('apt_mutasi_obat',$data);
				}
			}
		}
		
		$retur_jual=$this->mlaporanapt->getReturJual1($bulan,$tahun);
		if(!empty($retur_jual)){
			foreach($retur_jual as $dist){
				$kd_obat=$dist['kd_obat'];
				$qtykcl=$dist['qty_kcl'];
				if($this->mlaporanapt->isObatExist($kd_obat,$bulan,$tahun)){
					$data=array('retur_jual'=>$qtykcl);
					$this->mlaporanapt->update('apt_mutasi_obat',$data,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$kd_obat.'" and bulan="'.$bulan.'" and "'.$tahun.'"');
				}
				else{
					$data=array('tahun'=>$tahun,'bulan'=>$bulan,'kd_obat'=>$kd_obat,'kd_unit_apt'=>$kd_unit_apt,'saldo_awal'=>0,
								'retur_jual'=>$qtykcl);
					$this->mlaporanapt->insert('apt_mutasi_obat',$data);
				}
			}
		}
		
		$stok_opname=$this->mlaporanapt->stokopname($bulan,$tahun);
		if(!empty($stok_opname)){
			foreach($stok_opname as $dist){
				$kd_obat=$dist['kd_obat'];
				$qty=$dist['qty'];
				$harga=$dist['harga_pokok'];
				if($this->mlaporanapt->isObatExist($kd_obat,$bulan,$tahun)){
					$data=array('harga_beli'=>$harga,'stok_opname'=>$qty);
					$this->mlaporanapt->update('apt_mutasi_obat',$data,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$kd_obat.'" and bulan="'.$bulan.'" and "'.$tahun.'"');
				}
				else{
					$data=array('tahun'=>$tahun,'bulan'=>$bulan,'kd_obat'=>$kd_obat,'kd_unit_apt'=>$kd_unit_apt,'saldo_awal'=>0,
								'stok_opname'=>$qty);
					$this->mlaporanapt->insert('apt_mutasi_obat',$data);
				}
			}
		}		
		
		$obat=$this->mlaporanapt->ambilObat($bulan,$tahun);
		if(!empty($obat)){ //kalo misalnya data bulan ini ada
			foreach($obat as $dist){
				$kd_obat=$dist['kd_obat'];
				$hargabeli=$dist['harga_beli'];				
				$saldo_awal=$this->mlaporanapt->saldoawal($b,$a1,$kd_obat); //ngambil saldo akhir bulan kemarin sebagai saldo awal bln yg dipilih
				if($saldo_awal=='-'){ //kalo saldo awal yg bulan kmrn ga ada
					$stok=$this->mlaporanapt->ambilstok($kd_obat);
					$sisdet=$this->mlaporanapt->ambilbulansisdet(); $selisihbulan=$sisdet-$bulan2;
					$sisdettahun=$this->mlaporanapt->ambiltahunsisdet(); $selisihtahun=$sisdettahun-$tahun;
					if($selisihtahun==0){ //kalo dy mutasinya tahun sisdet
						if($selisihbulan==0){
							$out_unita=$this->mlaporanapt->ambiloutunit($bulan,$tahun,$kd_obat);
							$out_juala=$this->mlaporanapt->ambiloutjual($bulan,$tahun,$kd_obat);
							$retur_pbfa=$this->mlaporanapt->ambilreturpebef($bulan,$tahun,$kd_obat);
							$returjuala=$this->mlaporanapt->ambilreturjual($bulan,$tahun,$kd_obat);
							$in_pbfa=$this->mlaporanapt->ambilinpebef($bulan,$tahun,$kd_obat);
							$in_unita=$this->mlaporanapt->ambilinunit($bulan,$tahun,$kd_obat);
							$stokop=$this->mlaporanapt->ambilstokop($bulan,$tahun,$kd_obat);
							$saldo_awal=$stok+$out_unita+$out_juala+$retur_pbfa-$stokop-$in_pbfa-$in_unita-$returjuala; //berlku kalo dy mutasinya bulan sysdate
							$saldoakhir=$stok;
							$data=array('saldo_awal'=>$saldo_awal,'saldo_akhir'=>$saldoakhir);
							$this->mlaporanapt->update('apt_mutasi_obat',$data,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$kd_obat.'" and bulan="'.$bulan.'" and "'.$tahun.'"');
						}else{
							$sldakhir1=0; $saldoawal=0; $sldakhir=0;
							for($zet=$sisdet;$zet>=$bulan2;$zet--){
								if(strlen($zet)==1){$zet1='0'.$zet;}
								else{$zet1=$zet;}
								$out_unita=$this->mlaporanapt->ambiloutunit($zet1,$tahun,$kd_obat);
								$out_juala=$this->mlaporanapt->ambiloutjual($zet1,$tahun,$kd_obat);
								$retur_pbfa=$this->mlaporanapt->ambilreturpebef($zet1,$tahun,$kd_obat);
								$returjuala=$this->mlaporanapt->ambilreturjual($zet1,$tahun,$kd_obat);
								$in_pbfa=$this->mlaporanapt->ambilinpebef($zet1,$tahun,$kd_obat);
								$in_unita=$this->mlaporanapt->ambilinunit($zet1,$tahun,$kd_obat);
								$stokop=$this->mlaporanapt->ambilstokop($zet1,$tahun,$kd_obat);
								
								if($zet1==$sisdet){$sldakhir1=$stok;}
								else{
									$jet=$zet+1;
									$sldakhir1=$this->mlaporanapt->ngambilsaldoakhir($jet,$tahun,$kd_obat);
									if($sldakhir1=='-'){$sldakhir1=$stok;}
									//else{$sldakhir==$sldakhir1;}
								}
								//if($kd_obat=='CRH13' && $zet1=='06'){debugvar($kd_obat.' saldoakhirnya '.$sldakhir);}
								//$sldakhir=$stok;								
								$saldoawal=$sldakhir1+$out_unita+$out_juala+$retur_pbfa-$stokop-$in_pbfa-$in_unita-$returjuala;
								//if($zet==7){debugvar($zet1.' out jualnya '.$out_juala);}
								$data=array('saldo_awal'=>$saldoawal,'saldo_akhir'=>$sldakhir1);
								$this->mlaporanapt->update('apt_mutasi_obat',$data,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$kd_obat.'" and bulan="'.$zet1.'" and "'.$tahun.'"');
								//$sldakhir=$saldoawal;
							}
						}
					}else{ //kalo mutasinya di tahun sebelumnya
						$sldakhir1=0; //$sldakhir=$stok; 
						for($wew=$sisdettahun;$wew>=$tahun;$wew--){ //ngambil semua2nya dari tahun sisdet
							if($wew==$tahun){ //klo hasil dari wew sama dengan tahun yg dipilih
								for($zet=12;$zet>=$bulan2;$zet--){
									if(strlen($zet)==1){$zet1='0'.$zet;}
									else{$zet1=$zet;}
									$out_unita=$this->mlaporanapt->ambiloutunit($zet1,$wew,$kd_obat);
									$out_juala=$this->mlaporanapt->ambiloutjual($zet1,$wew,$kd_obat);
									$retur_pbfa=$this->mlaporanapt->ambilreturpebef($zet1,$wew,$kd_obat);
									$returjuala=$this->mlaporanapt->ambilreturjual($zet1,$wew,$kd_obat);
									$in_pbfa=$this->mlaporanapt->ambilinpebef($zet1,$wew,$kd_obat);
									$in_unita=$this->mlaporanapt->ambilinunit($zet1,$wew,$kd_obat);
									$stokop=$this->mlaporanapt->ambilstokop($zet1,$wew,$kd_obat);
									
									if($zet1==12){
										//$sldakhir=$stok;
										$jet=1; $jut=$wew+1;
										$bulanmin=$this->mlaporanapt->ngambilbulanmin($jut,$kd_obat);
										if($bulanmin=='-'){$sldakhir=$stok;}
										else{
											$sldakhir1=$this->mlaporanapt->ngambilsaldoakhir($bulanmin,$jut,$kd_obat);
											if($sldakhir1=='-'){$sldakhir=$stok;}
											else{$sldakhir==$sldakhir1;}
										}
									}
									else{
										$jet=$zet+1;
										$sldakhir1=$this->mlaporanapt->ngambilsaldoakhir($jet,$wew,$kd_obat);
										if($sldakhir1=='-'){$sldakhir=$stok;}
										else{$sldakhir==$sldakhir1;}
									}
									
									$saldo_awal=$sldakhir+$out_unita+$out_juala+$retur_pbfa-$stokop-$in_pbfa-$in_unita-$returjuala;
									$data=array('saldo_awal'=>$saldo_awal,'saldo_akhir'=>$sldakhir);
									$this->mlaporanapt->update('apt_mutasi_obat',$data,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$kd_obat.'" and bulan="'.$zet1.'" and "'.$wew.'"');
									//$sldakhir=$saldo_awal;
								}
							}
							else{
								for($zet=$sisdet;$zet>=1;$zet--){
									if(strlen($zet)==1){$zet1='0'.$zet;}
									else{$zet1=$zet;}
									$out_unita=$this->mlaporanapt->ambiloutunit($zet1,$wew,$kd_obat);
									$out_juala=$this->mlaporanapt->ambiloutjual($zet1,$wew,$kd_obat);
									$retur_pbfa=$this->mlaporanapt->ambilreturpebef($zet1,$wew,$kd_obat);
									$returjuala=$this->mlaporanapt->ambilreturjual($zet1,$wew,$kd_obat);
									$in_pbfa=$this->mlaporanapt->ambilinpebef($zet1,$wew,$kd_obat);
									$in_unita=$this->mlaporanapt->ambilinunit($zet1,$wew,$kd_obat);
									$stokop=$this->mlaporanapt->ambilstokop($zet1,$wew,$kd_obat);
									
									if($zet1==$sisdet){$sldakhir=$stok;}									
									else{
										$jet=$zet+1;
										$sldakhir1=$this->mlaporanapt->ngambilsaldoakhir($jet,$tahun,$kd_obat);
										if($sldakhir1=='-'){$sldakhir=$stok;}
										else{$sldakhir==$sldakhir1;}
									}									
									$saldo_awal=$sldakhir+$out_unita+$out_juala+$retur_pbfa-$stokop-$in_pbfa-$in_unita-$returjuala;
									//$saldoakhir=$saldo_awal+$in_pbfa+$in_unita+$returjuala-$out_juala+$out_unita+$retur_pbfa+$stokop;
									$data=array('saldo_awal'=>$saldo_awal,'saldo_akhir'=>$sldakhir);
									$this->mlaporanapt->update('apt_mutasi_obat',$data,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$kd_obat.'" and bulan="'.$zet1.'" and "'.$wew.'"');
									//$sldakhir=$saldo_awal;
								}
							}
							$sldakhir=$sldakhir;
						}
					}
				}
				else { //kalo yg bln kmrn ada
					$stok=$this->mlaporanapt->ambilstok($kd_obat);
					$out_unita=$this->mlaporanapt->ambiloutunit($bulan,$tahun,$kd_obat);
					$out_juala=$this->mlaporanapt->ambiloutjual($bulan,$tahun,$kd_obat);
					$retur_pbfa=$this->mlaporanapt->ambilreturpebef($bulan,$tahun,$kd_obat);
					$in_pbfa=$this->mlaporanapt->ambilinpebef($bulan,$tahun,$kd_obat);
					$in_unita=$this->mlaporanapt->ambilinunit($bulan,$tahun,$kd_obat);
					$stokop=$this->mlaporanapt->ambilstokop($bulan,$tahun,$kd_obat);
					$returjuala=$this->mlaporanapt->ambilreturjual($bulan,$tahun,$kd_obat);
					//$returjual=0;
					//$saldoakhir=($saldo_awal+(($in_pbfa+$in_unita)-$retur_pbfa))-((($out_juala+$out_unita)-$returjuala)+$stokop);
					$saldoakhir=$saldo_awal+$in_pbfa+$in_unita+$returjuala-$out_juala-$out_unita-$retur_pbfa+$stokop;
					//$data=array('saldo_awal'=>$saldo_awal);
					$data=array('saldo_awal'=>$saldo_awal,'saldo_akhir'=>$saldoakhir);
					$this->mlaporanapt->update('apt_mutasi_obat',$data,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$kd_obat.'" and bulan="'.$bulan.'" and "'.$tahun.'"');
				}
			}
		}

		$msg['status']=1;
		$msg['kd_unit_apt']=$kd_unit_apt;
		$msg['bulan']=$bulan;
		$msg['tahun']=$tahun;
		$msg['pesan']="";
		$msg['cetak']=0;
		$msg['items']=$this->mlaporanapt->getMutasiObat($kd_unit_apt,$bulan,$tahun);
		if($submit=="cetak"){
			$msg['cetak']=1;
		}
		echo json_encode($msg);
	}

	public function carimutasiobat2(){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');		
		$bulan=$this->input->post('bulan');
		$bulanInt=0;
		$tahun=$this->input->post('tahun');
		$bulankemarin=0;
		$tahunkemarin=0;
		$submit=$this->input->post('submit');
		$items=array();
		$msg=array();
		
		switch ($bulan) {
			case '01':
				# code...
				$bulanInt=1;
				$bulankemarin=12;
				$tahunkemarin=$tahun-1;
				break;
			
			case '02':
				# code...
				$bulanInt=2;
				$bulankemarin='01';
				$tahunkemarin=$tahun;
				break;
			
			case '03':
				# code...
				$bulanInt=3;
				$bulankemarin='02';
				$tahunkemarin=$tahun;
				break;
			
			case '04':
				# code...
				$bulanInt=4;
				$bulankemarin='03';
				$tahunkemarin=$tahun;
				break;
			
			case '05':
				# code...
				$bulanInt=5;
				$bulankemarin='04';
				$tahunkemarin=$tahun;
				break;
			
			case '06':
				# code...
				$bulanInt=6;
				$bulankemarin='05';
				$tahunkemarin=$tahun;
				break;
			
			case '07':
				# code...
				$bulanInt=7;
				$bulankemarin='06';
				$tahunkemarin=$tahun;
				break;
			
			case '08':
				# code...
				$bulanInt=8;
				$bulankemarin='07';
				$tahunkemarin=$tahun;
				break;
			
			case '09':
				# code...
				$bulanInt=9;
				$bulankemarin='08';
				$tahunkemarin=$tahun;
				break;
			
			case '10':
				# code...
				$bulanInt=10;
				$bulankemarin='09';
				$tahunkemarin=$tahun;
				break;
			
			case '11':
				# code...
				$bulanInt=11;
				$bulankemarin='10';
				$tahunkemarin=$tahun;
				break;
			
			case '12':
				# code...
				$bulanInt=12;
				$bulankemarin='11';
				$tahunkemarin=$tahun;
				break;
			
			default:
				# code...
				debugvar('There is error, script will not work');
				break;
		}
		$items=$this->mlaporanapt->ambilDataObat();
		//$this->db->trans_start();
		foreach ($items as $item) {
			# code...
			$obatmutasi=$this->mlaporanapt->getMutasiPerObat($item['kd_obat'],$bulankemarin,$tahunkemarin); //stok awal bulan ini adalah stok akhir bulan kemarin
			if(empty($obatmutasi)){
				$saldo_awal=0;
			}else{
				$saldo_awal=$obatmutasi['saldo_akhir'];//mengambil saldo akhir bulan kemarin dan di letakkan di saldo awal bulan ini
			}

			//mengambil data penerimaan dari pbf selama satu bulan
			$in_pbf=$this->mlaporanapt->getPenerimaanObat($item['kd_obat'],$bulan,$tahun);

			//mengambil data penerimaan dari unit lain atau gudang selama satu bulan
			$in_unit=$this->mlaporanapt->getPenerimaanDistribusiObat($item['kd_obat'],$bulan,$tahun); //in_unit --->kd_unit_tujuan

			//mengambil data pengeluaran dari penjualan selama satu bulan
			$out_jual=$this->mlaporanapt->getPenjualanObat($item['kd_obat'],$bulan,$tahun);

			//mengambil data pengeluaran ke unit lain atau gudang selama satu bulan
			$out_unit=$this->mlaporanapt->getPengeluaranDistribusiObat($item['kd_obat'],$bulan,$tahun); //in_unit --->kd_unit_tujuan

			//mengambil data retur ke pbf selama satu bulan
			$retur_pbf=$this->mlaporanapt->getReturPbfObat($item['kd_obat'],$bulan,$tahun);

			//mengambil data retur penjualan selama satu bulan
			$retur_jual=$this->mlaporanapt->getReturJualObat($item['kd_obat'],$bulan,$tahun);

			//mengambil stok opname obat selama sebulan
			$stok_opname=$this->mlaporanapt->stokopnameObat($item['kd_obat'],$bulan,$tahun);		
			
			$saldo_akhir=$saldo_awal+$stok_opname+$in_pbf+$in_unit+$retur_jual-$out_jual-$out_unit-$retur_pbf;					
			$this->db->query('replace into apt_mutasi_obat set tahun="'.$tahun.'",  
								bulan="'.$bulan.'", 
								kd_obat="'.$item['kd_obat'].'",
								kd_unit_apt="'.$kd_unit_apt.'",  
								saldo_awal="'.$saldo_awal.'",  
								in_pbf="'.$in_pbf.'",  
								in_unit="'.$in_unit.'",  
								retur_jual="'.$retur_jual.'",  
								out_jual="'.$out_jual.'",  
								out_unit="'.$out_unit.'",  
								retur_pbf="'.$retur_pbf.'",  
								saldo_akhir="'.$saldo_akhir.'",  
								stok_opname="'.$stok_opname.'"  
								');
		}
		//$this->db->trans_complete();
										

		$msg['status']=1;
		$msg['kd_unit_apt']=$kd_unit_apt;
		$msg['bulan']=$bulan;
		$msg['tahun']=$tahun;
		$msg['pesan']="";
		$msg['cetak']=0;
		//$msg['items']=$this->mlaporanapt->getMutasiObat($kd_unit_apt,$bulan,$tahun);
		$msg['items']=array();
		if($submit=="cetak"){
			$msg['cetak']=1;
		}
		echo json_encode($msg);
	}

	public function expire(){
		if(!$this->muser->isAkses("71")){
			$this->restricted();
			return false;
		}
		
		$kd_unit_asal=$this->session->userdata('kd_unit_apt');
		$hari=90;
		if($this->input->post('hari')!=''){
			$hari=$this->input->post('hari');
		}

		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Expire Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array('items'=>$this->mlaporanapt->getExpire($hari,$kd_unit_asal),
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'hari'=>$hari,
					'kd_unit_asal'=>$kd_unit_asal);

		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/expire',$data);
		$this->load->view('footer',$datafooter);
	}

	public function tutuppenjualan(){
		if(!$this->muser->isAkses("81")){
			$this->restricted();
			return false;
		}
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		//$periodeawal=date('d-m-Y');
		//$periodeakhir=date('d-m-Y');
		$tglskrg=date('Y-m-d');
		$tglkmrn=$this->mlaporanapt->ambiltglkmrn($tglskrg);
		$periodeawal=convertDate($tglkmrn);
		$periodeakhir=convertDate($tglskrg);
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		
		if($this->input->post('shift')!=''){
			$shift=$this->input->post('shift');
		}
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		$data=array('kd_unit_apt'=>$kd_unit_apt,
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir);
		
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/tutuppenjualan',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function simpantutuppenjualan(){
		$msg=array();
		$submit=$this->input->post('submit');
		$periodeawal=$this->input->post('periodeawal');
		$periodeakhir=$this->input->post('periodeakhir');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');

		$this->db->trans_start();

		$status=1;
		$jam=$this->mlaporanapt->ambiljamsekarang(); //ngambil jam skrg
		$tgl=convertDate($periodeakhir)." ".$jam; //gabungin tgl periodeakhir sama jam skrg
		$data=array('status'=>$status,
				    'tgl_tutup'=>$tgl);
		$this->mlaporanapt->update("apt_penjualan",$data,"date_format(tgl_penjualan,'%Y-%m-%d') between '".convertDate($periodeawal)."' and '".convertDate($periodeakhir)."' and kd_unit_apt='".$kd_unit_apt."' and status is null or status='0'");
		
		$this->db->trans_complete();
		$msg['pesan']="Data Berhasil Di Update";
		$msg['status']=1;
		$msg['keluar']=0;
			
		echo json_encode($msg);
	}
	
	public function periksatutuppenjualan(){
		$msg=array();
		//$submit=$this->input->post('submit');		
		$jumlaherror=0;
		$msg['status']=1;
		$msg['clearform']=0;
		$msg['pesanatas']="";
		$msg['pesanlain']="";		
		/*if($jumlaherror>0){
			$msg['status']=0;
			$msg['error']=$jumlaherror;
			$msg['pesanatas']="Terdapat beberapa kesalahan input silahkan cek inputan anda";
		}*/
		echo json_encode($msg);
	}
	
	public function rekappenjualan(){
		if(!$this->muser->isAkses("79")){
			$this->restricted();
			return false;
		}
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$is_lunas='';
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		$resep='';
		$status='';
		$kd_jenis_bayar='';
		$cust_code='';
		$shiftapt='';
		$tipe='';
		$jenis_pasien='';

		if($this->input->post('tipe')!=''){
			$tipe=$this->input->post('tipe');
		}
		if($this->input->post('jenis_pasien')!=''){
			$jenis_pasien=$this->input->post('jenis_pasien');
		}

		if($this->input->post('shiftapt')!=''){
			$shiftapt=$this->input->post('shiftapt');
		}		
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('is_lunas')!=''){
			$is_lunas=$this->input->post('is_lunas');
		}
		if($this->input->post('resep')!=''){
			$resep=$this->input->post('resep');
		}
		if($this->input->post('status')!=''){
			$status=$this->input->post('status');
		}
		if($this->input->post('kd_jenis_bayar')!=''){
			$kd_jenis_bayar=$this->input->post('kd_jenis_bayar');
		}
		if($this->input->post('cust_code')!=''){
			$cust_code=$this->input->post('cust_code');
		}
		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);
		
		$data=array('items'=>$this->mlaporanapt->getRekapPenjualanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$is_lunas,$resep,$status,$kd_jenis_bayar,$cust_code,$shiftapt,$jenis_pasien,$tipe),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'is_lunas'=>$is_lunas,
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'kd_unit_apt'=>$kd_unit_apt,
					'resep'=>$resep,
					'status'=>$status,
					'kd_jenis_bayar'=>$kd_jenis_bayar,
					'cust_code'=>$cust_code,
					'tipe'=>$tipe,
					'jenis_pasien'=>$jenis_pasien,
					'shiftapt'=>$shiftapt,
					'datashift'=>$this->mlaporanapt->ambilItemData('unit_shift','kd_unit="APT" '),
					'jenisbayar'=>$this->mlaporanapt->ambilData("apt_jenis_bayar","jenis_bayar<>'Transfer'"),
					'datacustomer'=>$this->mlaporanapt->ambilData('apt_customers'),
					'jenispasien'=>$this->mlaporanapt->ambilData("apt_customers",'type!=0'));

		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/rekappenjualan',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function rekapnopenjualan(){
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		$status='';
		$is_lunas='';
		$resep='';
		
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('status')!=''){
			$status=$this->input->post('status');
		}
		if($this->input->post('is_lunas')!=''){
			$is_lunas=$this->input->post('is_lunas');
		}	
		if($this->input->post('resep')!=''){
			$resep=$this->input->post('resep');
		}
		
		$cssfileheader=array('bootstrap.min.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);
		
		$data=array('items'=>$this->mlaporanapt->getRekapNoPenjualan($periodeawal,$periodeakhir,$kd_unit_apt,$status,$is_lunas,$resep),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'kd_unit_apt'=>$kd_unit_apt,
					'status'=>$status,
					'is_lunas'=>$is_lunas,
					'resep'=>$resep,
			);

		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/rekapnopenjualan',$data);
		$this->load->view('footer',$datafooter);
	}

	public function penjualanobat(){
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$kd_unit_apt='';
		$kd_obat='';
		$nama_obat='';
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}
		if($this->input->post('kd_obat')!=''){
			$kd_obat=$this->input->post('kd_obat');
		}
		if($this->input->post('nama_obat')!=''){
			$nama_obat=$this->input->post('nama_obat');
		}
		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);
		
		$data=array('items'=>$this->mlaporanapt->QueryPenjualanObat($periodeawal,$periodeakhir,$kd_unit_apt,$kd_obat),
					'datashift'=>$this->mlaporanapt->ambilItemData('unit_shift','kd_unit="APT" '),
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'kd_unit_apt'=>$kd_unit_apt,
					'kd_obat'=>$kd_obat,
					'nama_obat'=>$nama_obat,
			);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/penjualanobat',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function ambilitem(){
		$q=$this->input->get('query');
		$items=$this->mlaporanapt->tes($q);
		echo json_encode($items);
	}
	
	public function ambilitems(){
		$awal=$this->input->get('awal');
		$akhir=$this->input->get('akhir');
		$unit=$this->input->get('unit');
		$q=$this->input->get('query');
		$items=$this->mlaporanapt->detilnyaPenjualanobat($awal,$akhir,$unit,$q);
		echo json_encode($items);
	}
	
	public function rl1excel($periodeawal="",$periodeakhir="",$kd_unit_apt="",$kd_obat=""){ //bwt yg penjualan obat
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:G3');
		$objPHPExcel->getActiveSheet()->mergeCells('A4:G4');
		$objPHPExcel->getActiveSheet()->mergeCells('A5:G5');
		/*$objPHPExcel->getActiveSheet()->mergeCells('C7:D7');
		$objPHPExcel->getActiveSheet()->mergeCells('E7:F7');
		$objPHPExcel->getActiveSheet()->mergeCells('H7:L7');
		$objPHPExcel->getActiveSheet()->mergeCells('M7:O7');*/

		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(4.14); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(11.2); //TGL JUAL
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(16); //NO JUAL
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(9.5); //KODE
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(25); //NAMA
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(20); //UNIT
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(5.3); //QTY
		
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'5')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		$objPHPExcel->getActiveSheet()->setCellValue ('A2','LAPORAN PENJUALAN OBAT');
		$objPHPExcel->getActiveSheet()->setCellValue ('A3','RUMAH SAKIT IBNU SINA BALIKPAPAN ');
		
		if($kd_unit_apt==''){ $objPHPExcel->getActiveSheet()->setCellValue ('A4','Unit : Semua Unit Apotek');}
		else{
			$namaunit=$this->mlaporanapt->namaUnit($kd_unit_apt);
			$objPHPExcel->getActiveSheet()->setCellValue ('A4','Unit : '.$namaunit);
		}
		if($periodeawal!='' and $periodeakhir!=''){
			$tglawal=substr($periodeawal,0,2);		$tglakhir=substr($periodeakhir,0,2);
			$blnawal=substr($periodeawal,3,2);		$blnakhir=substr($periodeakhir,3,2);
			$thnawal=substr($periodeawal,6,10);		$thnakhir=substr($periodeakhir,6,10);
			
			if($blnawal=='01'){$blnawal1='Januari';} if($blnawal=='02'){$blnawal1='Februari';}	if($blnawal=='03'){$blnawal1='Maret';} if($blnawal=='04'){$blnawal1='April';}
			if($blnawal=='05'){$blnawal1='Mei';} if($blnawal=='06'){$blnawal1='Juni';} if($blnawal=='07'){$blnawal1='Juli';} if($blnawal=='08'){$blnawal1='Agustus';}
			if($blnawal=='09'){$blnawal1='September';} if($blnawal=='10'){$blnawal1='Oktober';}	if($blnawal=='11'){$blnawal1='November';} if($blnawal=='12'){$blnawal1='Desember';}
			
			if($blnakhir=='01'){$blnakhir1='Januari';} if($blnakhir=='02'){$blnakhir1='Februari';} if($blnakhir=='03'){$blnakhir1='Maret';}	if($blnakhir=='04'){$blnakhir1='April';}
			if($blnakhir=='05'){$blnakhir1='Mei';} if($blnakhir=='06'){$blnakhir1='Juni';} if($blnakhir=='07'){$blnakhir1='Juli';} if($blnakhir=='08'){$blnakhir1='Agustus';}
			if($blnakhir=='09'){$blnakhir1='September';} if($blnakhir=='10'){$blnakhir1='Oktober';} if($blnakhir=='11'){$blnakhir1='November';} if($blnakhir=='12'){$blnakhir1='Desember';}			
			$objPHPExcel->getActiveSheet()->setCellValue ('A5','Periode : '.$tglawal.' '.$blnawal1.' '.$thnawal.' s/d '.$tglakhir.' '.$blnakhir1.' '.$thnakhir);
		}

		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A7','NO.');
		$objPHPExcel->getActiveSheet()->setCellValue ('B7','TGL');
		$objPHPExcel->getActiveSheet()->setCellValue ('C7','NO.PENJUALAN');
		$objPHPExcel->getActiveSheet()->setCellValue ('D7','KODE');
		$objPHPExcel->getActiveSheet()->setCellValue ('E7','NAMA OBAT');
		$objPHPExcel->getActiveSheet()->setCellValue ('F7','UNIT APOTEK');
		$objPHPExcel->getActiveSheet()->setCellValue ('G7','QTY');
		$items=array();
		$items=$this->mlaporanapt->queryexcel($periodeawal,$periodeakhir,$kd_unit_apt,$kd_obat);
		$baris=8;
		$nomor=1;
		$total=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='G';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>12,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			//$objPHPExcel->getActiveSheet()->mergeCells('C'.$baris.':D'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['tgl_penjualan']);
			//$objPHPExcel->getActiveSheet()->mergeCells('E'.$baris.':F'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['no_penjualan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['kd_obat']);
			//$objPHPExcel->getActiveSheet()->mergeCells('H'.$baris.':L'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['nama_obat']);			
			//$objPHPExcel->getActiveSheet()->mergeCells('M'.$baris.':O'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['nama_unit_apt']);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['qty']);
			$nomor=$nomor+1; $baris=$baris+1;
		}	
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/laporanpenjualanobat.xls");
		header("Location: ".base_url()."download/laporanpenjualanobat.xls");
	}

	public function hnahargajualxls($periodeawal="",$periodeakhir="",$kd_unit_apt="",$tipe="",$jenis_pasien=""){ //bwt yg penjualan obat
		if($kd_unit_apt=="NULL")$kd_unit_apt="";
		if($jenis_pasien=="NULL")$jenis_pasien="";
		if($tipe=="NULL")$tipe="";
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:H3');
		$objPHPExcel->getActiveSheet()->mergeCells('A4:H4');
		$objPHPExcel->getActiveSheet()->mergeCells('A5:H5');
		/*$objPHPExcel->getActiveSheet()->mergeCells('C7:D7');
		$objPHPExcel->getActiveSheet()->mergeCells('E7:F7');
		$objPHPExcel->getActiveSheet()->mergeCells('H7:L7');
		$objPHPExcel->getActiveSheet()->mergeCells('M7:O7');*/

		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(4.14); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(11); //TGL JUAL
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(25); //NO JUAL
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(8); //KODE
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(15); //NAMA
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(15); //UNIT
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(15); //QTY
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(15); //QTY
		
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'5')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		$objPHPExcel->getActiveSheet()->setCellValue ('A2','LAPORAN MARGIN');
		$objPHPExcel->getActiveSheet()->setCellValue ('A3','RUMAH SAKIT IBNU SINA BALIKPAPAN ');
		
		if($kd_unit_apt==''){ $objPHPExcel->getActiveSheet()->setCellValue ('A4','Unit : Semua Unit Apotek');}
		else{
			$namaunit=$this->mlaporanapt->namaUnit($kd_unit_apt);
			$objPHPExcel->getActiveSheet()->setCellValue ('A4','Unit : '.$namaunit);
		}
		if($periodeawal!='' and $periodeakhir!=''){
			$tglawal=substr($periodeawal,0,2);		$tglakhir=substr($periodeakhir,0,2);
			$blnawal=substr($periodeawal,3,2);		$blnakhir=substr($periodeakhir,3,2);
			$thnawal=substr($periodeawal,6,10);		$thnakhir=substr($periodeakhir,6,10);
			
			if($blnawal=='01'){$blnawal1='Januari';} if($blnawal=='02'){$blnawal1='Februari';}	if($blnawal=='03'){$blnawal1='Maret';} if($blnawal=='04'){$blnawal1='April';}
			if($blnawal=='05'){$blnawal1='Mei';} if($blnawal=='06'){$blnawal1='Juni';} if($blnawal=='07'){$blnawal1='Juli';} if($blnawal=='08'){$blnawal1='Agustus';}
			if($blnawal=='09'){$blnawal1='September';} if($blnawal=='10'){$blnawal1='Oktober';}	if($blnawal=='11'){$blnawal1='November';} if($blnawal=='12'){$blnawal1='Desember';}
			
			if($blnakhir=='01'){$blnakhir1='Januari';} if($blnakhir=='02'){$blnakhir1='Februari';} if($blnakhir=='03'){$blnakhir1='Maret';}	if($blnakhir=='04'){$blnakhir1='April';}
			if($blnakhir=='05'){$blnakhir1='Mei';} if($blnakhir=='06'){$blnakhir1='Juni';} if($blnakhir=='07'){$blnakhir1='Juli';} if($blnakhir=='08'){$blnakhir1='Agustus';}
			if($blnakhir=='09'){$blnakhir1='September';} if($blnakhir=='10'){$blnakhir1='Oktober';} if($blnakhir=='11'){$blnakhir1='November';} if($blnakhir=='12'){$blnakhir1='Desember';}			
			$objPHPExcel->getActiveSheet()->setCellValue ('A5','Periode : '.$tglawal.' '.$blnawal1.' '.$thnawal.' s/d '.$tglakhir.' '.$blnakhir1.' '.$thnakhir);
		}

		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A7','NO.');
		$objPHPExcel->getActiveSheet()->setCellValue ('B7','KODE');
		$objPHPExcel->getActiveSheet()->setCellValue ('C7','NAMA OBAT');
		$objPHPExcel->getActiveSheet()->setCellValue ('D7','JUMLAH');
		$objPHPExcel->getActiveSheet()->setCellValue ('E7','HARGA BELI');
		$objPHPExcel->getActiveSheet()->setCellValue ('F7','TOTAL HARGA BELI');
		$objPHPExcel->getActiveSheet()->setCellValue ('G7','TOTAL HARGA JUAL');
		$objPHPExcel->getActiveSheet()->setCellValue ('H7','MARGIN');
		$items=array();
		$items=$this->mlaporanapt->getAllHNAPPN($periodeawal,$periodeakhir,$kd_unit_apt,$jenis_pasien,$tipe);
		$baris=8;
		$nomor=1;
		$total=0;
		$totalqty=0;
		$totalhargabeli=0;
		$totalhargajual=0;
		$totalmargin=0;

		foreach ($items as $item) {
            $totalqty=$totalqty+$item['qty'];
            $totalhargabeli=$totalhargabeli+($item['qty']*$item['harga_beli']);
            $totalhargajual=$totalhargajual+$item['total_harga_jual'];
            $totalmargin=$totalmargin+($item['total_harga_jual']-($item['qty']*$item['harga_beli']));
			# code...
			for($x='A';$x<='H';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>12,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['kd_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['nama_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['qty']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['harga_beli']);			
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['qty']*$item['harga_beli']);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['total_harga_jual']);
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$item['total_harga_jual']-($item['qty']*$item['harga_beli']));
			$nomor=$nomor+1; $baris=$baris+1;
		}	
			# code...
			for($x='A';$x<='H';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>12,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'');
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'T O T A L');
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$totalqty);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,'');			
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$totalhargabeli);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$totalhargajual);
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$totalmargin);

		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/laporanmargin.xls");
		header("Location: ".base_url()."download/laporanmargin.xls");
	}

	public function laporanpemakaianobat($bulan="",$tahun="",$kd_unit_apt="",$tipe="",$jenis_pasien=""){ //bwt yg penjualan obat
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel = IOFactory::load('download/templatepemakaianobat.xls');
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		/*$objPHPExcel->getActiveSheet()->mergeCells('C7:D7');
		$objPHPExcel->getActiveSheet()->mergeCells('E7:F7');
		$objPHPExcel->getActiveSheet()->mergeCells('H7:L7');
		$objPHPExcel->getActiveSheet()->mergeCells('M7:O7');*/

/*		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(4.14); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(11.2); //TGL JUAL
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(16); //NO JUAL
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(9.5); //KODE
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(25); //NAMA
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(20); //UNIT
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(5.3); //QTY
*/		
		if(!empty($jenis_pasien)){
			$datacustomer=$this->mlaporanapt->ambilItemData('apt_customers','cust_code="'.$jenis_pasien.'" ');
		}else{
			$datacustomer['customer']=" ";
		}

		$objPHPExcel->getActiveSheet()->setCellValue ('A6','LAPORAN PEMAKAIAN OBAT-OBATAN DAN JASA MEDIS '.strtoupper($datacustomer['customer']));
		$bln=array('01'=>'Januari',
					'02'=>'Februari',
					'03'=>'Maret',
					'04'=>'April',
					'05'=>'Mei',
					'06'=>'Juni',
					'07'=>'Juli',
					'08'=>'Agustus',
					'09'=>'September',
					'10'=>'Oktober',
					'11'=>'November',
					'12'=>'Desember'
				);
		$objPHPExcel->getActiveSheet()->setCellValue ('A7','BULAN '.$bln[$bulan].' '.$tahun);
		
		$items=array();
		$items=$this->mlaporanapt->getAllPenjualanApotekNonTunai($bulan,$tahun,$kd_unit_apt,$jenis_pasien,$tipe);
		$baris=11;
		$nomor=1;

		$totalobat=0;
		$totalup=0;
		$totalsetelahup=0;
		$totalservice=0;
		$totalbiayaobat=0;
		$totaladmjasa=0;
		$totalobatjasa=0;
		$totaljasamedis=0;
		$totaladmkartu=0;

		foreach ($items as $item) {
			# code...
			$up=(10/100)*$item['obat'];
			for($x='A';$x<='N';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else{
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>12,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['tgl_penjualan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['nama_pasien']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['no_peserta'].'/'.$item['nama_peserta']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['obat']);			
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$up);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['obat']+$up);
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$item['resep']+$item['adm_racik']);
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$item['obat']+$up+$item['resep']+$item['adm_racik']);
			$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,$item['jasa_medis']+$item['biaya_adm']+$item['biaya_kartu']);
			$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,$item['obat']+$up+$item['resep']+$item['adm_racik']+$item['jasa_medis']+$item['biaya_adm']+$item['biaya_kartu']);
			$objPHPExcel->getActiveSheet()->setCellValue ('L'.$baris,$item['keterangan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('M'.$baris,$item['jasa_medis']);
			$objPHPExcel->getActiveSheet()->setCellValue ('N'.$baris,$item['biaya_adm']+$item['biaya_kartu']);

			$totalobat=$totalobat+$item['obat'];
			$totalup=$totalup+$up;
			$totalsetelahup=$totalsetelahup+$item['obat']+$up;
			$totalservice=$totalservice+$item['resep']+$item['adm_racik'];
			$totalbiayaobat=$totalbiayaobat+$item['obat']+$up+$item['resep']+$item['adm_racik'];
			$totaladmjasa=$totaladmjasa+$item['jasa_medis']+$item['biaya_adm']+$item['biaya_kartu'];
			$totalobatjasa=$totalobatjasa+$item['obat']+$up+$item['resep']+$item['adm_racik']+$item['jasa_medis']+$item['biaya_adm']+$item['biaya_kartu'];
			$totaljasamedis=$totaljasamedis+$item['jasa_medis'];
			$totaladmkartu=$totaladmkartu+$item['biaya_adm']+$item['biaya_kartu'];

			$nomor=$nomor+1; $baris=$baris+1;
		}	

			for($x='A';$x<='N';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else{
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>12,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':D'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'TOTAL BIAYA');			
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,"=SUM(E11:E".($baris-1).")");			
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,"=SUM(F11:F".($baris-1).")");
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,"=SUM(G11:G".($baris-1).")");
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,"=SUM(H11:H".($baris-1).")");
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,"=SUM(I11:I".($baris-1).")");
			$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,"=SUM(J11:J".($baris-1).")");
			$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,"=SUM(K11:K".($baris-1).")");
			$objPHPExcel->getActiveSheet()->setCellValue ('L'.$baris,'');
			$objPHPExcel->getActiveSheet()->setCellValue ('M'.$baris,"=SUM(M11:M".($baris-1).")");
			$objPHPExcel->getActiveSheet()->setCellValue ('N'.$baris,"=SUM(N11:N".($baris-1).")");

			$baris=$baris+2;
			for($x='A';$x<='N';$x++){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
			
			}
			$objPHPExcel->getActiveSheet()->mergeCells('J'.$baris.':L'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,'Balikpapan, '.date('d F Y'));			
			$baris=$baris+1;
			for($x='A';$x<='N';$x++){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
			
			}
			$objPHPExcel->getActiveSheet()->mergeCells('D'.$baris.':G'.$baris);
			$objPHPExcel->getActiveSheet()->mergeCells('J'.$baris.':L'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'Dibuat,');			
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,'Diketahui,');			
			$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,'Diketahui,');			

			$baris=$baris+5;
			for($x='A';$x<='N';$x++){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
			
			}
			$objPHPExcel->getActiveSheet()->mergeCells('D'.$baris.':G'.$baris);
			$objPHPExcel->getActiveSheet()->mergeCells('J'.$baris.':L'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'Budi Sulistiowati');
			$apoteker=$this->mlaporanapt->ambilItemData('pegawai','jenis_pegawai=4');			
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$apoteker['nama_pegawai']);			
			$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,'M. Fakhrudin Yusuf, S. Farm, Apt');			

			$baris=$baris+1;
			for($x='A';$x<='N';$x++){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
			}
			$objPHPExcel->getActiveSheet()->mergeCells('D'.$baris.':G'.$baris);
			$objPHPExcel->getActiveSheet()->mergeCells('J'.$baris.':L'.$baris);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'Adm Apotek');			
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,'Apoteker');			
			$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,'Ka. Bidang Penunjang Medik');			

		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/laporanpemakaianobat.xls");
		header("Location: ".base_url()."download/laporanpemakaianobat.xls");
	}
	
	public function rl1exceldistribusi($periodeawal="",$periodeakhir="",$kd_unit_asal="",$kd_unit_tujuan=""){
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A2:I2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:I3');
		$objPHPExcel->getActiveSheet()->mergeCells('A4:I4');
		$objPHPExcel->getActiveSheet()->mergeCells('A6:I6');
		$objPHPExcel->getActiveSheet()->mergeCells('A7:I7');
		$objPHPExcel->getActiveSheet()->mergeCells('A8:I8');
		
		/*$objPHPExcel->getActiveSheet()->mergeCells('B10:C10');
		$objPHPExcel->getActiveSheet()->mergeCells('D10:E10');
		$objPHPExcel->getActiveSheet()->mergeCells('F10:H10');
		$objPHPExcel->getActiveSheet()->mergeCells('J10:L10');*/
		
		/*$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(2.14); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(6.34); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(8); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(10); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(10); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(9)->setWidth(15); //KODE
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(10)->setWidth(23); //NAMA OBAT
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(11)->setWidth(23); //NAMA OBAT
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(12)->setWidth(15); //NAMA OBAT
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(13)->setWidth(15); //SATUAN
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(14)->setWidth(15); //TGL EXPIRE
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(15)->setWidth(12); //QTY*/
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(4.14); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(15); //NODISTRIBUSI
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(20); //UNIT APOTEK
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(10); //KODE OBAT
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(26); //NAMA OBAT
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(10); //SATUAN
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(10); //TGL EXPIRE
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(6); //QTY
		
		for($x='A';$x<='I';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='I';$x++){
					$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
				}
		for($x='A';$x<='I';$x++){
					$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
				}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A2','RUMAH SAKIT ');
		$objPHPExcel->getActiveSheet()->setCellValue ('A3','IBNU SINA');
		$objPHPExcel->getActiveSheet()->setCellValue ('A4','Kota Balikpapan');		
		$objPHPExcel->getActiveSheet()->setCellValue ('A6','LAPORAN DETAIL DISTRIBUSI OBAT');
		
		if($kd_unit_asal==''){ $objPHPExcel->getActiveSheet()->setCellValue ('A7','Unit Asal : Semua Unit Apotek');}
		else{
			$namaunit=$this->mlaporanapt->namaUnit($kd_unit_asal);
			$objPHPExcel->getActiveSheet()->setCellValue ('A7','Unit Asal : '.$namaunit);
		}
		
		if($periodeawal!='' and $periodeakhir!=''){
			$tglawal=substr($periodeawal,0,2);		$tglakhir=substr($periodeakhir,0,2);
			$blnawal=substr($periodeawal,3,2);		$blnakhir=substr($periodeakhir,3,2);
			$thnawal=substr($periodeawal,6,10);		$thnakhir=substr($periodeakhir,6,10);
			
			if($blnawal=='01'){$blnawal1='Januari';} if($blnawal=='02'){$blnawal1='Februari';}	if($blnawal=='03'){$blnawal1='Maret';} if($blnawal=='04'){$blnawal1='April';}
			if($blnawal=='05'){$blnawal1='Mei';} if($blnawal=='06'){$blnawal1='Juni';} if($blnawal=='07'){$blnawal1='Juli';} if($blnawal=='08'){$blnawal1='Agustus';}
			if($blnawal=='09'){$blnawal1='September';} if($blnawal=='10'){$blnawal1='Oktober';}	if($blnawal=='11'){$blnawal1='November';} if($blnawal=='12'){$blnawal1='Desember';}
			
			if($blnakhir=='01'){$blnakhir1='Januari';} if($blnakhir=='02'){$blnakhir1='Februari';} if($blnakhir=='03'){$blnakhir1='Maret';}	if($blnakhir=='04'){$blnakhir1='April';}
			if($blnakhir=='05'){$blnakhir1='Mei';} if($blnakhir=='06'){$blnakhir1='Juni';} if($blnakhir=='07'){$blnakhir1='Juli';} if($blnakhir=='08'){$blnakhir1='Agustus';}
			if($blnakhir=='09'){$blnakhir1='September';} if($blnakhir=='10'){$blnakhir1='Oktober';} if($blnakhir=='11'){$blnakhir1='November';} if($blnakhir=='12'){$blnakhir1='Desember';}			
			
			$objPHPExcel->getActiveSheet()->setCellValue ('A8','Periode : '.$tglawal.' '.$blnawal1.' '.$thnawal.' s/d '.$tglakhir.' '.$blnakhir1.' '.$thnakhir);
		}

		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A10','NO.');
		$objPHPExcel->getActiveSheet()->setCellValue ('B10','NO. DIST.');
		$objPHPExcel->getActiveSheet()->setCellValue ('C10','TGL. DIST.');
		$objPHPExcel->getActiveSheet()->setCellValue ('D10','UNIT');
		$objPHPExcel->getActiveSheet()->setCellValue ('E10','KODE');
		$objPHPExcel->getActiveSheet()->setCellValue ('F10','NAMA OBAT');
		//$objPHPExcel->getActiveSheet()->setCellValue ('G10','SATUAN');
		//$objPHPExcel->getActiveSheet()->setCellValue ('H10','TGL. EXPIRE');
		$objPHPExcel->getActiveSheet()->setCellValue ('G10','QTY');
		$objPHPExcel->getActiveSheet()->setCellValue ('H10','HARGA');
		$objPHPExcel->getActiveSheet()->setCellValue ('I10','TOTAL');
		$items=array();
		$items=$this->mlaporanapt->getAllDistribusiApotek($periodeawal,$periodeakhir,$kd_unit_asal,$kd_unit_tujuan);
		$baris=11;
		$nomor=1;
		$total=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='I';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
				else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='H'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='I'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['no_distribusi']);
			$tgl_distribusi=explode(" ", $item['tgl_distribusi']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$tgl_distribusi[0]);
			$kodetujuan=$item['kd_unit_tujuan'];
			$unittujuan=$this->mlaporanapt->namaUnit($kodetujuan);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$unittujuan);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['kd_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['nama_obat']);			
			//$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['satuan_kecil']);
			//$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,convertDate($item['tgl_expire']));
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['qty']);
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$item['harga']);
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$item['qty']*$item['harga']);
			$nomor=$nomor+1; $baris=$baris+1;
		}	
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/laporanpenjualanobat.xls");
		header("Location: ".base_url()."download/laporanpenjualanobat.xls");
	}
	
	public function rl1excelpenjualanobatbydokter($periodeawal="",$periodeakhir="",$kd_unit="",$kd_dokter=""){
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:G3');
		$objPHPExcel->getActiveSheet()->mergeCells('A4:G4');
		$objPHPExcel->getActiveSheet()->mergeCells('A6:G6');
		$objPHPExcel->getActiveSheet()->mergeCells('A7:G7');
		$objPHPExcel->getActiveSheet()->mergeCells('A8:G8');
		
		/*$objPHPExcel->getActiveSheet()->mergeCells('B10:C10');
		$objPHPExcel->getActiveSheet()->mergeCells('D10:E10');
		$objPHPExcel->getActiveSheet()->mergeCells('F10:H10');
		$objPHPExcel->getActiveSheet()->mergeCells('J10:L10');*/
		
		/*$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(2.14); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(6.34); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(8); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(10); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(10); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(9)->setWidth(15); //KODE
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(10)->setWidth(23); //NAMA OBAT
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(11)->setWidth(23); //NAMA OBAT
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(12)->setWidth(15); //NAMA OBAT
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(13)->setWidth(15); //SATUAN
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(14)->setWidth(15); //TGL EXPIRE
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(15)->setWidth(12); //QTY*/
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(4.14); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(30); //NODISTRIBUSI
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(35); //UNIT APOTEK
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(20); //KODE OBAT
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(10); //NAMA OBAT
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(10); //SATUAN
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(10); //TGL EXPIRE
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(10); //QTY
		
		for($x='A';$x<='G';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='G';$x++){
					$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
				}
		for($x='A';$x<='G';$x++){
					$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
				}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A2','RUMAH SAKIT ');
		$objPHPExcel->getActiveSheet()->setCellValue ('A3','IBNU SINA');
		$objPHPExcel->getActiveSheet()->setCellValue ('A4','Kota Balikpapan');		
		$objPHPExcel->getActiveSheet()->setCellValue ('A6','LAPORAN PERESEPAN OBAT OLEH DOKTER');
		
		if($kd_unit==''){ $objPHPExcel->getActiveSheet()->setCellValue ('A7','Unit : Semua Unit Apotek');}
		else{
			$namaunit=$this->mlaporanapt->namaUnit($kd_unit);
			$objPHPExcel->getActiveSheet()->setCellValue ('A7','Unit : '.$namaunit);
		}
		
		if($periodeawal!='' and $periodeakhir!=''){
			$tglawal=substr($periodeawal,0,2);		$tglakhir=substr($periodeakhir,0,2);
			$blnawal=substr($periodeawal,3,2);		$blnakhir=substr($periodeakhir,3,2);
			$thnawal=substr($periodeawal,6,10);		$thnakhir=substr($periodeakhir,6,10);
			
			if($blnawal=='01'){$blnawal1='Januari';} if($blnawal=='02'){$blnawal1='Februari';}	if($blnawal=='03'){$blnawal1='Maret';} if($blnawal=='04'){$blnawal1='April';}
			if($blnawal=='05'){$blnawal1='Mei';} if($blnawal=='06'){$blnawal1='Juni';} if($blnawal=='07'){$blnawal1='Juli';} if($blnawal=='08'){$blnawal1='Agustus';}
			if($blnawal=='09'){$blnawal1='September';} if($blnawal=='10'){$blnawal1='Oktober';}	if($blnawal=='11'){$blnawal1='November';} if($blnawal=='12'){$blnawal1='Desember';}
			
			if($blnakhir=='01'){$blnakhir1='Januari';} if($blnakhir=='02'){$blnakhir1='Februari';} if($blnakhir=='03'){$blnakhir1='Maret';}	if($blnakhir=='04'){$blnakhir1='April';}
			if($blnakhir=='05'){$blnakhir1='Mei';} if($blnakhir=='06'){$blnakhir1='Juni';} if($blnakhir=='07'){$blnakhir1='Juli';} if($blnakhir=='08'){$blnakhir1='Agustus';}
			if($blnakhir=='09'){$blnakhir1='September';} if($blnakhir=='10'){$blnakhir1='Oktober';} if($blnakhir=='11'){$blnakhir1='November';} if($blnakhir=='12'){$blnakhir1='Desember';}			
			
			$objPHPExcel->getActiveSheet()->setCellValue ('A8','Periode : '.$tglawal.' '.$blnawal1.' '.$thnawal.' s/d '.$tglakhir.' '.$blnakhir1.' '.$thnakhir);
		}

		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A10','NO.');
		$objPHPExcel->getActiveSheet()->setCellValue ('B10','Nama Dokter');
		$objPHPExcel->getActiveSheet()->setCellValue ('C10','Kode Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('D10','Nama Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('E10','Pabrik');
		$objPHPExcel->getActiveSheet()->setCellValue ('F10','Total');
		$objPHPExcel->getActiveSheet()->setCellValue ('G10','Total Harga');
		$items=array();
		$items=$this->mlaporanapt->getResepByDokter($periodeawal,$periodeakhir,$kd_unit,$kd_dokter);
		$baris=11;
		$nomor=1;
		$total=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='G';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
				else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['nama_dokter']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['kd_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['nama_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['nama_pabrik']);
			if($item['total']!=0){
				$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['total']);			
			}
			if($item['total']!=0){
				$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['total_harga']);			
			}
			$nomor=$nomor+1; $baris=$baris+1;
		}	
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/laporanpenjualanobat.xls");
		header("Location: ".base_url()."download/laporanpenjualanobat.xls");
	}

	public function rl1excelpenerimaan($periodeawal="",$periodeakhir="",$kd_unit_apt="",$kd_supplier="",$kd_pabrik=""){
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath('images/logo1.jpg');
		$objDrawing->setHeight(70);
		$objDrawing->setCoordinates('A2');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A6:I6');
		$objPHPExcel->getActiveSheet()->mergeCells('A7:I7');
		$objPHPExcel->getActiveSheet()->mergeCells('A8:I8');
		$objPHPExcel->getActiveSheet()->mergeCells('A9:I9');
		
		//$objPHPExcel->getActiveSheet()->mergeCells('G11:I11');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(4); //kosong
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(12); //no
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(37); //no terima
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(10);//nama
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(6); //qty
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(11); //harga
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(12); //DISC
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(11); //PPN
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(8)->setWidth(13); //TOTAL
		/*$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(18); //no faktur
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(12); //tgl_terima
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(12); //tgl_tempo
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(18); //nama
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(10)->setWidth(10); //qty
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(11)->setWidth(15); //harga
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(12)->setWidth(8); //disc
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(13)->setWidth(8); //ppn
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(14)->setWidth(15);  //total*/
		for($x='A';$x<='I';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'9')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
				
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
				
		$queryjudul=$this->db->query("select profil.nama_profil,profil.alamat_profil,kelurahan.kelurahan,profil.kota from profil,kelurahan 
								where profil.kd_kelurahan=kelurahan.kd_kelurahan");
		$judul=$queryjudul->row_array();
		
		$objPHPExcel->getActiveSheet()->setCellValue ('C2',$judul['nama_profil']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C3',$judul['alamat_profil'].' '.$judul['kelurahan']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C4','Kota '.$judul['kota']);		
		$objPHPExcel->getActiveSheet()->setCellValue ('C6','LAPORAN PENERIMAAN OBAT');
		
		if($kd_supplier==''){ $objPHPExcel->getActiveSheet()->setCellValue ('A7','Supplier : Semua Supplier');}
		else{
			$namasupplier=$this->mlaporanapt->namaSupplier($kd_supplier);
			$objPHPExcel->getActiveSheet()->setCellValue ('A7','Supplier : '.$namasupplier);
		}
		
		if($kd_pabrik==''){ $objPHPExcel->getActiveSheet()->setCellValue ('A8','Pabrik : Semua Pabrik');}
		else{
			$querypabrik=$this->db->query("select nama_pabrik from apt_pabrik where kd_pabrik='$kd_pabrik'");
			$pabrik=$querypabrik->row_array();
			$objPHPExcel->getActiveSheet()->setCellValue ('A8','Pabrik : '.$pabrik['nama_pabrik']);
		}
		
		$unit=$this->mlaporanapt->ambilNamaUnit($kd_unit_apt);
		$objPHPExcel->getActiveSheet()->setCellValue ('A9','Unit : '.$unit);
	
		if($periodeawal!='' and $periodeakhir!=''){
			$tglawal=substr($periodeawal,0,2);		$tglakhir=substr($periodeakhir,0,2);
			$blnawal=substr($periodeawal,3,2);		$blnakhir=substr($periodeakhir,3,2);
			$thnawal=substr($periodeawal,6,10);		$thnakhir=substr($periodeakhir,6,10);
			
			if($blnawal=='01'){$blnawal1='Januari';} if($blnawal=='02'){$blnawal1='Februari';}	if($blnawal=='03'){$blnawal1='Maret';} if($blnawal=='04'){$blnawal1='April';}
			if($blnawal=='05'){$blnawal1='Mei';} if($blnawal=='06'){$blnawal1='Juni';} if($blnawal=='07'){$blnawal1='Juli';} if($blnawal=='08'){$blnawal1='Agustus';}
			if($blnawal=='09'){$blnawal1='September';} if($blnawal=='10'){$blnawal1='Oktober';}	if($blnawal=='11'){$blnawal1='November';} if($blnawal=='12'){$blnawal1='Desember';}
			
			if($blnakhir=='01'){$blnakhir1='Januari';} if($blnakhir=='02'){$blnakhir1='Februari';} if($blnakhir=='03'){$blnakhir1='Maret';}	if($blnakhir=='04'){$blnakhir1='April';}
			if($blnakhir=='05'){$blnakhir1='Mei';} if($blnakhir=='06'){$blnakhir1='Juni';} if($blnakhir=='07'){$blnakhir1='Juli';} if($blnakhir=='08'){$blnakhir1='Agustus';}
			if($blnakhir=='09'){$blnakhir1='September';} if($blnakhir=='10'){$blnakhir1='Oktober';} if($blnakhir=='11'){$blnakhir1='November';} if($blnakhir=='12'){$blnakhir1='Desember';}			
			
			$objPHPExcel->getActiveSheet()->setCellValue ('A10','Periode : '.$tglawal.' '.$blnawal1.' '.$thnawal.' s/d '.$tglakhir.' '.$blnakhir1.' '.$thnakhir);
		}

		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);

		if($kd_supplier!=''){$a1=" and apt_penerimaan.kd_supplier='$kd_supplier'";}
		else{$a1="";} $a=$a1;

		if($kd_pabrik!=''){$b1=" and apt_obat.kd_pabrik='$kd_pabrik'";}
		else{$b1="";} $b=$b1;

		$queryatas=$this->db->query("select apt_penerimaan.no_penerimaan,date_format(apt_penerimaan.tgl_penerimaan,'%d-%m-%Y %H:%i:%s') as tgl_penerimaan,
								apt_penerimaan.tgl_tempo,apt_penerimaan.kd_supplier,apt_supplier.nama,apt_pabrik.nama_pabrik 
								from apt_penerimaan,apt_supplier,apt_penerimaan_detail,apt_obat left join apt_pabrik on apt_obat.kd_pabrik=apt_pabrik.kd_pabrik where apt_penerimaan.kd_supplier=apt_supplier.kd_supplier and
								date_format(apt_penerimaan.tgl_penerimaan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and apt_penerimaan.kd_unit_apt='$kd_unit_apt' 
								$a and apt_penerimaan_detail.no_penerimaan=apt_penerimaan.no_penerimaan and 
								apt_penerimaan_detail.kd_obat=apt_obat.kd_obat $b group by apt_penerimaan.no_penerimaan order by apt_penerimaan.no_penerimaan");
		$qatas=$queryatas->result_array();
		$row=12;
		$grandtotalakhir=0;
		$grandtotalppn=0;
		$grandtotaldiskon=0;
		foreach ($qatas as $atas) {
			# code...
			$nomor=$atas['no_penerimaan'];

			$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$row,'No Penerimaan ');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$row,": ".$atas['no_penerimaan']);
			
			$objPHPExcel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$row,'Tgl Penerimaan ');
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$row,": ".$atas['tgl_penerimaan']);

			if($kd_supplier==''){
				$objPHPExcel->getActiveSheet()->setCellValue ('H'.$row,'Supplier ');
				$objPHPExcel->getActiveSheet()->setCellValue ('I'.$row,": ".$atas['nama']);
			}

			
			$row=$row+1;

			if($kd_pabrik==''){
				$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
				$objPHPExcel->getActiveSheet()->setCellValue ('A'.$row,'Pabrik ');
				$objPHPExcel->getActiveSheet()->setCellValue ('C'.$row,": ".$atas['nama_pabrik']);
			}

			$objPHPExcel->getActiveSheet()->mergeCells('D'.$row.':E'.$row);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$row,'Tgl Jatuh Tempo ');
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$row,": ".convertDate($atas['tgl_tempo']));

			$row=$row+1;

			$querybawah=$this->db->query("select apt_penerimaan.no_penerimaan,apt_penerimaan.no_faktur,date_format(apt_penerimaan.tgl_penerimaan,'%Y-%m-%d') as tgl_penerimaan,apt_penerimaan.tgl_tempo,apt_penerimaan_detail.kd_obat,apt_obat.nama_obat,apt_satuan_kecil.satuan_kecil,
									apt_penerimaan_detail.qty_kcl,apt_penerimaan_detail.harga_beli,apt_penerimaan_detail.disc_prs,apt_penerimaan_detail.isidiskon,apt_penerimaan_detail.ppn_item
									from apt_penerimaan,apt_penerimaan_detail, apt_obat,apt_satuan_kecil,apt_supplier,apt_unit where
									apt_penerimaan.no_penerimaan=apt_penerimaan_detail.no_penerimaan 
									and apt_penerimaan.kd_supplier=apt_supplier.kd_supplier 
									and apt_penerimaan.kd_unit_apt=apt_unit.kd_unit_apt 
									and apt_penerimaan_detail.kd_obat=apt_obat.kd_obat 
									and apt_penerimaan_detail.kd_unit_apt=apt_unit.kd_unit_apt 
									and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil 
									and date(apt_penerimaan.tgl_penerimaan) between '$periodeawal1' and '$periodeakhir1' $a $b 
									and apt_unit.kd_unit_apt='$kd_unit_apt' 
									and apt_penerimaan.no_penerimaan='$nomor' order by apt_penerimaan.no_penerimaan asc
									");

			for($x='A';$x<='I';$x++){
				$objPHPExcel->getActiveSheet()->getStyle($x.$row)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$row)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$row,'NO.');
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$row,'KODE');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$row,'NAMA OBAT');
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$row,'SAT');
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$row,'QTY');
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$row,'HARGA');
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$row,'DISCOUNT');
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$row,'PPN');
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$row,'JUMLAH');

			$qbawah=$querybawah->result_array();
	
			$nobawah=1;
			$totalakhir=0;
			$totalppn=0;
			$totaldiskon=0;
			$row=$row+1;
			foreach ($qbawah as $bawah) {
			for($x='A';$x<='I';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$row)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$row)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$row)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$row)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$row)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$row)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$row)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='H'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$row)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='I'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$row)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}			
				$objPHPExcel->getActiveSheet()->getStyle($x.$row)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

				# code...
				$disc_prs=$bawah['disc_prs'];
				$ppn_item=$bawah['ppn_item'];
				$isidiskon=$bawah['isidiskon'];
				$nilaidiskon=0;
				$harga_beli=$bawah['harga_beli'];
				$qty_kcl=$bawah['qty_kcl'];
				if($disc_prs==0){
					$hargabelidiscount=0;
				}
				else{
					$hargabelidiscount=($disc_prs/100)*$harga_beli*$qty_kcl;																	
				}
				//nyari jumlah1
				if($hargabelidiscount!=0){ //kalo pake diskon persen
					$jumlah1=($qty_kcl*$harga_beli)-$hargabelidiscount;
					$nilaidiskon=$hargabelidiscount;
				}
				else if($isidiskon!=0){ //kalo pake diskon bukan persen
					$jumlah1=($qty_kcl*$harga_beli)-$isidiskon;
					$nilaidiskon=$isidiskon;
				}
				else{
					$jumlah1=($qty_kcl*$harga_beli);
				}
				
				//total
				$isippn=$jumlah1*($ppn_item/100);
				$total=$jumlah1+$isippn;

				$objPHPExcel->getActiveSheet()->setCellValue ('A'.$row,$nobawah);
				$objPHPExcel->getActiveSheet()->setCellValue ('B'.$row,$bawah['kd_obat']);
				$objPHPExcel->getActiveSheet()->setCellValue ('C'.$row,$bawah['nama_obat']);
				$objPHPExcel->getActiveSheet()->setCellValue ('D'.$row,$bawah['satuan_kecil']);
				$objPHPExcel->getActiveSheet()->setCellValue ('E'.$row,$bawah['qty_kcl']);
				//$objPHPExcel->getActiveSheet()->setCellValue ('F'.$row,number_format($bawah['harga_beli'],2,',','.'));
				$objPHPExcel->getActiveSheet()->setCellValue ('F'.$row,$bawah['harga_beli']);
				if($disc_prs!=0){
					//$objPHPExcel->getActiveSheet()->setCellValue ('G'.$row,number_format($hargabelidiscount,2,',','.'));
					$objPHPExcel->getActiveSheet()->setCellValue ('G'.$row,$hargabelidiscount);
				} 
				else if($isidiskon!=0){
					//$objPHPExcel->getActiveSheet()->setCellValue ('G'.$row,number_format($isidiskon,2,',','.'));
					$objPHPExcel->getActiveSheet()->setCellValue ('G'.$row,$isidiskon);
				}
				else{
					$objPHPExcel->getActiveSheet()->setCellValue ('G'.$row,'0');
				}			
				//$objPHPExcel->getActiveSheet()->setCellValue ('H'.$row,number_format($isippn,2,',','.'));
				$objPHPExcel->getActiveSheet()->setCellValue ('H'.$row,$isippn);
				//$objPHPExcel->getActiveSheet()->setCellValue ('I'.$row,number_format($total,2,',','.'));
				$objPHPExcel->getActiveSheet()->setCellValue ('I'.$row,$total);

				$nobawah++;
				$totalakhir=$totalakhir+$total;
				$totalppn=$totalppn+$isippn;
				$totaldiskon=$totaldiskon+$nilaidiskon;
				$row++;
			}

			for($x='A';$x<='I';$x++){
				$objPHPExcel->getActiveSheet()->getStyle($x.$row)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$row)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':F'.$row);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$row,"S U B T O T A L");
			//$objPHPExcel->getActiveSheet()->setCellValue ('G'.$row,number_format($totaldiskon,2,',','.'));
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$row,$totaldiskon);
			//$objPHPExcel->getActiveSheet()->setCellValue ('H'.$row,number_format($totalppn,2,',','.'));
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$row,$totalppn);
			//$objPHPExcel->getActiveSheet()->setCellValue ('I'.$row,number_format($totalakhir,2,',','.'));
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$row,$totalakhir);
			$row=$row+2;
			//$row=$row+4;
			$grandtotalakhir=$grandtotalakhir+$totalakhir;
			$grandtotalppn=$grandtotalppn+$totalppn;
			$grandtotaldiskon=$grandtotaldiskon+$totaldiskon;
		}
			$row=$row-1;
			for($x='A';$x<='I';$x++){
				$objPHPExcel->getActiveSheet()->getStyle($x.$row)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$row)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':F'.$row);
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$row,"T O T A L");
			//$objPHPExcel->getActiveSheet()->setCellValue ('G'.$row,number_format($grandtotaldiskon,2,',','.'));
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$row,$grandtotaldiskon);
			//$objPHPExcel->getActiveSheet()->setCellValue ('H'.$row,number_format($grandtotalppn,2,',','.'));
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$row,$grandtotalppn);
			//$objPHPExcel->getActiveSheet()->setCellValue ('I'.$row,number_format($grandtotalakhir,2,',','.'));
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$row,$grandtotalakhir);


		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/laporanpenerimaanobat.xls");
		header("Location: ".base_url()."download/laporanpenerimaanobat.xls");
	}	
	
	public function paretoexcel($periodeawal="",$periodeakhir="",$kd_unit_apt="",$kd_pabrik=""){
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath('images/logo1.jpg');
		$objDrawing->setHeight(70);
		$objDrawing->setCoordinates('A2');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);		
		$objPHPExcel->getActiveSheet()->getPageSetup()->setScale(90);		
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A6:G6');
		$objPHPExcel->getActiveSheet()->mergeCells('A7:G7');
		$objPHPExcel->getActiveSheet()->mergeCells('A8:G8');
		$objPHPExcel->getActiveSheet()->mergeCells('A9:G9');
		$objPHPExcel->getActiveSheet()->mergeCells('A10:G10');
		
		//$objPHPExcel->getActiveSheet()->mergeCells('G11:I11');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(4); //kosong
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(12); //no
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(30); //no terima
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(20);//nama
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(8); //qty
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(6); //harga
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(14); //harga
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(6); //harga

		for($x='A';$x<='I';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'9')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
				
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
				
		$queryjudul=$this->db->query("select profil.nama_profil,profil.alamat_profil,kelurahan.kelurahan,profil.kota from profil,kelurahan 
								where profil.kd_kelurahan=kelurahan.kd_kelurahan");
		$judul=$queryjudul->row_array();
		
		$objPHPExcel->getActiveSheet()->setCellValue ('C2',$judul['nama_profil']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C3',$judul['alamat_profil'].' '.$judul['kelurahan']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C4','Kota '.$judul['kota']);		
		$objPHPExcel->getActiveSheet()->setCellValue ('A7','LAPORAN PARETO');
				
		if($kd_pabrik==''){ $objPHPExcel->getActiveSheet()->setCellValue ('A8','Pabrik : Semua Pabrik');}
		else{
			$querypabrik=$this->db->query("select nama_pabrik from apt_pabrik where kd_pabrik='$kd_pabrik'");
			$pabrik=$querypabrik->row_array();
			$objPHPExcel->getActiveSheet()->setCellValue ('A8','Pabrik : '.$pabrik['nama_pabrik']);
		}
		
		$unit=$this->mlaporanapt->ambilNamaUnit($kd_unit_apt);
		$objPHPExcel->getActiveSheet()->setCellValue ('A9','Unit : '.$unit);
	
		if($periodeawal!='' and $periodeakhir!=''){
			$tglawal=substr($periodeawal,0,2);		$tglakhir=substr($periodeakhir,0,2);
			$blnawal=substr($periodeawal,3,2);		$blnakhir=substr($periodeakhir,3,2);
			$thnawal=substr($periodeawal,6,10);		$thnakhir=substr($periodeakhir,6,10);
			
			if($blnawal=='01'){$blnawal1='Januari';} if($blnawal=='02'){$blnawal1='Februari';}	if($blnawal=='03'){$blnawal1='Maret';} if($blnawal=='04'){$blnawal1='April';}
			if($blnawal=='05'){$blnawal1='Mei';} if($blnawal=='06'){$blnawal1='Juni';} if($blnawal=='07'){$blnawal1='Juli';} if($blnawal=='08'){$blnawal1='Agustus';}
			if($blnawal=='09'){$blnawal1='September';} if($blnawal=='10'){$blnawal1='Oktober';}	if($blnawal=='11'){$blnawal1='November';} if($blnawal=='12'){$blnawal1='Desember';}
			
			if($blnakhir=='01'){$blnakhir1='Januari';} if($blnakhir=='02'){$blnakhir1='Februari';} if($blnakhir=='03'){$blnakhir1='Maret';}	if($blnakhir=='04'){$blnakhir1='April';}
			if($blnakhir=='05'){$blnakhir1='Mei';} if($blnakhir=='06'){$blnakhir1='Juni';} if($blnakhir=='07'){$blnakhir1='Juli';} if($blnakhir=='08'){$blnakhir1='Agustus';}
			if($blnakhir=='09'){$blnakhir1='September';} if($blnakhir=='10'){$blnakhir1='Oktober';} if($blnakhir=='11'){$blnakhir1='November';} if($blnakhir=='12'){$blnakhir1='Desember';}			
			
			$objPHPExcel->getActiveSheet()->setCellValue ('A10','Periode : '.$tglawal.' '.$blnawal1.' '.$thnawal.' s/d '.$tglakhir.' '.$blnakhir1.' '.$thnakhir);
		}

		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'12')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'12')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}

		$objPHPExcel->getActiveSheet()->setCellValue ('A12','NO.');
		$objPHPExcel->getActiveSheet()->setCellValue ('B12','Kode Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('C12','Nama Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('D12','Pabrik');
		$objPHPExcel->getActiveSheet()->setCellValue ('E12','Satuan');
		$objPHPExcel->getActiveSheet()->setCellValue ('F12','Qty');
		$objPHPExcel->getActiveSheet()->setCellValue ('G12','Nominal');
		$objPHPExcel->getActiveSheet()->setCellValue ('H12','Persen');
		$objPHPExcel->getActiveSheet()->setCellValue ('I12','Persen(K)');
		$items=array();
		$items=$this->mlaporanapt->pareto($periodeawal,$periodeakhir,$kd_unit_apt,$kd_pabrik);
		$baris=13;
		$no=1;
		$totalqty=0;
		$totalnominal=0;
		$totalpersen=0;
		$persenk=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='I';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='H'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='I'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}
			$persenk=$persenk+$item['persen'];
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$no);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['kd_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['nama_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['nama_pabrik']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['satuan_kecil']);
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['qty_kcl']);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['nominal']);
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,number_format($item['persen'],2,',',''));
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,number_format($persenk,2,',',''));
			$totalqty=$totalqty+$item['qty_kcl'];
			$totalnominal=$totalnominal+$item['nominal'];
			$totalpersen=$totalpersen+$item['persen'];
			$baris++;
			$no++;
		}
			for($x='A';$x<='I';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='H'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='I'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'');
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'');
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,'');
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,'T O T A L');
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$totalqty);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$totalnominal);
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$totalpersen);
		//debugvar($status);

		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/laporanpareto.xls");
		header("Location: ".base_url()."download/laporanpareto.xls");
	}	

	public function paretoapotekexcel($periodeawal="",$periodeakhir="",$kd_unit_apt="",$kd_pabrik=""){
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath('images/logo1.jpg');
		$objDrawing->setHeight(70);
		$objDrawing->setCoordinates('A2');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);		
		$objPHPExcel->getActiveSheet()->getPageSetup()->setScale(90);		
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A6:G6');
		$objPHPExcel->getActiveSheet()->mergeCells('A7:G7');
		$objPHPExcel->getActiveSheet()->mergeCells('A8:G8');
		$objPHPExcel->getActiveSheet()->mergeCells('A9:G9');
		$objPHPExcel->getActiveSheet()->mergeCells('A10:G10');
		
		//$objPHPExcel->getActiveSheet()->mergeCells('G11:I11');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(4); //kosong
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(12); //no
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(30); //no terima
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(20);//nama
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(8); //qty
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(6); //harga
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(14); //harga
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(6); //harga

		for($x='A';$x<='I';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'9')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
				
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
				
		$queryjudul=$this->db->query("select profil.nama_profil,profil.alamat_profil,kelurahan.kelurahan,profil.kota from profil,kelurahan 
								where profil.kd_kelurahan=kelurahan.kd_kelurahan");
		$judul=$queryjudul->row_array();
		
		$objPHPExcel->getActiveSheet()->setCellValue ('C2',$judul['nama_profil']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C3',$judul['alamat_profil'].' '.$judul['kelurahan']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C4','Kota '.$judul['kota']);		
		$objPHPExcel->getActiveSheet()->setCellValue ('A7','LAPORAN PARETO');
				
		if($kd_pabrik==''){ $objPHPExcel->getActiveSheet()->setCellValue ('A8','Pabrik : Semua Pabrik');}
		else{
			$querypabrik=$this->db->query("select nama_pabrik from apt_pabrik where kd_pabrik='$kd_pabrik'");
			$pabrik=$querypabrik->row_array();
			$objPHPExcel->getActiveSheet()->setCellValue ('A8','Pabrik : '.$pabrik['nama_pabrik']);
		}
		
		$unit=$this->mlaporanapt->ambilNamaUnit($kd_unit_apt);
		$objPHPExcel->getActiveSheet()->setCellValue ('A9','Unit : '.$unit);
	
		if($periodeawal!='' and $periodeakhir!=''){
			$tglawal=substr($periodeawal,0,2);		$tglakhir=substr($periodeakhir,0,2);
			$blnawal=substr($periodeawal,3,2);		$blnakhir=substr($periodeakhir,3,2);
			$thnawal=substr($periodeawal,6,10);		$thnakhir=substr($periodeakhir,6,10);
			
			if($blnawal=='01'){$blnawal1='Januari';} if($blnawal=='02'){$blnawal1='Februari';}	if($blnawal=='03'){$blnawal1='Maret';} if($blnawal=='04'){$blnawal1='April';}
			if($blnawal=='05'){$blnawal1='Mei';} if($blnawal=='06'){$blnawal1='Juni';} if($blnawal=='07'){$blnawal1='Juli';} if($blnawal=='08'){$blnawal1='Agustus';}
			if($blnawal=='09'){$blnawal1='September';} if($blnawal=='10'){$blnawal1='Oktober';}	if($blnawal=='11'){$blnawal1='November';} if($blnawal=='12'){$blnawal1='Desember';}
			
			if($blnakhir=='01'){$blnakhir1='Januari';} if($blnakhir=='02'){$blnakhir1='Februari';} if($blnakhir=='03'){$blnakhir1='Maret';}	if($blnakhir=='04'){$blnakhir1='April';}
			if($blnakhir=='05'){$blnakhir1='Mei';} if($blnakhir=='06'){$blnakhir1='Juni';} if($blnakhir=='07'){$blnakhir1='Juli';} if($blnakhir=='08'){$blnakhir1='Agustus';}
			if($blnakhir=='09'){$blnakhir1='September';} if($blnakhir=='10'){$blnakhir1='Oktober';} if($blnakhir=='11'){$blnakhir1='November';} if($blnakhir=='12'){$blnakhir1='Desember';}			
			
			$objPHPExcel->getActiveSheet()->setCellValue ('A10','Periode : '.$tglawal.' '.$blnawal1.' '.$thnawal.' s/d '.$tglakhir.' '.$blnakhir1.' '.$thnakhir);
		}

		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'12')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'12')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}

		$objPHPExcel->getActiveSheet()->setCellValue ('A12','NO.');
		$objPHPExcel->getActiveSheet()->setCellValue ('B12','Kode Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('C12','Nama Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('D12','Pabrik');
		$objPHPExcel->getActiveSheet()->setCellValue ('E12','Satuan');
		$objPHPExcel->getActiveSheet()->setCellValue ('F12','Qty');
		$objPHPExcel->getActiveSheet()->setCellValue ('G12','Nominal');
		$objPHPExcel->getActiveSheet()->setCellValue ('H12','Persen');
		$objPHPExcel->getActiveSheet()->setCellValue ('I12','Persen(K)');
		$items=array();
		$items=$this->mlaporanapt->paretoapotek($periodeawal,$periodeakhir,$kd_unit_apt,$kd_pabrik);
		$baris=13;
		$no=1;
		$totalqty=0;
		$totalnominal=0;
		$totalpersen=0;
		$persenk=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='I';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='H'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='I'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}
			$persenk=$persenk+$item['persen'];

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$no);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['kd_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['nama_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['nama_pabrik']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['satuan_kecil']);
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['qty_kcl']);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['nominal']);
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,number_format($item['persen'],2,',',''));
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,number_format($persenk,2,',',''));
			$totalqty=$totalqty+$item['qty_kcl'];
			$totalnominal=$totalnominal+$item['nominal'];
			$totalpersen=$totalpersen+$item['persen'];
			$baris++;
			$no++;
		}
			for($x='A';$x<='I';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='H'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='I'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'');
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'');
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,'');
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,'T O T A L');
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$totalqty);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$totalnominal);
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$totalpersen);
		//debugvar($status);

		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/laporanparetoapotek.xls");
		header("Location: ".base_url()."download/laporanparetoapotek.xls");
	}	

	public function kunjunganresepexcel($periodeawal="",$periodeakhir="",$kd_unit_apt=""){
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath('images/logo1.jpg');
		$objDrawing->setHeight(70);
		$objDrawing->setCoordinates('A2');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);		
		$objPHPExcel->getActiveSheet()->getPageSetup()->setScale(90);		
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A6:G6');
		$objPHPExcel->getActiveSheet()->mergeCells('A7:G7');
		$objPHPExcel->getActiveSheet()->mergeCells('A8:G8');
		$objPHPExcel->getActiveSheet()->mergeCells('A9:G9');
		$objPHPExcel->getActiveSheet()->mergeCells('A10:G10');
		
		//$objPHPExcel->getActiveSheet()->mergeCells('G11:I11');
		
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(4); //kosong
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(30); //no
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10); //no terima
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(10);//nama
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(10); //qty
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(10); //harga
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(10); //harga
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(10); //harga

		for($x='A';$x<='H';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'9')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
				
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
				
		$queryjudul=$this->db->query("select profil.nama_profil,profil.alamat_profil,kelurahan.kelurahan,profil.kota from profil,kelurahan 
								where profil.kd_kelurahan=kelurahan.kd_kelurahan");
		$judul=$queryjudul->row_array();
		
		$objPHPExcel->getActiveSheet()->setCellValue ('C2',$judul['nama_profil']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C3',$judul['alamat_profil'].' '.$judul['kelurahan']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C4','Kota '.$judul['kota']);		
		$objPHPExcel->getActiveSheet()->setCellValue ('A7','LAPORAN PARETO');
				
		if($kd_pabrik==''){ $objPHPExcel->getActiveSheet()->setCellValue ('A8','Pabrik : Semua Pabrik');}
		else{
			$querypabrik=$this->db->query("select nama_pabrik from apt_pabrik where kd_pabrik='$kd_pabrik'");
			$pabrik=$querypabrik->row_array();
			$objPHPExcel->getActiveSheet()->setCellValue ('A8','Pabrik : '.$pabrik['nama_pabrik']);
		}
		
		$unit=$this->mlaporanapt->ambilNamaUnit($kd_unit_apt);
		$objPHPExcel->getActiveSheet()->setCellValue ('A9','Unit : '.$unit);
	
		if($periodeawal!='' and $periodeakhir!=''){
			$tglawal=substr($periodeawal,0,2);		$tglakhir=substr($periodeakhir,0,2);
			$blnawal=substr($periodeawal,3,2);		$blnakhir=substr($periodeakhir,3,2);
			$thnawal=substr($periodeawal,6,10);		$thnakhir=substr($periodeakhir,6,10);
			
			if($blnawal=='01'){$blnawal1='Januari';} if($blnawal=='02'){$blnawal1='Februari';}	if($blnawal=='03'){$blnawal1='Maret';} if($blnawal=='04'){$blnawal1='April';}
			if($blnawal=='05'){$blnawal1='Mei';} if($blnawal=='06'){$blnawal1='Juni';} if($blnawal=='07'){$blnawal1='Juli';} if($blnawal=='08'){$blnawal1='Agustus';}
			if($blnawal=='09'){$blnawal1='September';} if($blnawal=='10'){$blnawal1='Oktober';}	if($blnawal=='11'){$blnawal1='November';} if($blnawal=='12'){$blnawal1='Desember';}
			
			if($blnakhir=='01'){$blnakhir1='Januari';} if($blnakhir=='02'){$blnakhir1='Februari';} if($blnakhir=='03'){$blnakhir1='Maret';}	if($blnakhir=='04'){$blnakhir1='April';}
			if($blnakhir=='05'){$blnakhir1='Mei';} if($blnakhir=='06'){$blnakhir1='Juni';} if($blnakhir=='07'){$blnakhir1='Juli';} if($blnakhir=='08'){$blnakhir1='Agustus';}
			if($blnakhir=='09'){$blnakhir1='September';} if($blnakhir=='10'){$blnakhir1='Oktober';} if($blnakhir=='11'){$blnakhir1='November';} if($blnakhir=='12'){$blnakhir1='Desember';}			
			
			$objPHPExcel->getActiveSheet()->setCellValue ('A10','Periode : '.$tglawal.' '.$blnawal1.' '.$thnawal.' s/d '.$tglakhir.' '.$blnakhir1.' '.$thnakhir);
		}

		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'12')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'12')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}

		$objPHPExcel->getActiveSheet()->setCellValue ('A12','NO.');
		$objPHPExcel->getActiveSheet()->setCellValue ('B12','Dokter');
		$objPHPExcel->getActiveSheet()->setCellValue ('C12','Umum');
		$objPHPExcel->getActiveSheet()->setCellValue ('D12','Perusahaan');
		$objPHPExcel->getActiveSheet()->setCellValue ('E12','BPJS');
		$objPHPExcel->getActiveSheet()->setCellValue ('F12','Asuransi');
		$objPHPExcel->getActiveSheet()->setCellValue ('G12','Sosial Dakwah');
		$objPHPExcel->getActiveSheet()->setCellValue ('H12','Total');
		$items=array();
		$items=$this->mlaporanapt->kunjunganresep($periodeawal,$periodeakhir,$kd_unit_apt);
		$baris=13;
		$no=1;
		$totalumum=0;
		$totalperusahaan=0;
		$totalbpjs=0;
		$totalasuransi=0;
		$totalsosialdakwah=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='H';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='H'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$no);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['nama_dokter']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['umum']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['perusahaan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['bpjs']);
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['asuransi']);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['sosialdakwah']);
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$item['umum']+$item['perusahaan']+$item['bpjs']+$item['asuransi']+$item['sosialdakwah']);
            $totalumum=$totalumum+$item['umum'];
            $totalperusahaan=$totalperusahaan+$item['perusahaan'];
            $totalbpjs=$totalbpjs+$item['bpjs'];
            $totalasuransi=$totalasuransi+$item['asuransi'];
            $totalsosialdakwah=$totalsosialdakwah+$item['sosialdakwah'];
			$baris++;
			$no++;
		}
			for($x='A';$x<='H';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='H'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'');
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'T O T A L');
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$totalumum);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$totalperusahaan);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$totalbpjs);
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$totalasuransi);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$totalsosialdakwah);
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$totalumum+$totalperusahaan+$totalbpjs+$totalasuransi+$totalsosialdakwah);
		//debugvar($status);

		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/laporankunjunganresep.xls");
		header("Location: ".base_url()."download/laporankunjunganresep.xls");
	}	

	public function rl1excelrekappenjualan($periodeawal="",$periodeakhir="",$shiftapt="",$is_lunas="",$kd_unit_apt="",$resep="",$jenis_pasien="",$tipe="",$dokter="",$kd_user=""){
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);		
		$objPHPExcel->getActiveSheet()->mergeCells('A1:M1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:M2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:M3');
		$objPHPExcel->getActiveSheet()->mergeCells('A4:M4');
		$objPHPExcel->getActiveSheet()->mergeCells('A5:M5');
		$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(3.75); //no
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(9.71); //tgl
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(15.43); //tgl
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(23); //kd obat
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(3); //kd obat
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(12); //nama obat
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(11); //jumlah
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(11); //satuan
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(8)->setWidth(11); //service
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(9)->setWidth(11); //total
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(10)->setWidth(11); //total
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(11)->setWidth(12); //total
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(12)->setWidth(3); //total
		
		for($x='A';$x<='M';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'1')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'5')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		
		if($is_lunas==1){$is_lunas1='Lunas';}
		else if($is_lunas==''){$is_lunas1='-';}
		else {$is_lunas1='Belum Lunas';}

		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);

		$queryuser=$this->db->query("select * from user a join pegawai b on a.id_pegawai=b.id_pegawai where id_user='".$kd_user."' ");
		$user=$queryuser->row_array();

		//$queryuserkasir=$this->db->query("select * from user a join pegawai b on a.id_pegawai=b.id_pegawai where id_user='".$kd_user_kasir."' ");
		//$userkasir=$queryuserkasir->row_array();

		if($is_lunas==1){$status='Lunas';}
		else if($is_lunas==''){$status='-';}
		else if($is_lunas==3){$status='Retur';}
		else {$status='Belum Lunas';}

		if($shiftapt==''){$shiftapt1='Semua Shift';}
		else {$shiftapt1='Shift '.$shiftapt;}

		$objPHPExcel->getActiveSheet()->setCellValue ('A1','Laporan Penjualan Obat');
		$query1=$this->db->query("select nama_unit_apt from apt_unit where kd_unit_apt='$kd_unit_apt' ");
		$row1=$query1->row_array();
		if (!empty($row1)){
			$objPHPExcel->getActiveSheet()->setCellValue ('A2','Unit :  '.$row1['nama_unit_apt']);
		}
		else {
			$objPHPExcel->getActiveSheet()->setCellValue ('A2','Unit : Semua Unit');
		}
		$objPHPExcel->getActiveSheet()->setCellValue ('A3','Periode :  '.$periodeawal.'   sampai   '.$periodeakhir);		
		$objPHPExcel->getActiveSheet()->setCellValue ('A4','Status :  '.$is_lunas1.' Shift :  '.$shiftapt1);		
		//$objPHPExcel->getActiveSheet()->setCellValue ('A5','User Apotek :  '.$user['nama_pegawai']);		
		//$objPHPExcel->getActiveSheet()->setCellValue ('A6','User Kasir :  '.$userkasir['nama_pegawai']);		
		
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}

		$objPHPExcel->getActiveSheet()->setCellValue ('A7','NO.');
		$objPHPExcel->getActiveSheet()->setCellValue ('B7','TGL');
		$objPHPExcel->getActiveSheet()->setCellValue ('C7','No Penjualan');
		$objPHPExcel->getActiveSheet()->setCellValue ('D7','Nama Pasien');
		$objPHPExcel->getActiveSheet()->setCellValue ('E7','Jns');
		$objPHPExcel->getActiveSheet()->setCellValue ('F7','Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('G7','Racik');
		$objPHPExcel->getActiveSheet()->setCellValue ('H7','Resep');
		$objPHPExcel->getActiveSheet()->setCellValue ('I7','Medis');
		$objPHPExcel->getActiveSheet()->setCellValue ('J7','Adm');
		$objPHPExcel->getActiveSheet()->setCellValue ('K7','Kartu');
		$objPHPExcel->getActiveSheet()->setCellValue ('L7','Total');
		$objPHPExcel->getActiveSheet()->setCellValue ('M7','');
		//debugvar($status);
		if($shiftapt=='NULL'){$shiftapt='';} 
		if($is_lunas=='NULL'){$is_lunas='';} 
		if($kd_unit_apt=='NULL'){$kd_unit_apt='';} 		
		if($resep=='NULL'){$resep='';} 
		if($jenis_pasien=='NULL'){$jenis_pasien='';} 
		if($dokter=='NULL'){$dokter='';} 
		if($tipe=='NULL'){$tipe='';} 
		if($kd_user=='NULL'){$kd_user='';} 

		$items=array();
		$items=$this->mlaporanapt->getAllPenjualanApotek($periodeawal,$periodeakhir,$shiftapt,$is_lunas,$kd_unit_apt,$resep,$jenis_pasien,$dokter,$tipe,$kd_user);		
		//debugvar($items);
		$baris=8;
		$nomor=1;

		$totalbiayaobat=0;
		$totaljasaresep=0;
		$totaljasamedis=0;
		$totaladm=0;
		$totalkartu=0;
		$totalracik=0;
		$grandtotal=0;

		$totalretur=0;
		$totaljasamedisretur=0;
		$totaladmretur=0;
		$totalkarturetur=0;
		$totaljasaresepretur=0;
		$totaljasaracikretur=0;
		$totalbiayaobatretur=0;

		$totaljasamedisbelumlunas=0;
		$totaladmbelumlunas=0;
		$totalkartubelumlunas=0;
		$totaljasaresepbelumlunas=0;
		$totalbiayaobatbelumlunas=0;
		$totaljasaracikbelumlunas=0;
		$totalbelumlunas=0;

		$totaluangkartu=0;
		$totaljasamedisuangkartu=0;
		$totaladmuangkartu=0;
		$totalkartuuangkartu=0;
		$totaljasaresepuangkartu=0;
		$totaljasaracikuangkartu=0;
		$totalbiayaobatuangkartu=0;

		$totaltunai1=0;
		$totaljasamedistunai=0;
		$totaladmtunai=0;
		$totalkartutunai=0;
		$totaljasareseptunai=0;
		$totaljasaraciktunai=0;
		$totalbiayaobattunai=0;

		foreach ($items as $item) {
			# code...
			for($x='A';$x<='M';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='H'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='I'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='J'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='K'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='L'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='M'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$querybiayaobat=$this->db->query('select ifnull(sum(harga_jual*qty),0) as total from apt_penjualan_detail where no_penjualan="'.$item['no_penjualan'].'" ');
			$biayaobat=$querybiayaobat->row_array();

			$queryjasaresep=$this->db->query('select ifnull(sum(adm_resep),0) as total from apt_penjualan_detail where no_penjualan="'.$item['no_penjualan'].'"  ');
			$admresep=$queryjasaresep->row_array();

			$querytunai=$this->db->query('select ifnull(sum(total),0) as total from apt_penjualan_bayar where kd_jenis_bayar="001" and no_penjualan="'.$item['no_penjualan'].'"  ');
			$uangtunai=$querytunai->row_array();

			$totaltunai=$totaltunai+$uangtunai['total'];
			$totaltunai1=$totaltunai1+$uangtunai['total'];

			$querykartu=$this->db->query('select ifnull(sum(total),0) as total from apt_penjualan_bayar where kd_jenis_bayar="004" and no_penjualan="'.$item['no_penjualan'].'" ');
			$uangkartu=$querykartu->row_array();
			$totaluangkartu=$totaluangkartu+$uangkartu['total'];


		    switch ($item['type']) {
		        case 0:
		            # code...
		            $item['tipe']='TU';
		            break;
		        case 1:
		            # code...
		            $item['tipe']='AS';
		            break;
		        case 2:
		            # code...
		            $item['tipe']='PR';
		            break;
		        case 3:
		            # code...
		            $item['tipe']='SD';
		            break;

		        case 4:
		            # code...
		            $item['tipe']='BP';
		            break;

		        default:
		            # code...
		            $item['tipe']='Tidak Terdefinisikan';
		           break;
		    }

			$totalbiayaobat=$totalbiayaobat+$biayaobat['total'];
			$totaljasaresep=$totaljasaresep+$admresep['total'];
			$totaljasamedis=$totaljasamedis+$item['jasa_medis'];
			$totaladm=$totaladm+$item['biaya_adm'];
			$totalkartu=$totalkartu+$item['biaya_kartu'];
			$totalracik=$totalracik+$item['adm_racik'];
			$grandtotal=$grandtotal+$item['total_transaksi'];

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['tgl_penjualan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['no_penjualan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['nama_pasien']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['tipe']);
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$biayaobat['total']);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['adm_racik']);		
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$admresep['total']);
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$item['jasa_medis']);
			$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,$item['biaya_adm']);
			$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,$item['biaya_kartu']);
			$objPHPExcel->getActiveSheet()->setCellValue ('L'.$baris,$item['total_transaksi']);
			if($item['is_lunas']==1){ 
				if($uangkartu['total']>0){
					$status="K";			
				}else{
					$status="L";			
				}
			} else if($item['is_lunas']==3) {
				$totalretur=$totalretur+$item['total_transaksi'];
				$totaladmretur=$totaladmretur+$item['biaya_adm'];
				$totaljasamedisretur=$totaljasamedisretur+$item['jasa_medis'];
				$totalkarturetur=$totalkarturetur+$item['biaya_kartu'];
				$totaljasaracikretur=$totaljasaracikretur+$item['adm_racik'];
				$totaljasaresepretur=$totaljasaresepretur+$admresep['total'];
				$totalbiayaobatretur=$totalbiayaobatretur+$biayaobat['total'];
				$status="R";
			}else {
				$totaladmbelumlunas=$totaladmbelumlunas+$item['biaya_adm'];
				$totaljasamedisbelumlunas=$totaljasamedisbelumlunas+$item['jasa_medis'];
				$totalkartubelumlunas=$totalkartubelumlunas+$item['biaya_kartu'];
				$totaljasaracikbelumlunas=$totaljasaracikbelumlunas+$item['adm_racik'];
				$totaljasaresepbelumlunas=$totaljasaresepbelumlunas+$admresep['total'];
				$totalbiayaobatbelumlunas=$totalbiayaobatbelumlunas+$biayaobat['total'];
				$totalbelumlunas=$totalbelumlunas+$item['total_transaksi'];
				$status="BL";
			}	

			if(!empty($uangkartu['total']) && empty($uangtunai['total'])){
				$totaladmuangkartu=$totaladmuangkartu+$item['biaya_adm'];
				$totaljasamedisuangkartu=$totaljasamedisuangkartu+$item['jasa_medis'];
				$totalkartuuangkartu=$totalkartuuangkartu+$item['biaya_kartu'];
				$totaljasaracikuangkartu=$totaljasaracikuangkartu+$item['adm_racik'];
				$totaljasaresepuangkartu=$totaljasaresepuangkartu+$admresep['total'];
				$totalbiayaobatuangkartu=$totalbiayaobatuangkartu+$biayaobat['total'];
			}		

			if(!empty($uangtunai['total']) && empty($uangkartu['total'])){
				$totaladmtunai=$totaladmtunai+$item['biaya_adm'];
				$totaljasamedistunai=$totaljasamedistunai+$item['jasa_medis'];
				$totalkartutunai=$totalkartutunai+$item['biaya_kartu'];
				$totaljasaraciktunai=$totaljasaraciktunai+$item['adm_racik'];
				$totaljasareseptunai=$totaljasareseptunai+$admresep['total'];
				$totalbiayaobattunai=$totalbiayaobattunai+$biayaobat['total'];
			}		

			$objPHPExcel->getActiveSheet()->setCellValue ('M'.$baris,$status);

			$nomor=$nomor+1; $baris=$baris+1; 
		}

		$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':E'.$baris);
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}

		$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'TOTAL');
		$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$totalbiayaobat);
		$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$totalracik);
		$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$totaljasaresep);
		$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$totaljasamedis);
		$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,$totaladm);
		$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,$totalkartu);
		$objPHPExcel->getActiveSheet()->setCellValue ('L'.$baris,$grandtotal);
		$objPHPExcel->getActiveSheet()->setCellValue ('M'.$baris,'');
		$baris=$baris+1;

		$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':E'.$baris);
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
		}

		$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'B E L U M   L U N A S');
		$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$totalbiayaobatbelumlunas);
		$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$totaljasaracikbelumlunas);
		$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$totaljasaresepbelumlunas);
		$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$totaljasamedisbelumlunas);
		$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,$totaladmbelumlunas);
		$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,$totalkartubelumlunas);
		$objPHPExcel->getActiveSheet()->setCellValue ('L'.$baris,$totalbelumlunas);
		$objPHPExcel->getActiveSheet()->setCellValue ('M'.$baris,'');
		$baris=$baris+1;

		$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':E'.$baris);
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
		}

		$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'T O T A L   R E T U R');
		$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$totalbiayaobatretur);
		$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$totaljasaracikretur);
		$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$totaljasaresepretur);
		$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$totaljasamedisretur);
		$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,$totaladmretur);
		$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,$totalkarturetur);
		$objPHPExcel->getActiveSheet()->setCellValue ('L'.$baris,$totalretur);
		$objPHPExcel->getActiveSheet()->setCellValue ('M'.$baris,'');
		$baris=$baris+1;

		$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':E'.$baris);
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
		}

		$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'P E M B A Y A R A N  V I A  K A R T U');
		$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$totalbiayaobatuangkartu);
		$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$totaljasaracikuangkartu);
		$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$totaljasaresepuangkartu);
		$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$totaljasamedisuangkartu);
		$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,$totaladmuangkartu);
		$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,$totalkartuuangkartu);
		$objPHPExcel->getActiveSheet()->setCellValue ('L'.$baris,$totaluangkartu);
		$objPHPExcel->getActiveSheet()->setCellValue ('M'.$baris,'');
		$baris=$baris+1;
		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':E'.$baris);
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
		}

		$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'S E T O R T U N A I');
		$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$totalbiayaobattunai);
		$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$totaljasaraciktunai);
		$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$totaljasareseptunai);
		$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$totaljasamedistunai);
		$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,$totaladmtunai);
		$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,$totalkartutunai);
		$objPHPExcel->getActiveSheet()->setCellValue ('L'.$baris,$totaltunai1);
		$objPHPExcel->getActiveSheet()->setCellValue ('M'.$baris,'');
		$baris=$baris+1;

		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/rekappenjualanobat.xls");
		header("Location: ".base_url()."download/rekappenjualanobat.xls");
	}
	
	public function rl1excelkartustok($kd_obat="",$kd_unit_apt="",$bulan="",$tahun=""){
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:H3');
		//$objPHPExcel->getActiveSheet()->mergeCells('A4:H4');
		$objPHPExcel->getActiveSheet()->mergeCells('A5:H5');
		$objPHPExcel->getActiveSheet()->mergeCells('A6:H6');
		$objPHPExcel->getActiveSheet()->mergeCells('A8:B8');
		$objPHPExcel->getActiveSheet()->mergeCells('A9:B9');
		$objPHPExcel->getActiveSheet()->mergeCells('A10:B10');

		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(2.14); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(4.34); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(18); //TGL
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(22); //KETERANGAN
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(25); //UNIT VENDOR
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(15); //NO BUKTI
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(7); //MASUK
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(7); //KELUAR
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(7); //SALDO
		
		for($x='A';$x<='H';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'5')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		/*for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}*/		
		$objPHPExcel->getActiveSheet()->setCellValue ('A2','RUMAH SAKIT IBNU SINA');
		//$objPHPExcel->getActiveSheet()->setCellValue ('A3','SAYANG IBU');
		$objPHPExcel->getActiveSheet()->setCellValue ('A3','Kota Balikpapan');		
		$objPHPExcel->getActiveSheet()->setCellValue ('A5','Kartu Stok Unit Apotek');		
		$bulan1="";
		if($bulan=='01'){$bulan1='Januari';} 	 if($bulan=='02'){$bulan1='Februari';}	if($bulan=='03'){$bulan1='Maret';} 	  if($bulan=='04'){$bulan1='April';}
		if($bulan=='05'){$bulan1='Mei';} 		 if($bulan=='06'){$bulan1='Juni';} 		if($bulan=='07'){$bulan1='Juli';} 	  if($bulan=='08'){$bulan1='Agustus';}
		if($bulan=='09'){$bulan1='September';}   if($bulan=='10'){$bulan1='Oktober';}	if($bulan=='11'){$bulan1='November';} if($bulan=='12'){$bulan1='Desember';}
		$objPHPExcel->getActiveSheet()->setCellValue ('A6',$bulan1.' '.$tahun);
		
		$objPHPExcel->getActiveSheet()->setCellValue ('A8','Kode Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('A9','Nama Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('A10','Unit Apotek');
		
		if($kd_obat==''){$objPHPExcel->getActiveSheet()->setCellValue ('C8',' : -');}
		else{$objPHPExcel->getActiveSheet()->setCellValue ('C8',' : '.$kd_obat);}
		$namaobat=$this->mlaporanapt->namaObat($kd_obat);
		//debugvar($namaobat);
		if($namaobat=='0' or $namaobat=='') {$objPHPExcel->getActiveSheet()->setCellValue ('C9',' : -');}
		else {$objPHPExcel->getActiveSheet()->setCellValue ('C9',' : '.$namaobat);}
		
		$namaunit=$this->mlaporanapt->namaUnit($kd_unit_apt);
		if($namaunit=='0' or $namaunit=='') {$objPHPExcel->getActiveSheet()->setCellValue ('C10',' : -');}
		else {$objPHPExcel->getActiveSheet()->setCellValue ('C10',' : '.$namaunit);}
		
		
		for($x='A';$x<='H';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'11')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'11')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>10 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A11','NO.');
		$objPHPExcel->getActiveSheet()->setCellValue ('B11','TANGGAL');
		$objPHPExcel->getActiveSheet()->setCellValue ('C11','KETERANGAN');
		$objPHPExcel->getActiveSheet()->setCellValue ('D11','UNIT / VENDOR');
		$objPHPExcel->getActiveSheet()->setCellValue ('E11','NO. BUKTI');
		$objPHPExcel->getActiveSheet()->setCellValue ('F11','MASUK');
		$objPHPExcel->getActiveSheet()->setCellValue ('G11','KELUAR');
		$objPHPExcel->getActiveSheet()->setCellValue ('H11','SALDO');
		$items=array();
		$items=$this->mlaporanapt->getKartuStok($kd_obat,$kd_unit_apt,$bulan,$tahun);
		$baris=12;
		$nomor=1;
		$total=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='H';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}				
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>10 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}
			if($item['kode']=="M"){ $saldo1=$item['masuk']+$saldoakhir; 
			}else if($item['kode']=="K"){ $saldo1=$saldoakhir-$item['keluar'];
			}else{ if($item['saldo']==''){ $saldo1=0; }
				   else{ $saldo1=$item['saldo']; }
			}
			$saldoakhir=$saldo1;

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['tgl']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['keterangan']);
			if($item['keterangan']=='Saldo Awal' or $item['keterangan']=='Penerimaan Vendor'){ $objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['unitvendor']);}
			else{
				$unitvendor=$item['unitvendor'];
				$namaunit=$this->mlaporanapt->namaUnit($unitvendor);
				if($namaunit=='0' or $namaunit=='') {$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,'-');}
				else {$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$namaunit);}				
			}					
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['no_bukti']);
			if($item['masuk']==0 or $item['masuk']==''){$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,'-');}
			else {$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,number_format($item['masuk']));}
			if($item['keluar']==0 or $item['keluar']==''){$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,'-');}
			else {$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,number_format($item['keluar']));}
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,number_format($saldoakhir));						
			$nomor=$nomor+1; $baris=$baris+1;
		}	
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/kartustokobat.xls");
		header("Location: ".base_url()."download/kartustokobat.xls");
	}
	
	public function rl1excelmutasiobat($kd_unit_apt="",$bulan="",$tahun=""){
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A2:M2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:M3');
		$objPHPExcel->getActiveSheet()->mergeCells('A4:M4');
		$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');
		$objPHPExcel->getActiveSheet()->mergeCells('A7:M7');
		$objPHPExcel->getActiveSheet()->mergeCells('A8:M8');

		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(2.14); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(4.14); //no
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(25.5); //nama obat
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(5); //saldo awal
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(9); //harga
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(13); //total awal
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(5); //pbf masuk
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(5); //unit masuk
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(6); //retur masuk
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(5); //jml masuk
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(10)->setWidth(13); //total masuk
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(6); //resep keluar
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(8)->setWidth(5); //unit keluar
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(9)->setWidth(6); //retur keluar
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(10)->setWidth(5); //jml keluar
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(15)->setWidth(13); //total keluar
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(11)->setWidth(8); //stokopname
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(12)->setWidth(17); //saldo akhir
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(13)->setWidth(17); //saldo akhir
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(14)->setWidth(17); //saldo akhir
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(15)->setWidth(17); //saldo akhir
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(16)->setWidth(17); //saldo akhir
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(17)->setWidth(17); //saldo akhir
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(18)->setWidth(13); //total akhir
		
		for($x='A';$x<='R';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='R';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='R';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='R';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='R';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='R';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A2','RUMAH SAKIT');
		$objPHPExcel->getActiveSheet()->setCellValue ('A3','IBNU SINA');
		$objPHPExcel->getActiveSheet()->setCellValue ('A4','Kota Balikpapan');		
		$objPHPExcel->getActiveSheet()->setCellValue ('A6','LAPORAN MUTASI OBAT');		
		$bulan1="";
		if($bulan=='01'){$bulan1='Januari';} 	 if($bulan=='02'){$bulan1='Februari';}	if($bulan=='03'){$bulan1='Maret';} 	  if($bulan=='04'){$bulan1='April';}
		if($bulan=='05'){$bulan1='Mei';} 		 if($bulan=='06'){$bulan1='Juni';} 		if($bulan=='07'){$bulan1='Juli';} 	  if($bulan=='08'){$bulan1='Agustus';}
		if($bulan=='09'){$bulan1='September';}   if($bulan=='10'){$bulan1='Oktober';}	if($bulan=='11'){$bulan1='November';} if($bulan=='12'){$bulan1='Desember';}
		$objPHPExcel->getActiveSheet()->setCellValue ('A7',$bulan1.' '.$tahun);
		$namaunit=$this->mlaporanapt->namaUnit($kd_unit_apt);
		$objPHPExcel->getActiveSheet()->setCellValue ('A8',$namaunit);
		
		for($x='A';$x<='R';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			$objPHPExcel->getActiveSheet()->getStyle($x.'11')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->getStyle($x.'11')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->mergeCells('A10:A11'); 	//$objPHPExcel->getActiveSheet()->getStyle('B10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('A10','NO.');
		$objPHPExcel->getActiveSheet()->mergeCells('B10:B11');  $objPHPExcel->getActiveSheet()->setCellValue ('B10','NAMA OBAT');
		$objPHPExcel->getActiveSheet()->mergeCells('C10:C11');  $objPHPExcel->getActiveSheet()->getStyle('C10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('C10','SLD. AWL');
		/*$objPHPExcel->getActiveSheet()->mergeCells('E10:E11');  $objPHPExcel->getActiveSheet()->setCellValue ('E10','HARGA');
		$objPHPExcel->getActiveSheet()->mergeCells('F10:F11');  $objPHPExcel->getActiveSheet()->getStyle('F10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('F10','TOTAL AWAL');*/
		$objPHPExcel->getActiveSheet()->mergeCells('D10:G10');  $objPHPExcel->getActiveSheet()->setCellValue ('D10','MASUK');
		$objPHPExcel->getActiveSheet()->setCellValue ('D11','PBF'); 
		$objPHPExcel->getActiveSheet()->setCellValue ('E11','Unit');
		$objPHPExcel->getActiveSheet()->setCellValue ('F11','Retur');
		$objPHPExcel->getActiveSheet()->setCellValue ('G11','Jml.');
		/*$objPHPExcel->getActiveSheet()->mergeCells('K10:K11');  $objPHPExcel->getActiveSheet()->getStyle('K10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('K10','TOTAL MASUK');*/
		$objPHPExcel->getActiveSheet()->mergeCells('H10:K10');  $objPHPExcel->getActiveSheet()->setCellValue ('H10','KELUAR');
		$objPHPExcel->getActiveSheet()->setCellValue ('H11','Resep');
		$objPHPExcel->getActiveSheet()->setCellValue ('I11','Unit');
		$objPHPExcel->getActiveSheet()->setCellValue ('J11','Retur');
		$objPHPExcel->getActiveSheet()->setCellValue ('K11','Jml.');
		/*$objPHPExcel->getActiveSheet()->mergeCells('P10:P11');  $objPHPExcel->getActiveSheet()->getStyle('P10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('P10','TOTAL KELUAR');*/
		$objPHPExcel->getActiveSheet()->mergeCells('L10:L11');  $objPHPExcel->getActiveSheet()->getStyle('L10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('L10','SALDO AKHIR');
		$objPHPExcel->getActiveSheet()->mergeCells('M10:M11');  $objPHPExcel->getActiveSheet()->getStyle('M10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('M10','HARGA BELI');
		$objPHPExcel->getActiveSheet()->mergeCells('N10:N11');  $objPHPExcel->getActiveSheet()->getStyle('N10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('N10','SALDO AKHIR X HARGA BELI');
		$objPHPExcel->getActiveSheet()->mergeCells('O10:O11');  $objPHPExcel->getActiveSheet()->getStyle('O10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('O10','PENYESUAIAN STOK');
		$objPHPExcel->getActiveSheet()->mergeCells('P10:P11');  $objPHPExcel->getActiveSheet()->getStyle('P10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('P10','HARGA X PENYESUAIAN STOK');
		$objPHPExcel->getActiveSheet()->mergeCells('Q10:Q11');  $objPHPExcel->getActiveSheet()->getStyle('Q10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('Q10','SALDO SETELAH PENYESUAIAN');
		$objPHPExcel->getActiveSheet()->mergeCells('R10:R11');  $objPHPExcel->getActiveSheet()->getStyle('R10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('R10','TOTAL');
		/*$objPHPExcel->getActiveSheet()->mergeCells('S10:S11');  $objPHPExcel->getActiveSheet()->getStyle('S10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('S10','TOTAL AKHIR');*/
		$items=array();
		$items=$this->mlaporanapt->getMutasiObat($kd_unit_apt,$bulan,$tahun);
		$baris=12;
		$nomor=1;
		$sumtotalawal=0; $sumtotalmasuk=0; $sumtotalkeluar=0;  $sumtotalakhir=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='R';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='H'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='I'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='J'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='K'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='L'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='M'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else{
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['nama_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['saldo_awal']); 	/*$sldawl=$item['saldo_awal'];
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,number_format($item['harga_beli'])); 	$hrg=$item['harga_beli'];	$totalawal=$sldawl*$hrg;
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,number_format($totalawal));*/
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['in_pbf']); 		$pbf=$item['in_pbf'];
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['in_unit']); 		$inunit=$item['in_unit'];
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['retur_jual']); 	$retpbf=$item['retur_pbf'];$jml=($pbf+$inunit)-$retpbf;
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['jum_masuk']);					/*$totalmasuk=$hrg*$jml;
			$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,number_format($totalmasuk));*/
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$item['out_jual']); 	$resep=$item['out_jual'];
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$item['out_unit']); 	$outunit=$item['out_unit']; $retkeluar=0;
			$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,$item['retur_pbf']); 			$jml1=($resep+$outunit)-$retkeluar;
			$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,$item['jum_keluar']); 				/*$totalkeluar=$hrg*$jml1;
			$objPHPExcel->getActiveSheet()->setCellValue ('P'.$baris,number_format($totalkeluar));*/
			$objPHPExcel->getActiveSheet()->setCellValue ('L'.$baris,($item['saldo_awal']+$item['jum_masuk'])-$item['jum_keluar']);
			$objPHPExcel->getActiveSheet()->setCellValue ('M'.$baris,$item['harga_beli']); 	
			$objPHPExcel->getActiveSheet()->setCellValue ('N'.$baris,(($item['saldo_awal']+$item['jum_masuk'])-$item['jum_keluar'])*$item['harga_beli']); 	
			$objPHPExcel->getActiveSheet()->setCellValue ('O'.$baris,$item['stok_opname']); 	
			$objPHPExcel->getActiveSheet()->setCellValue ('P'.$baris,$item['harga_beli']*$item['stok_opname']); 	
			$objPHPExcel->getActiveSheet()->setCellValue ('Q'.$baris,$item['saldo_akhir']); 	
			$objPHPExcel->getActiveSheet()->setCellValue ('R'.$baris,$item['saldo_akhir']*$item['harga_beli']); 	

			/*$saldoakhir=$item['saldo_akhir']; $totalakhir=$hrg*$saldoakhir;
			$objPHPExcel->getActiveSheet()->setCellValue ('S'.$baris,number_format($totalakhir));*/
			$nomor=$nomor+1; $baris=$baris+1; 
			//$sumtotalawal=$sumtotalawal+$totalawal; $sumtotalmasuk=$sumtotalmasuk+$totalmasuk; $sumtotalkeluar=$sumtotalkeluar+$totalkeluar; $sumtotalakhir=$sumtotalakhir+$totalakhir; 
		}
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/mutasiobat.xls");
		header("Location: ".base_url()."download/mutasiobat.xls");
	}
	
	public function rl1excelpersediaanobat($stok="",$isistok="",$kd_unit_apt="",$kd_golongan=""){
		if($kd_unit_apt=="NULL")$kd_unit_apt="";
		if($kd_golongan=="NULL")$kd_golongan="";
		$stok1="";  $stoka="";  $isistok1="";
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath('images/logo1.jpg');
		$objDrawing->setHeight(70);
		$objDrawing->setCoordinates('A2');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('C2:G2');
		$objPHPExcel->getActiveSheet()->mergeCells('C3:G3');
		$objPHPExcel->getActiveSheet()->mergeCells('C4:G4');
		$objPHPExcel->getActiveSheet()->mergeCells('C6:G6');
		$objPHPExcel->getActiveSheet()->mergeCells('C7:G7');
		$objPHPExcel->getActiveSheet()->mergeCells('C8:G8');
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(2.14); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(6); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(10); //kode
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(30); //nama obat
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(8.5); //satuan
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(11.5); //tgl expire
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(17); //stok
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(17); //harga beli
		//debugvar($kd_unit_apt);
		for($x='A';$x<='G';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}		

		$q=$this->db->query("select profil.nama_profil,profil.alamat_profil,kelurahan.kelurahan,profil.kota from profil,kelurahan 
								where profil.kd_kelurahan=kelurahan.kd_kelurahan");
		$qr=$q->row_array();

		$objPHPExcel->getActiveSheet()->setCellValue ('C2',$qr['nama_profil']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C3',$qr['alamat_profil'].' '.$qr['kelurahan']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C4','Kota '.$qr['kota']);		
		$objPHPExcel->getActiveSheet()->setCellValue ('C6','LAPORAN PERSEDIAAN OBAT / ALKES Per '.date('d-m-Y'));		
		
		$namaunit=$this->mlaporanapt->namaUnit($kd_unit_apt);
		if($namaunit=='0' or $namaunit=='') {$objPHPExcel->getActiveSheet()->setCellValue ('C7','Unit : Semua Unit Apotek');}
		else {$objPHPExcel->getActiveSheet()->setCellValue ('C7','Unit : '.$namaunit);}
						
		if($stok=='1'){$stok1="lebih besar dari"; $stoka=">";}
		else if($stok=='2'){$stok1="lebih kecil dari"; $stoka="<";}
		else if($stok=='3'){$stok1="lebih besar sama dengan"; $stoka=">=";}
		else if($stok=='4'){$stok1="lebih kecil sama dengan"; $stoka="<=";}
		else {$stok1="sama dengan"; $stoka="=";}
		
		if($isistok=='' or $isistok=='null'){$isistok1=0;} 
		else {$isistok1=$isistok;}		
		$objPHPExcel->getActiveSheet()->setCellValue ('C8','Stok '.$stok1.' '.$isistok1);
		
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A10','NO.');
		$objPHPExcel->getActiveSheet()->setCellValue ('B10','KODE');
		$objPHPExcel->getActiveSheet()->setCellValue ('C10','NAMA OBAT');
		$objPHPExcel->getActiveSheet()->setCellValue ('D10','SAT.');
		$objPHPExcel->getActiveSheet()->setCellValue ('E10','STOK');
		$objPHPExcel->getActiveSheet()->setCellValue ('F10','HNA + PPN');
		$objPHPExcel->getActiveSheet()->setCellValue ('G10','JUMLAH');
		$items=array();
		$items=$this->mlaporanapt->getAllPersediaanApotek($stok,$isistok,$kd_unit_apt,$kd_golongan);
		$baris=11;
		$nomor=1;
		$total=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='G';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['kd_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['nama_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['satuan_kecil']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['jml_stok']);
		    $ppn=(10/100)*$item['harga_pokok'];
		    $hnappn=$item['harga_pokok']+$ppn;
		    //$nilai=$item['jml_stok']*$item['harga_pokok'];
		    $nilai=$item['jml_stok']*$item['harga_beli'];
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['harga_beli']);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$nilai);
					
			$nomor=$nomor+1; $baris=$baris+1; 
			$total=$total+$nilai;
		}	
		for($x='A';$x<='G';$x++){
			if($x=='A'){
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
			}else if($x=='H'){
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
			}				
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
				'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => '000000')))));
		}
		//$objPHPExcel->getActiveSheet()->mergeCells('B8:I8');
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':F'.$baris);
		$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'T O T A L :');
		$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$total);
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/persediaanobat.xls");
		header("Location: ".base_url()."download/persediaanobat.xls");
	}
	
	public function stokopname(){
		$bulan=date('m');
		$tahun=date('Y');
		$stok='1';
		$isistok='';
		$kd_unit_apt='';
		$nama_unit_apt='';
		
		if($this->input->post('bulan')!=''){
			$bulan=$this->input->post('bulan');
		}
		if($this->input->post('tahun')!=''){
			$tahun=$this->input->post('tahun');
		}
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Penyesuaian Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array('items'=>$this->mlaporanapt->getWow($bulan,$tahun,$kd_unit_apt),
					'bulan'=>$bulan,
					'tahun'=>$tahun,
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'kd_unit_apt'=>$kd_unit_apt);

		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/lapstokopname',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function rl1excelstokopname($bulan="",$tahun="",$kd_unit_apt=""){
		$bln="";
		
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:H3');
		
		$objPHPExcel->getActiveSheet()->mergeCells('A4:A5'); $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->mergeCells('B4:B5'); $objPHPExcel->getActiveSheet()->getStyle('B4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->mergeCells('C4:C5'); $objPHPExcel->getActiveSheet()->getStyle('C4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->mergeCells('D4:D5'); $objPHPExcel->getActiveSheet()->getStyle('D4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->mergeCells('E4:E5'); $objPHPExcel->getActiveSheet()->getStyle('E4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->mergeCells('F4:F5'); $objPHPExcel->getActiveSheet()->getStyle('F4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->mergeCells('G4:G5'); $objPHPExcel->getActiveSheet()->getStyle('G4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->mergeCells('H4:H5'); $objPHPExcel->getActiveSheet()->getStyle('H4')->getAlignment()->setWrapText(true);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(2.14); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(5); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(17); //tgl stokopname
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(31); //nama
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(10); //satuan
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(7); //stok lama
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(7); //stok baru
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(13); //harga
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(13); //nilai
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(8)->setWidth(20); //nilai
		
		for($x='A';$x<='I';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'1')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}

		//debugvar($bulan1);
		if($bulan=='01'){$bln="Januari";}   
		if($bulan=='02'){$bln="Februari";} 
		if($bulan=='03'){$bln="Maret";} 	
		if($bulan=='04'){$bln="April";}
		if($bulan=='05'){$bln="Mei";}       
		if($bulan=='06'){$bln="Juni";}     
		if($bulan=='07'){$bln="Juli";}  	
		if($bulan=='08'){$bln="Agustus";}
		if($bulan=='09'){$bln="September";} 
		if($bulan=='10'){$bln="Oktober";}	 
		if($bulan=='11'){$bln="November";}	
		if($bulan=='12'){$bln="Desember";}
		
		$objPHPExcel->getActiveSheet()->setCellValue ('A1','STOCK OPNAME PER '.strtoupper($bln).' '.$tahun);
		
		$unit=$this->mlaporanapt->namaunit($kd_unit_apt);
		$objPHPExcel->getActiveSheet()->setCellValue ('A2',strtoupper($unit));		
		
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>10 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->getStyle($x.'5')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>10 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A4','NO');
		$objPHPExcel->getActiveSheet()->setCellValue ('B4','TGL. STOKOPNAME');
		$objPHPExcel->getActiveSheet()->setCellValue ('C4','NAMA OBAT');
		$objPHPExcel->getActiveSheet()->setCellValue ('D4','SATUAN');
		$objPHPExcel->getActiveSheet()->setCellValue ('E4','STOK LAMA');
		$objPHPExcel->getActiveSheet()->setCellValue ('F4','STOK BARU');
		$objPHPExcel->getActiveSheet()->setCellValue ('G4','HNA');
		$objPHPExcel->getActiveSheet()->setCellValue ('H4','NILAI');
		$objPHPExcel->getActiveSheet()->setCellValue ('I4','USER');
		$items=array();
		$items=$this->mlaporanapt->getWow($bulan,$tahun,$kd_unit_apt);
		//debugvar($items);
		$baris=6;
		$nomor=1;
		$total=0;
		//$nilai=0;
		foreach ($items as $item) {
			
			# code...
			for($x='A';$x<='I';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='H'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}	else if($x=='I'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}				
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>10 ,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['tanggal']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['nama_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['satuan_kecil']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['stok_lama']);
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['stok_baru']);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,'Rp '.$item['harga_beli']);
			$nilai=$item['harga_beli']*$item['stok_baru'];
			//$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,'Rp '.number_format($nilai,0,'',','));
			if($item['stok_baru']==0){
				$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,'Rp -');
			}
			else{
				$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,'Rp '.$nilai);
			}
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$item['nama_pegawai']);
			
			$nomor=$nomor+1; $baris=$baris+1; $total=$total+$nilai; $nilai=0;
		}	
		
		for($x='A';$x<='I';$x++){
			if($x=='A'){
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
			}else if($x=='F'){
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
			}				
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
				'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'size'		=>10 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => '000000')))));
		}
		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':G'.$baris);
		$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'TOTAL NILAI');
		$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,'Rp '.number_format($total,2,'.',','));
		
		for($x='D';$x<='I';$x++){
			if($x=='F'){ 
				$objPHPExcel->getActiveSheet()->getStyle($x.($baris+2))->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));				
				$objPHPExcel->getActiveSheet()->getStyle($x.($baris+1))->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));				
				$objPHPExcel->getActiveSheet()->getStyle($x.($baris+4))->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));				
				$objPHPExcel->getActiveSheet()->getStyle($x.($baris+1))->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));				
			}
			
			$objPHPExcel->getActiveSheet()->getStyle($x.($baris+2))->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
																					  'size'		=>10,'color'     => array('rgb' => '000000'))));
			$objPHPExcel->getActiveSheet()->getStyle($x.($baris+1))->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
																					  'size'		=>10,'color'     => array('rgb' => '000000'))));
			$objPHPExcel->getActiveSheet()->getStyle($x.($baris+4))->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
																					  'size'		=>10,'color'     => array('rgb' => '000000'))));
			$objPHPExcel->getActiveSheet()->getStyle($x.($baris+1))->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
																					  'size'		=>10,'color'     => array('rgb' => '000000'))));
		}
		
		/*$baris=$baris+2;
		$objPHPExcel->getActiveSheet()->mergeCells('F'.$baris.':H'.$baris);
		//$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,'Balikpapan, '.$tgl.' '.$bln.' '.$tahun);
		$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,'Balikpapan, ' .$bln.' '.$tahun);
		
		$baris=$baris+1;
		$objPHPExcel->getActiveSheet()->mergeCells('F'.$baris.':H'.$baris);
		$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,'Pengguna Anggaran');
		
		$baris=$baris+4;
		$objPHPExcel->getActiveSheet()->mergeCells('F'.$baris.':H'.$baris);
		$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,'Dr. Indah Puspitasari');
		
		$baris=$baris+1;
		$objPHPExcel->getActiveSheet()->mergeCells('F'.$baris.':H'.$baris);
		$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,'NIP. 19670530 199803 2 003');*/
		
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/lapstokopnameobat.xls");
		header("Location: ".base_url()."download/lapstokopnameobat.xls");
	}
	
	public function excelformstokopname($kd_unit_apt=""){
		$bln="";
		
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:H3');
		
		$objPHPExcel->getActiveSheet()->mergeCells('A4:A5'); $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->mergeCells('B4:B5'); $objPHPExcel->getActiveSheet()->getStyle('B4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->mergeCells('C4:C5'); $objPHPExcel->getActiveSheet()->getStyle('C4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->mergeCells('D4:D5'); $objPHPExcel->getActiveSheet()->getStyle('D4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->mergeCells('E4:E5'); $objPHPExcel->getActiveSheet()->getStyle('E4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->mergeCells('F4:F5'); $objPHPExcel->getActiveSheet()->getStyle('F4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->mergeCells('G4:G5'); $objPHPExcel->getActiveSheet()->getStyle('G4')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->mergeCells('H4:H5'); $objPHPExcel->getActiveSheet()->getStyle('H4')->getAlignment()->setWrapText(true);
		
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(2.14); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(5); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(17); //tgl stokopname
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(31); //nama
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(10); //satuan
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(7); //stok lama
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(7); //stok baru
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(13); //harga
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(13); //nilai
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(8)->setWidth(20); //nilai
		
		for($x='A';$x<='I';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'1')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}

		//debugvar($bulan1);

		
		$objPHPExcel->getActiveSheet()->setCellValue ('A1','FORM STOCK OPNAME ');
		
		$unit=$this->mlaporanapt->namaunit($kd_unit_apt);
		$objPHPExcel->getActiveSheet()->setCellValue ('A2',strtoupper($unit));		
		
		for($x='A';$x<='I';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>10 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->getStyle($x.'5')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>10 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A4','NO');
		$objPHPExcel->getActiveSheet()->setCellValue ('B4','KODE OBAT');
		$objPHPExcel->getActiveSheet()->setCellValue ('C4','NAMA OBAT');
		$objPHPExcel->getActiveSheet()->setCellValue ('D4','TGL EXPIRE');
		$objPHPExcel->getActiveSheet()->setCellValue ('E4','STOK BARU');
		$items=array();
		$items=$this->mlaporanapt->getFormStokopname($kd_unit_apt);
		//debugvar($items);
		$baris=6;
		$nomor=1;
		$total=0;
		//$nilai=0;
		foreach ($items as $item) {
			
			# code...
			for($x='A';$x<='I';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='H'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}	else if($x=='I'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}				
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>10 ,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['kd_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['nama_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['tgl_expire']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,"");
			$baris++;
		}	
		
		
		/*$baris=$baris+2;
		$objPHPExcel->getActiveSheet()->mergeCells('F'.$baris.':H'.$baris);
		//$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,'Balikpapan, '.$tgl.' '.$bln.' '.$tahun);
		$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,'Balikpapan, ' .$bln.' '.$tahun);
		
		$baris=$baris+1;
		$objPHPExcel->getActiveSheet()->mergeCells('F'.$baris.':H'.$baris);
		$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,'Pengguna Anggaran');
		
		$baris=$baris+4;
		$objPHPExcel->getActiveSheet()->mergeCells('F'.$baris.':H'.$baris);
		$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,'Dr. Indah Puspitasari');
		
		$baris=$baris+1;
		$objPHPExcel->getActiveSheet()->mergeCells('F'.$baris.':H'.$baris);
		$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,'NIP. 19670530 199803 2 003');*/
		
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/lapstokopnameobat.xls");
		header("Location: ".base_url()."download/lapstokopnameobat.xls");
	}
	
	public function statuspermintaanobat(){
		if(!$this->muser->isAkses("78")){
			$this->restricted();
			return false;
		}
		//$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		//$nama_unit_apt=$this->input->post('nama_unit_apt');
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$kd_unit_apt='';
		$nama_unit_apt='';
		$permintaan_status='';
		
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}
		if($this->input->post('nama_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('nama_unit_apt');
		}
		if($this->input->post('permintaan_status')!=''){
			$permintaan_status=$this->input->post('permintaan_status');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Status Permintaan Obat :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array(//'items'=>$this->mlaporanapt->getStatusPermintaan($periodeawal,$periodeakhir,$permintaan_status,$kd_unit_apt),
					'unitapotek'=>$this->mlaporanapt->ambilData("apt_unit","kd_unit_apt<>'U01'"),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'permintaan_status'=>$permintaan_status,
					'kd_unit_apt'=>$kd_unit_apt,
					'nama_unit_apt'=>$nama_unit_apt);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/statuspermintaanobat',$data);
		$this->load->view('footer',$datafooter);
	}

	public function datapermintaanobat($periodeawal="NULL",$periodeakhir="NULL",$permintaan_status="NULL",$kd_unit_apt="NULL")
	{
		$kd_unit_aptA=$this->session->userdata('kd_unit_apt');
		$kd_unit_aptB=$this->session->userdata('kd_unit_apt_gudang');
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		$search=$this->input->get_post('sSearch');
		if($permintaan_status==1){ //kalo sudah didistribusi
			$a1="and apt_permintaan_obat_det.jml_req=apt_permintaan_obat_det.jml_distribusi";
		}
		else if($permintaan_status==2){ //kalo udh didistribusi sebagian
			$a1="and apt_permintaan_obat_det.jml_req>apt_permintaan_obat_det.jml_distribusi and apt_permintaan_obat_det.jml_distribusi>0";
		}
		else if($permintaan_status==3){ //kalo belum didistribusi
			$a1="and apt_permintaan_obat_det.jml_distribusi='0'";
		}
		else if($permintaan_status=="NULL"){ //kalo belum datang
			$a1="";
		}
		else{ //kalo semuanya -->permintaan_status=0
			$a1="";
		}
		$a=$a1;
		if(!empty($search)){
			$s=" and apt_obat.nama_obat like '%".$search."%' ";
		}else{
			$s="";
		}

		if($kd_unit_aptA==$kd_unit_aptB){
			if($kd_unit_apt=='' || $kd_unit_apt=="NULL"){
				$this->datatables->select("concat(apt_permintaan_obat.no_permintaan,'/',date_format(apt_permintaan_obat.tgl_permintaan,'%d-%m-%Y')) as tgl_permintaan,
										apt_permintaan_obat_det.kd_obat,apt_obat.nama_obat,apt_satuan_kecil.satuan_kecil,apt_unit.nama_unit_apt,
										apt_permintaan_obat_det.jml_req,apt_permintaan_obat_det.jml_distribusi,(if(jml_distribusi>=jml_req,'Sudah di Distribusi',if(jml_distribusi=0,'Belum di Distribusi','Sudah di Distribusi Sebagian'))) as status,
										'ket' as ket,apt_permintaan_obat.no_permintaan,apt_permintaan_obat_det.urut,apt_permintaan_obat_det.keterangan,
									apt_permintaan_obat_det.tgl_expire",false);
				$this->datatables->from("apt_permintaan_obat,apt_permintaan_obat_det,apt_obat,apt_satuan_kecil,apt_unit");
				$this->datatables->edit_column('ket', '<div class="edit" id="$1||$2">$3</div>', 'apt_permintaan_obat.no_permintaan,apt_permintaan_obat_det.urut,apt_permintaan_obat_det.keterangan');
				$this->datatables->where("date(apt_permintaan_obat.tgl_permintaan) between '$periodeawal1' and '$periodeakhir1' and apt_permintaan_obat_det.no_permintaan=apt_permintaan_obat.no_permintaan and apt_permintaan_obat_det.kd_obat=apt_obat.kd_obat
									and apt_permintaan_obat.kd_unit_apt=apt_unit.kd_unit_apt and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil $a $s",null,false);
			}else{
				$this->datatables->select("concat(apt_permintaan_obat.no_permintaan,'/',date_format(apt_permintaan_obat.tgl_permintaan,'%d-%m-%Y')) as tgl_permintaan,
										apt_permintaan_obat_det.kd_obat,apt_obat.nama_obat,apt_satuan_kecil.satuan_kecil,apt_unit.nama_unit_apt,
										apt_permintaan_obat_det.jml_req,apt_permintaan_obat_det.jml_distribusi,(if(jml_distribusi>=jml_req,'Sudah di Distribusi',if(jml_distribusi=0,'Belum di Distribusi','Sudah di Distribusi Sebagian'))) as status,
										'ket' as ket,apt_permintaan_obat.no_permintaan,apt_permintaan_obat_det.urut,apt_permintaan_obat_det.keterangan,
									apt_permintaan_obat_det.tgl_expire",false);
				$this->datatables->from("apt_permintaan_obat,apt_permintaan_obat_det,apt_obat,apt_satuan_kecil,apt_unit");
				$this->datatables->edit_column('ket', '<div class="edit" id="$1||$2">$3</div>', 'apt_permintaan_obat.no_permintaan,apt_permintaan_obat_det.urut,apt_permintaan_obat_det.keterangan');
				$this->datatables->where("date(apt_permintaan_obat.tgl_permintaan) between '$periodeawal1' and '$periodeakhir1' and apt_permintaan_obat_det.no_permintaan=apt_permintaan_obat.no_permintaan and apt_permintaan_obat_det.kd_obat=apt_obat.kd_obat
									and apt_permintaan_obat.kd_unit_apt=apt_unit.kd_unit_apt and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil and apt_permintaan_obat.kd_unit_apt='$kd_unit_apt' $a $s",null,false);
			}		
		}
		else {
			$kd_unit_apt=$this->session->userdata('kd_unit_apt');
				$this->datatables->select("concat(apt_permintaan_obat.no_permintaan,'/',date_format(apt_permintaan_obat.tgl_permintaan,'%d-%m-%Y')) as tgl_permintaan,
										apt_permintaan_obat_det.kd_obat,apt_obat.nama_obat,apt_satuan_kecil.satuan_kecil,apt_unit.nama_unit_apt,
										apt_permintaan_obat_det.jml_req,apt_permintaan_obat_det.jml_distribusi,(if(jml_distribusi>=jml_req,'Sudah di Distribusi',if(jml_distribusi=0,'Belum di Distribusi','Sudah di Distribusi Sebagian'))) as status,
										'ket' as ket,apt_permintaan_obat.no_permintaan,apt_permintaan_obat_det.urut,apt_permintaan_obat_det.keterangan,
									apt_permintaan_obat_det.tgl_expire",false);
				$this->datatables->from("apt_permintaan_obat,apt_permintaan_obat_det,apt_obat,apt_satuan_kecil,apt_unit");
				$this->datatables->edit_column('ket', '<div class="edit" id="$1||$2">$3</div>', 'apt_permintaan_obat.no_permintaan,apt_permintaan_obat_det.urut,apt_permintaan_obat_det.keterangan');
				$this->datatables->where("date(apt_permintaan_obat.tgl_permintaan) between '$periodeawal1' and '$periodeakhir1' and apt_permintaan_obat_det.no_permintaan=apt_permintaan_obat.no_permintaan and apt_permintaan_obat_det.kd_obat=apt_obat.kd_obat
									and apt_permintaan_obat.kd_unit_apt=apt_unit.kd_unit_apt and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil and apt_permintaan_obat.kd_unit_apt='$kd_unit_apt' $a $s",null,false);
		}

		//$this->db->limit($limit,$offset);

		//$data['result'] = $this->datatables->generate();
		$results = $this->datatables->generate();
		//$data['aaData'] = $results['aaData']->;
		$x=json_decode($results);
		$b=$x->aaData;
		echo ($results);
	}
	
	public function statuspemesananobat(){
		if(!$this->muser->isAkses("90")){
			$this->restricted();
			return false;
		}
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$kd_supplier='';
		//$nama_unit_apt='';
		$pemesanan_status='';
		
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('kd_supplier')!=''){
			$kd_supplier=$this->input->post('kd_supplier');
		}
		/*if($this->input->post('nama_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('nama_unit_apt');
		}*/
		if($this->input->post('pemesanan_status')!=''){
			$pemesanan_status=$this->input->post('pemesanan_status');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Status Pemesanan :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array(//'items'=>$this->mlaporanapt->getStatusPemesanan($periodeawal,$periodeakhir,$pemesanan_status,$kd_supplier),
					'datasupplier'=>$this->mlaporanapt->ambilData("apt_supplier","is_aktif='1'"),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'pemesanan_status'=>$pemesanan_status,
					'kd_supplier'=>$kd_supplier
					//'nama_unit_apt'=>$nama_unit_apt
					);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/statuspemesananobat',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function datapemesananobat($periodeawal="NULL",$periodeakhir="NULL",$pemesanan_status="NULL",$kd_supplier="NULL")
	{
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		$search=$this->input->get_post('sSearch');
		if($pemesanan_status==1){ //kalo sudah datang
			$a1="and apt_pemesanan_detail.qty_kcl=apt_pemesanan_detail.jml_penerimaan";
		}
		else if($pemesanan_status==2){ //kalo udh datang sebagian
			$a1="and apt_pemesanan_detail.qty_kcl>apt_pemesanan_detail.jml_penerimaan and apt_pemesanan_detail.jml_penerimaan>0";
		}
		else if($pemesanan_status==3){ //kalo belum datang
			$a1="and apt_pemesanan_detail.jml_penerimaan='0'";
		}
		else if($pemesanan_status=="NULL"){ //kalo belum datang
			$a1="";
		}
		else{ //kalo semuanya -->pemesanan_status=0
			$a1="";
		}
		$a=$a1;
		if(!empty($search)){
			$s=" and apt_obat.nama_obat like '%".$search."%' ";
		}else{
			$s="";
		}
		if($kd_supplier=='' || $kd_supplier=="NULL"){
			$this->datatables->select("concat(apt_pemesanan.no_pemesanan,'/',date_format(apt_pemesanan.tgl_pemesanan,'%d-%m-%Y')) as tgl_pemesanan,
										apt_pemesanan_detail.kd_obat,apt_obat.nama_obat,apt_satuan_kecil.satuan_kecil,apt_supplier.nama,apt_pemesanan_detail.qty_kcl,
										apt_pemesanan_detail.jml_penerimaan,(if(jml_penerimaan>=qty_kcl,'Sudah Datang',if(jml_penerimaan=0,'Belum Datang','Sudah Datang Sebagian'))) as status,
										'ket' as ket,apt_pemesanan.no_pemesanan,apt_pemesanan_detail.urut,apt_pemesanan_detail.keterangan",false);
			$this->datatables->from("apt_pemesanan,apt_pemesanan_detail,apt_obat,apt_satuan_kecil,apt_supplier");
			$this->datatables->edit_column('ket', '<div class="edit" id="$1||$2">$3</div>', 'apt_pemesanan.no_pemesanan,apt_pemesanan_detail.urut,apt_pemesanan_detail.keterangan');
			$this->datatables->where("date_format(apt_pemesanan.tgl_pemesanan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and apt_pemesanan_detail.no_pemesanan=apt_pemesanan.no_pemesanan and apt_pemesanan_detail.kd_obat=apt_obat.kd_obat
								and apt_pemesanan.kd_supplier=apt_supplier.kd_supplier and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil $a $s",null,false);
		}else{
			$this->datatables->select("concat(apt_pemesanan.no_pemesanan,'/',date_format(apt_pemesanan.tgl_pemesanan,'%d-%m-%Y')) as tgl_pemesanan,
										apt_pemesanan_detail.kd_obat,apt_obat.nama_obat,apt_satuan_kecil.satuan_kecil,apt_supplier.nama,apt_pemesanan_detail.qty_kcl,
										apt_pemesanan_detail.jml_penerimaan,(if(jml_penerimaan>=qty_kcl,'Sudah Datang',if(jml_penerimaan=0,'Belum Datang','Sudah Datang Sebagian'))) as status,
										'ket' as ket,apt_pemesanan.no_pemesanan,apt_pemesanan_detail.urut,apt_pemesanan_detail.keterangan",false);
			$this->datatables->from("apt_pemesanan,apt_pemesanan_detail,apt_obat,apt_satuan_kecil,apt_supplier");
			$this->datatables->edit_column('ket', '<div class="edit" id="$1||$2">$3</div>', 'apt_pemesanan.no_pemesanan,apt_pemesanan_detail.urut,apt_pemesanan_detail.keterangan');
			$this->datatables->where("date_format(apt_pemesanan.tgl_pemesanan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and apt_pemesanan_detail.no_pemesanan=apt_pemesanan.no_pemesanan and apt_pemesanan_detail.kd_obat=apt_obat.kd_obat
								and apt_pemesanan.kd_supplier=apt_supplier.kd_supplier and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil and apt_pemesanan.kd_supplier='$kd_supplier' $a $s",null,false);
		}
			//$this->datatables->where("apt_obat.nama_obat LIKE '%".$search."%' OR apt_supplier.nama LIKE '%".$search."%' ",null,false);

		//$this->db->limit($limit,$offset);

		//$data['result'] = $this->datatables->generate();
		$results = $this->datatables->generate();
		//$data['aaData'] = $results['aaData']->;
		$x=json_decode($results);
		$b=$x->aaData;
		echo ($results);
	}
	
	public function logpenerimaan(){
		if(!$this->muser->isAkses("70")){
			$this->restricted();
			return false;
		}
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$kd_supplier='';
		$jenis='';
		$status='';
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('kd_supplier')!=''){
			$kd_supplier=$this->input->post('kd_supplier');
		}
		if($this->input->post('jenis')!=''){
			$jenis=$this->input->post('jenis');
		}
		if($this->input->post('status')!=''){
			$status=$this->input->post('status');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Laporan Log Penerimaan :: ".$this->title);

		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array('items'=>$this->mlaporanapt->getLog($periodeawal,$periodeakhir,$kd_supplier,$jenis,$status),
					'unitapotek'=>$this->mlaporanapt->ambilData('apt_unit'),
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'kd_supplier'=>$kd_supplier,
					'jenis'=>$jenis,
					'status'=>$status,
					'datasupplier'=>$this->mlaporanapt->ambilData("apt_supplier","is_aktif='1'")
					);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('laporanapotek/logpenerimaan',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function excelmutasiobat($kd_unit_apt="",$bulan="",$tahun=""){
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A2:M2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:M3');
		$objPHPExcel->getActiveSheet()->mergeCells('A4:M4');
		$objPHPExcel->getActiveSheet()->mergeCells('A6:M6');
		$objPHPExcel->getActiveSheet()->mergeCells('A7:M7');
		$objPHPExcel->getActiveSheet()->mergeCells('A8:M8');

		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(2.14); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(4.14); //no
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(32); //nama obat
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(8); //saldo awal
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(9); //harga
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(13); //total awal
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(6); //pbf masuk
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(6); //unit masuk
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(6); //retur masuk
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(7); //jml masuk
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(10)->setWidth(13); //total masuk
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(6); //resep keluar
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(8)->setWidth(6); //unit keluar
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(9)->setWidth(6); //retur keluar
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(10)->setWidth(7); //jml keluar
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(15)->setWidth(13); //total keluar
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(11)->setWidth(7); //stokopname
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(12)->setWidth(8); //saldo akhir
		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(18)->setWidth(13); //total akhir
		
		for($x='A';$x<='M';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A2','RUMAH SAKIT');
		$objPHPExcel->getActiveSheet()->setCellValue ('A3','IBNU SINA');
		$objPHPExcel->getActiveSheet()->setCellValue ('A4','Kota Balikpapan');		
		$objPHPExcel->getActiveSheet()->setCellValue ('A6','LAPORAN MUTASI OBAT');		
		$bulan1="";
		if($bulan=='01'){$bulan1='Januari';} 	 if($bulan=='02'){$bulan1='Februari';}	if($bulan=='03'){$bulan1='Maret';} 	  if($bulan=='04'){$bulan1='April';}
		if($bulan=='05'){$bulan1='Mei';} 		 if($bulan=='06'){$bulan1='Juni';} 		if($bulan=='07'){$bulan1='Juli';} 	  if($bulan=='08'){$bulan1='Agustus';}
		if($bulan=='09'){$bulan1='September';}   if($bulan=='10'){$bulan1='Oktober';}	if($bulan=='11'){$bulan1='November';} if($bulan=='12'){$bulan1='Desember';}
		$objPHPExcel->getActiveSheet()->setCellValue ('A7',$bulan1.' '.$tahun);
		$namaunit=$this->mlaporanapt->namaUnit($kd_unit_apt);
		$objPHPExcel->getActiveSheet()->setCellValue ('A8',$namaunit);
		
		for($x='A';$x<='M';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			$objPHPExcel->getActiveSheet()->getStyle($x.'11')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>10 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->getStyle($x.'11')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>10 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->mergeCells('A10:A11'); 	//$objPHPExcel->getActiveSheet()->getStyle('B10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('A10','NO');
		$objPHPExcel->getActiveSheet()->mergeCells('B10:B11');  $objPHPExcel->getActiveSheet()->setCellValue ('B10','NAMA OBAT');
		$objPHPExcel->getActiveSheet()->mergeCells('C10:C11');  $objPHPExcel->getActiveSheet()->getStyle('C10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('C10','SLD. AWL');
		/*$objPHPExcel->getActiveSheet()->mergeCells('E10:E11');  $objPHPExcel->getActiveSheet()->setCellValue ('E10','HARGA');
		$objPHPExcel->getActiveSheet()->mergeCells('F10:F11');  $objPHPExcel->getActiveSheet()->getStyle('F10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('F10','TOTAL AWAL');*/
		$objPHPExcel->getActiveSheet()->mergeCells('D10:G10');  $objPHPExcel->getActiveSheet()->setCellValue ('D10','MASUK');
		$objPHPExcel->getActiveSheet()->setCellValue ('D11','PBF'); 
		$objPHPExcel->getActiveSheet()->setCellValue ('E11','Unit');
		$objPHPExcel->getActiveSheet()->setCellValue ('F11','Retur');
		$objPHPExcel->getActiveSheet()->setCellValue ('G11','Jml.');
		/*$objPHPExcel->getActiveSheet()->mergeCells('K10:K11');  $objPHPExcel->getActiveSheet()->getStyle('K10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('K10','TOTAL MASUK');*/
		$objPHPExcel->getActiveSheet()->mergeCells('H10:K10');  $objPHPExcel->getActiveSheet()->setCellValue ('H10','KELUAR');
		$objPHPExcel->getActiveSheet()->setCellValue ('H11','Resep');
		$objPHPExcel->getActiveSheet()->setCellValue ('I11','Unit');
		$objPHPExcel->getActiveSheet()->setCellValue ('J11','Retur');
		$objPHPExcel->getActiveSheet()->setCellValue ('K11','Jml.');
		/*$objPHPExcel->getActiveSheet()->mergeCells('P10:P11');  $objPHPExcel->getActiveSheet()->getStyle('P10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('P10','TOTAL KELUAR');*/
		$objPHPExcel->getActiveSheet()->mergeCells('L10:L11');  $objPHPExcel->getActiveSheet()->getStyle('L10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('L10','STOKOPNAME');
		$objPHPExcel->getActiveSheet()->mergeCells('M10:M11');  $objPHPExcel->getActiveSheet()->getStyle('M10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('M10','SLD. AKR');
		/*$objPHPExcel->getActiveSheet()->mergeCells('S10:S11');  $objPHPExcel->getActiveSheet()->getStyle('S10')->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->setCellValue ('S10','TOTAL AKHIR');*/
		$items=array();
		$items=$this->mlaporanapt->ambilMutasiObat($kd_unit_apt,$bulan,$tahun);
		$baris=12;
		$nomor=1;
		$sumtotalawal=0; $sumtotalmasuk=0; $sumtotalkeluar=0;  $sumtotalakhir=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='M';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='H'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='I'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='J'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='K'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='L'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='M'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>10 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['nama_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['saldo_awal']); 	/*$sldawl=$item['saldo_awal'];
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,number_format($item['harga_beli'])); 	$hrg=$item['harga_beli'];	$totalawal=$sldawl*$hrg;
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,number_format($totalawal));*/
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['in_pbf']); 		$pbf=$item['in_pbf'];
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['in_unit']); 		$inunit=$item['in_unit'];
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['retur_jual']); 	$retkeluar=$item['retur_jual'];$jml=$pbf+$inunit+$retkeluar; //$retpbf=$item['retur_pbf'];$jml=($pbf+$inunit)-$retpbf;
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$jml);					/*$totalmasuk=$hrg*$jml;
			$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,number_format($totalmasuk));*/
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$item['out_jual']); 	$resep=$item['out_jual'];
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$item['out_unit']); 	$outunit=$item['out_unit']; $retpbf=$item['retur_pbf'];//$retkeluar=0;
			$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,$retpbf); 			$jml1=$resep+$outunit+$retpbf;
			$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,$jml1); 				/*$totalkeluar=$hrg*$jml1;
			$objPHPExcel->getActiveSheet()->setCellValue ('P'.$baris,number_format($totalkeluar));*/
			$objPHPExcel->getActiveSheet()->setCellValue ('L'.$baris,$item['stok_opname']);
			$objPHPExcel->getActiveSheet()->setCellValue ('M'.$baris,$item['saldo_akhir']); 	/*$saldoakhir=$item['saldo_akhir']; $totalakhir=$hrg*$saldoakhir;
			$objPHPExcel->getActiveSheet()->setCellValue ('S'.$baris,number_format($totalakhir));*/
			$nomor=$nomor+1; $baris=$baris+1; 
			//$sumtotalawal=$sumtotalawal+$totalawal; $sumtotalmasuk=$sumtotalmasuk+$totalmasuk; $sumtotalkeluar=$sumtotalkeluar+$totalkeluar; $sumtotalakhir=$sumtotalakhir+$totalakhir; 
		}
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/mutasiobat.xls");
		header("Location: ".base_url()."download/mutasiobat.xls");
	}

	public function statuspermintaanobatxls($periodeawal="",$periodeakhir="",$permintaan_status="",$kd_unit_apt=""){
		if($periodeawal=="NULL")$periodeawal="";
		if($periodeakhir=="NULL")$periodeakhir="";
		if($kd_unit_apt=="NULL")$kd_unit_apt="";
		if($permintaan_status=="NULL")$permintaan_status="";

		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath('images/logo1.jpg');
		$objDrawing->setHeight(70);
		$objDrawing->setCoordinates('A2');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");

		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(2.14); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(6); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(20); //kode
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(15); //nama obat
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(10); //satuan
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(35); //tgl expire
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(15); //stok
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(20); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(10); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(8)->setWidth(10); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(9)->setWidth(20); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(10)->setWidth(30); //harga beli
		//debugvar($kd_unit_apt);
		for($x='A';$x<='K';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}		

		$q=$this->db->query("select profil.nama_profil,profil.alamat_profil,kelurahan.kelurahan,profil.kota from profil,kelurahan 
								where profil.kd_kelurahan=kelurahan.kd_kelurahan");
		$qr=$q->row_array();

		$objPHPExcel->getActiveSheet()->setCellValue ('C2',$qr['nama_profil']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C3',$qr['alamat_profil'].' '.$qr['kelurahan']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C4','Kota '.$qr['kota']);		
		$objPHPExcel->getActiveSheet()->setCellValue ('C6','LAPORAN STATUS PERMINTAAN OBAT / ALKES');		
		
		$namaunit=$this->mlaporanapt->namaUnit($kd_unit_apt);
		if($namaunit=='0' or $namaunit=='') {$objPHPExcel->getActiveSheet()->setCellValue ('C7','Unit : Semua Unit Apotek');}
		else {$objPHPExcel->getActiveSheet()->setCellValue ('C7','Unit : '.$namaunit);}
								
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A10','NO');
		$objPHPExcel->getActiveSheet()->setCellValue ('B10','No Permintaan');
		$objPHPExcel->getActiveSheet()->setCellValue ('C10','Tgl Permintaan');
		$objPHPExcel->getActiveSheet()->setCellValue ('D10','Kode Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('E10','Nama Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('F10','Satuan');
		$objPHPExcel->getActiveSheet()->setCellValue ('G10','Unit');
		$objPHPExcel->getActiveSheet()->setCellValue ('H10','Jml Permintaan');
		$objPHPExcel->getActiveSheet()->setCellValue ('I10','Jml Distribusi');
		$objPHPExcel->getActiveSheet()->setCellValue ('J10','Status');
		$objPHPExcel->getActiveSheet()->setCellValue ('K10','Keterangan');
		$items=array();
		$items=$this->mlaporanapt->getStatusPermintaan($periodeawal,$periodeakhir,$permintaan_status,$kd_unit_apt);
		$baris=11;
		$nomor=1;
		$total=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='K';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}
			$ab=$item['jml_req'];
			$ac=$item['jml_distribusi'];
			if($ac>=$ab){$status="Sudah Didistribusi";}
			else if($ac==0){$status="Belum Didistribusi";}
			else {$status="Sudah Didistribusi Sebagian";}
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['no_permintaan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['tgl_permintaan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,"'".$item['kd_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['nama_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['satuan_kecil']);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['nama_unit_apt']);
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$item['jml_req']);
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$item['jml_distribusi']);
			$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,$status);
			$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,$item['keterangan']);
					
			$nomor=$nomor+1; $baris=$baris+1; 
			//$total=$total+$nilai;
		}	
		for($x='A';$x<='K';$x++){
			if($x=='A'){
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
			}else if($x=='H'){
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
			}				
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
				'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => '000000')))));
		}
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/statuspermintaanobat.xls");
		header("Location: ".base_url()."download/statuspermintaanobat.xls");
	}

	public function statuspemesananobatxls($periodeawal="",$periodeakhir="",$pemesanan_status="",$kd_supplier=""){
		if($periodeawal=="NULL")$periodeawal="";
		if($periodeakhir=="NULL")$periodeakhir="";
		if($kd_supplier=="NULL")$kd_supplier="";
		if($pemesanan_status=="NULL")$pemesanan_status="";

		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath('images/logo1.jpg');
		$objDrawing->setHeight(70);
		$objDrawing->setCoordinates('A2');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");

		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(2.14); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(6); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(20); //kode
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(15); //nama obat
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(10); //satuan
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(35); //tgl expire
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(15); //stok
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(20); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(10); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(8)->setWidth(10); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(9)->setWidth(20); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(10)->setWidth(30); //harga beli
		//debugvar($kd_unit_apt);
		for($x='A';$x<='K';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}		

		$q=$this->db->query("select profil.nama_profil,profil.alamat_profil,kelurahan.kelurahan,profil.kota from profil,kelurahan 
								where profil.kd_kelurahan=kelurahan.kd_kelurahan");
		$qr=$q->row_array();

		$objPHPExcel->getActiveSheet()->setCellValue ('C2',$qr['nama_profil']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C3',$qr['alamat_profil'].' '.$qr['kelurahan']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C4','Kota '.$qr['kota']);		
		$objPHPExcel->getActiveSheet()->setCellValue ('C6','LAPORAN STATUS PEMESANAN OBAT / ALKES');		
		
		$namaunit=$this->mlaporanapt->namaSupplier($kd_supplier);
		if($namaunit=='0' or $namaunit=='') {$objPHPExcel->getActiveSheet()->setCellValue ('C7','Supplier : Semua');}
		else {$objPHPExcel->getActiveSheet()->setCellValue ('C7','Unit : '.$namaunit);}
								
		for($x='A';$x<='K';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A10','NO');
		$objPHPExcel->getActiveSheet()->setCellValue ('B10','No Pemesanan');
		$objPHPExcel->getActiveSheet()->setCellValue ('C10','Tgl Pemesanan');
		$objPHPExcel->getActiveSheet()->setCellValue ('D10','Kode Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('E10','Nama Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('F10','Satuan');
		$objPHPExcel->getActiveSheet()->setCellValue ('G10','Supplier');
		$objPHPExcel->getActiveSheet()->setCellValue ('H10','Jml Pesan');
		$objPHPExcel->getActiveSheet()->setCellValue ('I10','Jml Terima');
		$objPHPExcel->getActiveSheet()->setCellValue ('J10','Status');
		$objPHPExcel->getActiveSheet()->setCellValue ('K10','Keterangan');
		$items=array();
		$items=$this->mlaporanapt->getStatusPemesanan($periodeawal,$periodeakhir,$pemesanan_status,$kd_supplier);
		$baris=11;
		$nomor=1;
		$total=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='K';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}
			$ab=$item['qty_kcl'];
			$ac=$item['jml_penerimaan'];
			if($ac>=$ab){$status="Sudah Datang";}
			else if($ac==0){$status="Belum Datang";}
			else {$status="Sudah Datang Sebagian";}
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['no_pemesanan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['tgl_pemesanan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,"'".$item['kd_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['nama_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['satuan_kecil']);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['nama']);
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$item['qty_kcl']);
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$item['jml_penerimaan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,$status);
			$objPHPExcel->getActiveSheet()->setCellValue ('K'.$baris,$item['keterangan']);
					
			$nomor=$nomor+1; $baris=$baris+1; 
			//$total=$total+$nilai;
		}	
		for($x='A';$x<='K';$x++){
			if($x=='A'){
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
			}else if($x=='H'){
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
			}				
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
				'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => '000000')))));
		}
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/statuspemesananobat.xls");
		header("Location: ".base_url()."download/statuspemesananobat.xls");
	}

	public function returunitxls($periodeawal="",$periodeakhir="",$kd_unit_apt=""){
		if($periodeawal=="NULL")$periodeawal="";
		if($periodeakhir=="NULL")$periodeakhir="";
		if($kd_unit_apt=="NULL")$kd_unit_apt="";

		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Logo');
		$objDrawing->setDescription('Logo');
		$objDrawing->setPath('images/logo1.jpg');
		$objDrawing->setHeight(70);
		$objDrawing->setCoordinates('A2');
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");

		//$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(2.14); 
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(6); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(20); //kode
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(15); //nama obat
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(10); //satuan
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(35); //tgl expire
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(15); //stok
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(20); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(10); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(8)->setWidth(10); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(9)->setWidth(20); //harga beli
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(10)->setWidth(30); //harga beli
		//debugvar($kd_unit_apt);
		for($x='A';$x<='J';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='J';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='J';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='J';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='J';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='J';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}		

		$q=$this->db->query("select profil.nama_profil,profil.alamat_profil,kelurahan.kelurahan,profil.kota from profil,kelurahan 
								where profil.kd_kelurahan=kelurahan.kd_kelurahan");
		$qr=$q->row_array();

		$objPHPExcel->getActiveSheet()->setCellValue ('C2',$qr['nama_profil']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C3',$qr['alamat_profil'].' '.$qr['kelurahan']);
		$objPHPExcel->getActiveSheet()->setCellValue ('C4','Kota '.$qr['kota']);		
		$objPHPExcel->getActiveSheet()->setCellValue ('C6','LAPORAN RETUR UNIT KE GUDANG');		
		
		$namaunit=$this->mlaporanapt->namaUnit($kd_unit_apt);
		if($namaunit=='0' or $namaunit=='') {$objPHPExcel->getActiveSheet()->setCellValue ('C7','Unit : Semua Unit Apotek');}
		else {$objPHPExcel->getActiveSheet()->setCellValue ('C7','Unit : '.$namaunit);}
								
		for($x='A';$x<='J';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>11 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A10','NO');
		$objPHPExcel->getActiveSheet()->setCellValue ('B10','No Retur');
		$objPHPExcel->getActiveSheet()->setCellValue ('C10','Tgl Retur');
		$objPHPExcel->getActiveSheet()->setCellValue ('D10','Kode Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('E10','Nama Obat');
		$objPHPExcel->getActiveSheet()->setCellValue ('F10','Satuan');
		$objPHPExcel->getActiveSheet()->setCellValue ('G10','Unit');
		$objPHPExcel->getActiveSheet()->setCellValue ('H10','Jml');
		$objPHPExcel->getActiveSheet()->setCellValue ('I10','Harga Beli');
		$objPHPExcel->getActiveSheet()->setCellValue ('J10','Alasan');
		$items=array();
		$items=$this->mlaporanapt->getAllReturApotek($periodeawal,$periodeakhir,$kd_unit_apt);
		$baris=11;
		$nomor=1;
		$total=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='J';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$nomor);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['no_retur']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['tanggal']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,"'".$item['kd_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['nama_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['satuan_kecil']);
			$objPHPExcel->getActiveSheet()->setCellValue ('G'.$baris,$item['nama_unit_apt']);
			$objPHPExcel->getActiveSheet()->setCellValue ('H'.$baris,$item['qty']);
			$objPHPExcel->getActiveSheet()->setCellValue ('I'.$baris,$item['harga_beli']);
			$objPHPExcel->getActiveSheet()->setCellValue ('J'.$baris,$item['alasan']);
					
			$nomor=$nomor+1; $baris=$baris+1; 
			//$total=$total+$nilai;
		}	
		for($x='A';$x<='J';$x++){
			if($x=='A'){
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
			}else if($x=='H'){
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
			}				
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
				'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('rgb' => '000000')))));
		}
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/lapreturunit.xls");
		header("Location: ".base_url()."download/lapreturunit.xls");
	}
	
	function simpanstatuspermintaan(){
		$id=$this->input->post('id');
		$value=$this->input->post('value');
		$idobat=explode("||", $id);
		$data=array('keterangan'=>$value);
		$this->mlaporanapt->update('apt_permintaan_obat_det',$data,'no_permintaan="'.$idobat[0].'" and urut="'.$idobat[1].'" ');
		echo $value;
	}	

	function simpanstatuspemesanan(){
		$id=$this->input->post('id');
		$value=$this->input->post('value');
		$idobat=explode("||", $id);
		$data=array('keterangan'=>$value);
		$this->mlaporanapt->update('apt_pemesanan_detail',$data,'no_pemesanan="'.$idobat[0].'" and urut="'.$idobat[1].'" ');
		echo $value;
	}	

	public function rl1excelkunjunganharian($tahun="",$bulan="",$kd_unit=""){
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		//$objPHPExcel = new PHPExcel(); 
		$objPHPExcel = IOFactory::load('download/templaterekapharian.xls');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:Z2');
		$objPHPExcel->getActiveSheet()->setCellValue ('A2','LAPORAN REKAP KUNJUNGAN HARIAN BULAN '.$bulan.' TAHUN '.$tahun.' ');

		$a_date = $tahun."-".$bulan."-01";
		$date = new DateTime($a_date);
		$date->modify('last day of this month');
		$lastday=$date->format('d');
		$total=array();                                                         
		for ($x=1; $x<=$lastday;$x++) {
			# code...
			$total[$x.'_1']=0;
			$total[$x.'_2']=0;
			$total[$x.'_3']=0;
		}

		$items=array();
		$items=$this->mlaporanapt->rekapkunjunganharian($tahun,$bulan);
		//debugvar($items);
		$baris=6;
		$nomor=1;
        $no=1;
		foreach ($items as $item) {
	        if($item['type']!=$tipesebelumnya && $no>1)$baris++;
	        $tipesebelumnya=$item['type'];
	        $no++;

			# code...
			for($x='A';$x<='C';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$item['bulan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['tahun']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['customer']);
			$header='D';
			$headerplus='A';
		    $a_date = $tahun."-".$bulan."-01";
		    $date = new DateTime($a_date);
		    $date->modify('last day of this month');
		    $lastday=$date->format('d');
		    $totalsamping=0;
			for ($y=1; $y<=$lastday;$y++) {
				$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item[$y.'_1']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item[$y.'_2']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item[$y.'_3']);
                $total[$y.'_1']=$total[$y.'_1']+$item[$y.'_1'];
                $total[$y.'_2']=$total[$y.'_2']+$item[$y.'_2'];
                $total[$y.'_3']=$total[$y.'_3']+$item[$y.'_3'];
                $totalsamping=$totalsamping+($item[$y.'_1']+$item[$y.'_2']+$item[$y.'_3']);	

			$objPHPExcel->getActiveSheet()->getStyle('CS'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));

				$objPHPExcel->getActiveSheet()->setCellValue ('CS'.$baris,$totalsamping);
				$header++;
			}
			$nomor=$nomor+1; $baris=$baris+1;
		}	

			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':C'.$baris);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle('A'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->getStyle('B'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->getStyle('C'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'T O T A L');
			$header='D';
			$headerplus='A';
		    $a_date = $tahun."-".$bulan."-01";
		    $date = new DateTime($a_date);
		    $date->modify('last day of this month');
		    $lastday=$date->format('d');
		    $totalsamping=0;
			for ($y=1; $y<=$lastday;$y++) {
				$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$total[$y.'_1']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$total[$y.'_2']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$total[$y.'_3']);
				$totalsemua=$totalsemua+($total[$y.'_1']+$total[$y.'_2']+$total[$y.'_3']);

				$objPHPExcel->getActiveSheet()->getStyle('CS'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));

					$objPHPExcel->getActiveSheet()->setCellValue ('CS'.$baris,$totalsemua);

				$header++;
			}










		$a_date = $tahun."-".$bulan."-01";
		$date = new DateTime($a_date);
		$date->modify('last day of this month');
		$lastday=$date->format('d');
		$total=array();                                                         
		for ($x=1; $x<=$lastday;$x++) {
			# code...
			$total[$x.'_1']=0;
			$total[$x.'_2']=0;
			$total[$x.'_3']=0;
		}

		$items2=array();
		$items2=$this->mlaporanapt->rekapkunjunganharianbytipe($tahun,$bulan);
		//debugvar($items);
		$baris=37;
		$nomor=1;
		$totalsemua=0;
		foreach ($items2 as $item2) {

			# code...
			for($x='A';$x<='C';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$item2['bulan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item2['tahun']);
	        if($item2['type']==0)$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'Tunai'); 
	        if($item2['type']==1)$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'Asuransi'); 
	        if($item2['type']==2)$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'Perusahaan'); 
	        if($item2['type']==3)$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'Sosial Dakwah'); 
	        if($item2['type']==4)$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'BPJS'); 
			//$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['customer']);
			$header='D';
			$headerplus='A';
		    $a_date = $tahun."-".$bulan."-01";
		    $date = new DateTime($a_date);
		    $date->modify('last day of this month');
		    $lastday=$date->format('d');
		    $totalsamping=0;
			for ($y=1; $y<=$lastday;$y++) {
				$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item2[$y.'_1']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item2[$y.'_2']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item2[$y.'_3']);
                $total[$y.'_1']=$total[$y.'_1']+$item2[$y.'_1'];
                $total[$y.'_2']=$total[$y.'_2']+$item2[$y.'_2'];
                $total[$y.'_3']=$total[$y.'_3']+$item2[$y.'_3'];
                $totalsamping=$totalsamping+($item2[$y.'_1']+$item2[$y.'_2']+$item2[$y.'_3']);	

			$objPHPExcel->getActiveSheet()->getStyle('CS'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));

				$objPHPExcel->getActiveSheet()->setCellValue ('CS'.$baris,$totalsamping);
				$header++;
			}
			$nomor=$nomor+1; $baris=$baris+1;
		}	

			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':C'.$baris);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle('A'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->getStyle('B'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->getStyle('C'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'T O T A L');
			$header='D';
			$headerplus='A';
		    $a_date = $tahun."-".$bulan."-01";
		    $date = new DateTime($a_date);
		    $date->modify('last day of this month');
		    $lastday=$date->format('d');
		    $totalsamping=0;
			for ($y=1; $y<=$lastday;$y++) {
				$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$total[$y.'_1']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$total[$y.'_2']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$total[$y.'_3']);
				$totalsemua=$totalsemua+($total[$y.'_1']+$total[$y.'_2']+$total[$y.'_3']);

				$objPHPExcel->getActiveSheet()->getStyle('CS'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));

					$objPHPExcel->getActiveSheet()->setCellValue ('CS'.$baris,$totalsemua);

				$header++;
			}

		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/rekapkunjunganharian.xls");
		header("Location: ".base_url()."download/rekapkunjunganharian.xls");
	}

	public function rl1excelpendapatanharian($tahun="",$bulan="",$kd_unit=""){
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		//$objPHPExcel = new PHPExcel(); 
		$objPHPExcel = IOFactory::load('download/templaterekapharian.xls');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:Z2');
		$objPHPExcel->getActiveSheet()->setCellValue ('A2','LAPORAN REKAP KUNJUNGAN HARIAN BULAN '.$bulan.' TAHUN '.$tahun.' ');

		$a_date = $tahun."-".$bulan."-01";
		$date = new DateTime($a_date);
		$date->modify('last day of this month');
		$lastday=$date->format('d');
		$total=array();                                                         
		for ($x=1; $x<=$lastday;$x++) {
			# code...
			$total[$x.'_1']=0;
			$total[$x.'_2']=0;
			$total[$x.'_3']=0;
		}

		$items=array();
		$items=$this->mlaporanapt->rekappendapatanharian($tahun,$bulan);
		//debugvar($items);
		$baris=6;
		$nomor=1;
        $no=1;
		foreach ($items as $item) {
	        if($item['type']!=$tipesebelumnya && $no>1)$baris++;
	        $tipesebelumnya=$item['type'];
	        $no++;

			# code...
			for($x='A';$x<='C';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$item['bulan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['tahun']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['customer']);
			$header='D';
			$headerplus='A';
		    $a_date = $tahun."-".$bulan."-01";
		    $date = new DateTime($a_date);
		    $date->modify('last day of this month');
		    $lastday=$date->format('d');
		    $totalsamping=0;
			for ($y=1; $y<=$lastday;$y++) {
				$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item[$y.'_1']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item[$y.'_2']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item[$y.'_3']);
                $total[$y.'_1']=$total[$y.'_1']+$item[$y.'_1'];
                $total[$y.'_2']=$total[$y.'_2']+$item[$y.'_2'];
                $total[$y.'_3']=$total[$y.'_3']+$item[$y.'_3'];
                $totalsamping=$totalsamping+($item[$y.'_1']+$item[$y.'_2']+$item[$y.'_3']);	

			$objPHPExcel->getActiveSheet()->getStyle('CS'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));

				$objPHPExcel->getActiveSheet()->setCellValue ('CS'.$baris,$totalsamping);
				$header++;
			}
			$nomor=$nomor+1; $baris=$baris+1;
		}	

			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':C'.$baris);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle('A'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->getStyle('B'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->getStyle('C'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'T O T A L');
			$header='D';
			$headerplus='A';
		    $a_date = $tahun."-".$bulan."-01";
		    $date = new DateTime($a_date);
		    $date->modify('last day of this month');
		    $lastday=$date->format('d');
		    $totalsamping=0;
			for ($y=1; $y<=$lastday;$y++) {
				$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$total[$y.'_1']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$total[$y.'_2']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$total[$y.'_3']);
				$totalsemua=$totalsemua+($total[$y.'_1']+$total[$y.'_2']+$total[$y.'_3']);

				$objPHPExcel->getActiveSheet()->getStyle('CS'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));

					$objPHPExcel->getActiveSheet()->setCellValue ('CS'.$baris,$totalsemua);

				$header++;
			}










		$a_date = $tahun."-".$bulan."-01";
		$date = new DateTime($a_date);
		$date->modify('last day of this month');
		$lastday=$date->format('d');
		$total=array();                                                         
		for ($x=1; $x<=$lastday;$x++) {
			# code...
			$total[$x.'_1']=0;
			$total[$x.'_2']=0;
			$total[$x.'_3']=0;
		}

		$items2=array();
		$items2=$this->mlaporanapt->rekappendapatanharianbytipe($tahun,$bulan);
		//debugvar($items);
		$baris=37;
		$nomor=1;
		$totalsemua=0;
		foreach ($items2 as $item2) {

			# code...
			for($x='A';$x<='C';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$item2['bulan']);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item2['tahun']);
	        if($item2['type']==0)$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'Tunai'); 
	        if($item2['type']==1)$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'Asuransi'); 
	        if($item2['type']==2)$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'Perusahaan'); 
	        if($item2['type']==3)$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'Sosial Dakwah'); 
	        if($item2['type']==4)$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,'BPJS'); 
			//$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['customer']);
			$header='D';
			$headerplus='A';
		    $a_date = $tahun."-".$bulan."-01";
		    $date = new DateTime($a_date);
		    $date->modify('last day of this month');
		    $lastday=$date->format('d');
		    $totalsamping=0;
			for ($y=1; $y<=$lastday;$y++) {
				$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item2[$y.'_1']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item2[$y.'_2']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item2[$y.'_3']);
                $total[$y.'_1']=$total[$y.'_1']+$item2[$y.'_1'];
                $total[$y.'_2']=$total[$y.'_2']+$item2[$y.'_2'];
                $total[$y.'_3']=$total[$y.'_3']+$item2[$y.'_3'];
                $totalsamping=$totalsamping+($item2[$y.'_1']+$item2[$y.'_2']+$item2[$y.'_3']);	

			$objPHPExcel->getActiveSheet()->getStyle('CS'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));

				$objPHPExcel->getActiveSheet()->setCellValue ('CS'.$baris,$totalsamping);
				$header++;
			}
			$nomor=$nomor+1; $baris=$baris+1;
		}	

			$objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':C'.$baris);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle('A'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->getStyle('B'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->getStyle('C'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'T O T A L');
			$header='D';
			$headerplus='A';
		    $a_date = $tahun."-".$bulan."-01";
		    $date = new DateTime($a_date);
		    $date->modify('last day of this month');
		    $lastday=$date->format('d');
		    $totalsamping=0;
			for ($y=1; $y<=$lastday;$y++) {
				$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$total[$y.'_1']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$total[$y.'_2']);
				$header++;
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$total[$y.'_3']);
				$totalsemua=$totalsemua+($total[$y.'_1']+$total[$y.'_2']+$total[$y.'_3']);

				$objPHPExcel->getActiveSheet()->getStyle('CS'.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));

					$objPHPExcel->getActiveSheet()->setCellValue ('CS'.$baris,$totalsemua);

				$header++;
			}

		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/rekappendapatanharian.xls");
		header("Location: ".base_url()."download/rekappendapatanharian.xls");
	}

	public function rl1excelkunjunganbulanan($tahun="",$bulan1="",$bulan2="",$kd_unit=""){

		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:G3');
		$objPHPExcel->getActiveSheet()->mergeCells('A4:G4');
		$objPHPExcel->getActiveSheet()->mergeCells('A6:G6');
		$objPHPExcel->getActiveSheet()->mergeCells('A7:G7');
		$objPHPExcel->getActiveSheet()->mergeCells('A8:G8');
				
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(10); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(10); //NODISTRIBUSI
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(40);  //TGL DISTRIBUSI
		
		for($x='A';$x<='G';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='G';$x++){
					$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
				}
		for($x='A';$x<='G';$x++){
					$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
				}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A2','RUMAH SAKIT ');
		$objPHPExcel->getActiveSheet()->setCellValue ('A3','IBNU SINA');
		$objPHPExcel->getActiveSheet()->setCellValue ('A4','Kota Balikpapan');		
		$objPHPExcel->getActiveSheet()->setCellValue ('A6','LAPORAN REKAP KUNJUNGAN BULANAN');
		
		if($kd_unit==''){ $objPHPExcel->getActiveSheet()->setCellValue ('A7','Unit : Semua Unit Apotek');}
		else{
			$namaunit=$this->mlaporanapt->namaUnit($kd_unit);
			$objPHPExcel->getActiveSheet()->setCellValue ('A7','Unit : '.$namaunit);
		}
		
		$objPHPExcel->getActiveSheet()->setCellValue ('A8','Periode :  '.$bulan1.'-'.$bulan2.' '.$tahun);

		for($x='A';$x<='C';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A10','Tahun');
		$objPHPExcel->getActiveSheet()->setCellValue ('B10','Jenis Pasien');
		$header='C';
		$headerplus='A';
		$totalbawah=array();
	    for ($x=intval($bulan1); $x<=$bulan2;$x++) {
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
			$objPHPExcel->getActiveSheet()->getStyle($header.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($header.'10')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ($header.'10',$x);
			$totalbawah[$x]=0;
			$header++;
	    }
			$objPHPExcel->getActiveSheet()->getStyle($header.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($header.'10')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ($header.'10','TOTAL');
		$items=array();
		$items=$this->mlaporanapt->rekapkunjunganbulanan($tahun,$bulan1,$bulan2);
		$baris=11;
		$nomor=1;
		$total=0;
		$no=1;
		foreach ($items as $item) {
	        if($item['type']!=$tipesebelumnya && $no>1)$baris++;
	        $tipesebelumnya=$item['type'];
	        $no++;
			# code...
			for($x='A';$x<='B';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$item['tahun']);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['customer']);
			$header='C';
			$headerplus='A';
			$totalsamping=0;
			for ($y=intval($bulan1); $y<=$bulan2;$y++) {
				$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item[$y]);
				$totalbawah[$y]=$totalbawah[$y]+$item[$y];
				$totalsamping=$totalsamping+$item[$y];
				$header++;
			}
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$totalsamping);
			$nomor=$nomor+1; $baris=$baris+1;
		}

		$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'');
		$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'T O T A L');
		$header='C';
		$headerplus='A';
		$totalsamping=0;
	    for ($x=intval($bulan1); $x<=$bulan2;$x++) {
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$totalbawah[$x]);
			$totalsamping=$totalsamping+$totalbawah[$x];
			$header++;
	    }
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$totalsamping);

		$baris=$baris+5;

		for($x='A';$x<='C';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'Tahun');
		$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'Jenis Pasien');
		$header='C';
		$headerplus='A';
        $totalbawah=array();
	    for ($x=intval($bulan1); $x<=$bulan2;$x++) {
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$x);
			$totalbawah[$x]=0;
			$header++;
	    }
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,'TOTAL');
		$items2=array();
		$items2=$this->mlaporanapt->rekapkunjunganbulananbytipe($tahun,$bulan1,$bulan2);
		$baris=$baris+1;
		$nomor=1;
		$total=0;
		$no=1;
		foreach ($items2 as $item2) {
			# code...
			for($x='A';$x<='B';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$item['tahun']);
			if($item2['type']==0)$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'Tunai');
	        if($item2['type']==1)$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'Asuransi');
	        if($item2['type']==2)$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'Perusahaan');
	        if($item2['type']==3)$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'Sosial Dakwah');;
	        if($item2['type']==4)$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'BPJS');
			//$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['customer']);
			$header='C';
			$headerplus='A';
			$totalsamping=0;
			for ($y=intval($bulan1); $y<=$bulan2;$y++) {
				$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item2[$y]);
				$totalsamping=$totalsamping+$item2[$y];
				$header++;
			}
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$totalsamping);
			$nomor=$nomor+1; $baris=$baris+1;
		}	




		$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'');
		$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'T O T A L');
		$header='C';
		$headerplus='A';
		$totalsamping=0;
	    for ($x=intval($bulan1); $x<=$bulan2;$x++) {
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$totalbawah[$x]);
			$totalbawah[$y]=$totalbawah[$y]+$item[$y];
			$totalsamping=$totalsamping+$totalbawah[$x];
			$header++;
	    }
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$totalsamping);



		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/rekapkunjunganbulanan.xls");
		header("Location: ".base_url()."download/rekapkunjunganbulanan.xls");
	}
	
	public function rl1excelpendapatanbulanan($tahun="",$bulan1="",$bulan2="",$kd_unit=""){

		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:G3');
		$objPHPExcel->getActiveSheet()->mergeCells('A4:G4');
		$objPHPExcel->getActiveSheet()->mergeCells('A6:G6');
		$objPHPExcel->getActiveSheet()->mergeCells('A7:G7');
		$objPHPExcel->getActiveSheet()->mergeCells('A8:G8');
				
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(10); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(10); //NODISTRIBUSI
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(40);  //TGL DISTRIBUSI
		
		for($x='A';$x<='G';$x++){ //bwt judul2nya
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}
		for($x='A';$x<='G';$x++){
					$objPHPExcel->getActiveSheet()->getStyle($x.'7')->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
				}
		for($x='A';$x<='G';$x++){
					$objPHPExcel->getActiveSheet()->getStyle($x.'8')->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
				}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A2','RUMAH SAKIT ');
		$objPHPExcel->getActiveSheet()->setCellValue ('A3','IBNU SINA');
		$objPHPExcel->getActiveSheet()->setCellValue ('A4','Kota Balikpapan');		
		$objPHPExcel->getActiveSheet()->setCellValue ('A6','LAPORAN REKAP PENDAPATAN BULANAN');
		
		if($kd_unit==''){ $objPHPExcel->getActiveSheet()->setCellValue ('A7','Unit : Semua Unit Apotek');}
		else{
			$namaunit=$this->mlaporanapt->namaUnit($kd_unit);
			$objPHPExcel->getActiveSheet()->setCellValue ('A7','Unit : '.$namaunit);
		}
		
		$objPHPExcel->getActiveSheet()->setCellValue ('A8','Periode :  '.$bulan1.'-'.$bulan2.' '.$tahun);

		for($x='A';$x<='C';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'10')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A10','Tahun');
		$objPHPExcel->getActiveSheet()->setCellValue ('B10','Jenis Pasien');
		$header='C';
		$headerplus='A';
		$totalbawah=array();
	    for ($x=intval($bulan1); $x<=$bulan2;$x++) {
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
			$objPHPExcel->getActiveSheet()->getStyle($header.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($header.'10')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ($header.'10',$x);
			$totalbawah[$x]=0;
			$header++;
	    }
			$objPHPExcel->getActiveSheet()->getStyle($header.'10')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($header.'10')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ($header.'10','TOTAL');
		$items=array();
		$items=$this->mlaporanapt->rekappendapatanbulanan($tahun,$bulan1,$bulan2);
		$baris=11;
		$nomor=1;
		$total=0;
		$no=1;
		foreach ($items as $item) {
	        if($item['type']!=$tipesebelumnya && $no>1)$baris++;
	        $tipesebelumnya=$item['type'];
	        $no++;
			# code...
			for($x='A';$x<='B';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$item['tahun']);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['customer']);
			$header='C';
			$headerplus='A';
			$totalsamping=0;
			for ($y=intval($bulan1); $y<=$bulan2;$y++) {
				$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item[$y]);
				$totalbawah[$y]=$totalbawah[$y]+$item[$y];
				$totalsamping=$totalsamping+$item[$y];
				$header++;
			}
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$totalsamping);
			$nomor=$nomor+1; $baris=$baris+1;
		}

		$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'');
		$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'T O T A L');
		$header='C';
		$headerplus='A';
		$totalsamping=0;
	    for ($x=intval($bulan1); $x<=$bulan2;$x++) {
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$totalbawah[$x]);
			$totalsamping=$totalsamping+$totalbawah[$x];
			$header++;
	    }
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$totalsamping);

		$baris=$baris+5;

		for($x='A';$x<='C';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'Tahun');
		$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'Jenis Pasien');
		$header='C';
		$headerplus='A';
        $totalbawah=array();
	    for ($x=intval($bulan1); $x<=$bulan2;$x++) {
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$x);
			$totalbawah[$x]=0;
			$header++;
	    }
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,'TOTAL');
		$items2=array();
		$items2=$this->mlaporanapt->rekappendapatanbulananbytipe($tahun,$bulan1,$bulan2);
		$baris=$baris+1;
		$nomor=1;
		$total=0;
		$no=1;
		foreach ($items2 as $item2) {
			# code...
			for($x='A';$x<='B';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>11 /*untuk ukuran tulisan yg hasil query bwt yg di dlm tabel*/,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,$item['tahun']);
			if($item2['type']==0)$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'Tunai');
	        if($item2['type']==1)$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'Asuransi');
	        if($item2['type']==2)$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'Perusahaan');
	        if($item2['type']==3)$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'Sosial Dakwah');;
	        if($item2['type']==4)$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'BPJS');
			//$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['customer']);
			$header='C';
			$headerplus='A';
			$totalsamping=0;
			for ($y=intval($bulan1); $y<=$bulan2;$y++) {
				$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$item2[$y]);
				$totalsamping=$totalsamping+$item2[$y];
				$header++;
			}
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$totalsamping);
			$nomor=$nomor+1; $baris=$baris+1;
		}	




		$objPHPExcel->getActiveSheet()->setCellValue ('A'.$baris,'');
		$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,'T O T A L');
		$header='C';
		$headerplus='A';
		$totalsamping=0;
	    for ($x=intval($bulan1); $x<=$bulan2;$x++) {
			$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(10);  //TGL DISTRIBUSI
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
			$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$totalbawah[$x]);
			$totalbawah[$y]=$totalbawah[$y]+$item[$y];
			$totalsamping=$totalsamping+$totalbawah[$x];
			$header++;
	    }
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->getAlignment()->applyFromArray(
					array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
			
				$objPHPExcel->getActiveSheet()->getStyle($header.$baris)->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																				'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12 /*untuk ukuran tulisan judul kolom2 di tabel*/,'color'     => array('rgb' => '000000')),
																				'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																				'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
				$objPHPExcel->getActiveSheet()->setCellValue ($header.$baris,$totalsamping);



		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/rekappendapatanbulanan.xls");
		header("Location: ".base_url()."download/rekappendapatanbulanan.xls");
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */