<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'controllers/rumahsakit.php');
//class Penjualan extends CI_Controller {
class Penjualan extends Rumahsakit {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	protected $title='SIM RS - Sistem Informasi Rumah Sakit';

	public function __construct(){
		parent::__construct();
		$this->load->model('apotek/mpenjualan');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		if(empty($kd_unit_apt)){
			redirect('/home/');
		}
	}
	
	public function restricted(){
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		//$this->load->view('master/header',$dataheader);
		$this->load->view('headerapotek',$dataheader);
		$data=array();
		parent::view_restricted($data);
		$this->load->view('footer');
	}
	
	public function index()	{
		if(!$this->muser->isAkses("57")){
			$this->restricted();
			return false;
		}
		
		$no_penjualan='';
		$kd_unit_apt='';
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		$is_lunas='';
		
		if($this->input->post('no_penjualan')!=''){
			$no_penjualan=$this->input->post('no_penjualan');
		}
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('is_lunas')!=''){
			$is_lunas=$this->input->post('is_lunas');
		}
		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>$this->title);
		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		$data=array('no_penjualan'=>$no_penjualan,
					'kd_unit_apt'=>$kd_unit_apt,
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'is_lunas'=>$is_lunas,
					'items'=>$this->mpenjualan->ambilDataPenjualan($no_penjualan,$kd_unit_apt,$periodeawal,$periodeakhir,$is_lunas)
					);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/transaksi/penjualan/penjualan',$data);
		$this->load->view('footer',$datafooter);
	}
		
	public function tambahpenjualan(){
		if(!$this->muser->isAkses("58")){
			$this->restricted();
			return false;
		}
		
		$kode=""; $no_penjualan="";
		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-timepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>$this->title);
		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		/*$kode=$this->mpenjualan->autoNumber(date('Y'),date('m'));
		$kodebaru=$kode+1;
		$kodebaru=str_pad($kodebaru,5,0,STR_PAD_LEFT); 
		$no_penjualan="P.".date('Y').".".date('m').".".$kodebaru;*/
		
		$data=array('no_penjualan'=>'',
					'jenispasien'=>$this->mpenjualan->ambilData('apt_customers'),
					'dataunit'=>$this->mpenjualan->ambilData('apt_unit'),
					'jenisbayar'=>$this->mpenjualan->ambilData('apt_jenis_bayar'),
					'itemsdetiltransaksi'=>$this->mpenjualan->getAllDetailPenjualan($no_penjualan),
					'itemtransaksi'=>$this->mpenjualan->ambilItemDataPenjualan($no_penjualan),
					'itembayar'=>$this->mpenjualan->getAllDataPembayaran($no_penjualan),
					'itembayarform'=>$this->mpenjualan->ambilTotal($no_penjualan),
					'itembungkus'=>$this->mpenjualan->ambilItemData('sys_setting','key_data="TARIF_PERBUNGKUS"'),
					'items'=>$this->mpenjualan->ambilDataPenjualan('','','','',''),
					'itemsah'=>$this->mpenjualan->ambilDataPenjualan1('','','','')
					);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/transaksi/penjualan/tambahpenjualan',$data);
		$this->load->view('footer',$datafooter);	
	}
	
	public function ubahpenjualan($no_penjualan=""){
		if(!$this->muser->isAkses("59")){
			$this->restricted();
			return false;
		}
		
		if(empty($no_penjualan))return false;
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-timepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);
		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);	
		
		$data=array(
			'jenispasien'=>$this->mpenjualan->ambilData('apt_customers'),			
			'dataunit'=>$this->mpenjualan->ambilData('apt_unit'),
			'jenisbayar'=>$this->mpenjualan->ambilData('apt_jenis_bayar'),
			'no_penjualan'=>$no_penjualan,
			'itemtransaksi'=>$this->mpenjualan->ambilItemDataPenjualan($no_penjualan),
			'itemsdetiltransaksi'=>$this->mpenjualan->getAllDetailPenjualan($no_penjualan),
			'itembayar'=>$this->mpenjualan->getAllDataPembayaran($no_penjualan),
			'itembayarform'=>$this->mpenjualan->ambilTotal($no_penjualan),
			'itembungkus'=>$this->mpenjualan->ambilItemData('sys_setting','key_data="TARIF_PERBUNGKUS"'),
			'items'=>$this->mpenjualan->ambilDataPenjualan('','','','',''),
			'itemsah'=>$this->mpenjualan->ambilDataPenjualan1('','','','')
			);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/transaksi/penjualan/tambahpenjualan',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function simpanpenjualan(){
		$msg=array();
		$submit=$this->input->post('submit');
		$no_penjualan=$this->input->post('no_penjualan');
		$no_pendaftaran=$this->input->post('no_pendaftaran');
		$adm_racik=$this->input->post('adm_racik');
		$is_lunas=$this->input->post('is_lunas');
		$tgl_penjualan=$this->input->post('tgl_penjualan');
		$tgl_penjualan2=$this->input->post('tgl_penjualan2');
		$resep=$this->input->post('resep');
		$tutup=$this->input->post('tutup');
		$kd_pasien=$this->input->post('kd_pasien');
		$nama_pasien=$this->input->post('nama_pasien');
		$cust_code=$this->input->post('cust_code');
		$cust_code1=$this->input->post('cust_code1');
		$total_transaksi=$this->input->post('total_transaksi');
		$kd_dokter=$this->input->post('kd_dokter');
		$kd_dokter1=$this->input->post('kd_dokter1');
		$nama_dokter=$this->input->post('nama_dokter');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');		
		$total_bayar=$this->input->post('total_bayar');
		$tglpenjualan=date('Y-m-d');
		//$kd_unit_kerja1=$this->input->post('kd_unit_kerja1');
		
		$kd_obat=$this->input->post('kd_obat');
		$nama_obat=$this->input->post('nama_obat');
		$satuan_kecil=$this->input->post('satuan_kecil');
		$qty=$this->input->post('qty');
		$qty11=$this->input->post('qty11');
		$tgl_expire=$this->input->post('tgl_expire');
		$harga_jual=$this->input->post('harga_jual'); 
		$racikan=$this->input->post('racikan');
		$adm_resep=$this->input->post('adm_resep'); //di grid bawah
		$total=$this->input->post('total');
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		$milik=$this->input->post('milik');
		$kd_user=$this->session->userdata('id_user');
		$jasa="0";
		$shiftapt="1";
		$kd_milik="01";
		//$status="0";
		
		$tgl_bayar=$this->input->post('tgl_bayar');
		$kd_jenis_bayar=$this->input->post('kd_jenis_bayar');
		$jenis_bayar=$this->input->post('jenis_bayar');
		$bayar=$this->input->post('bayar');
		$sisa=$this->input->post('sisa');
		$terima=$this->input->post('terima');
		$kembali=$this->input->post('kembali');
		
		$tgl_pelayanan=$this->input->post('tgl_pelayanan');
		$jam_pelayanan=$this->input->post('jam_pelayanan');
		$jam_penjualan=$this->input->post('jam_penjualan');
		$qty=$this->input->post('qty');
		
		$msg['no_penjualan']=$no_penjualan;

		if($submit=="tutuptrans"){
			if(empty($no_penjualan))return false;
			$updatedistribusi=array('tutup'=>1);
			$this->mpenjualan->update('apt_penjualan',$updatedistribusi,'no_penjualan="'.$no_penjualan.'"');
			$msg['status']=1;
			$msg['posting']=1;
			$msg['pesan']="Tutup Transaksi Berhasil";
			echo json_encode($msg);
			return false;
		}
		if($submit=="bukatrans"){
			if(empty($no_penjualan))return false;
			$updatedistribusi=array('tutup'=>0);
			$this->mpenjualan->update('apt_penjualan',$updatedistribusi,'no_penjualan="'.$no_penjualan.'"');
			$msg['status']=1;
			$msg['posting']=2;
			$msg['pesan']="Buka Transaksi Berhasil";
			echo json_encode($msg);
			return false;
		}
		
		/*if($submit=="cetak"){
			redirect('third-party/fpdf/cetakbill.php');
		}*/
		
		if($this->mpenjualan->isNumberExist($no_penjualan)){ //edit
			//$tgl_penjualan1=convertDate($tgl_penjualan)." ".$jam_pelayanan;
			if($tgl_penjualan==''){$tgl_penjualan=convertDate($tgl_penjualan2);}
			if($cust_code=='0'){$tgl_penjualan1=$tgl_penjualan2." ".$jam_penjualan;}
			else{$tgl_penjualan1=convertDate($tgl_penjualan)." ".$jam_penjualan;}			
			$datapenjualanedit=array('resep'=>$resep,'adm_racik'=>$adm_racik,'is_lunas'=>$is_lunas,'tgl_penjualan'=>$tgl_penjualan1,'kd_unit_apt'=>$kd_unit_apt,
				'shiftapt'=>$shiftapt,'tutup'=>$tutup,'kd_pasien'=>$kd_pasien,'nama_pasien'=>$nama_pasien,'cust_code'=>$cust_code,
				'total_transaksi'=>$total_transaksi,'kd_dokter'=>$kd_dokter,'dokter'=>$nama_dokter,'total_bayar'=>$total_bayar,'no_pendaftaran'=>$no_pendaftaran,'status'=>0);
			$this->mpenjualan->update('apt_penjualan',$datapenjualanedit,'no_penjualan="'.$no_penjualan.'"');	
			$urut=1;
			$this->mpenjualan->delete('apt_penjualan_detail','no_penjualan="'.$no_penjualan.'"');
			
			if(!empty($kd_obat)){
				foreach ($kd_obat as $key => $value){
					if(empty($value))continue;
					$harga_pokok=$this->mpenjualan->ambilItemData3($kd_unit_apt,$value);
					$markup=$harga_jual[$key]-$harga_pokok;
					$datadetiledit=array('no_penjualan'=>$no_penjualan,'urut'=>$urut,'kd_unit_apt'=>$kd_unit_apt,'kd_obat'=>$value,'kd_milik'=>$kd_milik,'tgl_expire'=>convertDate($tgl_expire[$key]),
						'qty'=>$qty[$key],'harga_pokok'=>$harga_pokok,'harga_jual'=>$harga_jual[$key],'total'=>$total[$key],'markup'=>$markup,'jasa'=>$jasa,'racikan'=>$racikan[$key],'adm_resep'=>$adm_resep[$key]);
					$this->mpenjualan->insert('apt_penjualan_detail',$datadetiledit);
					
					$urut++;
				}
			}
			$count=$this->mpenjualan->countObat($no_penjualan);
			$datatotal=array('total_transaksi'=>$total_transaksi,'total_bayar'=>$total_transaksi,'jum_item_obat'=>$count);
			$this->mpenjualan->update('apt_penjualan',$datatotal,'no_penjualan="'.$no_penjualan.'"');		
			$msg['pesan']="Data Berhasil Di Update";
			$msg['posting']=3;
		}else { //simpan baru
			if($tgl_penjualan==''){$tgl_penjualan=convertDate($tglpenjualan);}
			$tgl=explode("-", $tgl_penjualan);
			$kode=$this->mpenjualan->autoNumber($tgl[2],$tgl[1]);
			$kodebaru=$kode+1;
			$kodebaru=str_pad($kodebaru,5,0,STR_PAD_LEFT); 
			$no_penjualan="P.".$tgl[2].".".$tgl[1].".".$kodebaru;
			$msg['no_penjualan']=$no_penjualan;			
			
			if($cust_code=='0'){$tgl_penjualan1=$tglpenjualan." ".$jam_pelayanan;}
			else{$tgl_penjualan1=convertDate($tgl_penjualan)." ".$jam_pelayanan;}
			
			$datapenjualan=array('no_penjualan'=>$no_penjualan,'resep'=>$resep,'adm_racik'=>$adm_racik,'is_lunas'=>$is_lunas,'tgl_penjualan'=>$tgl_penjualan1,
				'kd_unit_apt'=>$kd_unit_apt,'shiftapt'=>$shiftapt,'tutup'=>$tutup,'kd_pasien'=>$kd_pasien,'nama_pasien'=>$nama_pasien,'cust_code'=>$cust_code,
				'total_transaksi'=>$total_transaksi,'kd_dokter'=>$kd_dokter,'dokter'=>$nama_dokter,'total_bayar'=>$total_bayar,'no_pendaftaran'=>$no_pendaftaran,
				'status'=>0,'tgl_tutup'=>'0000-00-00 00:00:00');
			
			$this->mpenjualan->insert('apt_penjualan',$datapenjualan);
			$urut=1;
			if(!empty($kd_obat)){
				foreach ($kd_obat as $key => $value){
					# code...
					if(empty($value))continue;
					$harga_pokok=$this->mpenjualan->ambilItemData3($kd_unit_apt,$value);					
					$markup=$harga_jual[$key]-$harga_pokok;						
					
					$datadetil=array('no_penjualan'=>$no_penjualan,'urut'=>$urut,'kd_unit_apt'=>$kd_unit_apt,'kd_obat'=>$value,'kd_milik'=>$kd_milik,'tgl_expire'=>convertDate($tgl_expire[$key]),
						'qty'=>$qty[$key],'harga_pokok'=>$harga_pokok,'harga_jual'=>$harga_jual[$key],'total'=>$total[$key],'jasa'=>$jasa,'markup'=>$markup,'racikan'=>$racikan[$key],'adm_resep'=>$adm_resep[$key]);
					$this->mpenjualan->insert('apt_penjualan_detail',$datadetil);	
					
					$urut++;				
				}
			}
			$count=$this->mpenjualan->countObat($no_penjualan);
			$datatotal=array('total_transaksi'=>$total_transaksi,'total_bayar'=>$total_transaksi,'jum_item_obat'=>$count);
			$this->mpenjualan->update('apt_penjualan',$datatotal,'no_penjualan="'.$no_penjualan.'"');
			$msg['pesan']="Data Berhasil Di Simpan";
			$msg['posting']=3;
		}
		$msg['status']=1;
		$msg['keluar']=0;
		$msg['simpanbayar']=0;
		if($submit=="simpankeluar"){
			$msg['keluar']=1;
		}
		if($submit=="simpanbayar"){ 
			$no=$this->mpenjualan->ambilUrut($no_penjualan);
			$urut_bayar=$no+1;
			if($kd_jenis_bayar=="001"){
				$tgl_bayar1=$tgl_bayar." ".$jam_pelayanan;
				$detilbayar=array('no_penjualan'=>$no_penjualan,'kd_jenis_bayar'=>$kd_jenis_bayar,'urut_bayar'=>$urut_bayar,
									  'tgl_bayar'=>$tgl_bayar1,'shiftapt'=>$shiftapt,'total'=>$bayar);

				$this->mpenjualan->insert('apt_penjualan_bayar',$detilbayar);
				$jumbayar=$this->mpenjualan->ambilTotalBayar($no_penjualan);
				
				$databayar=array('total_bayar'=>$jumbayar);
				$this->mpenjualan->update('apt_penjualan',$databayar,'no_penjualan="'.$no_penjualan.'"');
				if($jumbayar==$total_transaksi){
					$is_lunas=1;
					$tutuptransaksi=1;
					$lunas=array('is_lunas'=>$is_lunas,'tutup'=>$tutuptransaksi);
					$this->mpenjualan->update('apt_penjualan',$lunas,'no_penjualan="'.$no_penjualan.'"');					
				}
				else{$is_lunas=0;}
				
				if(!empty($kd_obat)){
					foreach ($kd_obat as $key => $value){
						$tglexpire=convertDate($tgl_expire[$key]);
						$jml_stok=$this->mpenjualan->ambilStok($kd_unit_apt,$value,$tglexpire);
						$sisastok=$jml_stok-$qty[$key];
						$datastok=array('jml_stok'=>$sisastok);
						$this->mpenjualan->update('apt_stok_unit',$datastok,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$value.'" and tgl_expire="'.convertDate($tgl_expire[$key]).'"');
					}
				}
				$msg['pesan']="Pembayaran berhasil disimpan";
				$msg['simpanbayar']=1;	
				$this->bill($no_penjualan);
			}
			else if($kd_jenis_bayar=="002"){
				$qty1=1;
			
				$tgl_bayar1=$tgl_bayar." ".$jam_pelayanan;
				$detilbayar=array('no_penjualan'=>$no_penjualan,'kd_jenis_bayar'=>$kd_jenis_bayar,'urut_bayar'=>$urut_bayar,
									  'tgl_bayar'=>$tgl_bayar1,'shiftapt'=>$shiftapt,'total'=>$total_transaksi);
				$this->mpenjualan->insert('apt_penjualan_bayar',$detilbayar);
				
				$urut=$this->mpenjualan->getMaxUrutPelayanan($no_pendaftaran);
				$tglpelayanan1=$tgl_pelayanan." ".$jam_pelayanan;
				$kd_jenis_tarif=$this->mpenjualan->ambilkodejenistarif($cust_code);
				$kd_pelayanan=$this->mpenjualan->ambilkodepelayanan();
				$kd_kelas=$this->mpenjualan->ambilkodekelas($no_pendaftaran);
				$tgl_berlaku=$this->mpenjualan->ambiltglberlaku($kd_pelayanan);
				$total=$total_transaksi*$qty1;
				/*$biaya_pelayanan=array('kd_kasir'=>'','no_pendaftaran'=>$no_pendaftaran,'urut'=>$urut+1,'tgl_pelayanan'=>$tglpelayanan1,
										'kd_jenis_tarif'=>$kd_jenis_tarif,'kd_pelayanan'=>$kd_pelayanan,'kd_kelas'=>$kd_kelas,'tgl_berlaku'=>$tgl_berlaku,
										'qty'=>$qty1,'harga'=>$total_transaksi,'total'=>$total,'shift'=>'','kd_unit_kerja_tr'=>$kd_unit_apt,
										'kd_dokter'=>$kd_dokter,'kd_user'=>0,'cust_code'=>$cust_code,'harga_asli'=>$total_transaksi,'no_transaksiresep'=>$no_penjualan);
				$this->mpenjualan->insert('biaya_pelayanan',$biaya_pelayanan);*/
				if(!empty($kd_obat)){
					foreach ($kd_obat as $key => $value){
						$tglexpire=convertDate($tgl_expire[$key]);
						$jml_stok=$this->mpenjualan->ambilStok($kd_unit_apt,$value,$tglexpire);
						$sisastok=$jml_stok-$qty[$key];
						$datastok=array('jml_stok'=>$sisastok);
						$this->mpenjualan->update('apt_stok_unit',$datastok,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$value.'" and tgl_expire="'.convertDate($tgl_expire[$key]).'"');
					}
				}
				
				$is_lunas=1; $tutuptransaksi=1;
				$lunas=array('is_lunas'=>$is_lunas,'tutup'=>$tutuptransaksi);				
				$this->mpenjualan->update('apt_penjualan',$lunas,'no_penjualan="'.$no_penjualan.'"');
				$kd_unit_kerja=$this->mpenjualan->ambilunit($no_pendaftaran); //105
				$parent=$this->mpenjualan->ambilparent($kd_unit_kerja);
				$kodeunitkerja="";
				if($parent==10){ //kalo kd_unit_kerjanya parentnya 10....
					$unit_kerja="Rawat Jalan"; 
					$kodeunitkerja=$kd_unit_kerja;
				}
				else if($parent==8){ //kalo kd_unit_kerjanya parentnya 8....
					$unit_kerja="Rawat Inap";
					$kodeunitkerja1=$this->mpenjualan->ambilunit1($no_pendaftaran); //ngambil unit kerja di tabel masuk_ruangan
					if($kodeunitkerja1==0){$kodeunitkerja=$kd_unit_kerja;}
					else {$kodeunitkerja=$kodeunitkerja1;}
				}
				else{ //kalo kd_unit_kerjanya parentnya kosong....
					//$unit_kerja="Rawat Jalan";
					if($kd_unit_kerja==1){$unit_kerja="Unit Gawat Darurat";}
					if($kd_unit_kerja==10){$unit_kerja="Rawat Jalan";}
					//if($kd_unit_kerja==7){$unit_kerja="Perinatologi";}
					if($kd_unit_kerja==8){$unit_kerja="Rawat Inap";}
					//if($kd_unit_kerja==9){$unit_kerja="Ruang Bersalin";}					
					$kodeunitkerja=$kd_unit_kerja;
				}			
				
				$biaya_pelayanan=array('kd_kasir'=>'','no_pendaftaran'=>$no_pendaftaran,'urut'=>$urut+1,'kd_unit_kerja'=>$kodeunitkerja,'tgl_pelayanan'=>$tglpelayanan1,
										'kd_jenis_tarif'=>$kd_jenis_tarif,'kd_pelayanan'=>$kd_pelayanan,'kd_kelas'=>$kd_kelas,'tgl_berlaku'=>$tgl_berlaku,
										'qty'=>$qty1,'harga'=>$total_transaksi,'total'=>$total,'shift'=>'','kd_unit_kerja_tr'=>$kd_unit_apt,
										'kd_dokter'=>$kd_dokter,'kd_user'=>0,'cust_code'=>$cust_code,'harga_asli'=>$total_transaksi,'no_transaksiresep'=>$no_penjualan);
				$this->mpenjualan->insert('biaya_pelayanan',$biaya_pelayanan);
				
				$msg['pesan']="Tagihan telah ditransfer ke unit : ".$unit_kerja;
				$msg['simpanbayar']=1;
				//redirect('transapotek/penjualan/bill');
				$this->bill($no_penjualan);
			}		
			else{}
		}
		if($submit=="transferapotek"){
			$no=$this->mpenjualan->ambilUrut($no_penjualan);
			$urut_bayar=$no+1;
			$qty1=1;
			$tgl_bayar1=$tgl_bayar." ".$jam_pelayanan;
			$detilbayar=array('no_penjualan'=>$no_penjualan,'kd_jenis_bayar'=>'002','urut_bayar'=>$urut_bayar,
								  'tgl_bayar'=>$tgl_bayar1,'shiftapt'=>$shiftapt,'total'=>$total_transaksi,'kd_user'=>$kd_user);
			$this->mpenjualan->insert('apt_penjualan_bayar',$detilbayar);
			
			$urut=$this->mpenjualan->getMaxUrutPelayanan($no_pendaftaran);
			$tglpelayanan1=$tgl_pelayanan." ".$jam_pelayanan;
			$kd_jenis_tarif=$this->mpenjualan->ambilkodejenistarif($cust_code);
			$kd_pelayanan=$this->mpenjualan->ambilkodepelayanan();
			$kd_kelas=$this->mpenjualan->ambilkodekelas($no_pendaftaran);
			$tgl_berlaku=$this->mpenjualan->ambiltglberlaku($kd_pelayanan);
			$total=$total_transaksi*$qty1;
			//debugvar($tgl_berlaku);
			if(!empty($kd_obat)){
				foreach ($kd_obat as $key => $value){
					$tglexpire=convertDate($tgl_expire[$key]);					
					$jml_stok=$this->mpenjualan->ambilStok($kd_unit_apt,$value,$tglexpire);
					$sisastok=$jml_stok-$qty[$key];
					$datastok=array('jml_stok'=>$sisastok);
					$this->mpenjualan->update('apt_stok_unit',$datastok,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$value.'" and tgl_expire="'.convertDate($tgl_expire[$key]).'"');
				}
			}
			
			$is_lunas=1; $tutuptransaksi=1;
			$lunas=array('is_lunas'=>$is_lunas,'tutup'=>$tutuptransaksi);
			$this->mpenjualan->update('apt_penjualan',$lunas,'no_penjualan="'.$no_penjualan.'"');
			
			$kodeunitkerja="";
			$kd_unit_kerja=$this->mpenjualan->ambilunit($no_pendaftaran);
			$parent=$this->mpenjualan->ambilparent($kd_unit_kerja);
			if($parent==10){
				$unit_kerja="Rawat Jalan"; 
				$kodeunitkerja=$kd_unit_kerja;
			}
			else if($parent==8){
				$unit_kerja="Rawat Inap";
				$kodeunitkerja1=$this->mpenjualan->ambilunit1($no_pendaftaran);
				if($kodeunitkerja1==0){$kodeunitkerja=$kd_unit_kerja;}
				else {$kodeunitkerja=$kodeunitkerja1;}
			}
			else{
				//$unit_kerja="Rawat Jalan";
				if($kd_unit_kerja==1){$unit_kerja="Unit Gawat Darurat";}
				if($kd_unit_kerja==10){$unit_kerja="Rawat Jalan";}
				//if($kd_unit_kerja==7){$unit_kerja="Perinatologi";}
				if($kd_unit_kerja==8){$unit_kerja="Rawat Inap";}
				//if($kd_unit_kerja==9){$unit_kerja="Ruang Bersalin";}
				$kodeunitkerja=$kd_unit_kerja;
			}
			//debugvar($kd_unit_kerja);
			$biaya_pelayanan=array('kd_kasir'=>'','no_pendaftaran'=>$no_pendaftaran,'urut'=>$urut+1,'kd_unit_kerja'=>$kodeunitkerja,'tgl_pelayanan'=>$tglpelayanan1,
									'kd_jenis_tarif'=>$kd_jenis_tarif,'kd_pelayanan'=>$kd_pelayanan,'kd_kelas'=>$kd_kelas,'tgl_berlaku'=>$tgl_berlaku,
									'qty'=>$qty1,'harga'=>$total_transaksi,'total'=>$total,'shift'=>'','kd_unit_kerja_tr'=>$kd_unit_apt,
									'kd_dokter'=>$kd_dokter,'kd_user'=>0,'cust_code'=>$cust_code,'harga_asli'=>$total_transaksi,'no_transaksiresep'=>$no_penjualan);
			$this->mpenjualan->insert('biaya_pelayanan',$biaya_pelayanan);
					
			$msg['pesan']="Tagihan telah ditransfer ke unit : ".$unit_kerja;
			$msg['simpanbayar']=1;
			$this->bill($no_penjualan);
		}
		
		echo json_encode($msg);
	}
	
	public function hapuspenjualan($no_penjualan=""){
		if(!$this->muser->isAkses("60")){
			$this->restricted();
			return false;
		}
		
		$msg=array();
		$error=0;
		if(empty($no_penjualan)){
			$msg['pesan']="Pilih Transaksi yang akan di hapus";
			echo "<script>Alert('".$msg['pesan']."')</script>";
		}else{
			$this->mpenjualan->delete('apt_penjualan','no_penjualan="'.$no_penjualan.'"');
			$this->mpenjualan->delete('apt_penjualan_detail','no_penjualan="'.$no_penjualan.'"');			
			redirect('/transapotek/penjualan/');
		}
	}
	
	public function ambildaftarobatbynama() {
		$nama_obat=$this->input->post('nama_obat');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		
		$this->datatables->select("apt_stok_unit.kd_obat, apt_obat.nama_obat, apt_satuan_kecil.satuan_kecil, date_format(apt_stok_unit.tgl_expire,'%d-%m-%Y') as tgl_expire,
						(apt_stok_unit.harga_pokok * apt_margin_harga.nilai_margin) as harga_jual,apt_stok_unit.jml_stok,'pilihan' as pilihan,ifnull(apt_obat.min_stok,0) as min_stok ",false);
		
		$this->datatables->from("apt_obat");
		$this->datatables->edit_column('pilihan', '<a class="btn" onclick=\'pilihobat("$1","$2","$3","$4","$5","$6","$7")\'>Pilih</a>','apt_stok_unit.kd_obat, apt_obat.nama_obat, apt_satuan_kecil.satuan_kecil, tgl_expire, harga_jual,apt_stok_unit.jml_stok,min_stok');		
		if(!empty($nama_obat))$this->datatables->like('apt_obat.nama_obat',$nama_obat,'both');
		
		$this->datatables->where('apt_stok_unit.kd_unit_apt',$kd_unit_apt);
		$this->datatables->where('apt_stok_unit.jml_stok >','0');
		$this->datatables->where('apt_obat.is_aktif','1');
		
		$this->datatables->join('apt_satuan_kecil','apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil');
		$this->datatables->join('apt_stok_unit','apt_obat.kd_obat=apt_stok_unit.kd_obat');
		$this->datatables->join('apt_golongan','apt_obat.kd_golongan=apt_golongan.kd_golongan');
		$this->datatables->join('apt_jenis_obat','apt_obat.kd_jenis_obat=apt_jenis_obat.kd_jenis_obat');
		$this->datatables->join('apt_unit','apt_stok_unit.kd_unit_apt=apt_unit.kd_unit_apt');
		$this->datatables->join('apt_margin_harga ','apt_jenis_obat.kd_jenis_obat=apt_margin_harga.kd_jenis_obat and apt_golongan.kd_golongan=apt_margin_harga.kd_golongan');
		$results = $this->datatables->generate();
		echo ($results);
	}
	
	public function ambildaftarobatbykode() {
		$kd_obat=$this->input->post('kd_obat');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		
		$this->datatables->select("apt_stok_unit.kd_obat, apt_obat.nama_obat, apt_satuan_kecil.satuan_kecil, date_format(apt_stok_unit.tgl_expire,'%d-%m-%Y') as tgl_expire,
						(apt_stok_unit.harga_pokok * apt_margin_harga.nilai_margin) as harga_jual,apt_stok_unit.jml_stok,'pilihan' as pilihan,ifnull(apt_obat.min_stok,0) as min_stok ",false);
		
		$this->datatables->from("apt_obat");
		$this->datatables->edit_column('pilihan', '<a class="btn" onclick=\'pilihobat("$1","$2","$3","$4","$5","$6","$7")\'>Pilih</a>','apt_stok_unit.kd_obat, apt_obat.nama_obat, apt_satuan_kecil.satuan_kecil, tgl_expire, harga_jual,apt_stok_unit.jml_stok,min_stok');		
		if(!empty($kd_obat))$this->datatables->like('apt_obat.kd_obat',$kd_obat,'both');
		
		$this->datatables->where('apt_stok_unit.kd_unit_apt',$kd_unit_apt);
		$this->datatables->where('apt_stok_unit.jml_stok >','0');
		$this->datatables->where('apt_obat.is_aktif','1');
		
		$this->datatables->join('apt_satuan_kecil','apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil');
		$this->datatables->join('apt_stok_unit','apt_obat.kd_obat=apt_stok_unit.kd_obat');
		$this->datatables->join('apt_golongan','apt_obat.kd_golongan=apt_golongan.kd_golongan');
		$this->datatables->join('apt_jenis_obat','apt_obat.kd_jenis_obat=apt_jenis_obat.kd_jenis_obat');
		$this->datatables->join('apt_unit','apt_stok_unit.kd_unit_apt=apt_unit.kd_unit_apt');
		$this->datatables->join('apt_margin_harga ','apt_jenis_obat.kd_jenis_obat=apt_margin_harga.kd_jenis_obat and apt_golongan.kd_golongan=apt_margin_harga.kd_golongan');
		$results = $this->datatables->generate();
		echo ($results);		
	}
	
	public function ambildokterbykode(){
		$q=$this->input->get('query');
		$items=$this->mpenjualan->ambilData3($q);
		echo json_encode($items);
		/*$kd_dokter=$this->input->post('kd_dokter');
		
		$this->datatables->select("kd_dokter,nama_dokter,'Pilihan' as pilihan",false);
		
		$this->datatables->from("apt_dokter");
		$this->datatables->edit_column('pilihan', '<a class="btn" onclick=\'pilihobat("$1","$2","$3")\'>Pilih</a>','kd_dokter, nama_dokter');		
		if(!empty($kd_dokter))$this->datatables->like('kd_dokter',$kd_dokter,'both');
		
		$results = $this->datatables->generate();
		echo ($results);*/		
	}
	
	public function ambildokterbynama(){
		$q=$this->input->get('query');
		$items=$this->mpenjualan->ambilData4($q);
		echo json_encode($items);
	}

	public function periksapenjualan() {
		$msg=array();
		$submit=$this->input->post('submit');
		$no_penjualan=$this->input->post('no_penjualan');
		$no_pendaftaran=$this->input->post('no_pendaftaran');
		$resep=$this->input->post('resep');
		$adm_racik=$this->input->post('adm_racik');
		$is_lunas=$this->input->post('is_lunas');
		$tgl_penjualan=$this->input->post('tgl_penjualan');
		$shiftapt=$this->input->post('shiftapt');
		$tutup=$this->input->post('tutup');
		$kd_pasien=$this->input->post('kd_pasien');
		$nama_pasien=$this->input->post('nama_pasien');		
		$cust_code=$this->input->post('cust_code');
		$cust_code1=$this->input->post('cust_code1');
		$total_transaksi=$this->input->post('total_transaksi');
		$kd_dokter=$this->input->post('kd_dokter');
		$kd_dokter1=$this->input->post('kd_dokter1');	
		$nama_dokter=$this->input->post('nama_dokter');
		$total_bayar=$this->input->post('total_bayar');
		//$kd_unit_kerja1=$this->input->post('kd_unit_kerja1');
		
		$kd_obat=$this->input->post('kd_obat');
		$nama_obat=$this->input->post('nama_obat');
		$satuan_kecil=$this->input->post('satuan_kecil');
		$qty=$this->input->post('qty');
		$qty11=$this->input->post('qty11');
		$tgl=$this->input->post('tgl');
		$hrg=$this->input->post('hrg'); 
		$racikan=$this->input->post('racikan');
		$adm_resep=$this->input->post('adm_resep');
		$total=$this->input->post('total');
		$nama_unit_apt=$this->input->post('nama_unit_apt');
		$milik=$this->input->post('milik');
		$jml_stok=$this->input->post('jml_stok');
		
		$total_transaksi=$this->input->post('total_transaksi');
		
		$tgl_pelayanan=$this->input->post('tgl_pelayanan');
		$jam_pelayanan=$this->input->post('jam_pelayanan');
		$jam_penjualan=$this->input->post('jam_penjualan');
		$qty=$this->input->post('qty');
		
		$jumlaherror=0;
		$msg['status']=1;
		$msg['clearform']=0;
		$msg['pesanatas']="";
		$msg['pesanlain']="";
		
		if($submit=="hapus"){
			if(empty($no_penjualan)){
				$msg['pesanatas']="Pilih Transaksi yang akan di hapus";
			}else{
				$this->mpenjualan->delete('apt_penjualan','no_penjualan="'.$no_penjualan.'"');
				$this->mpenjualan->delete('apt_penjualan_detail','no_penjualan="'.$no_penjualan.'"');
				$msg['pesanatas']="Data Berhasil Di Hapus";
			}
			$msg['status']=0;
			$msg['clearform']=1;
			echo json_encode($msg);
		}
		//else if($submit=="simpanbayar" or $submit=="bukatrans" or $submit=="tutuptrans" or $submit=="transferapotek"){}
		else if($submit=="simpanbayar" or $submit=="bukatrans" or $submit=="tutuptrans"){}
		else if($submit=="transferapotek"){
			if(empty($no_pendaftaran)){
				$msg['status']=0;
				//$nama=$this->mpenjualan->ambilNama($value);
				$msg['pesanlain'].="Anda tidak bisa melakukan transfer pembayaran tagihan apotek.<br/>";
			}
		}
		else{
			/*if(empty($tgl_penjualan)){
				$jumlaherror++;
				$msg['id'][]="tgl_penjualan";
				$msg['pesan'][]="Kolom Tanggal Harus di Isi";
			}*/
			if($cust_code==''){
				$jumlaherror++;
				$msg['id'][]="cust_code";
				$msg['pesan'][]="Jenis Pasien Harus di Pilih";
			}
			if($cust_code==''){
				$jumlaherror++;
				$msg['id'][]="nama_pasien";
				$msg['pesan'][]="Nama Pasien Harus di Isi";
			}
			if($total_transaksi==''){
				$jumlaherror++;
				$msg['id'][]="total_transaksi";
				$msg['pesan'][]="Jumlah transaksi masih kosong. Silahkan input obat";
			}
			
			if(!empty($kd_obat)){
				foreach ($kd_obat as $key => $value) {
					# code...
					if(empty($value))continue;
					/*if(empty($nama_obat[$key])){
						$msg['status']=0;
						$msg['pesanlain'].="Data masih ada yang kosong ! <br/>";
					}*/
					if(empty($qty[$key])){
						$msg['status']=0;
						$nama=$this->mpenjualan->ambilNama($value);
						$msg['pesanlain'].="Qty ".$nama." Tidak boleh Kosong <br/>";					
					}
					if($qty[$key]>$jml_stok[$key]){
						$msg['status']=0;
						$nama=$this->mpenjualan->ambilNama($value);
						$msg['pesanlain'].="Stok ".$nama." tidak mencukupi <br/>";
					}
				}
			}
			if($jumlaherror>0){
				$msg['status']=0;
				$msg['error']=$jumlaherror;
				$msg['pesanatas']="Terdapat beberapa kesalahan input silahkan cek inputan anda";
			}
		}
		echo json_encode($msg);
	}
	
	public function ambilitem()
	{
		$q=$this->input->get('query');
		$items=$this->mpenjualan->getPenjualan($q);

		echo json_encode($items);
	}
	
	public function ambilitems()
	{
		$q=$this->input->get('query');
		$items=$this->mpenjualan->getAllDetailPenjualan($q);

		echo json_encode($items);
	}
	
	public function ambilpasienbynama(){
		$q=$this->input->get('query');
		$tes=$this->input->get('tes');
		$dor=$this->input->get('dor');
		$items=$this->mpenjualan->ambilDataPasien($q,$tes,$dor);
		//debugvar($items);
		echo json_encode($items);
	}
	
	public function hapuspembayaran(){
		$q=$this->input->post('query');	//no_penjualan
		$q1=$this->input->post('query1'); //no_pendaftaran
		//debugvar($q1);
		$is_lunas=0;
		$tutuptransaksi=0;
		$lunas=array('is_lunas'=>$is_lunas,'tutup'=>$tutuptransaksi);		
		$this->mpenjualan->update('apt_penjualan',$lunas,'no_penjualan="'.$q.'"');
		
		$item=$this->mpenjualan->getAllDetailPenjualan($q);
		//debugvar($item);
		foreach($item as $itemdetil){ 
			$kode=$itemdetil['kd_obat'];
			$kiteye=$itemdetil['qty'];
			$tglexpire=convertDate($itemdetil['tgl_expire']);
			$kd_unit_apt=$itemdetil['kd_unit_apt'];			
			$jml_stok=$this->mpenjualan->ambilStok($kd_unit_apt,$kode,$tglexpire);
			$stokakhir=$kiteye+$jml_stok;
			$datastoka=array('jml_stok'=>$stokakhir);
			$this->mpenjualan->update('apt_stok_unit',$datastoka,'kd_unit_apt="'.$kd_unit_apt.'" and kd_obat="'.$kode.'" and tgl_expire="'.$tglexpire.'"');			
		}		
		
		$id_bayar=$this->mpenjualan->jenisbayar($q);
		if($id_bayar=='002'){
			//$this->mpenjualan->delete('biaya_pelayanan','no_transaksiresep="'.$q.'" and no_pendaftaran="'.$q1.'" and kd_pelayanan="TROAPT"');			
			$this->mpenjualan->delete('biaya_pelayanan','no_transaksiresep="'.$q.'" and no_pendaftaran="'.$q1.'"');
		}
		$items=$this->mpenjualan->delete('apt_penjualan_bayar','no_penjualan="'.$q.'"');		
		echo json_encode($items);
	}
	
	public function bill($no_penjualan=""){
		//error_reporting(0);
		$tmpdir = sys_get_temp_dir(); # ambil direktori temporary untuk simpan file. 
		$file = tempnam($tmpdir, 'ctk.doc'); # nama file temporary yang akan dicetak 
		$handle = fopen($file, 'w'); 
		$condensed = Chr(27) . Chr(33) . Chr(4); 
		$pagelength = Chr(27) . Chr(67) . Chr(48). Chr(6); 
		$bold1 = Chr(27) . Chr(69); 
		$bold0 = Chr(27) . Chr(70); 
		$initialized = chr(27).chr(64); 
		$condensed1 = chr(15); 
		$condensed0 = chr(18); 
		$right = chr(27).chr(97).chr(2); 
		$left = chr(27).chr(97).chr(0); 
		$center = chr(27).chr(97).chr(1); 
		
		//$kd_user=$this->session->userdata('id_user');
		$user=$this->mpenjualan->ambilnamauser();
		$item=$this->mpenjualan->ambilItemDataPenjualan($no_penjualan);
		$kd_pasien='';
		if($item['kd_pasien']==''){$kd_pasien="-";}
		else{$kd_pasien=$item['kd_pasien'];}
		
		$Data = $initialized; 
		$Data .= $condensed1; 
		//$Data .= $pagelength; 
		$Data .= "POLIKLINIK IBNUSINA (MUARA RAPAK BALIKPAPAN)\n"; 
		$Data .= "Faktur Standard Penjualan                                 Tunai\n"; 
		$Data .= "Print Date : ".date('d-m-Y h:i:s')."".str_pad($no_penjualan,30," ",STR_PAD_LEFT)."\n"; 
		$Data .= "Kasir      : ".$user."         \n"; 
		$Data .= "Pasien     : ".$kd_pasien." / ".$item['nama_pasien']."         \n"; 
		$Data .= "---------------------------------------------------------------\n"; 
		//$Data .= "No| ".$bold1."COBA CETAK".$bold0." |\n"; 
		$Data .= "No.| Nama Item                     | @Harga | Qty  |  Jml Harga\n"; 
		$Data .= "---------------------------------------------------------------\n"; 
		$subtotal=0;
		$no=1;
		$items=array();
		$items=$this->mpenjualan->getdetil($no_penjualan);		
		foreach ($items as $isi){
			# code...
			$Data .= "".$no."  | ".str_pad(substr($isi['nama_obat'],0,29),30," ",STR_PAD_RIGHT)."| ".str_pad($isi['harga_jual'], 5," ",STR_PAD_LEFT)."| ".str_pad($isi['qty'], 5," ",STR_PAD_LEFT)."| ".str_pad($isi['total'], 10," ",STR_PAD_LEFT)."\n"; 
			$subtotal=$subtotal+$isi['total'];
			$no++;
		}
		//$Data .= "02 | Sanmag Tab                    | 832    | 10   |       8316\n"; 
		$Data .= "---------------------------------------------------------------\n"; 
		$Data .= "Sub Total                          | Rp. ".str_pad($subtotal, 22," ",STR_PAD_LEFT)."\n"; 
		$Data .= "Grand Total                        | Rp. ".str_pad($subtotal, 22," ",STR_PAD_LEFT)."\n"; 
		$Data .= "                                   ".str_pad('', 28,"-",STR_PAD_LEFT)."\n"; 
		$Data .= "Jumlah Bayar                       | Rp. ".str_pad($subtotal, 22," ",STR_PAD_LEFT)."\n\n"; 
		$Data .= "----------------------Terima Kasih ----------------------------\n\n\n\n"; 
		fwrite($handle, $Data); 
		fclose($handle); 
		//copy($file.'images/logo.png', "//192.168.1.3/EPSONLX"); # Lakukan cetak 
		copy($file, "//PLANET-IT/EPSONLX"); # Lakukan cetak 
		unlink($file);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */