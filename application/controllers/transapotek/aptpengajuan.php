<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'controllers/rumahsakit.php');
//class Aptpengajuan extends CI_Controller {
class Aptpengajuan extends Rumahsakit {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	protected $title='SIM RS - Sistem Informasi Rumah Sakit';
	public $shift;

	public function __construct(){
		parent::__construct();
		$this->load->model('apotek/mpengajuan');
		$this->load->model('apotek/mgrouping');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		if(empty($kd_unit_apt)){
			redirect('/home/');
		}

        $queryunitshift=$this->db->query('select * from unit_shift where kd_unit="APT"'); 
        $unitshift=$queryunitshift->row_array();
		$this->shift=$unitshift['shift'];
	}
	
	public function restricted(){
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		//$this->load->view('master/header',$dataheader);
		$this->load->view('headerapotek',$dataheader);
		$data=array();
		parent::view_restricted($data);
		$this->load->view('footer');
	}
	
	public function index()	{
		if(!$this->muser->isAkses("29")){
			$this->restricted();
			return false;
		}
		
		$no_pengajuan='';
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		
		if($this->input->post('no_pengajuan')!=''){
			$no_pengajuan=$this->input->post('no_pengajuan');
		}
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js','vendor/jquery-1.9.1.min.js','vendor/jquery-migrate-1.1.1.min.js','vendor/jquery-ui-1.10.0.custom.min.js','vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js','lib/jquery.dataTables.min.js','lib/DT_bootstrap.js','lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Daftar Pengajuan :: ".$this->title);
		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$data=array('no_pengajuan'=>$no_pengajuan,
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'items'=>$this->mpengajuan->ambilDataPengajuan($no_pengajuan,$periodeawal,$periodeakhir));
		
		//debugvar($items);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/transaksi/pengajuan/aptpengajuan',$data);
		$this->load->view('footer',$datafooter);
	}
		
	public function tambahpengajuan(){	
		if(!$this->muser->isAkses("30")){
			$this->restricted();
			return false;
		}
		
		$kode=""; $no_pengajuan=""; $kd_applogin="";
		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-timepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array('jsfile'=>$jsfileheader,'cssfile'=>$cssfileheader,'title'=>"Tambah Pengajuan :: ".$this->title);
		$jsfooter=array();
		$datafooter=array('jsfile'=>$jsfooter);
		
		$kd_applogin=$this->mpengajuan->ambilApp();
		
		$data=array('no_pengajuan'=>'',
					//'datasupplier'=>$this->mpengajuan->ambilData('apt_supplier'),
					//'itemtransaksi'=>$this->mpengajuan->ambilItemData('apt_pengajuan','no_pengajuan="'.$no_pengajuan.'"'),
					'itemtransaksi'=>$this->mpengajuan->getPengajuan($no_pengajuan),
					'itemsdetiltransaksi'=>$this->mpengajuan->getAllDetailPengajuan($no_pengajuan),
					'itemapprove'=>$this->mpengajuan->ambilApprover(),
					'supplier'=>$this->mpengajuan->ambilData('apt_supplier'),
					//'applogin'=>$this->mpengajuan->ambilApp()
					'kd_applogin'=>$kd_applogin
					);
		
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/transaksi/pengajuan/tambahpengajuan',$data);
		$this->load->view('footer',$datafooter);	
	}
	
	public function ubahpengajuan($no_pengajuan=""){
		if(!$this->muser->isAkses("31")){
			$this->restricted();
			return false;
		}
		
		$sum="";
		$kd_applogin="";
		if(empty($no_pengajuan))return false;
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-timepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>"Edit Pengajuan :: ".$this->title
			);
		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);
		
		$kd_applogin=$this->mpengajuan->ambilApp();
		
		$data=array(//'datasupplier'=>$this->mpengajuan->ambilData('apt_supplier'),
					'no_pengajuan'=>$no_pengajuan,
					//'itemtransaksi'=>$this->mpengajuan->ambilItemData('apt_pengajuan','no_pengajuan="'.$no_pengajuan.'"'),
					'itemtransaksi'=>$this->mpengajuan->getPengajuan($no_pengajuan),
					'itemsdetiltransaksi'=>$this->mpengajuan->getAllDetailPengajuan($no_pengajuan),
					'itemapprove'=>$this->mpengajuan->tampilApprover($no_pengajuan),
					//'applogin'=>$this->mpengajuan->ambilApp()
					'supplier'=>$this->mpengajuan->ambilData('apt_supplier'),
					'kd_applogin'=>$kd_applogin
					);
		//debugvar($data['itemsdetiltransaksi']);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/transaksi/pengajuan/tambahpengajuan',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function simpanpengajuan(){
		//debugvar('xx');
		$msg=array();
		$submit=$this->input->post('submit');
		$no_pengajuan=$this->input->post('no_pengajuan');
		$tgl_pengajuan=$this->input->post('tgl_pengajuan');
		$keterangan=$this->input->post('keterangan');
		$jenis_sp=$this->input->post('jenis_sp');
		
		$kd_obat=$this->input->post('kd_obat');
		$nama_obat=$this->input->post('nama_obat');
		$qty_besar=$this->input->post('qty_besar');
		$kd_supplier=$this->input->post('kd_supplier');
		$satuan_besar=$this->input->post('satuan_besar');
		$qty_kecil=$this->input->post('qty_kecil');
		$satuan_kecil=$this->input->post('satuan_kecil'); 
		$harga_beli=$this->input->post('harga_beli');
		
		$nama_pegawai=$this->input->post('nama_pegawai');
		$status=$this->input->post('status');
		$kd_app=$this->input->post('kd_app');
		$kd_applogin=$this->input->post('kd_applogin');
		
		//$kd_user=$this->session->userdata('id_user'); 
		
		$msg['no_pengajuan']=$no_pengajuan;
		$msg['posting']=0;

		if($this->mpengajuan->isNumberExist($no_pengajuan)){ //edit
			$this->db->trans_start();
			$tgl_pengajuan1=convertDate($tgl_pengajuan);
		    $datapengajuananedit=array(//'tgl_pengajuan'=>convertDate($tgl_pengajuan),
										'tgl_pengajuan'=>$tgl_pengajuan1,
										'keterangan'=>$keterangan,
										'jenis_sp'=>$jenis_sp
									);
				
			$this->mpengajuan->update('apt_pengajuan',$datapengajuananedit,'no_pengajuan="'.$no_pengajuan.'"');
			$urut=1;
			
			$this->mpengajuan->delete('apt_pengajuan_detail','no_pengajuan="'.$no_pengajuan.'"');
			
			if(!empty($kd_obat)){
				foreach ($kd_obat as $key => $value){
					if(empty($value))continue;					

					$datadetil=array('no_pengajuan'=>$no_pengajuan,
									'urut_pengajuan'=>$urut,
									'kd_obat'=>$value,
									'kd_supplier'=>$kd_supplier[$key],
									'qty_besar'=>$qty_besar[$key],
									'kd_satuan_besar'=>$satuan_besar[$key],
									'qty_kecil'=>$qty_kecil[$key],
									'kd_satuan_kecil'=>$satuan_kecil[$key],
									'harga_beli'=>$harga_beli[$key]);

					$this->mpengajuan->insert('apt_pengajuan_detail',$datadetil);										
					$urut++;
				}
			}
			$this->db->trans_complete();
			$msg['pesan']="Data Berhasil Di Update";
		}else { //simpan baru
			$this->db->trans_start();
			$tgl=explode("-", $tgl_pengajuan);
			$kode=$this->mpengajuan->autoNumber($tgl[2],$tgl[1]);
			$kodebaru=$kode+1;
			$kodebaru=str_pad($kodebaru,4,0,STR_PAD_LEFT); 
			$no_pengajuan="PG.".$tgl[2].".".$tgl[1].".".$kodebaru;
			$msg['no_pengajuan']=$no_pengajuan;
			$tgl_pengajuan1=convertDate($tgl_pengajuan);
			$datapengajuan=array('no_pengajuan'=>$no_pengajuan,
									'tgl_pengajuan'=>$tgl_pengajuan1,
									'keterangan'=>$keterangan,
									'jenis_sp'=>$jenis_sp,
									'is_grouping'=>0,
									'status_approve'=>0);
			
			$this->mpengajuan->insert('apt_pengajuan',$datapengajuan);
			$urut=1;
			if(!empty($kd_obat)){
				foreach ($kd_obat as $key => $value){
					# code...
					if(empty($value))continue;
					//if($qty_kcl[$key]==''){$qty_kcl[$key]=0;}
					$datadetil=array('no_pengajuan'=>$no_pengajuan,
									'urut_pengajuan'=>$urut,
									'kd_obat'=>$value,
									'kd_supplier'=>$kd_supplier[$key],
									'qty_besar'=>$qty_besar[$key],
									'kd_satuan_besar'=>$satuan_besar[$key],
									'qty_kecil'=>$qty_kecil[$key],
									'kd_satuan_kecil'=>$satuan_kecil[$key],
									'harga_beli'=>$harga_beli[$key]);
					//debugvar($datadetil);
					$this->mpengajuan->insert('apt_pengajuan_detail',$datadetil);	
										
					$urut++;
				}
			}
			
			$msg['pesan']="Data Berhasil Di Simpan";
			$msg['posting']=3;
			$this->db->trans_complete();
		}


		if($submit=="buatpesanan"){
		//debugvar($no_grouping);	
		//debugvar($kd);
				//debugvar('1---> ok jawabannya');
				$this->db->trans_start();
				$supplier=$this->mgrouping->getAllSupplier($no_pengajuan);

				foreach($supplier as $supp){
					$kd_supp=$supp['kd_supplier'];
					
					$jambuat=$this->mgrouping->jamsekarang();
					$tglbuat=$this->mgrouping->tglsekarang();
					//$tglpesan=convertDate($tgl_grouping)." ".$jambuat;
					$tglpesan=convertDate($tglbuat)." ".$jambuat;
					//$tgl=explode("-", $tgl_grouping);
					$tgl=explode("-", $tglbuat);
					$kode=$this->mgrouping->autoNumberPemesanan($tgl[2],$tgl[1]);
					$kodebaru=$kode+1;
					$kodebaru=str_pad($kodebaru,4,0,STR_PAD_LEFT); 
					$no_pemesanan="PM.".$tgl[2].".".$tgl[1].".".$kodebaru;
					
					$obat=$this->mgrouping->ambilObat($no_pengajuan,$kd_supp);
					$urutobat=1;
					$datapesan=array('no_pemesanan'=>$no_pemesanan,
									'kd_supplier'=>$kd_supp,
									'tgl_pemesanan'=>$tglpesan,
									'jenis_sp'=>$jenis_sp,
									'keterangan'=>$keterangan,
									'tgl_tempo'=>convertDate($tglbuat),
									'status_approve'=>0,
									'no_pengajuan'=>$no_pengajuan);
					$this->mgrouping->insert('apt_pemesanan',$datapesan);

					foreach($obat as $obt){
						$ppn=10;					
						$dataobat=array('no_pemesanan'=>$no_pemesanan,
										'urut'=>$urutobat,
										'kd_obat'=>$obt['kd_obat'],
										'qty_besar'=>$obt['qty_besar'],
										'qty_kecil'=>$obt['qty_kecil'],
										'satuan_besar'=>$obt['kd_satuan_besar'],
										'satuan_kecil'=>$obt['kd_satuan_kecil'],
										'harga_beli'=>$obt['harga_beli'],
										'diskon'=>0,
										'ppn'=>$ppn,
										'jml_penerimaan'=>0);
						$this->mgrouping->insert('apt_pemesanan_detail',$dataobat);
						$this->db->update('apt_pengajuan_detail',array('no_pemesanan'=>$no_pemesanan),array('no_pengajuan'=>$obt['no_pengajuan'],'kd_obat'=>$obt['kd_obat'],'urut_pengajuan'=>$obt['urut_pengajuan']));
						$urutobat++;
					}
				}

				$sta=array('status_approve'=>1);
				$this->mgrouping->update('apt_pengajuan',$sta,'no_pengajuan="'.$no_pengajuan.'"');
				
				$this->db->trans_complete();
				
				$msg['pesan']="Pemesanan berhasil disimpan ";
				$msg['posting']=4;
			/*else{
				debugvar('0---> cancel jawabannya');
			}*/
			/**/
		}

		$msg['status']=1;
		$msg['keluar']=0;

		
		echo json_encode($msg);
	}
	
	public function hapuspengajuan($no_pengajuan=""){
		if(!$this->muser->isAkses("32")){
			$this->restricted();
			return false;
		}
		//$kd_unit_apt="";
		$msg=array();
		$error=0;
		if(empty($no_pengajuan)){
			$msg['pesan']="Pilih Transaksi yang akan di hapus";
			echo "<script>Alert('".$msg['pesan']."')</script>";
		}else{		
			$this->db->trans_start();
			$this->mpengajuan->delete('apt_app_pengajuan','no_pengajuan="'.$no_pengajuan.'"');
			$this->mpengajuan->delete('apt_pengajuan_detail','no_pengajuan="'.$no_pengajuan.'"');	
			$this->mpengajuan->delete('apt_pengajuan','no_pengajuan="'.$no_pengajuan.'"');
			$this->db->trans_complete();		
			redirect('/transapotek/aptpengajuan/');
		}
	}
		
	/*public function ambildaftarobatbynama(){
		$nama_obat=$this->input->post('nama_obat');
		
		$this->datatables->select("apt_obat.kd_obat, apt_obat.nama_obat, apt_satuan_kecil.satuan_kecil, ifnull( sum(substring_index( apt_stok_unit.jml_stok, '.', 1 )) , 0 ) as jml_stok, 'Pilihan' as pilihan,apt_obat.pembanding,nama_unit_apt",false);
		
		$this->datatables->from("apt_obat");
		$this->datatables->edit_column('pilihan', '<a class="btn" onclick=\'pilihobat("$1","$2","$3","$4","$5","$6")\'>Pilih</a>','apt_obat.kd_obat, apt_obat.nama_obat, apt_satuan_kecil.satuan_kecil, apt_obat.pembanding, jml_stok, nama_unit_apt');		
		if(!empty($nama_obat))$this->datatables->like('apt_obat.nama_obat',$nama_obat,'both');
		
		$this->datatables->where('apt_stok_unit.kd_unit_apt','U01');
		
		$this->datatables->join('apt_satuan_kecil','apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil','left');
		$this->datatables->join('apt_stok_unit','apt_obat.kd_obat=apt_stok_unit.kd_obat','left');
		$this->datatables->join('apt_unit','apt_stok_unit.kd_unit_apt=apt_unit.kd_unit_apt','left');
		$this->db->group_by("apt_stok_unit.kd_obat");
		$results = $this->datatables->generate();
		echo ($results);	
	}*/
	
	public function ambildaftarobatbynama(){
		$nama_obat=$this->input->post('nama_obat');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt_gudang');
		
		$this->datatables->select("apt_obat.kd_obat as kd_obat1, apt_obat.nama_obat, 		
			apt_satuan_besar.satuan_besar,apt_satuan_kecil.satuan_kecil,'Pilihan' as pilihan,
			apt_obat.pembanding,ifnull(obat_supplier.harga_beli,apt_obat.harga_beli) as harga_beli,
			obat_supplier.kd_supplier",false);
		
		$this->datatables->from("apt_obat");
		$this->datatables->edit_column('pilihan', 
			'<a class="btn" onclick=\'pilihobat("$1","$2","$3","$4","$5","$6")\'>Pilih</a>',
			'kd_obat1, apt_obat.nama_obat, apt_satuan_besar.satuan_besar, apt_satuan_kecil.satuan_kecil, 
			harga_beli, obat_supplier.kd_supplier');		
		if(!empty($nama_obat))$this->datatables->like('apt_obat.nama_obat',$nama_obat,'both');
		$this->datatables->where('is_aktif','1');
		$this->datatables->join('apt_satuan_kecil','apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil','left');

		$this->datatables->join('apt_satuan_besar','apt_obat.kd_satuan_besar=apt_satuan_besar.kd_satuan_besar','left');		

		$this->datatables->join('apt_obat_satuan','apt_obat.kd_obat=apt_obat_satuan.kd_obat and apt_obat.kd_satuan_besar=apt_obat_satuan.kd_satuan','left');		

		$this->datatables->join('(select kd_obat,kd_supplier,min(harga_beli) as harga_beli from apt_obat_distributor group by kd_obat,kd_supplier) as obat_supplier','apt_obat.kd_obat=obat_supplier.kd_obat','left');		
		$this->db->group_by("apt_obat.kd_obat");
		$results = $this->datatables->generate();
		echo ($results);	
	}
	
	public function ambildaftarobatbykode(){
		$kd_obat=$this->input->post('kd_obat');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt_gudang');
		
		$this->datatables->select("apt_obat.kd_obat as kd_obat1, apt_obat.nama_obat, 		
			apt_satuan_besar.satuan_besar,apt_satuan_kecil.satuan_kecil,'Pilihan' as pilihan,
			apt_obat.pembanding,ifnull(obat_supplier.harga_beli,apt_obat.harga_beli) as harga_beli,
			obat_supplier.kd_supplier",false);
		
		$this->datatables->from("apt_obat");
		$this->datatables->edit_column('pilihan', 
			'<a class="btn" onclick=\'pilihobat("$1","$2","$3","$4","$5","$6")\'>Pilih</a>',
			'kd_obat1, apt_obat.nama_obat, apt_satuan_besar.satuan_besar, apt_satuan_kecil.satuan_kecil, 
			harga_beli, obat_supplier.kd_supplier');		
		if(!empty($kd_obat))$this->datatables->like('apt_obat.kd_obat',$kd_obat,'both');
		$this->datatables->where('is_aktif','1');
		
		$this->datatables->join('apt_satuan_kecil','apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil','left');
		$this->datatables->join('apt_satuan_besar','apt_obat.kd_satuan_besar=apt_satuan_besar.kd_satuan_besar','left');		

		$this->datatables->join('(select kd_obat,kd_supplier,min(harga_beli) as harga_beli from apt_obat_distributor group by kd_obat,kd_supplier) as obat_supplier','apt_obat.kd_obat=obat_supplier.kd_obat','left');		

		$this->db->group_by("apt_obat.kd_obat");
		$results = $this->datatables->generate();
		echo ($results);			
	}
	
	public function ambilsupplierbykode(){
		$q=$this->input->get('query');
		$items=$this->mpengajuan->ambilData3($q);
		echo json_encode($items);
	}
	
	public function ambilsatuanobat(){
		$q=$this->input->get('query');
		$query=$this->db->query('select apt_obat_satuan.kd_satuan,apt_satuan_kecil.satuan_kecil from apt_obat_satuan join apt_satuan_kecil on apt_obat_satuan.kd_satuan=apt_satuan_kecil.kd_satuan_kecil where apt_obat_satuan.kd_obat ="'.$q.'" order by urut desc');
		$items=$query->result_array();
		echo json_encode($items);
	}
	
	public function ambilhargadistributor(){
		$q=$this->input->get('query');
		$distributor=$this->input->get('distributor');
		$query=$this->db->query('select ifnull(apt_obat_distributor.harga_beli,apt_obat.harga_beli) as harga_beli,ifnull(apt_obat_distributor.kd_obat,"kosong") as kd_obat from apt_obat left join (select * from apt_obat_distributor where apt_obat_distributor.kd_supplier="'.$distributor.'" ) as apt_obat_distributor on apt_obat.kd_obat=apt_obat_distributor.kd_obat where apt_obat.kd_obat ="'.$q.'"  ');
		$items=$query->row_array();
		echo json_encode($items);
	}
	
	public function ambildetilsatuanobat(){
		$q=$this->input->get('query');
		$satuan=$this->input->get('satuan');
		$query=$this->db->query('select apt_obat_satuan.* from apt_obat_satuan join apt_satuan_kecil on apt_obat_satuan.kd_satuan=apt_satuan_kecil.kd_satuan_kecil where apt_obat_satuan.kd_obat ="'.$q.'" and apt_obat_satuan.kd_satuan="'.$satuan.'" order by urut desc');
		$items=$query->row_array();
		echo json_encode($items);
	}

	public function ambilsupplierbynama(){
		$q=$this->input->get('query');
		$items=$this->mpengajuan->ambilData4($q);
		echo json_encode($items);
	}
	
	public function periksapengajuan() {
		$msg=array();
		$submit=$this->input->post('submit');
		$no_pengajuan=$this->input->post('no_pengajuan');
		$tgl_pengajuan=$this->input->post('tgl_pengajuan');
		//$tgl_tempo=$this->input->post('tgl_tempo');
		//$kd_supplier=$this->input->post('kd_supplier');
		//$nama=$this->input->post('nama');
		$keterangan=$this->input->post('keterangan');
		
		$kd_obat=$this->input->post('kd_obat');
		$nama_obat=$this->input->post('nama_obat');
		$qty_besar=$this->input->post('qty_besar');
		$satuan_besar=$this->input->post('satuan_besar');
		$qty_kecil=$this->input->post('qty_kecil');
		$satuan_kecil=$this->input->post('satuan_kecil'); 
		$harga_beli=$this->input->post('harga_beli');

		$nama_pegawai=$this->input->post('nama_pegawai');
		$status=$this->input->post('status');
		$kd_app=$this->input->post('kd_app');
		$kd_applogin=$this->input->post('kd_applogin');
		
		$jumlaherror=0;
		$msg['status']=1;
		$msg['clearform']=0;
		$msg['pesanatas']="";
		$msg['pesanlain']="";
		
		if(!empty($kd_obat)){
			foreach ($kd_obat as $key => $value) {
				# code...
				if(empty($value))continue;
				
				if(!$this->mpengajuan->isObat($value)){
					//if(empty($qty_kcl[$key]))$qty_kcl[$key]="xx";
					$msg['status']=0;
					$msg['pesanlain'].="Kd Obat ".$value." tidak Di ketahui<br/>";					
				}
				
				if(empty($qty_kecil[$key])){
					//if(empty($qty_kcl[$key]))$qty_kcl[$key]="xx";
					$msg['status']=0;
					$msg['pesanlain'].="Qty ".$value." tidak boleh Kosong <br/>";					
				}
				if(empty($qty_besar[$key])){
					//if(empty($qty_kcl[$key]))$qty_kcl[$key]="xx";
					$msg['status']=0;
					$msg['pesanlain'].="Qty ".$value." tidak boleh Kosong <br/>";					
				}
				if(empty($harga_beli[$key])){
					$msg['status']=0;
					$msg['pesanlain'].="Harga ".$value." tidak boleh Kosong <br/>";					
				}
			}
		}
		if($jumlaherror>0){
			$msg['status']=0;
			$msg['error']=$jumlaherror;
			$msg['pesanatas']="Terdapat beberapa kesalahan input silahkan cek inputan anda";
		}
		echo json_encode($msg);
	}
	
	public function ambilitem()
	{
		$q=$this->input->get('query');
		$items=$this->mpengajuan->getPengajuan($q);
		//$items=$this->mpengajuan->ambilItemData('apt_pengajuan','no_pengajuan="'.$q.'"');

		echo json_encode($items);
	}
	
	public function ambilitems()
	{
		$q=$this->input->get('query');
		$items=$this->mpengajuan->getAllDetailPengajuan($q);

		echo json_encode($items);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */