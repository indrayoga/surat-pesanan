<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH.'controllers/rumahsakit.php');
include_once(APPPATH.'third_party/phpexcelreader/Excel/reader.php');
//class Stokopname extends CI_Controller {
class Stokopname extends Rumahsakit {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	protected $title='SIM RS - Sistem Informasi Rumah Sakit';

	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('utilities');
		$this->load->library('pagination');
		$this->load->model('apotek/mstokopname');
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		if(empty($kd_unit_apt)){
			redirect('/home/');
		}
	}
	
	public function restricted(){
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/jquery.dualListBox-1.3.min.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		//$this->load->view('master/header',$dataheader);
		$this->load->view('headerapotek',$dataheader);
		$data=array();
		parent::view_restricted($data);
		$this->load->view('footer');
	}
	
	public function index()
	{
		if(!$this->muser->isAkses("80")){
			$this->restricted();
			return false;
		}
		
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-timepicker.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
			);

		$data=array('items'=>$this->mstokopname->ambilDataRelasi('apt_log_stokopname','apt_unit','apt_log_stokopname.kd_unit_apt=apt_unit.kd_unit_apt'));
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/transaksi/stokopname/daftarstokopname',$data);
		$this->load->view('footer',$datafooter);
	}
	
	public function stokopnameobat(){
		if(!$this->muser->isAkses("80")){
			$this->restricted();
			return false;
			
		}
		
		$tanggal=date('d-m-Y');
		$nama_obat='';
		$kd_obat='';
		//$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$kd_unit_apt='';
		//$kd_sumber='';
		$submit=$this->input->post('submit');
		$submit1=$this->input->post('submit1');
		$periodeawal=date('d-m-Y');
		$periodeakhir=date('d-m-Y');
		//debugvar($submit);
		if($this->input->post('nama_obat')!=''){
			$nama_obat=$this->input->post('nama_obat');
		}
		if($this->input->post('kd_obat')!=''){
			$kd_obat=$this->input->post('kd_obat');
		}
		if($this->input->post('periodeawal')!=''){
			$periodeawal=$this->input->post('periodeawal');
		}
		if($this->input->post('periodeakhir')!=''){
			$periodeakhir=$this->input->post('periodeakhir');
		}
		if($this->input->post('kd_unit_apt')!=''){
			$kd_unit_apt=$this->input->post('kd_unit_apt');
		}
		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-timepicker.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>"Penyesuaian Stok Obat :: ".$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
		);		
		
		$data=array('items'=>array(),
					//'items'=>$this->mstokopname->getStokopname($kd_barang,$kd_lokasi,$kd_kategori),
					'tanggal'=>$tanggal,
					'periodeawal'=>$periodeawal,
					'periodeakhir'=>$periodeakhir,
					'dataunit'=>$this->mstokopname->ambilData('apt_unit'),
					'nama_obat'=>$nama_obat,
					'kd_obat'=>$kd_obat,
					'kd_unit_apt'=>$kd_unit_apt
					);
		
		if($submit==1) $data['items']=$this->mstokopname->getStokopname($kd_obat,$kd_unit_apt);
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/transaksi/stokopname/stokopnameobat',$data);
		$this->load->view('footer',$datafooter);
	}

	public function exportstokopname(){
		if(!$this->muser->isAkses("92")){
			$this->restricted();
			return false;
		}
		
		$tanggal=date('d-m-Y');
		$kd_unit_apt='';

		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-timepicker.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
		);		
		
		$data=array(
					'dataunit'=>$this->mstokopname->ambilData('apt_unit'),
					'kd_unit_apt'=>$kd_unit_apt
					);
		
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/transaksi/stokopname/exportstokopname',$data);
		$this->load->view('footer',$datafooter);		
	}

	public function importstokopname(){
		if(!$this->muser->isAkses("93")){
			$this->restricted();
			return false;
		}
		
		$tanggal=date('d-m-Y');
		$kd_unit_apt='';

		$cssfileheader=array('bootstrap.css','bootstrap-responsive.min.css','font-awesome.min.css','style.css','prettify.css','jquery-ui.css','DT_bootstrap.css','responsive-tables.css','datepicker.css','theme.css');
		$jsfileheader=array('vendor/modernizr-2.6.2-respond-1.1.0.min.js',
							'vendor/jquery-1.9.1.min.js',
							'vendor/jquery-migrate-1.1.1.min.js',
							'vendor/jquery-ui-1.10.0.custom.min.js',
							'vendor/bootstrap.min.js',
							'lib/jquery.tablesorter.min.js',
							'lib/jquery.dataTables.min.js',
							'lib/DT_bootstrap.js',
							'lib/responsive-tables.js',
							'lib/bootstrap-datepicker.js',
							'lib/bootstrap-inputmask.js',
							'lib/bootstrap-timepicker.js',
							'lib/bootstrap-modal.js',
							'spin.js',
							'main.js');
		$dataheader=array(
			'jsfile'=>$jsfileheader,
			'cssfile'=>$cssfileheader,
			'title'=>$this->title
			);

		$jsfooter=array();
		$datafooter=array(
			'jsfile'=>$jsfooter
		);		
		
		$data=array(
					'dataunit'=>$this->mstokopname->ambilData('apt_unit'),
					'kd_unit_apt'=>$kd_unit_apt
					);
		
		//debugvar($data);
		$this->load->view('headerapotek',$dataheader);
		$this->load->view('apotek/transaksi/stokopname/importstokopname',$data);
		$this->load->view('footer',$datafooter);		
	}
	
	public function simpanstokopname(){	
		$msg=array();
		$submit=$this->input->post('submit');
		$tanggal=$this->input->post('tanggal');
		$kd_obat=$this->input->post('kd_obat');
		$nama_obat=$this->input->post('nama_obat');
		$kd_unit_apt=$this->input->post('kd_unit_apt');
		//$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		//$kd_lokasi=$this->input->post('kd_lokasi');
		
		$kd_obat2=$this->input->post('kd_obat2');
		$nama_obat2=$this->input->post('nama_obat2');
		$tgl_expire=$this->input->post('tgl_expire');
		$stoklama=$this->input->post('stoklama');
		$stokbaru=$this->input->post('stokbaru');
		$alasan=$this->input->post('alasan');
		$kd_unit_apt1=$this->input->post('kd_unit_apt1');
		$jam=$this->input->post('jam');
		$kd_milik="01";
		//$tanggal=$this->input->post('tanggal');
		//$kd_user=0;
		$this->db->trans_start();
		$msg['status']=1;
		$kd_user=$this->session->userdata('id_user'); 
		
		if($submit=="simpanstokopname"){
			//debugvar($tanggal);
			$kode=$this->mstokopname->nomor();
			$nomor=$kode+1;
			$msg['nomor']=$nomor;
			$selisih=$stokbaru-$stoklama;
			
			$tgl=convertDate($tanggal)." ".$jam;
			$datasimpan=array('nomor'=>$nomor,
							//'tanggal'=>convertDate($tanggal),
							'tanggal'=>$tgl,
							'kd_unit_apt'=>$kd_unit_apt1,
							'kd_obat'=>$kd_obat2,
							'kd_milik'=>$kd_milik,
							//'tgl_expired'=>convertDate($tgl_expire),
							'tgl_expired'=>$tgl_expire,
							'qty'=>$selisih,
							'alasan'=>$alasan,
							'kd_user'=>$kd_user,
							'stok_lama'=>$stoklama,
							'stok_baru'=>$stokbaru);
			//debugvar($datasimpan);
			$this->mstokopname->insert('history_perubahan_stok',$datasimpan);
			
			//debugvar('replace apt_stok_unit set kd_unit_apt="'.$kd_unit_apt1.'" and jml_stok="'.$stokbaru.'" and kd_obat="'.$kd_obat2.'" and tgl_expire="'.$tgl_expire.'" and kd_milik="01" ');
			$itemstok=$this->mstokopname->ambilitemData('apt_stok_unit',' kd_unit_apt="'.$kd_unit_apt1.'" and kd_obat="'.$kd_obat2.'" and tgl_expire="'.$tgl_expire.'" ');
			if(empty($itemstok)){
				$datastok=array('kd_unit_apt'=>$kd_unit_apt1,'kd_obat'=>$kd_obat2,'kd_milik'=>'01','tgl_expire'=>$tgl_expire,'jml_stok'=>$stokbaru); //BARANG  DONASI DARI PT.MERCK (TIDAK ADA FAKTUR)				
				$this->mstokopname->insert('apt_stok_unit',$datastok);				
			}else{
				$datastok=array('jml_stok'=>$stokbaru); //BARANG  DONASI DARI PT.MERCK (TIDAK ADA FAKTUR)
				$this->mstokopname->update('apt_stok_unit',$datastok,'kd_unit_apt="'.$kd_unit_apt1.'" and kd_obat="'.$kd_obat2.'" and tgl_expire="'.$tgl_expire.'"');				
			}
			
			$msg['pesan']="Stokopname berhasil disimpan";
		}

		$this->db->trans_complete();
		//echo json_encode($msg);
	}
	
	public function periksastokopname(){
		$msg=array();
		$submit=$this->input->post('submit');
		$kd_obat=$this->input->post('kd_obat');
		$nama_obat=$this->input->post('nama_obat');
		$kd_unit_apt=$this->input->post('kd_unit_apt');
		//$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		
		$kd_obat2=$this->input->post('kd_obat2');
		$nama_obat2=$this->input->post('nama_obat2');
		$tgl_expire=$this->input->post('tgl_expire');
		$stoklama=$this->input->post('stoklama');
		$stokbaru=$this->input->post('stokbaru');
		$alasan=$this->input->post('alasan');
		$kd_unit_apt1=$this->session->userdata('kd_unit_apt1');
		$tanggal=$this->input->post('tanggal');
		$jam=$this->input->post('jam');
		
		//$kd_user=0;
		//$kd_lokasi=$this->session->userdata('kd_lokasi');
		
		$jumlaherror=0;
		$msg['status']=1;
		$msg['clearform']=0;
		$msg['pesanatas']="";
		$msg['pesanlain']="";
		
		if($submit=="hapus"){
		}
		else{
			/*if(empty($stokbaru)){
				$jumlaherror++;
				$msg['id'][]="qty";
				$msg['pesan'][]="Kolom stok baru harus di isi";
			}*/
			if($jumlaherror>0){
				$msg['status']=0;
				$msg['error']=$jumlaherror;
				$msg['pesanatas']="Terdapat beberapa kesalahan input silahkan cek inputan anda";
			}
		}
		echo json_encode($msg);
	}

	public function periksaprosesimportstokopname(){
		$msg=array();
		$msg['kd_obat']="";
		$kd_unit_apt=$this->input->post('kd_unit_apt');
		//debugvar($_FILES);
		$data = new Spreadsheet_Excel_Reader();
		$data->read($_FILES['file']['tmp_name']); 
		$baris = $data->sheets[0]['numRows'];
		$jumlaherror=0;
		$msg['status']=1;
		$msg['clearform']=0;
		$msg['pesan']="";
		$msg['pesanlain']="";
		$x=7;
		while($x<=$data->sheets[0]['numRows']) {
			$obat = isset($data->sheets[0]['cells'][$x][1]) ? $data->sheets[0]['cells'][$x][1] : '';
			$unit = isset($data->sheets[0]['cells'][$x][2]) ? $data->sheets[0]['cells'][$x][2] : '';
			$tglexp = isset($data->sheets[0]['cells'][$x][5]) ? $data->sheets[0]['cells'][$x][5] : '';
			$jmlstok = isset($data->sheets[0]['cells'][$x][6]) ? $data->sheets[0]['cells'][$x][6] : '0';
			$stokopname = isset($data->sheets[0]['cells'][$x][7]) ? $data->sheets[0]['cells'][$x][7] : '0';
			if($unit!=$kd_unit_apt){
				$jumlaherror++;
				$msg['pesan'].="Kode Unit Pada Baris ".$x." dan Kolom B Tidak Cocok, Kode Yang Benar Adalah ".$kd_unit_apt." <br/>";
			}

			if(!$this->mstokopname->isObat($obat)){
				$jumlaherror++;
				$msg['pesan'].="Kode Obat Pada Baris ".$x." dan Kolom A Tidak Cocok Dengan yang Ada di Database <br/>";
			}
			if(!validateDateMySQL($tglexp) && $tglexp!="0000-00-00"){
				$jumlaherror++;
				$msg['pesan'].="Format Tanggal Pada Baris ".$x." dan Kolom E Tidak Sesuai, Gunakan Format yyyy-mm-dd <br/>";
			}
			if(!is_numeric($jmlstok)){
				$jumlaherror++;
				$msg['pesan'].="Nilai Jml Stok Pada Baris ".$x." dan Kolom F Harus Angka <br/>";				
			}
			if(!is_numeric($stokopname)){
				$jumlaherror++;
				$msg['pesan'].="Nilai Stokopname Pada Baris ".$x." dan Kolom G Harus Angka <br/>";				
			}
			$dataobat=$this->mstokopname->ambilitemData('apt_stok_unit','kd_obat="'.$obat.'" and kd_unit_apt="'.$unit.'" and tgl_expire="'.$tglexp.'" ');
			if(!empty($dataobat)){
				if($dataobat['jml_stok']!=$jmlstok){
					$jumlaherror++;
					$msg['pesan'].="Jumlah Stok Pada Baris ".$x." Tidak Sama dengan Jumlah stok yang ada di sistem, pastikan data yang di export adalah data yang terbaru <br/>";									
				}
			}

			$x++;
		}
		if($jumlaherror>0){
			$msg['status']=0;
		}else{
			$x=7;
			$this->db->trans_start();
			$kd_user=$this->session->userdata('id_user'); 
			while($x<=$data->sheets[0]['numRows']) {
				$obat = isset($data->sheets[0]['cells'][$x][1]) ? $data->sheets[0]['cells'][$x][1] : '';
				$unit = isset($data->sheets[0]['cells'][$x][2]) ? $data->sheets[0]['cells'][$x][2] : '';
				$tglexp = isset($data->sheets[0]['cells'][$x][5]) ? $data->sheets[0]['cells'][$x][5] : '';
				$jmlstok = isset($data->sheets[0]['cells'][$x][6]) ? $data->sheets[0]['cells'][$x][6] : '0';
				$stokopname = isset($data->sheets[0]['cells'][$x][7]) ? $data->sheets[0]['cells'][$x][7] : '0';
				$selisih=$stokopname-$jmlstok;
				$kode=$this->mstokopname->nomor();
				$nomor=$kode+1;
				$datasimpan=array('nomor'=>$nomor,
								//'tanggal'=>convertDate($tanggal),
								'tanggal'=>date('Y-m-d H:i:s'),
								'kd_unit_apt'=>$unit,
								'kd_obat'=>$obat,
								'kd_milik'=>'01',
								//'tgl_expired'=>convertDate($tgl_expire),
								'tgl_expired'=>$tglexp,
								'qty'=>$selisih,
								'alasan'=>"SO Via Excel",
								'kd_user'=>$kd_user,
								'stok_lama'=>$jmlstok,
								'stok_baru'=>$stokopname);
				//debugvar($datasimpan);
				$this->mstokopname->insert('history_perubahan_stok',$datasimpan);

				$this->db->query("replace into apt_stok_unit set kd_unit_apt='".$unit."',kd_obat='".$obat."',kd_milik='01',tgl_expire='".$tglexp."',jml_stok='".$stokopname."' ");
				
				$x++;
			}	

			if (move_uploaded_file($_FILES['file']['tmp_name'],"./stokopname/".$_FILES['file']['name'])) {
				$datafile=array('tanggal'=>date('Y-m-d h:i:s'),
								'kd_unit_apt'=>$unit,
								'file'=>$_FILES['file']['name']);
				$this->mstokopname->insert('apt_log_stokopname',$datafile);
			}			
	
			$this->db->trans_complete();
			$msg['pesan']="Import Data Berhasil";
		}
		//$kd_user=0;
		//$kd_lokasi=$this->session->userdata('kd_lokasi');
				
		echo json_encode($msg);
	}
	
	public function ambilobatbynama(){
		$q=$this->input->get('query');
		$items=$this->mstokopname->ambilData4($q);
		echo json_encode($items);
	}
	
	public function ambilitems() {
		$q=$this->input->get('query');
		$unit=$this->input->get('unit');
		$tgl=$this->input->get('tgl');
		$items=$this->mstokopname->ambilData2($q,$unit,convertDate($tgl));
		//debugvar($items);
		echo json_encode($items);
	}

	public function stokopnamexls($kd_unit_apt=""){ //bwt yg penjualan obat
		$kd_unit_apt=$this->input->post('kd_unit_apt');
		$this->load->library('phpexcel'); 
		$this->load->library('PHPExcel/iofactory');
		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->getProperties()->setTitle("title")->setDescription("description");
		$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:G3');
		$objPHPExcel->getActiveSheet()->mergeCells('A4:G4');

		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(10); //NO
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(10); //TGL JUAL
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(45); //NO JUAL
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(15); //KODE
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(15); //NAMA
		$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(10); //UNIT
		
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'2')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'3')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'4')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'5')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		}	
		$objPHPExcel->getActiveSheet()->setCellValue ('A2','STOKOPNAME');
		$objPHPExcel->getActiveSheet()->setCellValue ('A3','POLIKLINIK IBNU SINA BALIKPAPAN ');
		
		if($kd_unit_apt==''){ $objPHPExcel->getActiveSheet()->setCellValue ('A4','Unit : Semua Unit Apotek');}
		else{
			$namaunit=$this->mstokopname->namaUnit($kd_unit_apt);
			$objPHPExcel->getActiveSheet()->setCellValue ('A4','Unit : '.$namaunit);
		}

		for($x='A';$x<='G';$x++){
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->getAlignment()->applyFromArray(
				array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));
		
			$objPHPExcel->getActiveSheet()->getStyle($x.'6')->applyFromArray(array('font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'size'		=>12,'color'     => array('rgb' => '000000')),
																			'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
																			'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')))));
		}		
		$objPHPExcel->getActiveSheet()->setCellValue ('A6','KD OBAT.');
		$objPHPExcel->getActiveSheet()->setCellValue ('B6','KD UNIT');
		$objPHPExcel->getActiveSheet()->setCellValue ('C6','NAMA OBAT');
		$objPHPExcel->getActiveSheet()->setCellValue ('D6','HARGA BELI');
		$objPHPExcel->getActiveSheet()->setCellValue ('E6','TGL EXPIRE');
		$objPHPExcel->getActiveSheet()->setCellValue ('F6','JML STOK');
		$objPHPExcel->getActiveSheet()->setCellValue ('G6','STOK OPNAME');
		$items=array();
		$items=$this->mstokopname->getExportStokopname($kd_unit_apt);
		$baris=7;
		$nomor=1;
		$total=0;
		foreach ($items as $item) {
			# code...
			for($x='A';$x<='G';$x++){
				if($x=='A'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));					
				}else if($x=='B'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='C'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='D'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='E'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='F'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}else if($x=='G'){
					$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->getAlignment()->applyFromArray(
						array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'   => 0,));										
				}
			
				$objPHPExcel->getActiveSheet()->getStyle($x.$baris)->applyFromArray(array(
					'font'    => array('name'      => 'calibri','horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
					'size'		=>12,'color'     => array('rgb' => '000000')),'borders' => array('bottom'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')),'top'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),
					'left'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('rgb' => '000000')),'right'     => array('style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')))));
			}

			$objPHPExcel->getActiveSheet()->setCellValueExplicit ('A'.$baris,$item['kd_obat'], PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValue ('B'.$baris,$item['kd_unit_apt']);
			$objPHPExcel->getActiveSheet()->setCellValue ('C'.$baris,$item['nama_obat']);
			$objPHPExcel->getActiveSheet()->setCellValue ('D'.$baris,$item['harga_beli']);
			$objPHPExcel->getActiveSheet()->setCellValue ('E'.$baris,$item['tgl_expire']);			
			$objPHPExcel->getActiveSheet()->setCellValue ('F'.$baris,$item['jml_stok']);
			$nomor=$nomor+1; $baris=$baris+1;
		}	
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save("download/stokopname.xls");
		redirect(base_url()."download/stokopname.xls");
		//header("Location: ".base_url()."download/laporanpenjualanobat.xls");
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */