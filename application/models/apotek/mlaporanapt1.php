<?
class Mlaporanapt extends CI_Model {

	function __construct() {
		
		parent::__construct();
	}

	function getExpire($interval,$kd_unit){
		$query=$this->db->query("SELECT apt_stok_unit.*,apt_obat.nama_obat,date_add(now(),interval ".$interval." day) FROM apt_stok_unit join apt_obat on apt_stok_unit.kd_obat=apt_obat.kd_obat WHERE apt_stok_unit.tgl_expire between curdate() and date_add(curdate(),interval ".$interval." day) and kd_unit_apt='".$kd_unit."'  ");
		return $query->result_array();
	}

	function ambilData($table,$condition=""){
		if(!empty($condition)){
			$this->db->where($condition,null,false);
		}
		$query= $this->db->get($table);
		return $query->result_array();
	}

	function ambilItemData($table,$condition=""){
		if(!empty($condition)){
			$this->db->where($condition,null,false);
		}
		$query= $this->db->get($table);
		return $query->row_array();
	}

    function insert($table,$data) {
		$this->db->insert($table, $data);
		return $this->db->insert_id();
    }	

    function update($table,$data,$id) {
    	if(!empty($id)){
    		$this->db->where($id,null,false);
    	}
		
		$this->db->update($table, $data);
		return true;
    }	

    function delete($table,$id) {
		$this->db->where($id,null,false);
		$this->db->delete($table);
		return true;
    }	
	
	//function getAllPenerimaanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$shift,$tgl_tempo,$kd_supplier){
	//function getAllPenerimaanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$tgl_tempo,$kd_supplier,$kd_pabrik){
	function getAllPenerimaanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$kd_supplier,$kd_pabrik){
		//if(!empty($nama)) $this->db->like('nama_pasien',$nama);
		/*$this->db->select("apt_penerimaan.no_penerimaan,apt_penerimaan.no_faktur,apt_penerimaan.tgl_penerimaan,apt_penerimaan.tgl_tempo,apt_obat.nama_obat,apt_satuan_kecil.satuan_kecil,
						apt_penerimaan_detail.qty_kcl,apt_penerimaan_detail.harga_beli,apt_penerimaan_detail.disc_prs,((apt_penerimaan_detail.qty_kcl*apt_penerimaan_detail.harga_beli)-apt_penerimaan_detail.harga_belidisc) as jumlahharga,apt_penerimaan_detail.ppn_item,
						substring_index(((((apt_penerimaan_detail.qty_kcl*apt_penerimaan_detail.harga_beli)-apt_penerimaan_detail.harga_belidisc))+(((apt_penerimaan_detail.qty_kcl*apt_penerimaan_detail.harga_beli)-apt_penerimaan_detail.harga_belidisc)*(apt_penerimaan_detail.ppn_item/100))),'.',1) as jumlahtotal",FALSE);
		
		if(!empty($periodeawal))$this->db->where('date(apt_penerimaan.tgl_penerimaan)>=',$periodeawal);
		if(!empty($periodeakhir))$this->db->where('date(apt_penerimaan.tgl_penerimaan)<=',$periodeakhir);
		if(!empty($tgl_tempo)) $this->db->like('apt_penerimaan.tgl_tempo',$tgl_tempo);
		if(!empty($shift)) $this->db->like('apt_penerimaan.shift',$shift);
		if(!empty($kd_unit_apt))$this->db->like('apt_penerimaan.kd_unit_apt',$kd_unit_apt);
		if(!empty($kd_supplier))$this->db->like('apt_penerimaan.kd_supplier',$kd_supplier);
		
		$this->db->join('apt_penerimaan','apt_penerimaan.no_penerimaan=apt_penerimaan_detail.no_penerimaan','left');		
		$this->db->join('apt_supplier','apt_penerimaan.kd_supplier=apt_supplier.kd_supplier','left');
		$this->db->join('apt_unit','apt_penerimaan.kd_unit_apt=apt_unit.kd_unit_apt','left');
		$this->db->join('apt_obat','apt_obat.kd_obat=apt_penerimaan_detail.kd_obat','left');
		$this->db->join('apt_satuan_kecil','apt_satuan_kecil.kd_satuan_kecil=apt_obat.kd_satuan_kecil','left');
		
		$query=$this->db->get("apt_penerimaan_detail");
		return $query->result_array();*/
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		
		/*if($tgl_tempo!=''){$tgl_tempo1=convertDate($tgl_tempo); $a1=" and apt_penerimaan.tgl_tempo='$tgl_tempo1'";}
		else{$a1="";} $a=$a1;*/
		
		/*if($shift!=''){$b1=" and apt_penerimaan.shift='$shift'";}
		else{$b1="";} $b=$b1;*/
		if($kd_pabrik!=''){$b1=" and apt_obat.kd_pabrik='$kd_pabrik'";}
		else{$b1="";} $b=$b1;
		
		if($kd_supplier!=''){$c1=" and apt_supplier.kd_supplier='$kd_supplier'";}
		else{$c1="";} $c=$c1;
		
		/*$query=$this->db->query("select apt_penerimaan.no_penerimaan,apt_penerimaan.no_faktur,apt_penerimaan.tgl_penerimaan,apt_penerimaan.tgl_tempo,apt_penerimaan_detail.kd_obat,apt_obat.nama_obat,apt_satuan_kecil.satuan_kecil,
								apt_penerimaan_detail.qty_kcl,apt_penerimaan_detail.harga_beli,apt_penerimaan_detail.disc_prs,apt_penerimaan_detail.isidiskon,apt_penerimaan_detail.ppn_item
                                from apt_penerimaan,apt_penerimaan_detail, apt_obat,apt_satuan_kecil,apt_supplier,apt_unit where 
								apt_penerimaan.no_penerimaan=apt_penerimaan_detail.no_penerimaan and apt_penerimaan.kd_supplier=apt_supplier.kd_supplier and 
								apt_penerimaan.kd_unit_apt=apt_unit.kd_unit_apt and apt_penerimaan_detail.kd_obat=apt_obat.kd_obat and apt_penerimaan_detail.kd_unit_apt=apt_unit.kd_unit_apt 
								and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil and apt_penerimaan.tgl_penerimaan between '$periodeawal1' and '$periodeakhir1' $a $b and apt_unit.kd_unit_apt='$kd_unit_apt' $c order by apt_penerimaan.no_penerimaan asc");*/
		$query=$this->db->query("select apt_penerimaan.no_penerimaan,apt_penerimaan.no_faktur,date_format(apt_penerimaan.tgl_penerimaan,'%d-%m-%Y') as tgl_penerimaan,
								apt_penerimaan.tgl_tempo,apt_penerimaan.kd_supplier,apt_supplier.nama,apt_pabrik.nama_pabrik,apt_penerimaan_detail.kd_obat,apt_obat.nama_obat,apt_satuan_kecil.satuan_kecil, 
								apt_penerimaan_detail.qty_kcl,apt_penerimaan_detail.harga_beli,apt_penerimaan_detail.disc_prs,apt_penerimaan_detail.isidiskon,apt_penerimaan_detail.ppn_item
								from apt_penerimaan,apt_supplier,apt_penerimaan_detail,apt_satuan_kecil,apt_obat left join apt_pabrik on apt_obat.kd_pabrik=apt_pabrik.kd_pabrik
								where apt_penerimaan.kd_supplier=apt_supplier.kd_supplier and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil and 
								date_format(apt_penerimaan.tgl_penerimaan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and apt_penerimaan.kd_unit_apt='$kd_unit_apt' 
								and apt_penerimaan_detail.no_penerimaan=apt_penerimaan.no_penerimaan and 
								apt_penerimaan_detail.kd_obat=apt_obat.kd_obat $b $c order by apt_penerimaan.no_penerimaan");
		return $query->result_array();
	}
	
	//function getAllPenerimaanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$shift,$tgl_tempo,$kd_supplier){
	//function getAllPenerimaanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$tgl_tempo,$kd_supplier,$kd_pabrik){
	function getAllReturApotek($periodeawal,$periodeakhir,$kd_unit_apt){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		
		if($kd_unit_apt!=''){$b1=" and b.kd_unit_asal='$kd_unit_apt'";}
		else{$b1="";} $b=$b1;
				
		$query=$this->db->query("SELECT date_format(tgl_retur,'%d-%m-%Y') as tanggal,a.no_retur,c.nama_obat,a.qty,b.alasan 
								FROM `apt_retur_gudang_detail` a left join apt_retur_gudang b on a.no_retur=b.no_retur 
								left join apt_obat c on a.kd_obat=c.kd_obat left join apt_unit d on b.kd_unit_asal=d.kd_unit_apt where 
								date(tgl_retur) between '$periodeawal1' and '$periodeakhir1' and posting=1 $b1 ");
		return $query->result_array();
	}

	function getTotalHargaJual($no_penjualan){
		$query=$this->db->query('select ifnull(sum(harga_jual*qty),0) as total from apt_penjualan_detail where no_penjualan="'.$no_penjualan.'" ');
		$item=$query->row_array();
		return $item['total'];
	}

	function getTotalAdmResep($no_penjualan){
		$query=$this->db->query('select ifnull(sum(adm_resep),0) as total from apt_penjualan_detail where no_penjualan="'.$no_penjualan.'" ');
		$item=$query->row_array();
		return $item['total'];
	}

	function getAllPenjualanApotek($periodeawal,$periodeakhir,$shiftapt,$is_lunas,$kd_unit_apt,$resep,$jenis_pasien,$dokter,$tipe,$user,$userkasir=""){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		/*$query=$this->db->query("select apt_penjualan.no_penjualan,apt_penjualan.tgl_penjualan,apt_penjualan.nama_pasien,apt_dokter.nama_dokter,apt_penjualan.resep,apt_penjualan.adm_racik,
								apt_penjualan.total_transaksi,apt_penjualan.total_bayar from apt_penjualan LEFT JOIN apt_dokter on apt_penjualan.kd_dokter=apt_dokter.kd_dokter 
								left join apt_unit on apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt where apt_penjualan.tgl_penjualan between '$periodeawal1' and '$periodeakhir1' and apt_penjualan.shiftapt like '%$shiftapt%' 
								and apt_penjualan.is_lunas='$is_lunas' and apt_penjualan.kd_unit_apt like '%$kd_unit_apt%' and apt_penjualan.resep like '%$resep%' 
								order by apt_penjualan.no_penjualan desc");*/
		/*$query=$this->db->query("select apt_penjualan.no_penjualan,apt_penjualan.tgl_penjualan,apt_penjualan.nama_pasien,apt_dokter.nama_dokter,apt_penjualan.resep,apt_penjualan.adm_racik,
								apt_penjualan.total_transaksi,apt_penjualan.is_lunas from apt_penjualan LEFT JOIN apt_dokter on apt_penjualan.kd_dokter=apt_dokter.kd_dokter 
								left join apt_unit on apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt where apt_penjualan.tgl_penjualan between '$periodeawal1' and '$periodeakhir1' and apt_penjualan.shiftapt like '%$shiftapt%' 
								and apt_penjualan.is_lunas='$is_lunas' and apt_penjualan.kd_unit_apt='$kd_unit_apt' and apt_penjualan.resep like '%$resep%' 
								order by apt_penjualan.no_penjualan desc");*/
		$tanggal="";
		if($shiftapt!=''){
			if($shiftapt==3){
				$date = $periodeakhir1;
				$date1 = str_replace('-', '/', $date);
				$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));				
				$tanggal=" and apt_penjualan.tgl_penjualan between '$periodeawal1 21:30:00' and '$tomorrow 08:30:00' ";
				$a1=" and apt_penjualan.shiftapt='$shiftapt'";
			}else{
				$tanggal=" and date_format(apt_penjualan.tgl_penjualan, '%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' ";
				$a1=" and apt_penjualan.shiftapt='$shiftapt'";
			}
		}else{
			$tanggal=" and date_format(apt_penjualan.tgl_penjualan, '%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' ";
			$a1="";
		} 
		$a=$a1;
		if($tipe!=''){
			if($jenis_pasien==''){
				$jns=" and apt_customers.type='$tipe'";
			} else{
				$jns="and apt_penjualan.cust_code='$jenis_pasien'";	
			} 		
		}else{
			if($jenis_pasien!=''){
				$jns=" and apt_penjualan.cust_code='$jenis_pasien'";
			} else{
				$jns="";	
			} 		
		}
		if($dokter!=''){$dktr=" and apt_penjualan.kd_dokter='$dokter'";} else $dktr="";
		if($user!=''){$petugas=" and apt_penjualan.id_user='$user'";} else $petugas="";
		if($userkasir!=''){$petugaskasir=" and apt_penjualan.no_penjualan in (select no_penjualan from apt_penjualan_bayar where apt_penjualan_bayar.kd_user='$userkasir') ";} else $petugaskasir="";
		
		if($resep!=''){$b1=" and apt_penjualan.resep='$resep'";}
		else{$b1="";} $b=$b1;
		
		if($is_lunas!='' && $is_lunas !=3){
			$c1=" and apt_penjualan.is_lunas='$is_lunas'";
		}else if($is_lunas!='' && $is_lunas == 3){
			$c1 =" and apt_penjualan.no_penjualan in (select no_penjualan from retur_penjualan)";
		}else{
			$c1="";
		} 
		$c=$c1;
		
		$query=$this->db->query("
				select 
				    apt_penjualan.no_penjualan,
				    date_format(apt_penjualan.tgl_penjualan, '%d-%m-%Y') as tgl_penjualan,
				    apt_penjualan.nama_pasien,
				    apt_penjualan.dokter as nama_dokter,
				    apt_penjualan.resep,
				    apt_penjualan.adm_racik,
				    apt_penjualan.jasa_medis,
				    apt_penjualan.biaya_adm,
				    apt_penjualan.biaya_kartu,
				    apt_customers.customer,
				    apt_customers.type,
				    apt_penjualan.total_transaksi,
				    if(retur_penjualan.no_retur_penjualan is NULL,apt_penjualan.is_lunas,3) as is_lunas,
					retur_penjualan.no_retur_penjualan,
					pegawai.nama_pegawai
				from
				    apt_penjualan
					left join retur_penjualan on apt_penjualan.no_penjualan=retur_penjualan.no_penjualan
					left join apt_customers on apt_penjualan.cust_code = apt_customers.cust_code
					left join user on apt_penjualan.id_user=user.id_user
					left join pegawai on user.id_pegawai=pegawai.id_pegawai,
				    apt_unit
				where
				    apt_penjualan.kd_unit_apt = apt_unit.kd_unit_apt
				        $tanggal
				        and apt_penjualan.kd_unit_apt = '$kd_unit_apt' $c $a $b $jns $dktr $petugas $petugaskasir

				order by apt_customers.type,apt_penjualan.no_penjualan");
		return $query->result_array();
	}

	function getAllHNAPPN($periodeawal,$periodeakhir,$kd_unit_apt,$jenis_pasien,$tipe){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		if($kd_unit_apt!=''){$unit=" and b.kd_unit_apt='$kd_unit_apt'";} else $unit="";
		$tanggal=" date(b.tgl_penjualan) between '$periodeawal1' and '$periodeakhir1' ";
		if($tipe!=''){
			if($jenis_pasien==''){
				$jns=" and c.type='$tipe'";
			} else{
				$jns="and b.cust_code='$jenis_pasien'";	
			} 		
		}else{
			if($jenis_pasien!=''){
				$jns=" and b.cust_code='$jenis_pasien'";
			} else{
				$jns="";	
			} 		
		}
						
		$query=$this->db->query("
				select 
				    a.kd_obat,
				    e.nama_obat,
				    e.harga_dasar,
				    sum(qty) as qty,
				    sum(qty*harga_jual) as total_harga_jual
				from
				    apt_penjualan_detail a join apt_penjualan b on a.no_penjualan=b.no_penjualan 
					join apt_customers c on b.cust_code = c.cust_code
					join apt_unit d on b.kd_unit_apt=d.kd_unit_apt
					join apt_obat e on a.kd_obat=e.kd_obat
				where
				        $tanggal
				        $unit $jns 
				        and is_lunas=1 and returapt is null
				group by a.kd_obat
				        ");
		return $query->result_array();
	}

	function getAllPenjualanApotekNonTunai($periodeawal,$periodeakhir,$kd_unit_apt,$jenis_pasien,$tipe){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		/*$query=$this->db->query("select apt_penjualan.no_penjualan,apt_penjualan.tgl_penjualan,apt_penjualan.nama_pasien,apt_dokter.nama_dokter,apt_penjualan.resep,apt_penjualan.adm_racik,
								apt_penjualan.total_transaksi,apt_penjualan.total_bayar from apt_penjualan LEFT JOIN apt_dokter on apt_penjualan.kd_dokter=apt_dokter.kd_dokter 
								left join apt_unit on apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt where apt_penjualan.tgl_penjualan between '$periodeawal1' and '$periodeakhir1' and apt_penjualan.shiftapt like '%$shiftapt%' 
								and apt_penjualan.is_lunas='$is_lunas' and apt_penjualan.kd_unit_apt like '%$kd_unit_apt%' and apt_penjualan.resep like '%$resep%' 
								order by apt_penjualan.no_penjualan desc");*/
		/*$query=$this->db->query("select apt_penjualan.no_penjualan,apt_penjualan.tgl_penjualan,apt_penjualan.nama_pasien,apt_dokter.nama_dokter,apt_penjualan.resep,apt_penjualan.adm_racik,
								apt_penjualan.total_transaksi,apt_penjualan.is_lunas from apt_penjualan LEFT JOIN apt_dokter on apt_penjualan.kd_dokter=apt_dokter.kd_dokter 
								left join apt_unit on apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt where apt_penjualan.tgl_penjualan between '$periodeawal1' and '$periodeakhir1' and apt_penjualan.shiftapt like '%$shiftapt%' 
								and apt_penjualan.is_lunas='$is_lunas' and apt_penjualan.kd_unit_apt='$kd_unit_apt' and apt_penjualan.resep like '%$resep%' 
								order by apt_penjualan.no_penjualan desc");*/
		$tanggal=" and date(a.tgl_penjualan) between '$periodeawal1' and '$periodeakhir1' ";
		if($tipe!=''){
			if($jenis_pasien==''){
				$jns=" and b.type='$tipe'";
			} else{
				$jns="and a.cust_code='$jenis_pasien'";	
			} 		
		}else{
			if($jenis_pasien!=''){
				$jns=" and a.cust_code='$jenis_pasien'";
			} else{
				$jns="";	
			} 		
		}
		
		$query=$this->db->query("select date_format( a.tgl_penjualan, '%d-%b-%y' ) AS tgl_penjualan, a.no_penjualan, a.nama_pasien, b.customer, c.obat, c.resep, a.adm_racik, a.jasa_medis, a.biaya_adm, a.biaya_kartu
						FROM apt_penjualan AS a
						JOIN apt_customers b ON a.cust_code = b.cust_code
						JOIN (
						SELECT no_penjualan, sum( qty * harga_jual ) AS obat, sum( adm_resep ) AS resep
						FROM apt_penjualan_detail a1
						GROUP BY a1.no_penjualan
						) AS c ON a.no_penjualan = c.no_penjualan
						WHERE b.TYPE !=0 and a.kd_unit_apt = '$kd_unit_apt' 
						$tanggal $jns
						order by b.type,a.no_penjualan");
		return $query->result_array();
	}
	
	function getAllReturPenjualanApotek($periodeawal,$periodeakhir,$shiftapt,$kd_unit_apt,$resep,$jenis_pasien,$dokter){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		/*$query=$this->db->query("select apt_penjualan.no_penjualan,apt_penjualan.tgl_penjualan,apt_penjualan.nama_pasien,apt_dokter.nama_dokter,apt_penjualan.resep,apt_penjualan.adm_racik,
								apt_penjualan.total_transaksi,apt_penjualan.total_bayar from apt_penjualan LEFT JOIN apt_dokter on apt_penjualan.kd_dokter=apt_dokter.kd_dokter 
								left join apt_unit on apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt where apt_penjualan.tgl_penjualan between '$periodeawal1' and '$periodeakhir1' and apt_penjualan.shiftapt like '%$shiftapt%' 
								and apt_penjualan.is_lunas='$is_lunas' and apt_penjualan.kd_unit_apt like '%$kd_unit_apt%' and apt_penjualan.resep like '%$resep%' 
								order by apt_penjualan.no_penjualan desc");*/
		/*$query=$this->db->query("select apt_penjualan.no_penjualan,apt_penjualan.tgl_penjualan,apt_penjualan.nama_pasien,apt_dokter.nama_dokter,apt_penjualan.resep,apt_penjualan.adm_racik,
								apt_penjualan.total_transaksi,apt_penjualan.is_lunas from apt_penjualan LEFT JOIN apt_dokter on apt_penjualan.kd_dokter=apt_dokter.kd_dokter 
								left join apt_unit on apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt where apt_penjualan.tgl_penjualan between '$periodeawal1' and '$periodeakhir1' and apt_penjualan.shiftapt like '%$shiftapt%' 
								and apt_penjualan.is_lunas='$is_lunas' and apt_penjualan.kd_unit_apt='$kd_unit_apt' and apt_penjualan.resep like '%$resep%' 
								order by apt_penjualan.no_penjualan desc");*/
		if($shiftapt!=''){$a1=" and retur_penjualan.shiftapt='$shiftapt'";}else{$a1="";} $a=$a1;
		if($jenis_pasien!=''){$jns=" and retur_penjualan.cust_code='$jenis_pasien'";} else $jns="";
		if($dokter!=''){$dktr=" and apt_penjualan.kd_dokter='$dokter'";} else $dktr="";
		
		if($resep!=''){$b1=" and apt_penjualan.resep='$resep'";}
		else{$b1="";} $b=$b1;
		
		$query=$this->db->query("
				select 
				    apt_penjualan.no_penjualan,
				    date_format(retur_penjualan.tgl_returpenjualan, '%d-%m-%Y') as tgl_returpenjualan,
				    apt_penjualan.nama_pasien,
				    apt_penjualan.dokter as nama_dokter,
				    apt_penjualan.resep,
				    apt_penjualan.adm_racik,
				    apt_customers.customer,
				    apt_penjualan.total_transaksi,
				    retur_penjualan.no_retur_penjualan,
					retur_penjualan.alasan
				from
				    apt_penjualan
					join retur_penjualan on apt_penjualan.no_penjualan=retur_penjualan.no_penjualan
					left join apt_customers on apt_penjualan.cust_code = apt_customers.cust_code,
				    apt_unit
				where
				    apt_penjualan.kd_unit_apt = apt_unit.kd_unit_apt
				        and date_format(retur_penjualan.tgl_returpenjualan, '%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1'
				        and apt_penjualan.kd_unit_apt = '$kd_unit_apt' $a $b $jns $dktr

				order by retur_penjualan.no_retur_penjualan");
		return $query->result_array();
	}
	//function getAllPersediaanApotek($stok,$kd_jenis_obat,$isistok,$kd_sub_jenis,$kd_unit_apt,$kd_golongan){
	function getAllPersediaanApotek($stok,$isistok,$kd_unit_apt,$kd_golongan){
		$kd_unit_aptA=$this->session->userdata('kd_unit_apt');
		$kd_unit_aptB=$this->session->userdata('kd_unit_apt_gudang');
		$stok1="";
		if($stok==1){$stok1=">";}
		if($stok==2){$stok1="<";}
		if($stok==3){$stok1=">=";}
		if($stok==4){$stok1="<=";}
		if($stok==5){$stok1="=";}
		
		/*if($kd_jenis_obat=='' or $kd_jenis_obat=='null'){$a1="";}
		else{$a1="and apt_obat.kd_jenis_obat='$kd_jenis_obat'";} $a=$a1;

		if($kd_sub_jenis=='' or $kd_sub_jenis=='null'){$b1="";}
		else{$b1="and apt_obat.kd_sub_jenis='$kd_sub_jenis'";} $b=$b1;*/
		if($kd_unit_apt==''){$a1="";}
		else{$a1="and apt_stok_unit.kd_unit_apt='$kd_unit_apt'";} $a=$a1;
		
		if($kd_golongan==''){$c1="";}
		else{$c1="and apt_obat.kd_golongan='$kd_golongan'";} $c=$c1;

		if($isistok==''){$isistok1=0;}
		else{$isistok1=$isistok;}
		
		if($kd_unit_aptA==$kd_unit_aptB){
			$query=$this->db->query("select apt_stok_unit.kd_obat,apt_obat.nama_obat,apt_satuan_kecil.satuan_kecil,apt_stok_unit.tgl_expire,sum(apt_stok_unit.jml_stok) as jml_stok,apt_stok_unit.harga_pokok,apt_obat.harga_beli
					from apt_obat,apt_satuan_kecil,apt_stok_unit,apt_unit,apt_golongan
					where apt_stok_unit.kd_obat=apt_obat.kd_obat and apt_stok_unit.kd_unit_apt=apt_unit.kd_unit_apt and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil
					and apt_obat.kd_golongan=apt_golongan.kd_golongan
					$a and apt_stok_unit.jml_stok $stok1 $isistok1 $c group by apt_obat.kd_obat");
		}
		else {
			$query=$this->db->query("select apt_stok_unit.kd_obat,apt_obat.nama_obat,apt_satuan_kecil.satuan_kecil,apt_stok_unit.tgl_expire,sum(apt_stok_unit.jml_stok) as jml_stok,apt_stok_unit.harga_pokok,apt_obat.harga_beli
					from apt_obat,apt_satuan_kecil,apt_stok_unit,apt_unit,apt_golongan
					where apt_stok_unit.kd_obat=apt_obat.kd_obat and apt_stok_unit.kd_unit_apt=apt_unit.kd_unit_apt and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil
					and apt_obat.kd_golongan=apt_golongan.kd_golongan
					and apt_stok_unit.kd_unit_apt='$kd_unit_aptA' and apt_stok_unit.jml_stok $stok1 $isistok1 $c group by apt_obat.kd_obat");
		}
					//and apt_stok_unit.kd_unit_apt='$kd_unit_apt' and apt_stok_unit.jml_stok $stok1 $isistok1 $c order by apt_obat.kd_obat");
					//and apt_stok_unit.kd_unit_apt='$kd_unit_apt' and apt_stok_unit.jml_stok $stok1 $isistok1 $a $b $c order by apt_obat.kd_obat");
		return $query->result_array();
	}
	
	function getAllDistribusiApotek($periodeawal,$periodeakhir,$kd_unit_asal,$kd_unit_tujuan){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		$kd_unit_asal=$this->session->userdata('kd_unit_apt');
		if(!empty($kd_unit_tujuan))$tujuan=" and kd_unit_tujuan='".$kd_unit_tujuan."' "; else $tujuan="";
		$query=$this->db->query("select apt_distribusi_detail.no_distribusi,date_format(apt_distribusi.tgl_distribusi,'%d-%m-%Y %h:%i:%s') as tgl_distribusi,apt_distribusi_detail.kd_obat,apt_obat.nama_obat,apt_distribusi_detail.harga,apt_satuan_kecil.satuan_kecil,apt_tujuan.nama_unit_apt as nama_unit_tujuan,
								apt_distribusi_detail.tgl_expire,apt_distribusi_detail.qty,apt_distribusi.kd_unit_tujuan from apt_distribusi,apt_distribusi_detail,apt_unit,apt_unit as apt_tujuan,apt_obat,apt_satuan_kecil 
								where apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and
								date(apt_distribusi.tgl_distribusi) between '$periodeawal1' and '$periodeakhir1' and apt_distribusi.kd_unit_asal='$kd_unit_asal' $tujuan
								and apt_distribusi_detail.kd_obat=apt_obat.kd_obat and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil
								and apt_distribusi.kd_unit_asal=apt_unit.kd_unit_apt
								and apt_distribusi.kd_unit_tujuan=apt_tujuan.kd_unit_apt
								order by apt_distribusi.no_distribusi asc");
		return $query->result_array();
	}
	
	/*function ambilData4($nama_obat){
		$query=$this->db->query("select kd_obat,nama_obat from apt_obat where nama_obat like '%$nama_obat%'");
		return $query->result_array();
	}*/
	
	function ambilData4($nama_obat){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select distinct apt_stok_unit.kd_obat,apt_obat.nama_obat from apt_stok_unit,apt_obat,apt_unit where apt_stok_unit.kd_obat=apt_obat.kd_obat
								and apt_stok_unit.kd_unit_apt=apt_unit.kd_unit_apt and apt_stok_unit.kd_unit_apt='$kd_unit_apt' and apt_obat.nama_obat like '%$nama_obat%'");
		return $query->result_array();
	}
	
	function ambilData5($kd_obat){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select distinct apt_stok_unit.kd_obat,apt_obat.nama_obat from apt_stok_unit,apt_obat,apt_unit where apt_stok_unit.kd_obat=apt_obat.kd_obat
								and apt_stok_unit.kd_unit_apt=apt_unit.kd_unit_apt and apt_stok_unit.kd_unit_apt='$kd_unit_apt' and apt_obat.kd_obat like '%$kd_obat%'");
		return $query->result_array();
	}
	
	function getKartuStok($kd_obat,$kd_unit_apt,$bulan,$tahun){
	//function getKartuStok($kd_obat,$bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$wow='01-'.$bulan.'-'.$tahun.' 00:00:00';
		//debugvar($kd_unit_apt);
		//kalo dy nerima dr unit lain, brrti pake kd_unit_tujuan
		//kalo dy ngasi ke unit lain, brrti pake kd_unit_asal
		/*select SYSDATE() as tgl,'Saldo Awal' as keterangan,'-' as unitvendor,'-' as no_bukti,'-' as masuk,'-' as keluar,'-' as kode,saldo_awal as saldo
			from apt_mutasi_obat where kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt' and bulan='$bulan' and tahun='$tahun'*/
		switch ($bulan) {
			case '01':
				# code...
				$bulanInt=1;
				$bulankemarin=12;
				$tahunkemarin=$tahun-1;
				break;
			
			case '02':
				# code...
				$bulanInt=2;
				$bulankemarin='01';
				$tahunkemarin=$tahun;
				break;
			
			case '03':
				# code...
				$bulanInt=3;
				$bulankemarin='02';
				$tahunkemarin=$tahun;
				break;
			
			case '04':
				# code...
				$bulanInt=4;
				$bulankemarin='03';
				$tahunkemarin=$tahun;
				break;
			
			case '05':
				# code...
				$bulanInt=5;
				$bulankemarin='04';
				$tahunkemarin=$tahun;
				break;
			
			case '06':
				# code...
				$bulanInt=6;
				$bulankemarin='05';
				$tahunkemarin=$tahun;
				break;
			
			case '07':
				# code...
				$bulanInt=7;
				$bulankemarin='06';
				$tahunkemarin=$tahun;
				break;
			
			case '08':
				# code...
				$bulanInt=8;
				$bulankemarin='07';
				$tahunkemarin=$tahun;
				break;
			
			case '09':
				# code...
				$bulanInt=9;
				$bulankemarin='08';
				$tahunkemarin=$tahun;
				break;
			
			case '10':
				# code...
				$bulanInt=10;
				$bulankemarin='09';
				$tahunkemarin=$tahun;
				break;
			
			case '11':
				# code...
				$bulanInt=11;
				$bulankemarin='10';
				$tahunkemarin=$tahun;
				break;
			
			case '12':
				# code...
				$bulanInt=12;
				$bulankemarin='11';
				$tahunkemarin=$tahun;
				break;
			
			default:
				# code...
				debugvar('There is error, script will not work');
				break;
		}

		$query=$this->db->query("select '$wow' as tgl,'Saldo Awal' as keterangan,'-' as unitvendor,'-' as no_bukti,'-' as masuk,'-' as keluar,'-' as kode,
								ifnull(saldo_akhir,0) as saldo
								from apt_mutasi_obat,apt_obat,apt_unit where apt_mutasi_obat.kd_obat=apt_obat.kd_obat and apt_mutasi_obat.kd_unit_apt=apt_unit.kd_unit_apt and 
								apt_mutasi_obat.kd_unit_apt='$kd_unit_apt' and apt_obat.kd_obat='$kd_obat' and apt_mutasi_obat.tahun='$tahunkemarin' and apt_mutasi_obat.bulan='$bulankemarin'
								
								union
								select date_format(apt_penerimaan.tgl_penerimaan,'%d-%m-%Y %H:%i:%s') as tgl,'Penerimaan Vendor' as keterangan,apt_supplier.nama as unitvendor,apt_penerimaan.no_penerimaan as no_bukti,(apt_penerimaan_detail.qty_kcl+apt_penerimaan_detail.bonus) as masuk,
								'-' as keluar,'M' as kode,0 as saldo from apt_penerimaan,apt_penerimaan_detail,apt_supplier,apt_obat,apt_unit where apt_penerimaan.no_penerimaan=apt_penerimaan_detail.no_penerimaan and 
								apt_penerimaan.kd_unit_apt=apt_unit.kd_unit_apt and apt_penerimaan.kd_supplier=apt_supplier.kd_supplier and apt_penerimaan_detail.kd_obat=apt_obat.kd_obat and apt_obat.kd_obat='$kd_obat' and
								apt_penerimaan.kd_unit_apt='$kd_unit_apt' and posting=1 and month(apt_penerimaan.tgl_penerimaan)='$bulan' and year(apt_penerimaan.tgl_penerimaan)='$tahun'  
								
								union
								select date_format(apt_distribusi.tgl_distribusi,'%d-%m-%Y %H:%i:%s') as tgl,'Penerimaan dari Unit' as keterangan,apt_distribusi.kd_unit_asal as unitvendor,apt_distribusi.no_distribusi as no_bukti,apt_distribusi_detail.qty as masuk, 
                                '-' as keluar, 'M' as kode, 0 as saldo from apt_unit,apt_distribusi,apt_distribusi_detail,apt_obat 
                                where apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_distribusi.kd_unit_tujuan=apt_unit.kd_unit_apt and apt_distribusi_detail.kd_obat=apt_obat.kd_obat
                                and apt_obat.kd_obat='$kd_obat' and posting=1 and apt_distribusi.kd_unit_tujuan='$kd_unit_apt' and month(apt_distribusi.tgl_distribusi)='$bulan' and year(apt_distribusi.tgl_distribusi)='$tahun'
								
								union
								select date_format(apt_distribusi.tgl_distribusi,'%d-%m-%Y %H:%i:%s') as tgl,'Pengeluaran ke Unit' as keterangan,apt_distribusi.kd_unit_tujuan as unitvendor,apt_distribusi.no_distribusi as no_bukti,'-' as masuk,
								apt_distribusi_detail.qty as keluar,'K' as kode,0 as saldo from apt_distribusi,apt_distribusi_detail,apt_unit,apt_obat where apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi
								and apt_distribusi.kd_unit_asal=apt_unit.kd_unit_apt and apt_distribusi_detail.kd_obat=apt_obat.kd_obat and apt_obat.kd_obat='$kd_obat' and posting=1 and apt_distribusi.kd_unit_asal='$kd_unit_apt' and
								month(apt_distribusi.tgl_distribusi)='$bulan' and year(apt_distribusi.tgl_distribusi)='$tahun' 
								
								union
								select date_format(apt_penjualan.tgl_penjualan,'%d-%m-%Y %H:%i:%s') as tgl,'Penjualan Resep' as keterangan,apt_penjualan.kd_unit_apt as unitvendor,apt_penjualan.no_penjualan as no_bukti, '-' as masuk,
								apt_penjualan_detail.qty as keluar,'K' as kode,0 as saldo from apt_penjualan,apt_penjualan_detail,apt_unit,apt_obat where apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan and
								apt_penjualan_detail.kd_obat=apt_obat.kd_obat and apt_obat.kd_obat='$kd_obat' and apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt and apt_penjualan.kd_unit_apt='$kd_unit_apt' 
                                and month(apt_penjualan.tgl_penjualan)='$bulan' and year(apt_penjualan.tgl_penjualan)='$tahun'
								
								union
								select date_format(apt_retur_gudang.tgl_retur,'%d-%m-%Y %H:%i:%s') as tgl,'Retur Ke Gudang' as keterangan,apt_retur_gudang.kd_unit_tujuan as unitvendor,apt_retur_gudang.no_retur as no_bukti, '-' as masuk,
								apt_retur_gudang_detail.qty as keluar,'K' as kode,0 as saldo from apt_retur_gudang,apt_retur_gudang_detail,apt_unit,apt_obat where apt_retur_gudang.no_retur=apt_retur_gudang_detail.no_retur and
								apt_retur_gudang_detail.kd_obat=apt_obat.kd_obat and apt_obat.kd_obat='$kd_obat' and apt_retur_gudang.posting=1 and apt_retur_gudang.kd_unit_asal=apt_unit.kd_unit_apt and apt_retur_gudang.kd_unit_asal='$kd_unit_apt' 
                                and month(apt_retur_gudang.tgl_retur)='$bulan' and year(apt_retur_gudang.tgl_retur)='$tahun'
                                
								union
								select date_format(apt_retur_gudang.tgl_retur,'%d-%m-%Y %H:%i:%s') as tgl,'Retur Dari Unit' as keterangan,apt_retur_gudang.kd_unit_tujuan as unitvendor,apt_retur_gudang.no_retur as no_bukti, apt_retur_gudang_detail.qty as masuk,
								'-' as keluar,'M' as kode,0 as saldo from apt_retur_gudang,apt_retur_gudang_detail,apt_unit,apt_obat where apt_retur_gudang.no_retur=apt_retur_gudang_detail.no_retur and
								apt_retur_gudang_detail.kd_obat=apt_obat.kd_obat and apt_obat.kd_obat='$kd_obat' and apt_retur_gudang.posting=1 and apt_retur_gudang.kd_unit_asal=apt_unit.kd_unit_apt and apt_retur_gudang.kd_unit_tujuan='$kd_unit_apt' 
                                and month(apt_retur_gudang.tgl_retur)='$bulan' and year(apt_retur_gudang.tgl_retur)='$tahun'
                                
								union
								select date_format(apt_retur_obat.tgl_retur,'%d-%m-%Y %:%i:%s') as tgl,concat('Retur Penerimaan No ',apt_retur_obat.no_penerimaan) as keterangan,apt_supplier.nama as unitvendor,apt_retur_obat.no_retur as no_bukti,'-' as masuk,
								apt_retur_obat_detail.qty_kcl as keluar,'K' as kode,0 as saldo from apt_retur_obat,apt_retur_obat_detail,apt_penerimaan,apt_supplier,apt_obat,apt_unit where apt_retur_obat.no_retur=apt_retur_obat_detail.no_retur and 
								apt_retur_obat.no_penerimaan=apt_penerimaan.no_penerimaan and apt_penerimaan.kd_unit_apt=apt_unit.kd_unit_apt and apt_penerimaan.kd_supplier=apt_supplier.kd_supplier and apt_retur_obat_detail.kd_obat=apt_obat.kd_obat and apt_obat.kd_obat='$kd_obat' and
								apt_penerimaan.kd_unit_apt='$kd_unit_apt' and apt_retur_obat.status_approve=1 and month(apt_retur_obat.tgl_retur)>='$bulan' and year(apt_retur_obat.tgl_retur)='$tahun'  

                                union select 
								    date_format(retur_penjualan.tgl_returpenjualan,'%d-%m-%Y %H:%i:%s') as tgl,
								    concat('Retur Penjualan ',retur_penjualan.no_penjualan)  as keterangan,
								    retur_penjualan.kd_unit_apt as unitvendor,
								    retur_penjualan.no_retur_penjualan as no_bukti,
								    retur_penjualan_detail.qty as masuk,
								    '-' as keluar,
								    'M' as kode,
								    0 as saldo
								from
								    retur_penjualan,
								    retur_penjualan_detail,
								    apt_unit,
								    apt_obat
								where
								    retur_penjualan.no_retur_penjualan = retur_penjualan_detail.no_retur_penjualan
								        and retur_penjualan_detail.kd_obat = apt_obat.kd_obat
								        and apt_obat.kd_obat = '$kd_obat'
								        and retur_penjualan.kd_unit_apt = apt_unit.kd_unit_apt
								        and retur_penjualan.kd_unit_apt = '$kd_unit_apt'
								        and tutup=1
								        and month(retur_penjualan.tgl_returpenjualan) = '$bulan'
								        and year(retur_penjualan.tgl_returpenjualan) = '$tahun' 
								
								union
								select date_format(history_perubahan_stok.tanggal,'%d-%m-%Y %H:%i:%s') as tgl,concat('Penyesuaian Stok Oleh ',user.username,' Alasan: ',history_perubahan_stok.alasan) as keterangan,history_perubahan_stok.kd_unit_apt as unitvendor,history_perubahan_stok.nomor as no_bukti,
								(history_perubahan_stok.stok_baru - history_perubahan_stok.stok_lama) as masuk,'-' as keluar,'M' as kode,0 as saldo from history_perubahan_stok,apt_unit,apt_obat,user where 
								history_perubahan_stok.kd_obat=apt_obat.kd_obat and history_perubahan_stok.kd_unit_apt=apt_unit.kd_unit_apt and history_perubahan_stok.kd_user=user.id_user and history_perubahan_stok.kd_unit_apt='$kd_unit_apt'
								and history_perubahan_stok.kd_obat='$kd_obat' and month(history_perubahan_stok.tanggal)='$bulan' and year(history_perubahan_stok.tanggal)='$tahun' order by tgl");
								//order by tgl");
		return $query->result_array();
	}
	
	function getKartuStok1($kd_obat,$kd_unit_apt,$bulan,$tahun){
	//function getKartuStok($kd_obat,$bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$wow='01-'.$bulan.'-'.$tahun.' 00:00:00';
		//debugvar($kd_unit_apt);
		//kalo dy nerima dr unit lain, brrti pake kd_unit_tujuan
		//kalo dy ngasi ke unit lain, brrti pake kd_unit_asal
		/*select SYSDATE() as tgl,'Saldo Awal' as keterangan,'-' as unitvendor,'-' as no_bukti,'-' as masuk,'-' as keluar,'-' as kode,saldo_awal as saldo
			from apt_mutasi_obat where kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt' and bulan='$bulan' and tahun='$tahun'*/
		$query=$this->db->query("select '$wow' as tgl,'Saldo Awal' as keterangan,'-' as unitvendor,'-' as no_bukti,'-' as masuk,'-' as keluar,'-' as kode,
								((sum(apt_stok_unit.jml_stok)) + (select ifnull((sum(apt_penjualan_detail.qty)),0) from apt_obat,
								apt_penjualan,apt_penjualan_detail,apt_unit where apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan and 
								apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt and apt_penjualan_detail.kd_obat=apt_obat.kd_obat and apt_penjualan_detail.kd_obat='$kd_obat' 
								and apt_penjualan.kd_unit_apt='$kd_unit_apt' and tutup=1 and month(apt_penjualan.tgl_penjualan)>='$bulan' and year(apt_penjualan.tgl_penjualan)>='$tahun') + 
								(select ifnull((sum(apt_distribusi_detail.qty)),0) from apt_obat,apt_distribusi,apt_distribusi_detail,apt_unit
								where apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_distribusi_detail.kd_obat=apt_obat.kd_obat
								and apt_distribusi.kd_unit_asal=apt_unit.kd_unit_apt and apt_distribusi_detail.kd_obat='$kd_obat' and apt_distribusi.kd_unit_asal='$kd_unit_apt' and posting=1
								and month(apt_distribusi.tgl_distribusi)>='$bulan' and year(apt_distribusi.tgl_distribusi)>='$tahun') + 
								(select ifnull((sum(apt_retur_obat_detail.qty_kcl)),0) from apt_obat,apt_retur_obat,apt_retur_obat_detail,apt_unit
								where apt_obat.kd_obat=apt_retur_obat_detail.kd_obat and apt_retur_obat.no_retur=apt_retur_obat_detail.no_retur and 
								apt_retur_obat_detail.kd_unit_apt=apt_unit.kd_unit_apt and apt_retur_obat_detail.kd_unit_apt='$kd_unit_apt' and status_approve=1 and 
								month(apt_retur_obat.tgl_retur)>='$bulan' and year(apt_retur_obat.tgl_retur)>='$tahun' and apt_obat.kd_obat='$kd_obat') + (select 
						            ifnull((sum(apt_retur_gudang_detail.qty)), 0)
						        from
						            apt_obat,
						            apt_retur_gudang,
						            apt_retur_gudang_detail,
						            apt_unit
						        where
						            apt_obat.kd_obat = apt_retur_gudang_detail.kd_obat
						                and apt_retur_gudang.no_retur = apt_retur_gudang_detail.no_retur
						                and apt_retur_gudang.kd_unit_asal = apt_unit.kd_unit_apt
						                and apt_retur_gudang.kd_unit_asal = '$kd_unit_apt'
										and posting = 1
						                and month(apt_retur_gudang.tgl_retur) >= '$bulan'
						                and year(apt_retur_gudang.tgl_retur) >= '$tahun'
						                and apt_obat.kd_obat = '$kd_obat') +
								(select ifnull((sum(history_perubahan_stok.qty)*(-1)),0) from history_perubahan_stok,apt_obat,apt_unit
								where history_perubahan_stok.kd_obat=apt_obat.kd_obat and history_perubahan_stok.kd_unit_apt=apt_unit.kd_unit_apt
								and history_perubahan_stok.kd_unit_apt='$kd_unit_apt' and history_perubahan_stok.kd_obat='$kd_obat'
								and month(history_perubahan_stok.tanggal)>='$bulan' and year(history_perubahan_stok.tanggal)>='$tahun') - 
								(select ifnull((sum(apt_penerimaan_detail.qty_kcl)),0) as penerimaan from apt_penerimaan,apt_penerimaan_detail,apt_obat,apt_unit
								where apt_penerimaan.no_penerimaan=apt_penerimaan_detail.no_penerimaan and apt_penerimaan_detail.kd_unit_apt=apt_unit.kd_unit_apt
								and apt_penerimaan_detail.kd_obat=apt_obat.kd_obat and apt_penerimaan_detail.kd_obat='$kd_obat' and posting=1 and apt_penerimaan.kd_unit_apt='$kd_unit_apt'
								and month(apt_penerimaan.tgl_penerimaan)>='$bulan' and year(apt_penerimaan.tgl_penerimaan)>='$tahun') - (select 
					            ifnull((sum(retur_penjualan_detail.qty)), 0)
					        from
					            apt_obat,
					            retur_penjualan,
					            retur_penjualan_detail,
					            apt_unit
					        where
					            retur_penjualan.no_retur_penjualan = retur_penjualan_detail.no_retur_penjualan
					                and retur_penjualan.kd_unit_apt = apt_unit.kd_unit_apt
					                and retur_penjualan_detail.kd_obat = apt_obat.kd_obat
					                and retur_penjualan_detail.kd_obat = '$kd_obat'
					                and retur_penjualan.kd_unit_apt = '$kd_unit_apt'
					                and tutup=1
					                and month(retur_penjualan.tgl_returpenjualan) >= '$bulan'
					                and year(retur_penjualan.tgl_returpenjualan) >= '$tahun')  - 
								(select ifnull((sum(apt_distribusi_detail.qty)),0) from apt_obat,apt_distribusi,apt_distribusi_detail,apt_unit
								where apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_distribusi_detail.kd_obat=apt_obat.kd_obat
								and apt_distribusi.kd_unit_tujuan=apt_unit.kd_unit_apt and apt_distribusi_detail.kd_obat='$kd_obat' and apt_distribusi.kd_unit_tujuan='$kd_unit_apt' and posting=1
								and month(apt_distribusi.tgl_distribusi)>='$bulan' and year(apt_distribusi.tgl_distribusi)>='$tahun') - 0) as saldo
								from apt_stok_unit,apt_obat,apt_unit where apt_stok_unit.kd_obat=apt_obat.kd_obat and apt_stok_unit.kd_unit_apt=apt_unit.kd_unit_apt and 
								apt_stok_unit.kd_unit_apt='$kd_unit_apt' and apt_obat.kd_obat='$kd_obat'
								
								union
								select date_format(apt_penerimaan.tgl_penerimaan,'%d-%m-%Y %h:%i:%s') as tgl,'Penerimaan Vendor' as keterangan,apt_supplier.nama as unitvendor,apt_penerimaan.no_penerimaan as no_bukti,apt_penerimaan_detail.qty_kcl as masuk,
								'-' as keluar,'M' as kode,0 as saldo from apt_penerimaan,apt_penerimaan_detail,apt_supplier,apt_obat,apt_unit where apt_penerimaan.no_penerimaan=apt_penerimaan_detail.no_penerimaan and 
								apt_penerimaan.kd_unit_apt=apt_unit.kd_unit_apt and apt_penerimaan.kd_supplier=apt_supplier.kd_supplier and apt_penerimaan_detail.kd_obat=apt_obat.kd_obat and apt_obat.kd_obat='$kd_obat' and
								apt_penerimaan.kd_unit_apt='$kd_unit_apt' and posting=1 and month(apt_penerimaan.tgl_penerimaan)>='$bulan' and year(apt_penerimaan.tgl_penerimaan)='$tahun'  
								
								union
								select date_format(apt_distribusi.tgl_distribusi,'%d-%m-%Y %h:%i:%s') as tgl,'Penerimaan dari Unit' as keterangan,apt_distribusi.kd_unit_asal as unitvendor,apt_distribusi.no_distribusi as no_bukti,apt_distribusi_detail.qty as masuk, 
                                '-' as keluar, 'M' as kode, 0 as saldo from apt_unit,apt_distribusi,apt_distribusi_detail,apt_obat 
                                where apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_distribusi.kd_unit_tujuan=apt_unit.kd_unit_apt and apt_distribusi_detail.kd_obat=apt_obat.kd_obat
                                and apt_obat.kd_obat='$kd_obat' and posting=1 and apt_distribusi.kd_unit_tujuan='$kd_unit_apt' and month(apt_distribusi.tgl_distribusi)='$bulan' and year(apt_distribusi.tgl_distribusi)='$tahun'
								
								union
								select date_format(apt_distribusi.tgl_distribusi,'%d-%m-%Y %h:%i:%s') as tgl,'Pengeluaran ke Unit' as keterangan,apt_distribusi.kd_unit_tujuan as unitvendor,apt_distribusi.no_distribusi as no_bukti,'-' as masuk,
								apt_distribusi_detail.qty as keluar,'K' as kode,0 as saldo from apt_distribusi,apt_distribusi_detail,apt_unit,apt_obat where apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi
								and apt_distribusi.kd_unit_asal=apt_unit.kd_unit_apt and apt_distribusi_detail.kd_obat=apt_obat.kd_obat and apt_obat.kd_obat='$kd_obat' and posting=1 and apt_distribusi.kd_unit_asal='$kd_unit_apt' and
								month(apt_distribusi.tgl_distribusi)>='$bulan' and year(apt_distribusi.tgl_distribusi)='$tahun' 
								
								union
								select date_format(apt_penjualan.tgl_penjualan,'%d-%m-%Y %h:%i:%s') as tgl,'Penjualan Resep' as keterangan,apt_penjualan.kd_unit_apt as unitvendor,apt_penjualan.no_penjualan as no_bukti, '-' as masuk,
								apt_penjualan_detail.qty as keluar,'K' as kode,0 as saldo from apt_penjualan,apt_penjualan_detail,apt_unit,apt_obat where apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan and
								apt_penjualan_detail.kd_obat=apt_obat.kd_obat and apt_obat.kd_obat='$kd_obat' and apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt and apt_penjualan.kd_unit_apt='$kd_unit_apt' 
                                and month(apt_penjualan.tgl_penjualan)>='$bulan' and year(apt_penjualan.tgl_penjualan)='$tahun'
								
								union
								select date_format(apt_retur_gudang.tgl_retur,'%d-%m-%Y %h:%i:%s') as tgl,'Retur Ke Gudang' as keterangan,apt_retur_gudang.kd_unit_tujuan as unitvendor,apt_retur_gudang.no_retur as no_bukti, '-' as masuk,
								apt_retur_gudang_detail.qty as keluar,'K' as kode,0 as saldo from apt_retur_gudang,apt_retur_gudang_detail,apt_unit,apt_obat where apt_retur_gudang.no_retur=apt_retur_gudang_detail.no_retur and
								apt_retur_gudang_detail.kd_obat=apt_obat.kd_obat and apt_obat.kd_obat='$kd_obat' and apt_retur_gudang.posting=1 and apt_retur_gudang.kd_unit_asal=apt_unit.kd_unit_apt and apt_retur_gudang.kd_unit_asal='$kd_unit_apt' 
                                and month(apt_retur_gudang.tgl_retur)>='$bulan' and year(apt_retur_gudang.tgl_retur)='$tahun'
                                
								union
								select date_format(apt_retur_obat.tgl_retur,'%d-%m-%Y %h:%i:%s') as tgl,concat('Retur Penerimaan No ',apt_retur_obat.no_penerimaan) as keterangan,apt_supplier.nama as unitvendor,apt_retur_obat.no_retur as no_bukti,'-' as masuk,
								apt_retur_obat_detail.qty_kcl as keluar,'K' as kode,0 as saldo from apt_retur_obat,apt_retur_obat_detail,apt_penerimaan,apt_supplier,apt_obat,apt_unit where apt_retur_obat.no_retur=apt_retur_obat_detail.no_retur and 
								apt_retur_obat.no_penerimaan=apt_penerimaan.no_penerimaan and apt_penerimaan.kd_unit_apt=apt_unit.kd_unit_apt and apt_penerimaan.kd_supplier=apt_supplier.kd_supplier and apt_retur_obat_detail.kd_obat=apt_obat.kd_obat and apt_obat.kd_obat='$kd_obat' and
								apt_penerimaan.kd_unit_apt='$kd_unit_apt' and apt_retur_obat.status_approve=1 and month(apt_retur_obat.tgl_retur)>='$bulan' and year(apt_retur_obat.tgl_retur)='$tahun'  

                                union select 
								    date_format(retur_penjualan.tgl_returpenjualan,'%d-%m-%Y %h:%i:%s') as tgl,
								    concat('Retur Penjualan ',retur_penjualan.no_penjualan)  as keterangan,
								    retur_penjualan.kd_unit_apt as unitvendor,
								    retur_penjualan.no_retur_penjualan as no_bukti,
								    retur_penjualan_detail.qty as masuk,
								    '-' as keluar,
								    'M' as kode,
								    0 as saldo
								from
								    retur_penjualan,
								    retur_penjualan_detail,
								    apt_unit,
								    apt_obat
								where
								    retur_penjualan.no_retur_penjualan = retur_penjualan_detail.no_retur_penjualan
								        and retur_penjualan_detail.kd_obat = apt_obat.kd_obat
								        and apt_obat.kd_obat = '$kd_obat'
								        and retur_penjualan.kd_unit_apt = apt_unit.kd_unit_apt
								        and retur_penjualan.kd_unit_apt = '$kd_unit_apt'
								        and tutup=1
								        and month(retur_penjualan.tgl_returpenjualan) >= '$bulan'
								        and year(retur_penjualan.tgl_returpenjualan) >= '$tahun' 
								
								union
								select date_format(history_perubahan_stok.tanggal,'%d-%m-%Y %h:%i:%s') as tgl,'Stokopname' as keterangan,history_perubahan_stok.kd_unit_apt as unitvendor,history_perubahan_stok.nomor as no_bukti,
								history_perubahan_stok.qty as masuk,'-' as keluar,'M' as kode,0 as saldo from history_perubahan_stok,apt_unit,apt_obat where 
								history_perubahan_stok.kd_obat=apt_obat.kd_obat and history_perubahan_stok.kd_unit_apt=apt_unit.kd_unit_apt and history_perubahan_stok.kd_unit_apt='$kd_unit_apt'
								and history_perubahan_stok.kd_obat='$kd_obat' and month(history_perubahan_stok.tanggal)>='$bulan' and year(history_perubahan_stok.tanggal)>='$tahun' order by tgl");
								//order by tgl");
		return $query->result_array();
	}
	
	function getObat($kd_unit_apt,$bulan,$tahun){
		/*$query=$this->db->query("select apt_obat.kd_obat,apt_obat.nama_obat,(apt_penerimaan_detail.qty_kcl+apt_distribusi_detail.qty) as in_pbf
								from apt_obat,apt_penerimaan,apt_penerimaan_detail,apt_distribusi,apt_distribusi_detail,apt_unit 
								where apt_penerimaan.no_penerimaan=apt_penerimaan_detail.no_penerimaan and apt_penerimaan_detail.kd_obat=apt_obat.kd_obat and
								apt_penerimaan.kd_unit_apt=apt_unit.kd_unit_apt and apt_penerimaan.kd_unit_apt='$kd_unit_apt' and month(apt_penerimaan.tgl_penerimaan)='$bulan' and year(apt_penerimaan.tgl_penerimaan)='$tahun'
								and apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_distribusi.kd_unit_asal=apt_unit.kd_unit_apt and
								apt_distribusi_detail.kd_obat=apt_obat.kd_obat and apt_distribusi.kd_unit_asal='$kd_unit_apt' and 
								month(apt_distribusi.tgl_distribusi)='$bulan' and year(apt_distribusi.tgl_distribusi)='$tahun' order by apt_obat.kd_obat");*/
								
		$query=$this->db->query("select apt_obat.kd_obat,apt_obat.nama_obat from apt_obat,apt_penerimaan,apt_penerimaan_detail,apt_unit where apt_obat.kd_obat=apt_penerimaan_detail.kd_obat
								and apt_penerimaan.no_penerimaan=apt_penerimaan_detail.no_penerimaan and apt_unit.kd_unit_apt=apt_penerimaan.kd_unit_apt
								and apt_penerimaan.kd_unit_apt='$kd_unit_apt' and month(apt_penerimaan.tgl_penerimaan)='$bulan' and year(apt_penerimaan.tgl_penerimaan)='$tahun'
								union 
								select apt_obat.kd_obat,apt_obat.nama_obat from apt_obat,apt_distribusi,apt_distribusi_detail,apt_unit where apt_obat.kd_obat=apt_distribusi_detail.kd_obat
								and apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_unit.kd_unit_apt=apt_distribusi.kd_unit_asal 
								and apt_distribusi.kd_unit_asal='$kd_unit_apt' and month(apt_distribusi.tgl_distribusi)='$bulan' and year(apt_distribusi.tgl_distribusi)='$tahun'
								union
								select apt_obat.kd_obat,apt_obat.nama_obat from apt_obat,apt_penjualan,apt_penjualan_detail,apt_unit where apt_obat.kd_obat=apt_penjualan_detail.kd_obat
								and apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan and apt_unit.kd_unit_apt=apt_penjualan.kd_unit_apt and 
								apt_penjualan.kd_unit_apt='$kd_unit_apt' and month(apt_penjualan.tgl_penjualan)='$bulan' and year(apt_penjualan.tgl_penjualan)='$tahun'
								union
                                select apt_obat.kd_obat,apt_obat.nama_obat from apt_obat,history_perubahan_stok,apt_unit where apt_obat.kd_obat=history_perubahan_stok.kd_obat 
                                and history_perubahan_stok.kd_unit_apt=apt_unit.kd_unit_apt and history_perubahan_stok.kd_unit_apt='$kd_unit_apt' and month(history_perubahan_stok.tanggal)='$bulan'
                                and year(history_perubahan_stok.tanggal)='$tahun'
								union
								select apt_obat.kd_obat,apt_obat.nama_obat from apt_obat,apt_stok_unit,apt_unit where apt_obat.kd_obat=apt_stok_unit.kd_obat and apt_stok_unit.kd_unit_apt=apt_unit.kd_unit_apt
                                and apt_stok_unit.kd_unit_apt='$kd_unit_apt'
								");
		return $query->result_array();
	}
	
	function ambilMutasiObat($kd_unit_apt,$bulan,$tahun){
		$query=$this->db->query("select apt_mutasi_obat.saldo_awal,apt_mutasi_obat.kd_obat,apt_obat.nama_obat,apt_mutasi_obat.harga_beli,apt_mutasi_obat.in_pbf,apt_mutasi_obat.in_unit,
								apt_mutasi_obat.retur_pbf,apt_mutasi_obat.out_jual,apt_mutasi_obat.out_unit,apt_mutasi_obat.retur_jual,apt_mutasi_obat.saldo_akhir,apt_mutasi_obat.stok_opname 
								from apt_obat,apt_mutasi_obat,apt_unit where apt_obat.kd_obat=apt_mutasi_obat.kd_obat and apt_mutasi_obat.kd_unit_apt=apt_unit.kd_unit_apt 
								and apt_mutasi_obat.kd_unit_apt='$kd_unit_apt' and apt_mutasi_obat.bulan='$bulan' and apt_mutasi_obat.tahun='$tahun'");
		return $query->result_array();
	}
	
	function getSaldoAwal($b,$a1,$nama,$kd_unit_apt){
		$query=$this->db->query("select apt_mutasi_obat.saldo_akhir as saldo_awal from apt_mutasi_obat,apt_obat,apt_unit where apt_obat.kd_obat=apt_mutasi_obat.kd_obat and 
								apt_mutasi_obat.kd_unit_apt=apt_unit.kd_unit_apt and apt_mutasi_obat.tahun='$b' and apt_mutasi_obat.bulan='$a1' and 
								apt_obat.nama_obat='$nama' and apt_mutasi_obat.kd_unit_apt='$kd_unit_apt'");
		 $item=$query->row_array();
		 if(empty($item)) return 0;
		 return $item['saldo_awal'];
	}
	
	function getHarga($kd_unit_apt,$nama){
		$query=$this->db->query("select apt_stok_unit.harga_pokok from apt_obat,apt_stok_unit,apt_unit where apt_obat.kd_obat=apt_stok_unit.kd_obat and
								apt_stok_unit.kd_unit_apt=apt_unit.kd_unit_apt and apt_stok_unit.kd_unit_apt='$kd_unit_apt' and apt_obat.nama_obat='$nama'");
		$item=$query->row_array();
		if(empty($item)) return 0;
		 return $item['harga_pokok'];
	}
	
	function getInPbf($kd_unit_apt,$bulan,$tahun,$nama){
		$query=$this->db->query("select sum(apt_penerimaan_detail.qty_kcl) as in_pbf from apt_obat,apt_penerimaan,apt_penerimaan_detail,apt_unit 
								where apt_penerimaan.no_penerimaan=apt_penerimaan_detail.no_penerimaan and apt_obat.kd_obat=apt_penerimaan_detail.kd_obat
								and apt_penerimaan.kd_unit_apt=apt_unit.kd_unit_apt and apt_penerimaan.kd_unit_apt='$kd_unit_apt' and month(apt_penerimaan.tgl_penerimaan)='$bulan' 
								and year(apt_penerimaan.tgl_penerimaan)='$tahun' and apt_obat.nama_obat='$nama'");
		$item=$query->row_array();
		if(empty($item)) return 0;
		return $item['in_pbf'];
	}
	
	function getInUnit($kd_unit_apt,$bulan,$tahun,$nama){
		$query=$this->db->query("select sum(apt_distribusi_detail.qty) as in_unit from apt_obat,apt_distribusi,apt_distribusi_detail,apt_unit 
								where apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_obat.kd_obat=apt_distribusi_detail.kd_obat
								and apt_distribusi.kd_unit_tujuan=apt_unit.kd_unit_apt and apt_distribusi.kd_unit_tujuan='$kd_unit_apt' and month(apt_distribusi.tgl_distribusi)='$bulan' 
								and year(apt_distribusi.tgl_distribusi)='$tahun' and apt_obat.nama_obat='$nama'");
		$item=$query->row_array();
		if(empty($item)) return 0;
		return $item['in_unit'];
	}
	
	function getReturPbf($kd_unit_apt,$bulan,$tahun,$nama){
		$query=$this->db->query("select sum(apt_retur_obat_detail.qty_kcl) as retur_pbf from apt_obat,apt_retur_obat,apt_retur_obat_detail,apt_unit
								where apt_obat.kd_obat=apt_retur_obat_detail.kd_obat and apt_retur_obat.no_retur=apt_retur_obat_detail.no_retur and 
								apt_retur_obat_detail.kd_unit_apt=apt_unit.kd_unit_apt and apt_retur_obat_detail.kd_unit_apt='$kd_unit_apt' and month(apt_retur_obat.tgl_retur)='$bulan' and year(apt_retur_obat.tgl_retur)='$tahun'
								and apt_obat.nama_obat='$nama'");
		$item=$query->row_array();
		if(empty($item)) return 0;
		return $item['retur_pbf'];
	}
	
	function getOutJual($kd_unit_apt,$bulan,$tahun,$nama){
		$query=$this->db->query("select sum(apt_penjualan_detail.qty) as out_jual from apt_obat,apt_penjualan,apt_penjualan_detail,apt_unit
								where apt_obat.kd_obat=apt_penjualan_detail.kd_obat and apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan
								and apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt and apt_penjualan.kd_unit_apt=apt_penjualan_detail.kd_unit_apt
								and apt_penjualan.kd_unit_apt='$kd_unit_apt' and month(apt_penjualan.tgl_penjualan)='$bulan' and year(apt_penjualan.tgl_penjualan)='$tahun'
								and apt_obat.nama_obat='$nama'");
		$item=$query->row_array();
		if(empty($item)) return 0;
		return $item['out_jual'];
	}
	
	function getHargaJual($kd_unit_apt,$bulan,$tahun,$nama){
		$query=$this->db->query("select apt_penjualan_detail.harga_jual from apt_obat,apt_penjualan,apt_penjualan_detail,apt_unit
								where apt_obat.kd_obat=apt_penjualan_detail.kd_obat and apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan
								and apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt and apt_penjualan.kd_unit_apt=apt_penjualan_detail.kd_unit_apt
								and apt_penjualan.kd_unit_apt='$kd_unit_apt' and month(apt_penjualan.tgl_penjualan)='$bulan' and year(apt_penjualan.tgl_penjualan)='$tahun'
								and apt_obat.nama_obat='$nama'");
		$item=$query->row_array();
		if(empty($item)) return 0;
		return $item['harga_jual'];
	}
	
	function getOutUnit($kd_unit_apt,$bulan,$tahun,$nama){
		$query=$this->db->query("select sum(apt_distribusi_detail.qty) as out_unit from apt_obat,apt_distribusi,apt_distribusi_detail,apt_unit
								where apt_obat.kd_obat=apt_distribusi_detail.kd_obat and apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi
								and apt_distribusi.kd_unit_asal=apt_unit.kd_unit_apt and apt_distribusi.kd_unit_asal='$kd_unit_apt' and month(apt_distribusi.tgl_distribusi)='$bulan'
								and year(apt_distribusi.tgl_distribusi)='$tahun' and apt_obat.nama_obat='$nama'");
		$item=$query->row_array();
		if(empty($item)) return 0;
		return $item['out_unit'];
	}
	
	function isNumberExist($kd_unit_apt,$bulan,$tahun){
		$this->db->where('kd_unit_apt',$kd_unit_apt);
		$this->db->where('bulan',$bulan);
		$this->db->where('tahun',$tahun);
		$query=$this->db->get('apt_mutasi_obat');
		$count=$query->num_rows();
		if($count){
			return true;
		}
		return false;
	}
	
	function isObatExist($kd_obat,$bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$this->db->where('kd_unit_apt',$kd_unit_apt);
		$this->db->where('kd_obat',$kd_obat);
		$this->db->where('bulan',$bulan);
		$this->db->where('tahun',$tahun);
		$query=$this->db->get('apt_mutasi_obat');
		$count=$query->num_rows();
		if($count){
			return true;
		}
		return false;
	}
	
	function namaUnit($kd_unit_apt){
		$query=$this->db->query("select nama_unit_apt from apt_unit where kd_unit_apt='$kd_unit_apt'");
		$namaunit=$query->row_array();
		if(empty($namaunit)) return 0;
		return $namaunit['nama_unit_apt'];
	}
	
	function namaObat($kd_obat){
		$query=$this->db->query("select nama_obat from apt_obat where kd_obat='$kd_obat'");
		$namaobat=$query->row_array();
		if(empty($namaobat)) return 0;
		return $namaobat['nama_obat'];
	}
	
	function getStokopname($kd_unit_apt,$bulan,$tahun,$nama){
		$query=$this->db->query("select sum(history_perubahan_stok.qty) as qty from apt_obat,history_perubahan_stok,apt_unit 
								where apt_obat.kd_obat=history_perubahan_stok.kd_obat and history_perubahan_stok.kd_unit_apt=apt_unit.kd_unit_apt
								and history_perubahan_stok.kd_unit_apt='$kd_unit_apt' and month(history_perubahan_stok.tanggal)='$bulan' and
								year(history_perubahan_stok.tanggal)='$tahun' and apt_obat.nama_obat='$nama'");
		$item=$query->row_array();
		if(empty($item)) return 0;
		return $item['qty'];
	}
	
	// function ambilstok($kd_obat,$kd_unit_apt){
	function ambilstok($kd_obat){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select sum(jml_stok) as jml_stok from apt_stok_unit where kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt'");
		$jml_stok=$query->row_array();
		if(empty($jml_stok)) return 0;
		return $jml_stok['jml_stok'];
	}
	
	function ambilout($kd_obat,$bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select out_unit as a from apt_mutasi_obat where kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt' and bulan='$bulan' and tahun='$tahun'");
		$a=$query->row_array();
		if(empty($a)) return 0;
		return $a['a'];
	}
	
	function ambilin($kd_obat,$bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select in_pbf as b from apt_mutasi_obat where kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt' and bulan='$bulan' and tahun='$tahun'");
		$b=$query->row_array();
		if(empty($b)) return 0;
		return $b['b'];
	}
	
	function ambilHargaBeli($kd_unit_apt,$kd_obat){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select harga_pokok from apt_stok_unit where kd_unit_apt='$kd_unit_apt' and kd_obat='$kd_obat'");
		$harga_pokok=$query->row_array();
		if(empty($harga_pokok)) return 0;
		return $harga_pokok['harga_pokok'];
	}
	
	function getDistribusi($bulan,$tahun){ 
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		/*$query=$this->db->query("select apt_obat.kd_obat,apt_obat.nama_obat,apt_distribusi.tgl_distribusi,apt_distribusi_detail.qty,apt_stok_unit.harga_pokok as harga_beli from apt_obat,apt_distribusi,apt_distribusi_detail,
                                apt_unit,apt_stok_unit where apt_obat.kd_obat=apt_distribusi_detail.kd_obat and apt_distribusi.kd_unit_asal=apt_stok_unit.kd_unit_apt
								and apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_unit.kd_unit_apt=apt_distribusi.kd_unit_asal 
								and apt_stok_unit.kd_obat=apt_obat.kd_obat 
								and apt_distribusi.kd_unit_asal='$kd_unit_apt' and month(apt_distribusi.tgl_distribusi)='$bulan' and year(apt_distribusi.tgl_distribusi)='$tahun'");*/
		$query=$this->db->query("select apt_obat.kd_obat,apt_obat.nama_obat,apt_distribusi.tgl_distribusi,sum(apt_distribusi_detail.qty) as qty,apt_stok_unit.harga_pokok as harga_beli from apt_obat,apt_distribusi,apt_distribusi_detail,
                                apt_unit,apt_stok_unit where apt_obat.kd_obat=apt_distribusi_detail.kd_obat and apt_distribusi.kd_unit_asal=apt_stok_unit.kd_unit_apt
								and apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_unit.kd_unit_apt=apt_distribusi.kd_unit_asal 
								and apt_stok_unit.kd_obat=apt_obat.kd_obat and apt_distribusi.kd_unit_asal='$kd_unit_apt' and month(apt_distribusi.tgl_distribusi)='$bulan' and year(apt_distribusi.tgl_distribusi)='$tahun'
								group by apt_obat.kd_obat");
		return $query->result_array();
	}

	function getPengeluaranDistribusiObat($obat,$bulan,$tahun){ 
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		/*$query=$this->db->query("select apt_obat.kd_obat,apt_obat.nama_obat,apt_distribusi.tgl_distribusi,apt_distribusi_detail.qty,apt_stok_unit.harga_pokok as harga_beli from apt_obat,apt_distribusi,apt_distribusi_detail,
                                apt_unit,apt_stok_unit where apt_obat.kd_obat=apt_distribusi_detail.kd_obat and apt_distribusi.kd_unit_asal=apt_stok_unit.kd_unit_apt
								and apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_unit.kd_unit_apt=apt_distribusi.kd_unit_asal 
								and apt_stok_unit.kd_obat=apt_obat.kd_obat 
								and apt_distribusi.kd_unit_asal='$kd_unit_apt' and month(apt_distribusi.tgl_distribusi)='$bulan' and year(apt_distribusi.tgl_distribusi)='$tahun'");*/
		$query=$this->db->query("select apt_obat.kd_obat,apt_obat.nama_obat,apt_distribusi.tgl_distribusi,ifnull(sum(apt_distribusi_detail.qty),0) as qty from apt_obat,apt_distribusi,apt_distribusi_detail,
                                apt_unit where apt_obat.kd_obat=apt_distribusi_detail.kd_obat
								and apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_unit.kd_unit_apt=apt_distribusi.kd_unit_asal and posting=1
								and apt_obat.kd_obat='$obat' and apt_distribusi.kd_unit_asal='$kd_unit_apt' and month(apt_distribusi.tgl_distribusi)='$bulan' and year(apt_distribusi.tgl_distribusi)='$tahun'
								group by apt_obat.kd_obat");
		$item = $query->row_array();

		$queryreturgudang=$this->db->query("SELECT apt_obat.kd_obat, apt_obat.nama_obat, apt_retur_gudang.tgl_retur, ifnull( sum( apt_retur_gudang_detail.qty ) , 0 ) AS qty
								FROM apt_obat, apt_retur_gudang, apt_retur_gudang_detail, apt_unit
								WHERE apt_obat.kd_obat = apt_retur_gudang_detail.kd_obat
								AND apt_retur_gudang.no_retur = apt_retur_gudang_detail.no_retur
								AND apt_unit.kd_unit_apt = apt_retur_gudang.kd_unit_tujuan
								AND apt_obat.kd_obat = '$obat' and posting=1
								AND apt_retur_gudang.kd_unit_asal ='$kd_unit_apt' and month(apt_retur_gudang.tgl_retur)='$bulan' and year(apt_retur_gudang.tgl_retur)='$tahun'
								group by apt_obat.kd_obat");
		$itemreturgudang =  $queryreturgudang->row_array();

		if(empty($item)) $item['qty']= 0;
		if(empty($itemreturgudang)) $itemreturgudang['qty']= 0;
		return $item['qty']+$itemreturgudang['qty'];
	}
	
	function getDistribusi1($bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		/*$query=$this->db->query("select apt_obat.kd_obat,apt_obat.nama_obat,apt_distribusi.tgl_distribusi,apt_distribusi_detail.qty,apt_stok_unit.harga_pokok as harga_beli from apt_obat,apt_distribusi,apt_distribusi_detail,
                                apt_unit,apt_stok_unit where apt_obat.kd_obat=apt_distribusi_detail.kd_obat and apt_distribusi.kd_unit_tujuan=apt_stok_unit.kd_unit_apt
								and apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_unit.kd_unit_apt=apt_distribusi.kd_unit_tujuan 
								and apt_stok_unit.kd_obat=apt_obat.kd_obat 
								and apt_distribusi.kd_unit_tujuan='$kd_unit_apt' and month(apt_distribusi.tgl_distribusi)='$bulan' and year(apt_distribusi.tgl_distribusi)='$tahun'");*/
		$query=$this->db->query("select apt_obat.kd_obat,apt_obat.nama_obat,apt_distribusi.tgl_distribusi,sum(apt_distribusi_detail.qty) as qty,apt_stok_unit.harga_pokok as harga_beli from apt_obat,apt_distribusi,apt_distribusi_detail,
                                apt_unit,apt_stok_unit where apt_obat.kd_obat=apt_distribusi_detail.kd_obat and apt_distribusi.kd_unit_tujuan=apt_stok_unit.kd_unit_apt
								and apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_unit.kd_unit_apt=apt_distribusi.kd_unit_tujuan 
								and apt_stok_unit.kd_obat=apt_obat.kd_obat and apt_distribusi.kd_unit_tujuan='$kd_unit_apt' and month(apt_distribusi.tgl_distribusi)='$bulan' and year(apt_distribusi.tgl_distribusi)='$tahun'
								group by apt_obat.kd_obat");
		return $query->result_array();
	}

	function getPenerimaanDistribusiObat($obat,$bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		/*$query=$this->db->query("select apt_obat.kd_obat,apt_obat.nama_obat,apt_distribusi.tgl_distribusi,apt_distribusi_detail.qty,apt_stok_unit.harga_pokok as harga_beli from apt_obat,apt_distribusi,apt_distribusi_detail,
                                apt_unit,apt_stok_unit where apt_obat.kd_obat=apt_distribusi_detail.kd_obat and apt_distribusi.kd_unit_tujuan=apt_stok_unit.kd_unit_apt
								and apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_unit.kd_unit_apt=apt_distribusi.kd_unit_tujuan 
								and apt_stok_unit.kd_obat=apt_obat.kd_obat 
								and apt_distribusi.kd_unit_tujuan='$kd_unit_apt' and month(apt_distribusi.tgl_distribusi)='$bulan' and year(apt_distribusi.tgl_distribusi)='$tahun'");*/
		$query=$this->db->query("select apt_obat.kd_obat,apt_obat.nama_obat,apt_distribusi.tgl_distribusi,ifnull(sum(apt_distribusi_detail.qty),0) as qty from apt_obat,apt_distribusi,apt_distribusi_detail,
                                apt_unit where apt_obat.kd_obat=apt_distribusi_detail.kd_obat
								and apt_distribusi.no_distribusi=apt_distribusi_detail.no_distribusi and apt_unit.kd_unit_apt=apt_distribusi.kd_unit_tujuan
								and apt_obat.kd_obat='$obat' and posting=1
								and apt_distribusi.kd_unit_tujuan='$kd_unit_apt' and month(apt_distribusi.tgl_distribusi)='$bulan' and year(apt_distribusi.tgl_distribusi)='$tahun'
								group by apt_obat.kd_obat");
		$item =  $query->row_array();

		$queryreturgudang=$this->db->query("SELECT apt_obat.kd_obat, apt_obat.nama_obat, apt_retur_gudang.tgl_retur, ifnull( sum( apt_retur_gudang_detail.qty ) , 0 ) AS qty
								FROM apt_obat, apt_retur_gudang, apt_retur_gudang_detail, apt_unit
								WHERE apt_obat.kd_obat = apt_retur_gudang_detail.kd_obat
								AND apt_retur_gudang.no_retur = apt_retur_gudang_detail.no_retur
								AND apt_unit.kd_unit_apt = apt_retur_gudang.kd_unit_tujuan
								AND apt_obat.kd_obat ='$obat' and posting=1
								AND apt_retur_gudang.kd_unit_tujuan ='$kd_unit_apt' and month(apt_retur_gudang.tgl_retur)='$bulan' and year(apt_retur_gudang.tgl_retur)='$tahun'
								group by apt_obat.kd_obat");
		$itemreturgudang =  $queryreturgudang->row_array();

		if(empty($item)) $item['qty']= 0;
		if(empty($itemreturgudang)) $itemreturgudang['qty']= 0;
		return $item['qty']+$itemreturgudang['qty'];
	}

	function getPenerimaan($bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select apt_penerimaan_detail.kd_obat,apt_penerimaan.tgl_penerimaan,apt_penerimaan_detail.harga_beli,sum(apt_penerimaan_detail.qty_kcl) as qty_kcl from 
								apt_obat,apt_penerimaan,apt_penerimaan_detail,apt_unit where apt_penerimaan.no_penerimaan=apt_penerimaan_detail.no_penerimaan 
								and apt_penerimaan_detail.kd_obat=apt_obat.kd_obat and apt_penerimaan.kd_unit_apt=apt_unit.kd_unit_apt and
								apt_penerimaan.kd_unit_apt='$kd_unit_apt' and month(apt_penerimaan.tgl_penerimaan)='$bulan' and year(apt_penerimaan.tgl_penerimaan)='$tahun'
								group by apt_obat.kd_obat");
		return $query->result_array();
	}

	function getPenerimaanObat($obat,$bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select apt_penerimaan_detail.kd_obat,apt_penerimaan.tgl_penerimaan,apt_penerimaan_detail.harga_beli,ifnull(sum(apt_penerimaan_detail.qty_kcl+apt_penerimaan_detail.bonus),0) as qty_kcl from 
								apt_obat,apt_penerimaan,apt_penerimaan_detail,apt_unit where apt_penerimaan.no_penerimaan=apt_penerimaan_detail.no_penerimaan 
								and apt_penerimaan_detail.kd_obat=apt_obat.kd_obat and apt_penerimaan.kd_unit_apt=apt_unit.kd_unit_apt and posting=1 and
								apt_obat.kd_obat='$obat' and apt_penerimaan.kd_unit_apt='$kd_unit_apt' and month(apt_penerimaan.tgl_penerimaan)='$bulan' and year(apt_penerimaan.tgl_penerimaan)='$tahun'
								group by apt_obat.kd_obat");
		$item= $query->row_array();
		if(empty($item))return 0;
		return $item['qty_kcl'];
	}
	
	function getPenjualan($bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		/*$query=$this->db->query("select apt_penjualan_detail.kd_obat,apt_penjualan.tgl_penjualan,apt_penjualan_detail.qty,
								apt_penjualan_detail.harga_jual from apt_obat,apt_penjualan,apt_penjualan_detail,apt_unit 
								where apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan and 
								apt_penjualan_detail.kd_obat=apt_obat.kd_obat and apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt 
								and apt_penjualan.kd_unit_apt='$kd_unit_apt' and month(apt_penjualan.tgl_penjualan)='$bulan' and 
								year(apt_penjualan.tgl_penjualan)='$tahun'");*/
		$query=$this->db->query("select apt_penjualan_detail.kd_obat,apt_penjualan.tgl_penjualan,sum(apt_penjualan_detail.qty) as qty,
								apt_penjualan_detail.harga_jual from apt_obat,apt_penjualan,apt_penjualan_detail,apt_unit 
								where apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan and 
								apt_penjualan_detail.kd_obat=apt_obat.kd_obat and apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt 
								and apt_penjualan.kd_unit_apt='$kd_unit_apt' and month(apt_penjualan.tgl_penjualan)='$bulan' and 
								year(apt_penjualan.tgl_penjualan)='$tahun'
								group by apt_obat.kd_obat");
		return $query->result_array();
	}

	function getPenjualanObat($kd_obat,$bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		/*$query=$this->db->query("select apt_penjualan_detail.kd_obat,apt_penjualan.tgl_penjualan,apt_penjualan_detail.qty,
								apt_penjualan_detail.harga_jual from apt_obat,apt_penjualan,apt_penjualan_detail,apt_unit 
								where apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan and 
								apt_penjualan_detail.kd_obat=apt_obat.kd_obat and apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt 
								and apt_penjualan.kd_unit_apt='$kd_unit_apt' and month(apt_penjualan.tgl_penjualan)='$bulan' and 
								year(apt_penjualan.tgl_penjualan)='$tahun'");*/
		$query=$this->db->query("select apt_penjualan_detail.kd_obat,apt_penjualan.tgl_penjualan,ifnull(sum(apt_penjualan_detail.qty),0) as qty,
								apt_penjualan_detail.harga_jual from apt_obat,apt_penjualan,apt_penjualan_detail,apt_unit 
								where apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan and 
								apt_penjualan_detail.kd_obat=apt_obat.kd_obat and apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt 
								and apt_obat.kd_obat='$kd_obat' and apt_penjualan.kd_unit_apt='$kd_unit_apt' and month(apt_penjualan.tgl_penjualan)='$bulan' and 
								year(apt_penjualan.tgl_penjualan)='$tahun'
								group by apt_obat.kd_obat");
		$item = $query->row_array();
		if(empty($item))return 0;
		return $item['qty'];
	}
	
	function getReturPbf1($bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select apt_retur_obat_detail.kd_obat,apt_retur_obat.tgl_retur,sum(apt_retur_obat_detail.qty_kcl) as qty_kcl
								from apt_obat,apt_retur_obat,apt_retur_obat_detail,apt_unit 
								where apt_retur_obat.no_retur=apt_retur_obat_detail.no_retur and 
								apt_retur_obat_detail.kd_obat=apt_obat.kd_obat and apt_retur_obat_detail.kd_unit_apt=apt_unit.kd_unit_apt 
								and apt_retur_obat_detail.kd_unit_apt='$kd_unit_apt' and month(apt_retur_obat.tgl_retur)='$bulan' and 
								year(apt_retur_obat.tgl_retur)='$tahun'
								group by apt_obat.kd_obat");
		return $query->result_array();
	}

	function getReturPbfObat($obat,$bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select apt_retur_obat_detail.kd_obat,apt_retur_obat.tgl_retur,ifnull(sum(apt_retur_obat_detail.qty_kcl),0) as qty_kcl
								from apt_obat,apt_retur_obat,apt_retur_obat_detail,apt_unit 
								where apt_retur_obat.no_retur=apt_retur_obat_detail.no_retur and 
								apt_retur_obat_detail.kd_obat=apt_obat.kd_obat and apt_retur_obat_detail.kd_unit_apt=apt_unit.kd_unit_apt 
								and apt_obat.kd_obat='$obat' and apt_retur_obat_detail.kd_unit_apt='$kd_unit_apt' and month(apt_retur_obat.tgl_retur)='$bulan' and 
								year(apt_retur_obat.tgl_retur)='$tahun' and posting=1
								group by apt_obat.kd_obat");
		$item = $query->row_array();
		if(empty($item))return 0;
		return $item['qty_kcl'];
	}
	
	function getMutasiObat($kd_unit_apt,$bulan,$tahun){
	//function getMutasiObat($bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');  //$totalakhir=$hrg*$saldoakhir; 
		$query=$this->db->query("select apt_mutasi_obat.kd_obat,apt_obat.nama_obat,ifnull(apt_mutasi_obat.saldo_awal,0) as saldo_awal,ifnull(apt_mutasi_obat.in_pbf,0) as in_pbf,ifnull(apt_mutasi_obat.in_unit,0) as in_unit,ifnull(apt_mutasi_obat.retur_jual,0) as retur_jual,
								ifnull(apt_mutasi_obat.out_jual,0) as out_jual,ifnull(apt_mutasi_obat.out_unit,0) as out_unit,ifnull(apt_mutasi_obat.retur_pbf,0) as retur_pbf,ifnull(apt_mutasi_obat.saldo_akhir,0) as saldo_akhir,ifnull(apt_mutasi_obat.harga_beli,0) as harga_beli,
								ifnull(apt_mutasi_obat.stok_opname,0) as stok_opname,ifnull(apt_mutasi_obat.harga_beli,0) as harga_beli,ifnull((apt_mutasi_obat.saldo_awal*apt_mutasi_obat.harga_beli),0) as total_awal, 
								ifnull(((apt_mutasi_obat.in_pbf+apt_mutasi_obat.in_unit)-apt_mutasi_obat.retur_pbf),0) as jum_masuk, ifnull((((apt_mutasi_obat.in_pbf+apt_mutasi_obat.in_unit)-apt_mutasi_obat.retur_pbf)*apt_mutasi_obat.harga_beli),0) as total_masuk, 
								ifnull(((apt_mutasi_obat.out_jual+apt_mutasi_obat.out_unit)-apt_mutasi_obat.retur_jual),0) as jum_keluar,
								ifnull((((apt_mutasi_obat.out_jual+apt_mutasi_obat.out_unit)-apt_mutasi_obat.retur_jual)*apt_mutasi_obat.harga_beli),0) as total_keluar,
								ifnull((apt_mutasi_obat.saldo_akhir*apt_mutasi_obat.harga_beli),0) as total_akhir
								from apt_mutasi_obat,apt_obat where apt_mutasi_obat.kd_obat=apt_obat.kd_obat and apt_mutasi_obat.kd_unit_apt='$kd_unit_apt' and 
								apt_mutasi_obat.bulan='$bulan' and apt_mutasi_obat.tahun='$tahun' order by apt_mutasi_obat.kd_obat");
		return $query->result_array();
	}
	
	function stokopname($bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select history_perubahan_stok.kd_obat,history_perubahan_stok.qty,apt_stok_unit.harga_pokok from apt_unit,
								history_perubahan_stok,apt_obat,apt_stok_unit where history_perubahan_stok.kd_obat=apt_obat.kd_obat and
								apt_stok_unit.kd_obat=apt_obat.kd_obat and history_perubahan_stok.kd_unit_apt=apt_unit.kd_unit_apt and
								apt_stok_unit.kd_unit_apt=apt_unit.kd_unit_apt
								and history_perubahan_stok.kd_unit_apt='$kd_unit_apt' and month(history_perubahan_stok.tanggal)='$bulan'
								and year(history_perubahan_stok.tanggal)='$tahun'");
		return $query->result_array();
	}

	function stokopnameObat($obat,$bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select history_perubahan_stok.kd_obat,ifnull( sum( history_perubahan_stok.stok_baru - history_perubahan_stok.stok_lama ) , 0 ) as jumlah from apt_unit,
								history_perubahan_stok,apt_obat where history_perubahan_stok.kd_obat=apt_obat.kd_obat and
								history_perubahan_stok.kd_unit_apt=apt_unit.kd_unit_apt and history_perubahan_stok.kd_obat='$obat' and
								history_perubahan_stok.kd_unit_apt='$kd_unit_apt' and month(history_perubahan_stok.tanggal)='$bulan'
								and year(history_perubahan_stok.tanggal)='$tahun'
								GROUP BY history_perubahan_stok.kd_unit_apt, history_perubahan_stok.kd_obat ");
		$item = $query->row_array();
		if(empty($item))return 0;
		return $item['jumlah'];
	}
	
	function ambilObat($b,$a1){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select kd_obat,harga_beli from apt_mutasi_obat where kd_unit_apt='$kd_unit_apt' and bulan='$b' and tahun='$a1'");
		return $query->result_array();
	}

	function getMutasiPerObat($obat,$bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select * from apt_mutasi_obat where kd_unit_apt='$kd_unit_apt' and bulan='$bulan' and tahun='$tahun' and kd_obat='$obat' ");
		return $query->row_array();
	}

	function ambilDataObat(){
		//$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select kd_obat from apt_obat ");
		return $query->result_array();
	}
		
	function ambilyesterday($tgl_penjualan){
		$tanggal=convertDate($tgl_penjualan);
		$query=$this->db->query("SELECT DATE_SUB('$tanggal',interval 1 day) as yesterday");
		return $query->row_array();
	}
	
	function saldoawal($b,$a1,$kd_obat){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select saldo_akhir as saldoawal from apt_mutasi_obat where bulan='$b' and tahun='$a1' and kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt'");
		$saldo_awal=$query->row_array();
		if(empty($saldo_awal)) return '-';
		return $saldo_awal['saldoawal'];
	}
	
	function ambiloutunit($bulan,$tahun,$kd_obat){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select out_unit from apt_mutasi_obat where bulan='$bulan' and tahun='$tahun' and kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt'");
		$out_unita=$query->row_array();
		if(empty($out_unita)) return 0;
		return $out_unita['out_unit'];
	}
	
	function ambiloutjual($bulan,$tahun,$kd_obat){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select out_jual from apt_mutasi_obat where bulan='$bulan' and tahun='$tahun' and kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt'");
		$out_juala=$query->row_array();
		if(empty($out_juala)) return 0;
		return $out_juala['out_jual'];
	}
	
	function ambilreturpebef($bulan,$tahun,$kd_obat){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select retur_pbf from apt_mutasi_obat where bulan='$bulan' and tahun='$tahun' and kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt'");
		$retur_pbfa=$query->row_array();
		if(empty($retur_pbfa)) return 0;
		return $retur_pbfa['retur_pbf'];
	}
	
	function ambilinpebef($bulan,$tahun,$kd_obat){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select in_pbf from apt_mutasi_obat where bulan='$bulan' and tahun='$tahun' and kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt'");
		$in_pbfa=$query->row_array();
		if(empty($in_pbfa)) return 0;
		return $in_pbfa['in_pbf'];
	}
	
	function ambilinunit($bulan,$tahun,$kd_obat){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select in_unit from apt_mutasi_obat where bulan='$bulan' and tahun='$tahun' and kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt'");
		$in_unita=$query->row_array();
		if(empty($in_unita)) return 0;
		return $in_unita['in_unit'];
	}
	
	function ambilstokop($bulan,$tahun,$kd_obat){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select stok_opname from apt_mutasi_obat where bulan='$bulan' and tahun='$tahun' and kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt'");
		$stokop=$query->row_array();
		if(empty($stokop)) return 0;
		return $stokop['stok_opname'];
	}
	
	function ambilNamaUnit($kd_unit_apt){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select nama_unit_apt from apt_unit where kd_unit_apt='$kd_unit_apt'");
		$unit=$query->row_array();
		return $unit['nama_unit_apt'];
	}
	
	function namaSupplier($kd_supplier){
		$query=$this->db->query("select nama from apt_supplier where kd_supplier='$kd_supplier'");
		$supplier=$query->row_array();
		if(empty($supplier)) return 0;
		return $supplier['nama'];
	}
	
	function ambiltotalakhir($tgl_penjualan,$kd_unit_apt,$is_lunas,$resep){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$tgl_penjualan1=convertDate($tgl_penjualan);
		if($resep!=''){$b1=" and apt_penjualan.resep='$resep'";}
		else{$b1="";} $b=$b1;
		$query=$this->db->query("select sum(total_transaksi-adm_racik) as totalakhir from apt_penjualan where 
					tgl_penjualan between concat((SELECT DATE_SUB('$tgl_penjualan1',interval 1 day) as yesterday),' ','14:00:00') and concat('$tgl_penjualan1',' ','13:59:59') 
					and is_lunas='$is_lunas' and kd_unit_apt='$kd_unit_apt' $b");
		$totalakhir=$query->row_array();
		return $totalakhir['totalakhir'];
	}
	
	/*function getRekapPenjualanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$is_lunas,$resep,$status){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		
		if($resep!=''){$b1=" and apt_penjualan.resep='$resep'";}
		else{$b1="";} $b=$b1;
		
		$query1=$this->db->query("select adddate('$periodeawal1',1) as tgl");
		$tgl=$query1->row_array();
		$tgltambah=$tgl['tgl'];
		
		if($status==0){ //belum tutuppenjualan
			$query=$this->db->query("select 'Tunai' as jenis_bayar,apt_penjualan_detail.kd_obat,apt_obat.nama_obat,sum(apt_penjualan_detail.qty) as qty,apt_penjualan_detail.harga_jual,
									(sum(apt_penjualan_detail.qty)*apt_penjualan_detail.harga_jual) as total,sum(apt_penjualan_detail.adm_resep) as service,
									((sum(apt_penjualan_detail.qty)*apt_penjualan_detail.harga_jual)+sum(apt_penjualan_detail.adm_resep)) as totalsemua,
									(0.2*((sum(apt_penjualan_detail.qty)*apt_penjualan_detail.harga_jual)+sum(apt_penjualan_detail.adm_resep))) as untung,0 as diskon
									from apt_penjualan_detail,apt_penjualan,apt_obat 
									where apt_penjualan_detail.no_penjualan=apt_penjualan.no_penjualan and apt_penjualan_detail.kd_obat=apt_obat.kd_obat and 
									date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and apt_penjualan.kd_unit_apt='$kd_unit_apt' and 
									apt_penjualan.is_lunas='$is_lunas' $b and apt_penjualan.status='0' and apt_penjualan.tgl_tutup='0000-00-00 00:00:00' 
									group by apt_penjualan_detail.kd_obat,apt_penjualan_detail.harga_jual");
		}
		else{ //tutuppenjualan
			if($periodeawal==$periodeakhir){ //tglperiode sama
				$query=$this->db->query("select 'Tunai' as jenis_bayar,apt_penjualan_detail.kd_obat,apt_obat.nama_obat,sum(apt_penjualan_detail.qty) as qty,apt_penjualan_detail.harga_jual,
										(sum(apt_penjualan_detail.qty)*apt_penjualan_detail.harga_jual) as total,sum(apt_penjualan_detail.adm_resep) as service,
										((sum(apt_penjualan_detail.qty)*apt_penjualan_detail.harga_jual)+sum(apt_penjualan_detail.adm_resep)) as totalsemua,
										(0.2*((sum(apt_penjualan_detail.qty)*apt_penjualan_detail.harga_jual)+sum(apt_penjualan_detail.adm_resep))) as untung,0 as diskon
										from apt_penjualan_detail,apt_penjualan,apt_obat where apt_penjualan_detail.no_penjualan=apt_penjualan.no_penjualan and 
										apt_penjualan_detail.kd_obat=apt_obat.kd_obat and date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and
										apt_penjualan.kd_unit_apt='$kd_unit_apt' and apt_penjualan.is_lunas='$is_lunas' $b and apt_penjualan.status='1' and date_format(apt_penjualan.tgl_tutup,'%Y-%m-%d')='$periodeawal1' 
										group by apt_penjualan_detail.kd_obat,apt_penjualan_detail.harga_jual");
			}
			else{ //tglperiode beda
				$query=$this->db->query("select 'Tunai' as jenis_bayar,apt_penjualan_detail.kd_obat,apt_obat.nama_obat,sum(apt_penjualan_detail.qty) as qty,apt_penjualan_detail.harga_jual,
										(sum(apt_penjualan_detail.qty)*apt_penjualan_detail.harga_jual) as total,sum(apt_penjualan_detail.adm_resep) as service,
										((sum(apt_penjualan_detail.qty)*apt_penjualan_detail.harga_jual)+sum(apt_penjualan_detail.adm_resep)) as totalsemua,
										(0.2*((sum(apt_penjualan_detail.qty)*apt_penjualan_detail.harga_jual)+sum(apt_penjualan_detail.adm_resep))) as untung,0 as diskon
										from apt_penjualan_detail,apt_penjualan,apt_obat where apt_penjualan_detail.no_penjualan=apt_penjualan.no_penjualan and 
										apt_penjualan_detail.kd_obat=apt_obat.kd_obat and date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and
										apt_penjualan.kd_unit_apt='$kd_unit_apt' and apt_penjualan.is_lunas='$is_lunas' $b and apt_penjualan.status='1' and date_format(apt_penjualan.tgl_tutup,'%Y-%m-%d') between '$tgltambah' and '$periodeakhir1'
										group by apt_penjualan_detail.kd_obat,apt_penjualan_detail.harga_jual");
			}
		}
		return $query->result_array();
	}*/
	
	function getRekapPenjualanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$is_lunas,$resep,$status,$kd_jenis_bayar,$cust_code,$shiftapt,$jenis_pasien,$tipe){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		
		if($resep!=''){$b1=" and apt_penjualan.resep='$resep'";}
		else{$b1="";} $b=$b1;
		
		//if($kd_jenis_bayar!=''){$c1=" and apt_penjualan_bayar.kd_jenis_bayar='$kd_jenis_bayar'";}
		if($kd_jenis_bayar!=''){
			if($kd_jenis_bayar=='001'){
				$c1=" and apt_penjualan_bayar.kd_jenis_bayar='$kd_jenis_bayar'";				
			}else if($kd_jenis_bayar=='003'){
				$c1=" and apt_penjualan_bayar.kd_jenis_bayar='$kd_jenis_bayar'";								
			}else{
				//$c1="";
			}
		}else{
			$c1="";
		} 
		$c=$c1;
		
		if($cust_code!=''){$d1=" and apt_penjualan.cust_code='$cust_code'";}
		else{$d1="";} $d=$d1;
		
		//if($shiftapt!=''){$e1=" and apt_penjualan.shiftapt='$shiftapt'";}
		//else{$e1="";} $e=$e1;
		$tanggal="";
		if($shiftapt!=''){
			if($shiftapt==3){
				$date = $periodeakhir1;
				$date1 = str_replace('-', '/', $date);
				$tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));				
				$tanggal=" and apt_penjualan.tgl_penjualan between '$periodeawal1 21:30:00' and '$tomorrow 08:30:00' ";
				$e1=" and apt_penjualan.shiftapt='$shiftapt'";
			}else{
				$tanggal=" and date_format(apt_penjualan.tgl_penjualan, '%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' ";
				$e1=" and apt_penjualan.shiftapt='$shiftapt'";
			}
		}else{
			$tanggal=" and date_format(apt_penjualan.tgl_penjualan, '%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' ";
			$e1="";
		} 
		$e=$e1;

		if($tipe!=''){
			if($jenis_pasien==''){
				$jns=" and apt_customers.type='$tipe'";
			} else{
				$jns="and apt_penjualan.cust_code='$jenis_pasien'";	
			} 		
		}else{
			if($jenis_pasien!=''){
				$jns=" and apt_penjualan.cust_code='$jenis_pasien'";
			} else{
				$jns="";	
			} 		
		}

		$query1=$this->db->query("select adddate('$periodeawal1',1) as tgl");
		$tgl=$query1->row_array();
		$tgltambah=$tgl['tgl'];

			$query=$this->db->query("select apt_penjualan_detail.kd_obat,apt_obat.nama_obat,apt_penjualan.shiftapt,date_format(apt_penjualan.tgl_penjualan,'%d-%m-%Y') as tanggal,
										sum(apt_penjualan_detail.qty) as qty,apt_penjualan_detail.harga_jual,sum(apt_penjualan_detail.adm_resep) as service,
										apt_satuan_kecil.satuan_kecil,sum(apt_penjualan_detail.total) as totalsemua,0 as diskon,apt_customers.customer from apt_penjualan left join retur_penjualan on apt_penjualan.no_penjualan=retur_penjualan.no_penjualan,apt_penjualan_detail,apt_obat,apt_satuan_kecil,
										apt_customers 
										where apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan
										and apt_penjualan.cust_code=apt_customers.cust_code
										and apt_penjualan_detail.kd_obat=apt_obat.kd_obat
										and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil 
										and apt_penjualan.kd_unit_apt='$kd_unit_apt' and retur_penjualan.no_retur_penjualan is NULL $tanggal
										$b $c $d $e $jns

										group by apt_penjualan_detail.kd_obat,apt_penjualan_detail.harga_jual
										order by apt_customers.type,apt_penjualan.no_penjualan
										");			
		
/*		if($status==0){ //belum tutuppenjualan
			$query=$this->db->query("select apt_jenis_bayar.jenis_bayar,apt_penjualan_detail.kd_obat,apt_obat.nama_obat,apt_penjualan.shiftapt,date_format(apt_penjualan.tgl_penjualan,'%d-%m-%Y') as tanggal,
										sum(apt_penjualan_detail.qty) as qty,apt_penjualan_detail.harga_jual,sum(apt_penjualan_detail.adm_resep) as service,
										apt_satuan_kecil.satuan_kecil,sum(apt_penjualan_detail.total) as totalsemua,0 as diskon,apt_customers.customer from apt_penjualan,apt_penjualan_detail,apt_obat,apt_satuan_kecil,
										apt_jenis_bayar,apt_customers 
										where apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan
										and apt_penjualan.cust_code=apt_customers.cust_code
										and apt_penjualan_detail.kd_obat=apt_obat.kd_obat
										and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil 
										and apt_penjualan.kd_unit_apt='$kd_unit_apt' and apt_penjualan.status='0' and 
										date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' 
										and apt_penjualan.tgl_tutup='0000-00-00 00:00:00' $b $c $d $e $jns

										group by apt_penjualan_detail.kd_obat,apt_penjualan_detail.harga_jual,apt_jenis_bayar.kd_jenis_bayar
										order by date_format(apt_penjualan.tgl_penjualan,'%d-%m-%Y') asc,apt_penjualan.shiftapt asc
										");			
		}
		else{ //tutuppenjualan
			if($periodeawal==$periodeakhir){ //tglperiode sama
				$query=$this->db->query("select apt_jenis_bayar.jenis_bayar,apt_penjualan_detail.kd_obat,apt_obat.nama_obat,apt_penjualan.shiftapt,date_format(apt_penjualan.tgl_penjualan,'%d-%m-%Y') as tanggal,
										sum(apt_penjualan_detail.qty) as qty,apt_penjualan_detail.harga_jual,sum(apt_penjualan_detail.adm_resep) as service,
										apt_satuan_kecil.satuan_kecil,sum(apt_penjualan_detail.total) as totalsemua,0 as diskon from apt_penjualan,apt_penjualan_detail,apt_obat,apt_satuan_kecil,
										(select * from apt_penjualan_bayar group by no_penjualan) as apt_penjualan_bayar,apt_jenis_bayar,apt_customers  
										where apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan
										and apt_penjualan.cust_code=apt_customers.cust_code
										and apt_penjualan_bayar.no_penjualan=apt_penjualan.no_penjualan and apt_penjualan_detail.kd_obat=apt_obat.kd_obat
										and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil and apt_penjualan_bayar.kd_jenis_bayar=apt_jenis_bayar.kd_jenis_bayar
										and apt_penjualan.kd_unit_apt='$kd_unit_apt' and apt_penjualan.status='1' and 
										date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and 
										date_format(apt_penjualan.tgl_tutup,'%Y-%m-%d')='$periodeawal1' $b $c $d $e
										order by apt_penjualan.shiftapt asc,date_format(apt_penjualan.tgl_penjualan,'%d-%m-%Y') desc
										group by apt_penjualan_detail.kd_obat,apt_penjualan_detail.harga_jual,apt_jenis_bayar.kd_jenis_bayar");				
			}
			else{ //tglperiode beda
				$query=$this->db->query("select apt_jenis_bayar.jenis_bayar,apt_penjualan_detail.kd_obat,apt_obat.nama_obat,apt_penjualan.shiftapt,date_format(apt_penjualan.tgl_penjualan,'%d-%m-%Y') as tanggal,
										sum(apt_penjualan_detail.qty) as qty,apt_penjualan_detail.harga_jual,sum(apt_penjualan_detail.adm_resep) as service,
										apt_satuan_kecil.satuan_kecil,sum(apt_penjualan_detail.total) as totalsemua,0 as diskon from apt_penjualan,apt_penjualan_detail,apt_obat,apt_satuan_kecil,
										(select * from apt_penjualan_bayar group by no_penjualan) as apt_penjualan_bayar,apt_jenis_bayar ,apt_customers 
										where apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan
										and apt_penjualan.cust_code=apt_customers.cust_code
										and apt_penjualan_bayar.no_penjualan=apt_penjualan.no_penjualan and apt_penjualan_detail.kd_obat=apt_obat.kd_obat
										and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil and apt_penjualan_bayar.kd_jenis_bayar=apt_jenis_bayar.kd_jenis_bayar
										and apt_penjualan.kd_unit_apt='$kd_unit_apt' and apt_penjualan.status='1' and 
										date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and 
										date_format(apt_penjualan.tgl_tutup,'%Y-%m-%d') between '$tgltambah' and '$periodeakhir1' $b $c $d $e
										order by apt_penjualan.shiftapt asc,date_format(apt_penjualan.tgl_penjualan,'%d-%m-%Y') desc
										group by apt_penjualan_detail.kd_obat,apt_penjualan_detail.harga_jual,apt_jenis_bayar.kd_jenis_bayar");
			}
		}
		*/
		return $query->result_array();
	}

	function getRekapRacikPenjualanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$is_lunas,$resep,$status,$kd_jenis_bayar,$cust_code,$shiftapt,$jenis_pasien,$tipe){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		
		if($resep!=''){$b1=" and apt_penjualan.resep='$resep'";}
		else{$b1="";} $b=$b1;
		
		//if($kd_jenis_bayar!=''){$c1=" and apt_penjualan_bayar.kd_jenis_bayar='$kd_jenis_bayar'";}
		if($kd_jenis_bayar!=''){
			if($kd_jenis_bayar=='001'){
				$c1=" and apt_penjualan_bayar.kd_jenis_bayar='$kd_jenis_bayar'";				
			}else if($kd_jenis_bayar=='003'){
				$c1=" and apt_penjualan_bayar.kd_jenis_bayar='$kd_jenis_bayar'";								
			}else{
				//$c1="";
			}
		}else{
			$c1="";
		} 
		$c=$c1;
		
		if($cust_code!=''){$d1=" and apt_penjualan.cust_code='$cust_code'";}
		else{$d1="";} $d=$d1;
		
		if($shiftapt!=''){$e1=" and apt_penjualan.shiftapt='$shiftapt'";}
		else{$e1="";} $e=$e1;

		if($tipe!=''){
			if($jenis_pasien==''){
				$jns=" and apt_customers.type='$tipe'";
			} else{
				$jns="and apt_penjualan.cust_code='$jenis_pasien'";	
			} 		
		}else{
			if($jenis_pasien!=''){
				$jns=" and apt_penjualan.cust_code='$jenis_pasien'";
			} else{
				$jns="";	
			} 		
		}

		$query1=$this->db->query("select adddate('$periodeawal1',1) as tgl");
		$tgl=$query1->row_array();
		$tgltambah=$tgl['tgl'];

			$query=$this->db->query("select ifnull(sum(apt_penjualan.adm_racik),0) as totalracik from apt_penjualan left join retur_penjualan on apt_penjualan.no_penjualan=retur_penjualan.no_penjualan,
										apt_customers 
										where 
										apt_penjualan.cust_code=apt_customers.cust_code
										and apt_penjualan.kd_unit_apt='$kd_unit_apt' and retur_penjualan.no_retur_penjualan is NULL and
										date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' 
										$b $c $d $e $jns

										");			
		
		$item= $query->row_array();
		return $item['totalracik'];
	}

	function getRekapNoPenjualan($periodeawal,$periodeakhir,$kd_unit_apt,$status,$is_lunas,$resep){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		
		if($resep!=''){$b1=" and apt_penjualan.resep='$resep'";}
		else{$b1="";} $b=$b1;
		
		$query1=$this->db->query("select adddate('$periodeawal1',1) as tgl");
		$tgl=$query1->row_array();
		$tgltambah=$tgl['tgl'];
		
		if($status==0){
			$query=$this->db->query("select date_format(apt_penjualan.tgl_penjualan,'%d-%m-%Y %H:%m:%s') as tgl_penjualan,apt_penjualan.no_penjualan,apt_penjualan.nama_pasien,apt_customers.customer,
									apt_penjualan.is_lunas,apt_penjualan.status,date_format(apt_penjualan.tgl_tutup,'%d-%m-%Y %H:%m:%s') as tgl_tutup,ifnull((apt_penjualan.total_transaksi-apt_penjualan.adm_racik),0) as total_transaksi 
									from apt_penjualan,apt_customers,apt_unit where apt_penjualan.cust_code=apt_customers.cust_code and apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt 
									and date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and apt_penjualan.kd_unit_apt='$kd_unit_apt' and 
									apt_penjualan.is_lunas='$is_lunas' $b and apt_penjualan.status='0' and apt_penjualan.tgl_tutup='0000-00-00 00:00:00' ");
		}
		else{
			if($periodeawal==$periodeakhir){
				$query=$this->db->query("select date_format(apt_penjualan.tgl_penjualan,'%d-%m-%Y %H:%m:%s') as tgl_penjualan,apt_penjualan.no_penjualan,apt_penjualan.nama_pasien,apt_customers.customer,
									apt_penjualan.is_lunas,apt_penjualan.status,date_format(apt_penjualan.tgl_tutup,'%d-%m-%Y %H:%m:%s') as tgl_tutup,ifnull((apt_penjualan.total_transaksi-apt_penjualan.adm_racik),0) as total_transaksi
									from apt_penjualan,apt_customers,apt_unit where apt_penjualan.cust_code=apt_customers.cust_code and apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt and
									date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and
									apt_penjualan.kd_unit_apt='$kd_unit_apt' and apt_penjualan.is_lunas='$is_lunas' $b and apt_penjualan.status='1' and date_format(apt_penjualan.tgl_tutup,'%Y-%m-%d')='$periodeawal1'");
			}
			else{
				$query=$this->db->query("select date_format(apt_penjualan.tgl_penjualan,'%d-%m-%Y %H:%m:%s') as tgl_penjualan,apt_penjualan.no_penjualan,apt_penjualan.nama_pasien,apt_customers.customer,
									apt_penjualan.is_lunas,apt_penjualan.status,date_format(apt_penjualan.tgl_tutup,'%d-%m-%Y %H:%m:%s') as tgl_tutup,ifnull((apt_penjualan.total_transaksi-apt_penjualan.adm_racik),0) as total_transaksi 
									from apt_penjualan,apt_customers,apt_unit where apt_penjualan.cust_code=apt_customers.cust_code and apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt and
									date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and
									apt_penjualan.kd_unit_apt='$kd_unit_apt' and apt_penjualan.is_lunas='$is_lunas' $b and apt_penjualan.status='1' and date_format(apt_penjualan.tgl_tutup,'%Y-%m-%d') between '$tgltambah' and '$periodeakhir1'");
			}
		}
		return $query->result_array();
	}
	
	function QueryPenjualanObat($periodeawal,$periodeakhir,$kd_unit_apt,$kd_obat){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		
		if($kd_unit_apt!=''){$a1=" and apt_penjualan.kd_unit_apt='$kd_unit_apt'";}
		else{$a1="";} $a=$a1;
		
		if($kd_obat!=''){$b1=" and apt_penjualan_detail.kd_obat='$kd_obat'";}
		else{$b1="";} $b=$b1;
		
		$query=$this->db->query("select apt_penjualan_detail.kd_obat as kd_obat1,apt_penjualan.kd_unit_apt,apt_obat.nama_obat,sum(apt_penjualan_detail.qty) as qty, 
								(select ifnull((sum(jml_stok)),0) from apt_stok_unit where kd_unit_apt='u01' and kd_obat=kd_obat1) as jml_stok
								from apt_penjualan,apt_penjualan_detail,apt_obat,
								apt_stok_unit,apt_unit where apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan and apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt 
								and apt_penjualan_detail.kd_obat=apt_obat.kd_obat and apt_stok_unit.kd_obat=apt_obat.kd_obat and apt_penjualan_detail.tgl_expire=apt_stok_unit.tgl_expire
								and apt_stok_unit.kd_unit_apt=apt_unit.kd_unit_apt and date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1'
								$a $b group by apt_penjualan_detail.kd_obat");
		return $query->result_array();
	}
	
	function detilnyaPenjualanobat($periodeawal,$periodeakhir,$kd_unit_apt,$kd_obat){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		
		if($kd_unit_apt!=''){$a1=" and apt_penjualan.kd_unit_apt='$kd_unit_apt'";}
		else{$a1="";} $a=$a1;
		
		$query=$this->db->query("select date_format(apt_penjualan.tgl_penjualan,'%d-%m-%Y') as tgl_penjualan,apt_penjualan.no_penjualan,apt_penjualan.kd_unit_apt,
								apt_unit.nama_unit_apt,apt_penjualan_detail.qty from apt_penjualan,apt_penjualan_detail,apt_obat,apt_unit where
								apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan and apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt and
								apt_penjualan_detail.kd_obat=apt_obat.kd_obat and date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1'
								$a and apt_penjualan_detail.kd_obat='$kd_obat'");
		return $query->result_array();
	}
	
	function tes($kd_obat){		
		$query=$this->db->query("select kd_obat,nama_obat from apt_obat where kd_obat='$kd_obat'");
		return $query->row_array();
	}
	
	function queryexcel($periodeawal,$periodeakhir,$kd_unit_apt,$kd_obat){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		
		if($kd_unit_apt!=''){$a1=" and apt_penjualan.kd_unit_apt='$kd_unit_apt'";}
		else{$a1="";} $a=$a1;
		
		if($kd_obat!=''){$b1=" and apt_penjualan_detail.kd_obat='$kd_obat'";}
		else{$b1="";} $b=$b1;
		
		$query=$this->db->query("select date_format(apt_penjualan.tgl_penjualan,'%d-%m-%Y') as tgl_penjualan,apt_penjualan_detail.kd_obat,apt_obat.nama_obat,apt_penjualan.no_penjualan,
								apt_unit.nama_unit_apt,apt_penjualan_detail.qty from apt_penjualan,apt_penjualan_detail,apt_obat,apt_unit 
								where apt_penjualan.no_penjualan=apt_penjualan_detail.no_penjualan and apt_penjualan.kd_unit_apt=apt_unit.kd_unit_apt and 
								apt_penjualan_detail.kd_obat=apt_obat.kd_obat and date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' 
								$a $b");
		return $query->result_array();
	}
	
	function ambiljamsekarang(){
		$query=$this->db->query("select date_format(sysdate(),'%H:%i:%s') as jam");
		$jam=$query->row_array();
		return $jam['jam'];
	}
	
	function ambiltglkmrn($tglskrg){
		//$query=$this->db->query("select date_format(sysdate(),'%Y-%m-%d') as tglskrg");
		$query=$this->db->query("SELECT DATE_SUB('$tglskrg',interval 1 day) as tglkmrn");
		$tglkmrn=$query->row_array();
		return $tglkmrn['tglkmrn'];
	}
	
	function ambilIsiPenjualan($periodeawal,$periodeakhir,$kd_unit_apt,$shiftapt,$is_lunas){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		
		if($shiftapt!=''){$a1=" and apt_penjualan.shiftapt='$shiftapt'";}
		else{$a1="";} $a=$a1;
		
		if($is_lunas!=''){$b1=" and apt_penjualan.is_lunas='$is_lunas'";}
		else{$b1="";} $b=$b1;
		$query=$this->db->query("select apt_penjualan_detail.no_penjualan,apt_penjualan_detail.kd_obat,date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') as tgl_penjualan,apt_obat.nama_obat,
								apt_satuan_kecil.satuan_kecil,apt_penjualan_detail.qty,apt_penjualan_detail.tgl_expire,apt_penjualan_detail.racikan,
								apt_penjualan_detail.adm_resep,apt_penjualan_detail.harga_jual,(select setting as jasabungkus from sys_setting where key_data='TARIF_PERBUNGKUS') as jasabungkus 
								from apt_penjualan_detail,apt_penjualan,(select * from apt_penjualan_bayar group by no_penjualan) as apt_penjualan_bayar,apt_obat,apt_satuan_kecil,apt_jenis_bayar
								where apt_penjualan_detail.no_penjualan=apt_penjualan.no_penjualan and apt_penjualan_detail.kd_obat=apt_obat.kd_obat and 
								apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil and apt_penjualan_bayar.no_penjualan=apt_penjualan.no_penjualan and
								apt_penjualan_bayar.kd_jenis_bayar=apt_jenis_bayar.kd_jenis_bayar and date_format(apt_penjualan.tgl_penjualan,'%Y-%m-%d') 
								between '$periodeawal1' and '$periodeakhir1' and apt_penjualan.kd_unit_apt='$kd_unit_apt' $a $b order by apt_penjualan.no_penjualan,apt_penjualan_detail.kd_obat");
		return $query->result_array();
	}

	function getResepByDokter($periodeawal,$periodeakhir,$kd_unit_apt,$kd_dokter){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		if(!empty($kd_dokter)){
			$dok=" where kd_dokter='".$kd_dokter."'";
			if($kd_dokter=='99'){
				$dok1=" and (b.kd_dokter is null or b.kd_dokter='99') ";				
			}else{
				$dok1=" and b.kd_dokter='".$kd_dokter."'";
			}
		}else{
			$dok="";
			$dok1="";
		}
		$query=$this->db->query("select 1 as urut,kd_dokter,nama_dokter,'' as kd_obat,'' as nama_obat,0 as total from apt_dokter ".$dok."
								union
								(SELECT 2 as urut,ifnull(b.kd_dokter,'99') as kd_dokter,ifnull(d.nama_dokter,'-'),a.kd_obat,c.nama_obat,sum(a.qty) as total 
								FROM `apt_penjualan_detail` a join apt_penjualan b on a.no_penjualan=b.no_penjualan 
								join apt_obat c on a.kd_obat=c.kd_obat left join apt_dokter d on b.kd_dokter=d.kd_dokter 
								where date(b.tgl_penjualan) between '".$periodeawal1."' and '".$periodeakhir1."' and is_lunas=1 and returapt is null and b.kd_unit_apt='".$kd_unit_apt."' ".$dok1."
								group by b.kd_dokter,a.kd_obat order by total asc)
								order by kd_dokter,urut asc,total desc");
		return $query->result_array();
	}
	
	function getWow($bulan,$tahun,$kd_unit_apt){
		$query=$this->db->query("select history_perubahan_stok.tanggal,history_perubahan_stok.kd_obat,apt_obat.nama_obat,apt_satuan_kecil.satuan_kecil,history_perubahan_stok.stok_lama,
								history_perubahan_stok.stok_baru,apt_obat.harga_beli,history_perubahan_stok.tgl_expired,pegawai.nama_pegawai 
								from history_perubahan_stok join user on history_perubahan_stok.kd_user=user.id_user join pegawai on user.id_pegawai=pegawai.id_pegawai,
								apt_obat,apt_unit,apt_satuan_kecil
								where history_perubahan_stok.kd_obat=apt_obat.kd_obat and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil and
								month(history_perubahan_stok.tanggal)='$bulan' and year(history_perubahan_stok.tanggal)='$tahun' and history_perubahan_stok.kd_unit_apt=apt_unit.kd_unit_apt
								and history_perubahan_stok.kd_unit_apt='$kd_unit_apt' order by history_perubahan_stok.kd_obat");
		return $query->result_array(); 
	}
	
	function getFormStokopname($kd_unit_apt){
		$query=$this->db->query("select *
								from apt_obat left join apt_stok_unit on apt_obat.kd_obat=apt_stok_unit.kd_obat where apt_stok_unit.kd_unit_apt='".$kd_unit_apt."' ");
		return $query->result_array(); 
	}
	
	function getStatusPermintaan($periodeawal,$periodeakhir,$permintaan_status,$kd_unit_apt){
		$kd_unit_aptA=$this->session->userdata('kd_unit_apt');
		$kd_unit_aptB=$this->session->userdata('kd_unit_apt_gudang');
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		if($permintaan_status==1){ //kalo sudah didistribusi
			$a1="and apt_permintaan_obat_det.jml_req=apt_permintaan_obat_det.jml_distribusi";
		}
		else if($permintaan_status==2){ //kalo udh didistribusi sebagian
			$a1="and apt_permintaan_obat_det.jml_req>apt_permintaan_obat_det.jml_distribusi and apt_permintaan_obat_det.jml_distribusi>0";
		}
		else if($permintaan_status==3){ //kalo belum didistribusi
			$a1="and apt_permintaan_obat_det.jml_distribusi='0'";
		}
		else{ //kalo semuanya -->permintaan_status=0
			$a1="";
		}
		$a=$a1;
		if($kd_unit_aptA==$kd_unit_aptB){
			if($kd_unit_apt==''){
				$query=$this->db->query("select apt_permintaan_obat.no_permintaan,apt_permintaan_obat_det.urut,apt_permintaan_obat_det.keterangan,date_format(apt_permintaan_obat.tgl_permintaan,'%d-%m-%Y %H:%i:%s') as tgl_permintaan,apt_permintaan_obat_det.kd_obat,apt_obat.nama_obat,
									apt_satuan_kecil.satuan_kecil,apt_permintaan_obat_det.tgl_expire,apt_permintaan_obat_det.jml_req,apt_permintaan_obat_det.jml_distribusi,apt_unit.nama_unit_apt
									from apt_permintaan_obat,apt_permintaan_obat_det,apt_obat,apt_satuan_kecil,apt_unit
									where date_format(apt_permintaan_obat.tgl_permintaan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and apt_permintaan_obat_det.no_permintaan=apt_permintaan_obat.no_permintaan and apt_permintaan_obat_det.kd_obat=apt_obat.kd_obat
									and apt_permintaan_obat.kd_unit_apt=apt_unit.kd_unit_apt and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil $a order by apt_permintaan_obat.no_permintaan desc");
			}
			else{
				$query=$this->db->query("select apt_permintaan_obat.no_permintaan,apt_permintaan_obat_det.urut,apt_permintaan_obat_det.keterangan,date_format(apt_permintaan_obat.tgl_permintaan,'%d-%m-%Y %H:%i:%s') as tgl_permintaan,apt_permintaan_obat_det.kd_obat,apt_obat.nama_obat,
									apt_satuan_kecil.satuan_kecil,apt_permintaan_obat_det.tgl_expire,apt_permintaan_obat_det.jml_req,apt_permintaan_obat_det.jml_distribusi,apt_unit.nama_unit_apt
									from apt_permintaan_obat,apt_permintaan_obat_det,apt_obat,apt_satuan_kecil,apt_unit
									where date_format(apt_permintaan_obat.tgl_permintaan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and apt_permintaan_obat_det.no_permintaan=apt_permintaan_obat.no_permintaan and apt_permintaan_obat_det.kd_obat=apt_obat.kd_obat
									and apt_permintaan_obat.kd_unit_apt=apt_unit.kd_unit_apt and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil and apt_permintaan_obat.kd_unit_apt='$kd_unit_apt' $a order by apt_permintaan_obat.no_permintaan desc");
			}
		}
		else {
			$kd_unit_apt=$this->session->userdata('kd_unit_apt');
			$query=$this->db->query("select apt_permintaan_obat.no_permintaan,apt_permintaan_obat_det.urut,apt_permintaan_obat_det.keterangan,date_format(apt_permintaan_obat.tgl_permintaan,'%d-%m-%Y %H:%i:%s') as tgl_permintaan,apt_permintaan_obat_det.kd_obat,apt_obat.nama_obat,
									apt_satuan_kecil.satuan_kecil,apt_permintaan_obat_det.tgl_expire,apt_permintaan_obat_det.jml_req,apt_permintaan_obat_det.jml_distribusi,apt_unit.nama_unit_apt
									from apt_permintaan_obat,apt_permintaan_obat_det,apt_obat,apt_satuan_kecil,apt_unit
									where date_format(apt_permintaan_obat.tgl_permintaan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and apt_permintaan_obat_det.no_permintaan=apt_permintaan_obat.no_permintaan and apt_permintaan_obat_det.kd_obat=apt_obat.kd_obat
									and apt_permintaan_obat.kd_unit_apt=apt_unit.kd_unit_apt and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil and apt_permintaan_obat.kd_unit_apt='$kd_unit_apt' $a order by apt_permintaan_obat.no_permintaan desc");
		}
		return $query->result_array();
	}
	
	function getStatusPemesanan($periodeawal,$periodeakhir,$pemesanan_status,$kd_supplier){
		//$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		if($pemesanan_status==1){ //kalo sudah datang
			$a1="and apt_pemesanan_detail.qty_kcl=apt_pemesanan_detail.jml_penerimaan";
		}
		else if($pemesanan_status==2){ //kalo udh datang sebagian
			$a1="and apt_pemesanan_detail.qty_kcl>apt_pemesanan_detail.jml_penerimaan and apt_pemesanan_detail.jml_penerimaan>0";
		}
		else if($pemesanan_status==3){ //kalo belum datang
			$a1="and apt_pemesanan_detail.jml_penerimaan='0'";
		}
		else{ //kalo semuanya -->pemesanan_status=0
			$a1="";
		}
		$a=$a1;
		if($kd_supplier==''){
			$query=$this->db->query("select apt_pemesanan.no_pemesanan,apt_pemesanan_detail.urut,apt_pemesanan_detail.keterangan,date_format(apt_pemesanan.tgl_pemesanan,'%d-%m-%Y %H:%i:%s') as tgl_pemesanan,apt_pemesanan_detail.kd_obat,apt_obat.nama_obat,
								apt_satuan_kecil.satuan_kecil,apt_pemesanan_detail.qty_kcl,apt_pemesanan_detail.jml_penerimaan,apt_supplier.nama
								from apt_pemesanan,apt_pemesanan_detail,apt_obat,apt_satuan_kecil,apt_supplier
								where date_format(apt_pemesanan.tgl_pemesanan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and apt_pemesanan_detail.no_pemesanan=apt_pemesanan.no_pemesanan and apt_pemesanan_detail.kd_obat=apt_obat.kd_obat
								and apt_pemesanan.kd_supplier=apt_supplier.kd_supplier and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil $a order by apt_pemesanan.no_pemesanan desc");
		}
		else{
			$query=$this->db->query("select apt_pemesanan.no_pemesanan,apt_pemesanan_detail.urut,apt_pemesanan_detail.keterangan,date_format(apt_pemesanan.tgl_pemesanan,'%d-%m-%Y %H:%i:%s') as tgl_pemesanan,apt_pemesanan_detail.kd_obat,apt_obat.nama_obat,
								apt_satuan_kecil.satuan_kecil,apt_pemesanan_detail.qty_kcl,apt_pemesanan_detail.jml_penerimaan,apt_supplier.nama
								from apt_pemesanan,apt_pemesanan_detail,apt_obat,apt_satuan_kecil,apt_supplier
								where date_format(apt_pemesanan.tgl_pemesanan,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' and apt_pemesanan_detail.no_pemesanan=apt_pemesanan.no_pemesanan and apt_pemesanan_detail.kd_obat=apt_obat.kd_obat
								and apt_pemesanan.kd_supplier=apt_supplier.kd_supplier and apt_obat.kd_satuan_kecil=apt_satuan_kecil.kd_satuan_kecil and apt_pemesanan.kd_supplier='$kd_supplier' $a order by apt_pemesanan.no_pemesanan desc");
		}
		return $query->result_array();
	}
	
	function getLog($periodeawal,$periodeakhir,$kd_supplier,$jenis){
		$periodeawal1=convertDate($periodeawal);
		$periodeakhir1=convertDate($periodeakhir);
		
		if($kd_supplier!=''){$a1="and apt_penerimaan.kd_supplier='$kd_supplier'";}
		else{$a1="";} $a=$a1;
		
		if($jenis!=''){$b1="and apt_log_penerimaan.jenis='$jenis'";}
		else{$b1="";} $b=$b1;
		
		$query=$this->db->query("select apt_penerimaan.no_penerimaan,apt_penerimaan.kd_supplier,apt_supplier.nama,date_format(apt_log_penerimaan.tgl,'%d-%m-%Y %H:%i:%s') as tgl,
								apt_log_penerimaan.alasan,pegawai.nama_pegawai from apt_penerimaan,apt_supplier,apt_log_penerimaan,user,pegawai 
								where apt_log_penerimaan.no_penerimaan=apt_penerimaan.no_penerimaan and apt_penerimaan.kd_supplier=apt_supplier.kd_supplier
								and apt_log_penerimaan.kd_user=user.id_user and user.id_pegawai=pegawai.id_pegawai and 
								date_format(apt_log_penerimaan.tgl,'%Y-%m-%d') between '$periodeawal1' and '$periodeakhir1' $a $b order by apt_log_penerimaan.tgl desc");
		return $query->result_array();
	}
	
	function getReturJual1($bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select retur_penjualan_detail.kd_obat,retur_penjualan.tgl_returpenjualan,sum(retur_penjualan_detail.qty) as qty_kcl
								from apt_obat,retur_penjualan,retur_penjualan_detail,apt_unit 
								where retur_penjualan_detail.no_retur_penjualan=retur_penjualan.no_retur_penjualan and 
								retur_penjualan_detail.kd_obat=apt_obat.kd_obat and retur_penjualan_detail.kd_unit_apt=apt_unit.kd_unit_apt 
								and retur_penjualan_detail.kd_unit_apt='$kd_unit_apt' and month(retur_penjualan.tgl_returpenjualan)='$bulan' and 
								year(retur_penjualan.tgl_returpenjualan)='$tahun'
								group by apt_obat.kd_obat");
		return $query->result_array();
	}

	function getReturJualObat($obat,$bulan,$tahun){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select retur_penjualan_detail.kd_obat,retur_penjualan.tgl_returpenjualan,ifnull(sum(retur_penjualan_detail.qty),0) as qty_kcl
								from apt_obat,retur_penjualan,retur_penjualan_detail,apt_unit 
								where retur_penjualan_detail.no_retur_penjualan=retur_penjualan.no_retur_penjualan and 
								retur_penjualan_detail.kd_obat=apt_obat.kd_obat and retur_penjualan_detail.kd_unit_apt=apt_unit.kd_unit_apt and apt_obat.kd_obat='$obat' 
								and retur_penjualan_detail.kd_unit_apt='$kd_unit_apt' and month(retur_penjualan.tgl_returpenjualan)='$bulan' and 
								year(retur_penjualan.tgl_returpenjualan)='$tahun' and tutup=1
								group by apt_obat.kd_obat");
		$item = $query->row_array();
		if(empty($item))return 0;
		return $item['qty_kcl'];
	}
		
	function ambilbulansisdet(){
		$query=$this->db->query("select month(sysdate()) as sisdet");
		$sisdet=$query->row_array();
		//if(empty($sisdet)) return 0;
		return $sisdet['sisdet'];
	}
	
	function ambiltahunsisdet(){
		$query=$this->db->query("select year(sysdate()) as sisdettahun");
		$sisdettahun=$query->row_array();
		//if(empty($sisdet)) return 0;
		return $sisdettahun['sisdettahun'];
	}
	
	function ambilreturjual($bulan,$tahun,$kd_obat){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select retur_jual from apt_mutasi_obat where bulan='$bulan' and tahun='$tahun' and kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt'");
		$retur_juala=$query->row_array();
		if(empty($retur_juala)) return 0;
		return $retur_juala['retur_jual'];
	}
	
	function ngambilsaldoakhir($bulan,$tahun,$kd_obat){
		if(strlen($bulan)==1){$bulan1='0'.$bulan;}
		else{$bulan1=$bulan;}
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select saldo_awal as saldoakhir from apt_mutasi_obat where bulan='$bulan1' and tahun='$tahun' and kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt'");
		$saldoakhir=$query->row_array();
		if(empty($saldoakhir)) return '-';
		return $saldoakhir['saldoakhir'];
	}
	
	
	function ngambilbulanmin($tahun,$kd_obat){
		$kd_unit_apt=$this->session->userdata('kd_unit_apt');
		$query=$this->db->query("select min(bulan) as minbulan from apt_mutasi_obat where kd_obat='$kd_obat' and kd_unit_apt='$kd_unit_apt' and tahun='$tahun'");
		$minbulan=$query->row_array();
		if(empty($minbulan)) return '-';
		return $minbulan['minbulan'];
	}
	
}
?>