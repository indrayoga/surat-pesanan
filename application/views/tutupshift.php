<script type="text/javascript">
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
         
        return false;
        return true;
    }
    
    $(document).ready(function() {
        $('#nama_pabrik').focus();
            $('#form').ajaxForm({
                beforeSubmit: function(a,f,o) {
                    o.dataType = "json";
                    $('div.error').removeClass('error');
                    $('span.help-inline').html('');
                    $('#progress').show();
                    $('body').append('<div id="overlay1" style="position: fixed;height: 100%;width: 100%;z-index: 1000000;"></div>');
                    $('body').addClass('body1');
                    z=true;
                    $.ajax({
                    url: "<?php echo base_url(); ?>index.php/rumahsakit/periksashift",
                    type:"POST",
                    async: false,
                    data: $.param(a),
                    success: function(data){
                        //alert(data.status);
                        if(parseInt(data.status)==1){
                            z=data.status;
                            //alert('aa');
                            //alert($('input[name="harga"]').val());
                        }else if(parseInt(data.status)==0){
                            //alert('xxx');
                            $('#progress').hide();
                            $('body').removeClass('body1');
                            z=data.status;
                            for(yangerror=0;yangerror<=data.error;yangerror++){
                            //  alert(data.id[yangerror]);
                                $('#'+data.id[yangerror]).siblings('.help-inline').html('<p class="text-error">'+data.pesan[yangerror]+'</p>');
                                $('#error').html('<div class="alert alert-error fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>Terdapat beberapa kesalahan input silahkan cek inputan anda</div>');
                            }
                            $('#error').html('<div class="alert alert-error fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesanatas+'<br/>'+data.pesanlain+'</div>');
                            $('#overlay1').remove();
                            $('body').removeClass('body1');

                        }
                    },
                    dataType: 'json'
                    });

                    if(z==0)return false;
                },
                dataType:  'json',
                success: function(data) {
                //alert(data);
                if (typeof data == 'object' && data.nodeType)
                data = elementToString(data.documentElement, true);
                else if (typeof data == 'object')
                //data = objToString(data);
                    if(parseInt(data.status)==1) //jika berhasil
                    {
                        //apa yang terjadi jika berhasil
                        $('#progress').hide();
                        $('#overlay1').remove();
                        $('body').removeClass('body1');
                        $('#error').show();
                        $('#error').html('<div class="alert alert-success fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesan+'</div>');
                         window.location.href='<?php echo base_url(); ?>index.php/home/';
                    }
                    else if(parseInt(data.status)==0) //jika gagal
                    {
                        //apa yang terjadi jika gagal
                        $('#progress').hide();
                        $('#overlay1').remove();
                        $('body').removeClass('body1');
                        $('body').removeClass('body1');
                        $('#error').html('<div class="alert alert-success fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesan+'</div>');
                    }

                }
            });       

    });
</script>
             <div id="error"></div>

            <form class="form-horizontal" id="form" action="<?php echo base_url() ?>index.php/rumahsakit/updateshift" method="post" >
            <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-edit"></i></div>
                                            <h5>Tutup Shift</h5>
                                            <!-- .toolbar -->
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
                                                 	<li><button class="btn" type="submit"> <i class="icon-save icon-share-alt"></i> Update Shift</button></li>
                                                    <!--<li class="dropdown">
                                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                                            <i class="icon-th-large"></i>
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="">q</a></li>
                                                            <li><a href="">a</a></li>
                                                            <li><a href="">b</a></li>
                                                        </ul>
                                                    </li>-->
                                                    <li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.toolbar -->
                                        </header>
                                        <?php 
                                            $queryunitshift=$this->db->query('select * from unit_shift where kd_unit="'.$unit.'"'); 
                                            $unitshift=$queryunitshift->row_array();
                                            if($unitshift['shift']==$unitshift['jml_shift']){
                                                $shiftselanjutnya=1;
                                            }else{
                                                $shiftselanjutnya=$unitshift['shift']+1;
                                            }
                                        ?>                                        
                                        <div id="div-1" class="accordion-body collapse in body">
                                                <input type="hidden" name="unit" id="unit" value="<?php echo $unit; ?>">
                                                <div class="control-group">
                                                    <label for="shiftselanjutnya" class="control-label">Shift Selanjutnya</label>
                                                    <div class="controls with-tooltip">
                                                        <input type="text" id="shiftselanjutnya" name="" disabled class="span3 input-tooltip" data-original-title="" data-placement="bottom" value="<?php echo $shiftselanjutnya ?>"/>
                                                        <input type="hidden" id="shiftselanjutnya" name="shiftselanjutnya" class="span3 input-tooltip" data-original-title="" data-placement="bottom" value="<?php echo $shiftselanjutnya ?>"/>
                                                        <input type="hidden" id="shiftsebelumnya" name="shiftsebelumnya" class="span3 input-tooltip" data-original-title="" data-placement="bottom" value="<?php echo $unitshift['shift'] ?>"/>
                                                        <input type="text" id="jamupdate" name="jamupdate" disabled class="span3 input-tooltip" data-original-title="" data-placement="bottom" value="<?php echo date('d-m-Y h:i:s') ?>"/>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="shiftselanjutnya" class="control-label">Log Shift</label>
                                                    <div class="controls with-tooltip">
                                                        <textarea class="span11" style="text-align:left;height:250px"  disabled><?php
                                                            foreach ($items as $item) {
                                                                # code...
                                                                echo "-User ".$item['username']." Melakukan Tutup Shift ".$item['shift']." Pada Tanggal ".$item['tanggal_tutup']."\n";
                                                            }
                                                            ?></textarea>
                                                    </div>
                                                </div>                                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TEXT INPUT FIELD-->                            

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->
            </form>

<script type="text/javascript">
    $('.with-tooltip').tooltip({
        selector: ".input-tooltip"
    });

</script>