<script type="text/javascript">
	$(document).ready(function() {
       // $('#tipe').trigger('change');
        $('#kd_jenis_bayar').trigger('change');
	});
</script>
            <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-edit"></i></div>
                                            <h5>LAPORAN REKAP PENJUALAN PER OBAT</h5>
                                            <!-- .toolbar -->
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
                                                    <li><a target="_blank" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>third-party/fpdf/rekappenjualanobat.php?periodeawal=<?php echo $periodeawal ?>&periodeakhir=<?php echo $periodeakhir; ?>&is_lunas=<?php echo $is_lunas; ?>&tipe=<?php echo $tipe; ?>&kd_unit_apt=<?php echo $kd_unit_apt; ?>&resep=<?php echo $resep; ?>&status=<?php echo $status; ?>&kd_jenis_bayar=<?php echo $kd_jenis_bayar; ?>&cust_code=<?php echo $cust_code; ?>&jenis_pasien=<?php echo $jenis_pasien; ?>&shiftapt=<?php echo $shiftapt; ?>"> <i class="icon-print"></i> PDF</a></li>
													<li><a target="" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>index.php/transapotek/laporanapt/rl1excelrekappenjualan/<?php echo $periodeawal ?>/<?php echo $periodeakhir; ?>/<?php if(empty($kd_unit_apt )) echo "null"; else echo $kd_unit_apt; ?>/<?php if(empty($is_lunas)) echo "null"; else  echo $is_lunas; ?>/<?php if(empty($resep)) echo "null"; else echo $resep; ?>/<?php if(empty($status)) echo "null"; else echo $status; ?>/<?php if(empty($kd_jenis_bayar)) echo "null"; else echo $kd_jenis_bayar; ?>/<?php if(empty($cust_code)) echo "null"; else echo $cust_code; ?>/<?php if(empty($shiftapt)) echo "null"; else echo $shiftapt; ?>/<?php if(empty($jenis_pasien)) echo "null"; else echo $jenis_pasien; ?>/<?php if(!is_numeric($tipe)) echo "null"; else echo $tipe; ?>"> <i class="icon-print"></i> Export to Excel</a></li>
                                                    <li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.toolbar value="<-?php echo $periodeawal; ?>"-->
                                        </header>
                                        <div id="div-1" class="accordion-body collapse in body">
                                            <form class="form-horizontal" method="POST" action="<?php echo base_url() ?>index.php/transapotek/laporanapt/rekappenjualan">
                                                <div class="row-fluid">
													<div class="span12">
														<div class="span6">
															<div class="control-group">
																<label for="periodeawal" class="control-label">Periode</label>
																<div class="controls with-tooltip">
																	<input type="text" id="periodeawal" name="periodeawal" class="input-small input-tooltip" data-mask="99-99-9999"
																		   value="<?php echo $periodeawal?>" data-original-title="masukkan tanggal awal" data-placement="bottom"/>
																		   -
																	<input type="text" id="periodeakhir" name="periodeakhir" class="input-small input-tooltip" data-mask="99-99-9999"
																		   value="<?php echo $periodeakhir?>" data-original-title="masukkan tanggal akhir" data-placement="bottom"/>
																</div>
															</div> 
														</div>														
													</div>
												</div>
												<div class="row-fluid">												
                                                    <div class="span12">
														<div class="span6">
                                                            <div class="control-group">
                                                                <label for="kd_unit_apt" class="control-label">Unit Apotek</label>
                                                                <div class="controls with-tooltip">
																	<input type="text" name="nama_unit_apt" id="nama_unit_apt" value="<?php if($unit=$this->mlaporanapt->ambilNamaUnit($this->session->userdata('kd_unit_apt'))) echo $unit; ?>" readonly class="span7 input-tooltip" data-original-title="nama unit" data-placement="bottom"/>
																	<input type="hidden" name="kd_unit_apt" id="kd_unit_apt" value="<?php echo $this->session->userdata('kd_unit_apt'); ?>" readonly class="span2 input-tooltip" data-original-title="kd unit apt " data-placement="bottom"/>
                                                                </div>
															</div>
                                                        </div>
													</div>
												</div>
                                                <div class="row-fluid">                                             
                                                    <div class="span12">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label for="shiftapt" class="control-label">Shift</label>
                                                                <div class="controls with-tooltip">
                                                                    <select name="shiftapt" id="shiftapt" class="input-medium">
                                                                        <option value="">Semua Shift</option>
                                                                        <?php
                                                                        for ($i=1; $i <= $datashift['jml_shift']; $i++) {
                                                                            # code...
                                                                             if($i==$shiftapt)$sel="selected=selected";else $sel="";
                                                                        ?>
                                                                        <option value="<?php echo $i ?>"  <?php echo $sel; ?>><?php echo "Shift ".$i; ?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </select>                                                                                                                                       
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                                                                                                                                                                          
												<div class="row-fluid">												
                                                    <div class="span12">
                                                        <div class="span6">
                                                            <div class="control-group">
																<label for="resep" class="control-label">Resep</label>
                                                                <div class="controls with-tooltip">
                                                                    <select name="resep" id="resep" class="input-medium">
																		<option value="" <?php if($resep=='2') echo "selected=selected"; ?>>Semua</option>
																		<option value="0" <?php if($resep=='0') echo "selected=selected"; ?>>Tanpa Resep</option>
                                                                        <option value="1" <?php if($resep=='1') echo "selected=selected"; ?>>Dengan Resep</option>                                                                                                                                                
                                                                    </select>
																	<!--<select name="is_lunas" id="is_lunas" class="input-medium">-->
																		<!--option value="" <-?php if($is_lunas=='') echo "selected=selected"; ?>>Semua</option-->
																		<!--<option value="0" <?php if($is_lunas=='0') echo "selected=selected"; ?>>Belum Lunas</option>                                                                                                                                                
																		<option value="1" <?php if($is_lunas=='1') echo "selected=selected"; ?>>Lunas</option>                                                                        
                                                                        -->
                                                                    </select>
                                                                </div>																
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
												<!--<div class="row-fluid">												
                                                    <div class="span12">
                                                        <div class="span6">
                                                            <div class="control-group">
																<label for="status" class="control-label">Status Transaksi</label>
                                                                <div class="controls with-tooltip">
                                                                    <select name="status" id="status" class="input-large">
																		<option value="0" <?php if($status=='0') echo "selected=selected"; ?>>Belum Tutup Penjualan</option>                                                                                                                                                
																		<option value="1" <?php if($status=='1') echo "selected=selected"; ?>>Tutup Penjualan</option>                                                                        
                                                                    </select>
                                                                </div>																
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>-->
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label for="jenis_pasien" class="control-label">Jenis Pasien</label>
                                                                <div class="controls with-tooltip">
                                                                    <select name="tipe" id="tipe" class="input-medium" tabindex="2">
                                                                        <option value="">Pilih Tipe Pasien</option>
                                                                        <option value="0" <?php  if(isset($tipe) && $tipe==0) echo "selected=selected";?> >Tunai</option>
                                                                        <option value="1" <?php  if(isset($tipe) && $tipe==1) echo "selected=selected"; ?>>Asuransi</option>
                                                                        <option value="2" <?php  if(isset($tipe) && $tipe==2) echo "selected=selected"; ?>>Perusahaan</option>
                                                                        <option value="3" <?php  if(isset($tipe) && $tipe==3) echo "selected=selected"; ?>>Sosial Dakwah</option>
                                                                        <option value="4" <?php  if(isset($tipe) && $tipe==4) echo "selected=selected";?>>BPJS</option>
                                                                    </select>
                                                                    <select name="jenis_pasien" id="jenis_pasien" class="input-medium">
                                                                        <option value="" tipe="11111">Pilih Semua</option>
                                                                        <?php
                                                                        foreach ($datacustomer as $key => $customer) {
                                                                            # code...
                                                                            if($jenis_pasien==$customer['cust_code']) $sel="selected=selected"; else $sel="";
                                                                        ?>
                                                                        <option value="<?php echo $customer['cust_code']; ?>" <?php echo $sel; ?> tipe="<?php if(!empty($customer)) echo $customer['type'] ?>"> <?php echo $customer['customer']; ?> </option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </select>   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label for="text1" class="control-label">&nbsp;</label>
                                                    <div class="controls with-tooltip">
                                                        <button class="btn btn-primary" type="submit"><i class="icon-search"></i> Cari</button>
                                                        <button class="btn " type="submit" name="reset" value="reset"><i class="icon-undo"></i> Reset</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TEXT INPUT FIELD-->                            
                            <!--Begin Datatables-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-move"></i></div>
											<?php if($kd_jenis_bayar==''){ ?>
												<h5></h5>
											<?php } else {
													//$jenisbayar=$this->mlaporanapt->jenisbayar($kd_jenis_bayar);
													if($cust_code==''){ ?>
														<h5><?php //echo //strtoupper($jenisbayar); ?></h5>
													<?php } else { 
														//$jenispasien=$this->mlaporanapt->jenispasien($cust_code);?>															
														<h5><?php // echo strtoupper($jenisbayar).' - '.$jenispasien; ?></h5>
                                            <?php }} ?>
                                        </header>
                                        <div id="collapse4" class="body">
                                            <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                                                <thead>
                                                    <tr style="font-size:90% !important;">
                                                        <th style="text-align:center;">No</th>
                                                        <th style="text-align:center;">Tgl</th>
                                                        <th style="text-align:center;">Shift</th>
                                                        <th style="text-align:center;">Sub</th>
                                                        <th style="text-align:center;">Kd.Obat</th>
														<th style="text-align:center;">Nama Obat</th>
                                                        <th style="text-align:center;">Jml</th>
                                                        <th style="text-align:center;">Hrg Sat</th>
														<th style="text-align:center;">Total</th>
														<th style="text-align:center;">Service</th>
														<!--th style="text-align:center;">Untung</th-->
														<th style="text-align:center;">Total Semua</th>																												
														<!--th style="text-align:center;">Status Pasien</th-->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    //$no=1;
													$totalpenjualan=0;
                                                    $no=1;
                                                    foreach ($items as $item) {	
														$total=$item['qty']*$item['harga_jual'];
														$untung=0.2*$item['totalsemua'];
                                                    ?>
                                                        <tr style="font-size:90% !important;">
                                                            <td style="text-align:center;"><?php echo $no; ?></td>
                                                            <td style="text-align:center;"><?php echo $item['tanggal'] ?></td>
                                                            <td style="text-align:left;">Shift <?php echo $item['shiftapt'] ?></td>
                                                            <td style="text-align:center;"><?php echo $item['customer'] ?></td>
															<td style="text-align:center;"><?php echo $item['kd_obat'] ?></td>
															<td><?php echo $item['nama_obat'] ?></td>
															<td style="text-align:right;"><?php echo $item['qty'] ?></td> 
															<td style="text-align:right;"><?php echo number_format($item['harga_jual'],2,',','.') ?></td>
															<td style="text-align:right;"><?php echo number_format($total,2,',','.') ?></td>
															<td style="text-align:right;"><?php echo number_format($item['service'],2,',','.') ?></td>
															<!--td style="text-align:right;"><-?php echo number_format($untung,2,',','.') ?></td-->
															<td style="text-align:right;"><?php echo number_format($item['totalsemua'],2,',','.') ?></td>																														
                                                        </tr>                                                    
                                                    <?php
                                                    $no++;
														$totalpenjualan=$totalpenjualan+($item['totalsemua']);
                                                    }
                                                    ?>
                                                </tbody>
												<tfoot>
                                                    <tr>
                                                        <?php $biayaracik=$this->mlaporanapt->getRekapRacikPenjualanApotek($periodeawal,$periodeakhir,$kd_unit_apt,$is_lunas,$resep,$status,$kd_jenis_bayar,$cust_code,$shiftapt,$jenis_pasien,$tipe); ?>
                                                        <th colspan="12" style="text-align:right;" class="header">Total Biaya Racik (Rp) : <input style="text-align:right;width:130px;font-size:95% !important;" type="text" class="input-medium cleared" id="" value="<?php  echo number_format($biayaracik,2,',','.');?>" disabled></th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="12" style="text-align:right;" class="header">Total Penjualan (Rp) : <input style="text-align:right;width:130px;font-size:95% !important;" type="text" class="input-medium cleared" id="totalpenjualan" value="<?php  echo number_format($totalpenjualan+$biayaracik,2,',','.');?>" disabled></th>
                                                    </tr>
												</tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Datatables-->

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->




<script type="text/javascript">
$('#tipe').change(function(){
    //alert('xx');
    var val=$(this).val();
    //alert(val);
    //$('#cust_code option[tipe!='+val+']').attr('hidden','hidden');
    $('#jenis_pasien option[tipe!='+val+']').attr('hidden','hidden');
    $('#jenis_pasien option[tipe='+val+']').removeAttr('hidden');
    $('#jenis_pasien option[tipe='+val+']').show();
    $('#jenis_pasien option[tipe=11111]').show();
    <?php //if(isset) ?>
    $('#jenis_pasien option[tipe='+val+']').prop('selected',true);
    $('#jenis_pasien option[tipe=11111]').prop('selected',true);
});

	$('#dataTable').dataTable({
		"aaSorting": [[ 0, "asc" ]],
        "aoColumns": [
              { "sType": "numeric" },
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null
            ],
		"sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "Show _MENU_ entries"
		}
	});
	
	$('#kd_jenis_bayar').change(function(){
		if($(this).val()=="003"){
			//$('#kd_sumber').text('Simpan Transfer');
			$('#cust_code').prop("disabled", false);
		}
		else {
			$('#cust_code').prop("disabled", true);
		}
	})
	
	/*$('#tgl_penjualan').change(function(){
		$.ajax({
			url: '<?php echo base_url() ?>index.php/transapotek/laporanapt/ambiltglkemaren/',
			async:true,
			type:'post',
			data:{query:$('#tgl_penjualan').val()},
			success:function(data,result){	
				//alert(result);
				alert(this.data);
				alert(this.result);
				/*$.each(result,function(i,l){
					alert('2');
					$('#tgl_kemaren').html(result.yesterday);
					alert('3');
				});	*/	
			/*},
			dataType:'json'                         
		});	 
    });*/
	
	$('.with-tooltip').tooltip({
		selector: ".input-tooltip"
	});
	
	$('#periodeawal').datepicker({
		format: 'dd-mm-yyyy'
	});
	
	$('#periodeakhir').datepicker({
		format: 'dd-mm-yyyy'
	});

</script>