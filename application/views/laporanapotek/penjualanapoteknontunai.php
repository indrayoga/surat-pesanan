
            <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-edit"></i></div>
                                            <h5>LAPORAN PENJUALAN OBAT / ALKES PER RESEP NON TUNAI</h5>
                                            <!-- .toolbar -->
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
                                                    <li><a target="_blank" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>index.php/transapotek/laporanapt/laporanpemakaianobat/<?php echo $bulan ?>/<?php echo $tahun; ?>/<?php echo $kd_unit_apt; ?>/<?php echo $tipe; ?>/<?php echo $jenis_pasien; ?>"> <i class="icon-print"></i> Excel</a></li>
                                                    <li><a target="_blank" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>third-party/fpdf/laporanpemakaianobat.php?bulan=<?php echo $bulan ?>&tahun=<?php echo $tahun; ?>&kd_unit_apt=<?php echo $kd_unit_apt; ?>&jenis_pasien=<?php echo $jenis_pasien; ?>&tipe=<?php echo $tipe; ?>"> <i class="icon-print"></i> PDF</a></li>
                                                    <li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.toolbar value="<-?php echo $periodeawal; ?>"-->
                                        </header>
                                        <div id="div-1" class="accordion-body collapse in body">
                                            <form class="form-horizontal" method="POST" action="<?php echo base_url() ?>index.php/transapotek/laporanapt/penjualanapoteknontunai">
                                                <div class="row-fluid">
													<div class="span12">
														<div class="span5">
															<div class="control-group">
																<label for="periodeawal" class="control-label">Tgl. Penjualan</label>
																<div class="controls with-tooltip">
                                                                    <select name="bulan" id="bulan">
                                                                        <option value="01" <?php if($bulan=='01')echo "selected=selected"; ?> >Januari</option>
                                                                        <option value="02" <?php if($bulan=='02')echo "selected=selected"; ?>>Februari</option>
                                                                        <option value="03" <?php if($bulan=='03')echo "selected=selected"; ?>>Maret</option>
                                                                        <option value="04" <?php if($bulan=='04')echo "selected=selected"; ?>>April</option>
                                                                        <option value="05" <?php if($bulan=='05')echo "selected=selected"; ?>>Mei</option>
                                                                        <option value="06" <?php if($bulan=='06')echo "selected=selected"; ?>>Juni</option>
                                                                        <option value="07" <?php if($bulan=='07')echo "selected=selected"; ?>>Juli</option>
                                                                        <option value="08" <?php if($bulan=='08')echo "selected=selected"; ?>>Agustus</option>
                                                                        <option value="09" <?php if($bulan=='09')echo "selected=selected"; ?>>September</option>
                                                                        <option value="10" <?php if($bulan=='10')echo "selected=selected"; ?>>Oktober</option>
                                                                        <option value="11" <?php if($bulan=='11')echo "selected=selected"; ?>>November</option>
                                                                        <option value="12" <?php if($bulan=='12')echo "selected=selected"; ?>>Desember</option>
                                                                    </select>
																	<input type="text" id="tahun" name="tahun" class="input-small input-tooltip" data-mask="9999"
																		   data-original-title="masukkan tahun" value="<?php echo $tahun; ?>" data-placement="bottom"/>
																</div>
															</div> 
														</div>
													</div>
												</div>
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label for="kd_unit_apt" class="control-label">Unit Apotek</label>
                                                                <div class="controls with-tooltip">
                                                                    <input type="text" name="nama_unit_apt" id="nama_unit_apt" value="<?php if($unit=$this->mlaporanapt->ambilNamaUnit($this->session->userdata('kd_unit_apt'))) echo $unit; ?>" readonly class="span7 input-tooltip" data-original-title="nama unit" data-placement="bottom"/>
                                                                    <input type="hidden" name="kd_unit_apt" id="kd_unit_apt" value="<?php echo $this->session->userdata('kd_unit_apt'); ?>" readonly class="span2 input-tooltip" data-original-title="kd unit apt " data-placement="bottom"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label for="jenis_pasien" class="control-label">Jenis Pasien</label>
                                                                <div class="controls with-tooltip">
                                                                    <select name="tipe" id="tipe" class="input-medium" tabindex="2">
                                                                        <option value="" <?php  if(isset($tipe) && $tipe=="") echo "selected=selected"; ?> >Pilih Tipe Pasien</option>
                                                                        <option value="1" <?php  if(isset($tipe) && $tipe==1) echo "selected=selected"; ?>>Asuransi</option>
                                                                        <option value="2" <?php  if(isset($tipe) && $tipe==2) echo "selected=selected"; ?>>Perusahaan</option>
                                                                        <option value="3" <?php  if(isset($tipe) && $tipe==3) echo "selected=selected"; ?>>Sosial Dakwah</option>
                                                                        <option value="4" <?php  if(isset($tipe) && $tipe==4) echo "selected=selected"; ?>>BPJS</option>
                                                                    </select>
                                                                    <select name="jenis_pasien" id="jenis_pasien" class="input-medium">
                                                                        <?php
                                                                        foreach ($datacustomer as $key => $customer) {
                                                                            # code...
                                                                            if($jenis_pasien==$customer['cust_code']) $sel="selected=selected"; else $sel="";
                                                                        ?>
                                                                        <option value="<?php echo $customer['cust_code']; ?>" <?php echo $sel; ?> tipe="<?php if(!empty($customer)) echo $customer['type'] ?>"> <?php echo $customer['customer']; ?> </option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                        <option value="" tipe="11111">Pilih Semua</option>
                                                                    </select>   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="text1" class="control-label">&nbsp;</label>
                                                    <div class="controls with-tooltip">
                                                        <button class="btn btn-primary" type="submit"><i class="icon-search"></i> Cari</button>
                                                        <button class="btn " type="submit" name="reset" value="reset"><i class="icon-undo"></i> Reset</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TEXT INPUT FIELD-->                            
                            <!--Begin Datatables-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-move"></i></div>
                                            <h5></h5>
                                        </header>
                                        <div id="collapse4" class="body">
                                            <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                                                <thead>
                                                    <tr style="font-size:90% !important;">
                                                        <th style="text-align:center;">No</th>
                                                        <th style="text-align:center;">Jns Pasien</th>
                                                        <th style="text-align:center;">No. Penjualan</th>
														<th style="text-align:center;">Tgl. Penjualan</th>
                                                        <th style="text-align:center;">Nama Pasien</th>
                                                        <th style="text-align:center;">Nama Karyawan</th>
                                                        <th style="text-align:center;">Obat</th>
                                                        <th style="text-align:center;">UP 10%</th>
                                                        <th style="text-align:center;">Biaya Obat Setelah UP</th>
														<th style="text-align:center;">Service</th>
														<!--th style="text-align:center;">Jasa Resep</th-->
														<th style="text-align:center;">Biaya Obat</th>
                                                        <th style="text-align:center;">Biaya Jasa &amp; Adm Kartu</th>                                                      
                                                        <th style="text-align:center;">Jumlah Biaya Jasa + Obat</th>                                                      
                                                        <th style="text-align:center;">Jasa Medis</th>
                                                        <th style="text-align:center;">Adm Kartu</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $no=1;
                                                    foreach ($items as $item) {
                                                        $up=(10/100)*$item['obat'];
                                                    ?>
                                                        <tr style="font-size:90% !important;">
                                                            <td style="text-align:center;"><?php echo $no."."; ?></td>
                                                            <td style="text-align:left;"><?php echo $item['customer'] ?></td>
                                                            <td style="text-align:center;"><?php echo $item['no_penjualan'] ?></td>
                                                            <td style="text-align:center;"><?php echo $item['tgl_penjualan'] ?></td>
															<td><?php echo $item['nama_pasien'] ?>&nbsp;</td>
															<td><?php echo $item['no_peserta'].'/'.$item['nama_peserta'];  ?>&nbsp;</td>
                                                            <td style="text-align:right;"><?php echo number_format($item['obat'],2,'.',','); ?></td>
                                                            <td style="text-align:right;"><?php echo number_format($up,2,'.',','); ?></td>
                                                            <td style="text-align:right;"><?php echo number_format($item['obat']+$up,2,'.',','); ?></td>
															<td style="text-align:right;"><?php echo number_format($item['resep']+$item['adm_racik'],2,'.',','); ?></td>
															<!--td style="text-align:right;"><-?php echo number_format($item['adm_resep']) ?></td-->
															<td style="text-align:right;"><?php echo number_format($item['obat']+$up+$item['resep']+$item['adm_racik'],2,'.',',') ?></td>
															<!--td style="text-align:right;"><-?php echo number_format($item['total_bayar']) ?></td-->
                                                            <td style="text-align:right;"><?php echo number_format($item['jasa_medis']+$item['biaya_adm']+$item['biaya_kartu'],2,'.',','); ?></td>
                                                            <td style="text-align:right;"><?php echo number_format($item['obat']+$up+$item['resep']+$item['adm_racik']+$item['jasa_medis']+$item['biaya_adm']+$item['biaya_kartu'],2,'.',','); ?></td>
                                                            <td style="text-align:right;"><?php echo number_format($item['jasa_medis'],2,'.',','); ?>&nbsp;</td>
                                                            <td style="text-align:right;"><?php echo number_format($item['biaya_adm']+$item['biaya_kartu'],2,'.',','); ?>&nbsp;</td>
                                                        </tr>                                                    
                                                    <?php
                                                    $no++;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Datatables-->

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->




<script type="text/javascript">
$('#tipe').change(function(){
    //alert('xx');
    var val=$(this).val();
    //alert(val);
    //$('#cust_code option[tipe!='+val+']').attr('hidden','hidden');
    $('#jenis_pasien option[tipe!='+val+']').attr('hidden','hidden');
    $('#jenis_pasien option[tipe='+val+']').removeAttr('hidden');
    $('#jenis_pasien option[tipe='+val+']').show();
    $('#jenis_pasien option[tipe=11111]').show();
    <?php //if(isset) ?>
    $('#jenis_pasien option[tipe='+val+']').prop('selected',true);
    $('#jenis_pasien option[tipe=11111]').prop('selected',true);
});


	$('#dataTable').dataTable({
		"aaSorting": [[ 0, "asc" ]],
		"sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "Show _MENU_ entries"
		}
	});
	
	$('.with-tooltip').tooltip({
		selector: ".input-tooltip"
	});
		
	/*$('#tgl_tempo').datepicker({
		format: 'yyyy-mm-dd'
	});*/
</script>