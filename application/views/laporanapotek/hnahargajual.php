
            <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-edit"></i></div>
                                            <h5>LAPORAN MARGIN</h5>
                                            <!-- .toolbar -->
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
                                                    <li><a target="_blank" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>third-party/fpdf/laporanhnahargajual.php?periodeawal=<?php echo $periodeawal ?>&periodeakhir=<?php echo $periodeakhir; ?>&kd_unit_apt=<?php echo $kd_unit_apt; ?>&jenis_pasien=<?php echo $jenis_pasien; ?>&tipe=<?php echo $tipe; ?>"> <i class="icon-print"></i> Summary PDF</a></li>
                                                    <li><a target="_blank" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>index.php/transapotek/laporanapt/hnahargajualxls/<?php echo $periodeawal ?>/<?php echo $periodeakhir; ?>/<?php if(!empty($kd_unit_apt))echo $kd_unit_apt; else echo "NULL"; ?>/<?php if(!empty($tipe))echo $tipe;else echo 'NULL' ?>/<?php if(empty($jenis_pasien))echo $jenis_pasien;else echo 'NULL'; ?>" > <i class="icon-print"></i> Excel</a></li>
                                                    <li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.toolbar value="<-?php echo $periodeawal; ?>"-->
                                        </header>
                                        <div id="div-1" class="accordion-body collapse in body">
                                            <form class="form-horizontal" method="POST" action="<?php echo base_url() ?>index.php/transapotek/laporanapt/hnahargajual">
                                                <div class="row-fluid">
													<div class="span12">
														<div class="span5">
															<div class="control-group">
																<label for="periodeawal" class="control-label">Tgl. Penjualan</label>
																<div class="controls with-tooltip">
																	<input type="text" id="periodeawal" name="periodeawal" class="input-small input-tooltip" data-mask="99-99-9999"
																		   data-original-title="masukkan tanggal awal" value="<?php echo $periodeawal?>" data-placement="bottom"/>
																		   s/d
																	<input type="text" id="periodeakhir" name="periodeakhir" class="input-small input-tooltip" data-mask="99-99-9999"
																		   data-original-title="masukkan tanggal akhir" value="<?php echo $periodeakhir?>" data-placement="bottom"/>
																</div>
															</div> 
														</div>
													</div>
												</div>
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label for="kd_unit_apt" class="control-label">Unit Apotek</label>
                                                                <div class="controls with-tooltip">
                                                                	<select name="kd_unit_apt">
                                                                		<option value="">Semua Unit</option>
                                                                		<?php
                                                                		foreach ($dataunit as $unit) {
                                                                			# code...
                                                                			?>
                                                                			<option value="<?php echo $unit['kd_unit_apt'] ?>" <?php if($kd_unit_apt==$unit['kd_unit_apt']) echo "selected=selected"; ?> ><?php echo $unit['nama_unit_apt'] ?></option>
                                                                			<?php
                                                                		}
                                                                		?>
                                                                	</select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label for="jenis_pasien" class="control-label">Jenis Pasien</label>
                                                                <div class="controls with-tooltip">
                                                                    <select name="tipe" id="tipe" class="input-medium" tabindex="2">
                                                                        <option value="" <?php  if(isset($tipe) && $tipe==="") echo "selected=selected"; ?> >Pilih Tipe Pasien</option>
                                                                        <option value="0" <?php  if(isset($tipe) && $tipe==='0') echo "selected=selected"; ?> >Tunai</option>
                                                                        <option value="1" <?php  if(isset($tipe) && $tipe==1) echo "selected=selected"; ?>>Asuransi</option>
                                                                        <option value="2" <?php  if(isset($tipe) && $tipe==2) echo "selected=selected"; ?>>Perusahaan</option>
                                                                        <option value="3" <?php  if(isset($tipe) && $tipe==3) echo "selected=selected"; ?>>Sosial Dakwah</option>
                                                                        <option value="4" <?php  if(isset($tipe) && $tipe==4) echo "selected=selected"; ?>>BPJS</option>
                                                                    </select>
                                                                    <select name="jenis_pasien" id="jenis_pasien" class="input-medium">
                                                                        <?php
                                                                        foreach ($datacustomer as $key => $customer) {
                                                                            # code...
                                                                            if($jenis_pasien===$customer['cust_code']) $sel="selected=selected"; else $sel="";
                                                                        ?>
                                                                        <option value="<?php echo $customer['cust_code']; ?>" <?php echo $sel; ?> tipe="<?php if(!empty($customer)) echo $customer['type'] ?>"> <?php echo $customer['customer']; ?> </option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                        <option value="" tipe="11111" <?php  if(isset($tipe) && $tipe==="") echo "selected=selected"; ?> >Pilih Semua</option>
                                                                    </select>   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="text1" class="control-label">&nbsp;</label>
                                                    <div class="controls with-tooltip">
                                                        <button class="btn btn-primary" type="submit"><i class="icon-search"></i> Cari</button>
                                                        <button class="btn " type="submit" name="reset" value="reset"><i class="icon-undo"></i> Reset</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TEXT INPUT FIELD-->                            
                            <!--Begin Datatables-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-move"></i></div>
                                            <h5></h5>
                                        </header>
                                        <div id="collapse4" class="body">
                                            <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                                                <thead>
                                                    <tr style="font-size:90% !important;">
                                                        <th style="text-align:center;">No</th>
                                                        <th style="text-align:center;">Kode Obat</th>
                                                        <th style="text-align:center;">Nama Obat</th>
														<th style="text-align:center;">Jumlah</th>
                                                        <th style="text-align:center;">Harga Beli</th>
                                                        <th style="text-align:center;">Total Harga Beli</th>
                                                        <th style="text-align:center;">Total Harga Jual</th>
                                                        <th style="text-align:center;">Margin</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $no=1;
                                                    $totalqty=0;
                                                    $totalhargabeli=0;
                                                    $totalhargajual=0;
                                                    $totalmargin=0;
                                                    foreach ($items as $item) {
                                                    $totalqty=$totalqty+$item['qty'];
                                                    $totalhargabeli=$totalhargabeli+($item['qty']*$item['harga_beli']);
                                                    $totalhargajual=$totalhargajual+$item['total_harga_jual'];
                                                    $totalmargin=$totalmargin+($item['total_harga_jual']-($item['qty']*$item['harga_beli']));
                                                    ?>
                                                        <tr style="font-size:90% !important;">
                                                            <td style="text-align:center;"><?php echo $no."."; ?> </td>
                                                            <td style="text-align:center;"><?php echo $item['kd_obat'] ?> </td>
                                                            <td style="text-align:left;"><?php echo $item['nama_obat'] ?> </td>
                                                            <td style="text-align:center;"><?php echo number_format($item['qty'],0,',','.') ?> </td>
                                                            <td style="text-align:right;"><?php echo number_format($item['harga_beli'],2,',','.') ?> </td>
                                                            <td style="text-align:right;"><?php echo number_format($item['qty']*$item['harga_beli'],2,',','.') ?> </td>
                                                            <td style="text-align:right;"><?php echo number_format($item['total_harga_jual'],2,',','.') ?> </td>
                                                            <td style="text-align:right;"><?php echo number_format($item['total_harga_jual']-($item['qty']*$item['harga_beli']),2,',','.') ?> </td>
                                                        </tr>                                                    
                                                    <?php
                                                    $no++;
                                                    }
                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="3">T O T A L</td>
                                                        <td><?php echo number_format($totalqty,0,',','.') ?></td>
                                                        <td></td>
                                                        <td><?php echo number_format($totalhargabeli,0,',','.') ?> </td>
                                                        <td><?php echo number_format($totalhargajual,0,',','.') ?> </td>
                                                        <td><?php echo number_format($totalmargin,0,',','.') ?> </td>                                                    
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Datatables-->

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->




<script type="text/javascript">
$(document).ready(function(){
    $('#tipe').trigger('change');
});

$('#tipe').change(function(){
    //alert('xx');
    var val=$(this).val();
    //alert(val);
    //$('#cust_code option[tipe!='+val+']').attr('hidden','hidden');
    $('#jenis_pasien option[tipe!='+val+']').attr('hidden','hidden');
    $('#jenis_pasien option[tipe='+val+']').removeAttr('hidden');
    $('#jenis_pasien option[tipe='+val+']').show();
    $('#jenis_pasien option[tipe=11111]').show();
    <?php if(isset($jenis_pasien)){ ?>
    $('#jenis_pasien option[value=<?php echo $jenis_pasien; ?>]').prop('selected',true);
    <?php }else{ ?>
    $('#jenis_pasien option[tipe='+val+']').prop('selected',true);
    $('#jenis_pasien option[tipe=11111]').prop('selected',true);
    <?php } ?>
});


	$('#dataTable').dataTable({
		"aaSorting": [[ 0, "asc" ]],
		"sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "Show _MENU_ entries"
		}
	});
	
	$('.with-tooltip').tooltip({
		selector: ".input-tooltip"
	});
	
	$('#periodeawal').datepicker({
		format: 'dd-mm-yyyy'
	});
			
	$('#periodeakhir').datepicker({
		format: 'dd-mm-yyyy'
	});
	
	/*$('#tgl_tempo').datepicker({
		format: 'yyyy-mm-dd'
	});*/
</script>