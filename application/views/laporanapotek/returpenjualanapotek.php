
            <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-edit"></i></div>
                                            <h5>LAPORAN PENJUALAN OBAT / ALKES PER RESEP</h5>
                                            <!-- .toolbar -->
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
                                                    <li><a target="_blank" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>third-party/fpdf/laporanreturpenjualansummaryapotek.php?periodeawal=<?php echo $periodeawal ?>&periodeakhir=<?php echo $periodeakhir; ?>&shiftapt=<?php echo $shiftapt; ?>&kd_unit_apt=<?php echo $kd_unit_apt; ?>&resep=<?php echo $resep; ?>&jenis_pasien=<?php echo $jenis_pasien; ?>&dokter=<?php echo $dktr; ?>"> <i class="icon-print"></i> Summary PDF</a></li>
                                                    <li><a target="_blank" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>third-party/fpdf/laporanreturpenjualanapotek.php?periodeawal=<?php echo $periodeawal ?>&periodeakhir=<?php echo $periodeakhir; ?>&shiftapt=<?php echo $shiftapt; ?>&kd_unit_apt=<?php echo $kd_unit_apt; ?>&resep=<?php echo $resep; ?>&jenis_pasien=<?php echo $jenis_pasien; ?>&dokter=<?php echo $dktr; ?>"> <i class="icon-print"></i> Details PDF</a></li>
                                                    <li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.toolbar value="<-?php echo $periodeawal; ?>"-->
                                        </header>
                                        <div id="div-1" class="accordion-body collapse in body">
                                            <form class="form-horizontal" method="POST" action="<?php echo base_url() ?>index.php/transapotek/laporanapt/returpenjualanapotek">
                                                <div class="row-fluid">
													<div class="span12">
														<div class="span5">
															<div class="control-group">
																<label for="periodeawal" class="control-label">Tgl. Penjualan</label>
																<div class="controls with-tooltip">
																	<input type="text" id="periodeawal" name="periodeawal" class="input-small input-tooltip" data-mask="99-99-9999"
																		   data-original-title="masukkan tanggal awal" value="<?php echo $periodeawal?>" data-placement="bottom"/>
																		   s/d
																	<input type="text" id="periodeakhir" name="periodeakhir" class="input-small input-tooltip" data-mask="99-99-9999"
																		   data-original-title="masukkan tanggal akhir" value="<?php echo $periodeakhir?>" data-placement="bottom"/>
																</div>
															</div> 
														</div>
													</div>
												</div>
												<div class="row-fluid">												
                                                    <div class="span12">
                                                        <div class="span5">
                                                            <div class="control-group">
                                                                <label for="shiftapt" class="control-label">Shift</label>
                                                                <div class="controls with-tooltip">
                                                                    <select name="shiftapt" id="shiftapt" class="input-medium">
                                                                        <option value="">Semua Shift</option>
                                                                        <?php
                                                                        for ($i=1; $i <= $datashift['jml_shift']; $i++) {
                                                                            # code...
                                                                             if($i==$shiftapt)$sel="selected=selected";else $sel="";
                                                                        ?>
                                                                        <option value="<?php echo $i ?>"  <?php echo $sel; ?>><?php echo "Shift ".$i; ?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </select>																																		
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 	
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label for="kd_unit_apt" class="control-label">Unit Apotek</label>
                                                                <div class="controls with-tooltip">
                                                                    <input type="text" name="nama_unit_apt" id="nama_unit_apt" value="<?php if($unit=$this->mlaporanapt->ambilNamaUnit($this->session->userdata('kd_unit_apt'))) echo $unit; ?>" readonly class="span7 input-tooltip" data-original-title="nama unit" data-placement="bottom"/>
                                                                    <input type="hidden" name="kd_unit_apt" id="kd_unit_apt" value="<?php echo $this->session->userdata('kd_unit_apt'); ?>" readonly class="span2 input-tooltip" data-original-title="kd unit apt " data-placement="bottom"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label for="jenis_pasien" class="control-label">Jenis Pasien</label>
                                                                <div class="controls with-tooltip">
                                                                    <select name="jenis_pasien" id="jenis_pasien" class="input-medium">
                                                                        <option value="">Pilih Customer</option>
                                                                        <?php
                                                                        foreach ($datacustomer as $key => $customer) {
                                                                            # code...
                                                                            if($jenis_pasien==$customer['cust_code']) $sel="selected=selected"; else $sel="";
                                                                        ?>
                                                                        <option value="<?php echo $customer['cust_code']; ?>" <?php echo $sel; ?>> <?php echo $customer['customer']; ?> </option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </select>   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label for="dokter" class="control-label">Dokter</label>
                                                                <div class="controls with-tooltip">
                                                                    <select name="dokter" id="dokter" class="input-medium">
                                                                        <option value="">Pilih Dokter</option>
                                                                        <?php
                                                                        foreach ($datadokter as $key => $dokter) {
                                                                            # code...
                                                                            if($dktr==$dokter['kd_dokter']) {$sel="selected=selected";} else $sel="";
                                                                        ?>
                                                                        <option value="<?php echo $dokter['kd_dokter']; ?>" <?php echo $sel; ?>> <?php echo $dokter['nama_dokter']; ?> </option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </select>   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="text1" class="control-label">&nbsp;</label>
                                                    <div class="controls with-tooltip">
                                                        <button class="btn btn-primary" type="submit"><i class="icon-search"></i> Cari</button>
                                                        <button class="btn " type="submit" name="reset" value="reset"><i class="icon-undo"></i> Reset</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TEXT INPUT FIELD-->                            
                            <!--Begin Datatables-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-move"></i></div>
                                            <h5></h5>
                                        </header>
                                        <div id="collapse4" class="body">
                                            <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                                                <thead>
                                                    <tr style="font-size:90% !important;">
                                                        <th style="text-align:center;">No</th>
                                                        <th style="text-align:center;">Jns Pasien</th>
                                                        <th style="text-align:center;">No. Retur Penjualan</th>
                                                        <th style="text-align:center;">No. Penjualan</th>
														<th style="text-align:center;">Tgl. Retur Penjualan</th>
                                                        <th style="text-align:center;">Nama Pasien</th>
                                                        <th style="text-align:center;">Nama Dokter</th>
														<th style="text-align:center;">Alasan</th>
														<th style="text-align:center;">Jasa Racik</th>
														<!--th style="text-align:center;">Jasa Resep</th-->
														<th style="text-align:center;">Jml. Trans.</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $no=1;
                                                    foreach ($items as $item) {
														if($item['resep']=="1"){
															$resep="Ya";
														}else{
															$resep="Tidak";
														}
                                                    ?>
                                                        <tr style="font-size:90% !important;">
                                                            <td style="text-align:center;"><?php echo $no."."; ?></td>
                                                            <td style="text-align:center;"><?php echo $item['customer'] ?></td>
                                                            <td style="text-align:center;"><?php echo $item['no_retur_penjualan'] ?></td>
                                                            <td style="text-align:center;"><?php echo $item['no_penjualan'] ?></td>
                                                            <td style="text-align:center;"><?php echo $item['tgl_returpenjualan'] ?></td>
															<td><?php echo $item['nama_pasien'] ?>&nbsp;</td>
															<td><?php echo $item['nama_dokter'] ?>&nbsp;</td>
															<td style="text-align:center;"><?php echo $item['alasan'] ?></td>
															<td style="text-align:right;"><?php echo number_format($item['adm_racik']) ?></td>
															<!--td style="text-align:right;"><-?php echo number_format($item['adm_resep']) ?></td-->
															<td style="text-align:right;"><?php echo number_format($item['total_transaksi']) ?></td>
															<!--td style="text-align:right;"><-?php echo number_format($item['total_bayar']) ?></td-->
                                                        </tr>                                                    
                                                    <?php
                                                    $no++;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Datatables-->

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->




<script type="text/javascript">
	$('#dataTable').dataTable({
		"aaSorting": [[ 0, "asc" ]],
		"sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "Show _MENU_ entries"
		}
	});
	
	$('.with-tooltip').tooltip({
		selector: ".input-tooltip"
	});
	
	$('#periodeawal').datepicker({
		format: 'dd-mm-yyyy'
	});
			
	$('#periodeakhir').datepicker({
		format: 'dd-mm-yyyy'
	});
	
	/*$('#tgl_tempo').datepicker({
		format: 'yyyy-mm-dd'
	});*/
</script>