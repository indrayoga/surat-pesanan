<?php
		//debugvar($stok);

?>
            <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-edit"></i></div>
                                            <h5>LAPORAN PERSEDIAAN OBAT / ALKES</h5>
                                            <!-- .toolbar -->
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
                                                    <?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang')){ ?>
                                                        <li><a class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>index.php/transapotek/laporanapt/rl1excelpersediaanobat/<?php echo $stok; ?>/<?php if(empty($isistok))echo 0; else echo $isistok; ?>/<?php echo $this->session->userdata('kd_unit_apt'); ?>/<?php echo $kd_golongan; ?>"> <i class="icon-print"></i> EXCEL</a></li>
                                                    <?php } else { ?>
                                                        <li><a class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>index.php/transapotek/laporanapt/rl1excelpersediaanobat/<?php echo $stok; ?>/<?php if(empty($isistok))echo 0; else echo $isistok; ?>/<?php if(empty($kd_unit_apt)) echo 'NULL'; else echo $kd_unit_apt; ?>/<?php if(empty($kd_golongan)) echo 'NULL'; else echo $kd_golongan; ?>"> <i class="icon-print"></i> EXCEL</a></li>
                                                    <?php } ?> 

                                                    <?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang')){ ?>
                                                        <li><a target="_blank" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>third-party/fpdf/laporanpersediaanapotek.php?kd_golongan=<?php echo $kd_golongan; ?>&stok=<?php echo $stok; ?>&isistok=<?php echo $isistok; ?>&kd_unit_apt=<?php echo $this->session->userdata('kd_unit_apt'); ?>"> <i class="icon-print"></i> PDF</a></li>
                                                    <?php } else { ?>
                                                        <li><a target="_blank" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>third-party/fpdf/laporanpersediaanapotek.php?kd_golongan=<?php echo $kd_golongan; ?>&stok=<?php echo $stok; ?>&isistok=<?php echo $isistok; ?>&kd_unit_apt=<?php echo $kd_unit_apt; ?>"> <i class="icon-print"></i> PDF</a></li>
                                                    <?php } ?>                                                                                                      
													<!--li><a target="" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<-?php echo base_url() ?>index.php/transapotek/laporanapt/rl1excelpersediaanobat/<-?php if(empty($stok)) echo "1"; else echo $stok; ?>/<-?php if(empty($kd_jenis_obat)) echo "null"; else echo $kd_jenis_obat; ?>/<-?php if(empty($isistok)) echo "null"; else echo $isistok; ?>/<-?php if(empty($kd_sub_jenis)) echo "null"; else echo $kd_sub_jenis; ?>/<-?php if(empty($kd_unit_apt)) echo "null"; else echo $kd_unit_apt; ?>/<-?php if(empty($kd_golongan )) echo "null"; else echo $kd_golongan; ?>"> <i class="icon-print"></i> Export to Excel</a></li-->
													<li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.toolbar value="<-?php echo $periodeawal; ?>"-->
                                        </header>
                                        <div id="div-1" class="accordion-body collapse in body">
                                            <form class="form-horizontal" method="POST" action="<?php echo base_url() ?>index.php/transapotek/laporanapt/persediaanapotek">
                                                <!--div class="row-fluid">
													<div class="span12">
														<div class="span5">
															<div class="control-group">
																<label for="kd_jenis_obat" class="control-label">Jenis</label>
                                                                <div class="controls with-tooltip">
                                                                    <select id="kd_jenis_obat" name="kd_jenis_obat" class="input-large">
                                                                        <option value="">--pilih--</option>
                                                                        <-?php
                                                                        foreach ($jenisobat as $jenis) {
                                                                            # code...
																			if ($jenis['kd_jenis_obat']== $kd_jenis_obat){
																				$cek = "selected=selected";
																			}
																			else {
																				$cek = "";
																			}
                                                                        ?>
                                                                        <option value="<-?php echo $jenis['kd_jenis_obat'] ?>" <-?php echo $cek; ?>><-?php echo $jenis['jenis_obat'] ?></option>
                                                                        <-?php
                                                                        }
                                                                        ?>
                                                                    </select>																	
                                                                </div>
															</div> 
														</div>
													</div>
												</div-->
												<!--div class="row-fluid">
													<div class="span12">
														<div class="span5">
															<div class="control-group">
																<label for="kd_sub_jenis" class="control-label">Sub Jenis</label>
                                                                <div class="controls with-tooltip">
                                                                    <select id="kd_sub_jenis" name="kd_sub_jenis" class="input-large">
                                                                        <option value="">Semua</option>
                                                                        <-?php
                                                                        foreach ($subjenis as $sub) {
                                                                            # code...
																			if ($sub['kd_sub_jenis']== $kd_sub_jenis){
																				$cek = "selected=selected";
																			}
																			else {
																				$cek = "";
																			}
                                                                        ?>
                                                                        <option value="<-?php echo $sub['kd_sub_jenis'] ?>" <-?php echo $cek; ?>><-?php echo $sub['sub_jenis'] ?></option>
                                                                        <-?php
                                                                        }
                                                                        ?>
                                                                    </select>																	
                                                                </div>
															</div>
														</div>														
													</div>
												</div-->
												<div class="row-fluid">												
                                                    <div class="span12">
                                                        <div class="span5">
                                                            <div class="control-group">
                                                                <label for="kd_golongan" class="control-label">Golongan</label>
                                                                <div class="controls with-tooltip">
                                                                    <select id="kd_golongan" name="kd_golongan" class="input-large">
                                                                        <option value="">Semua</option>
                                                                        <?php
                                                                        foreach ($golongan as $gol) {
                                                                            # code...
																			if ($gol['kd_golongan']== $kd_golongan){
																				$cek = "selected=selected";
																			}
																			else {
																				$cek = "";
																			}
                                                                        ?>
                                                                        <option value="<?php echo $gol['kd_golongan'] ?>" <?php echo $cek; ?>><?php echo $gol['golongan'] ?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>														
                                                    </div>
                                                </div>
												<div class="row-fluid">												
													<div class="span12">
														<div class="span5">
															<div class="control-group">
																<label for="stok" class="control-label">Stok</label>
                                                                <div class="controls with-tooltip">																
                                                                    <select name="stok" id="stok" class="input-small">
																		<option value="1" <?php if($stok=='1') echo "selected=selected"; ?>> > </option>
                                                                        <option value="2" <?php if($stok=='2') echo "selected=selected"; ?>> < </option>
                                                                        <option value="3" <?php if($stok=='3') echo "selected=selected"; ?>> >= </option>
																		<option value="4" <?php if($stok=='4') echo "selected=selected"; ?>> <= </option>
																		<option value="5" <?php if($stok=='5') echo "selected=selected"; ?>> = </option>																		
                                                                    </select>
																	<input type="text" name="isistok" id="isistok" class="input-small input-tooltip cleared" data-original-title="isistok" value="<?php echo $isistok?>" data-placement="bottom"/>
                                                                    <span class="help-inline"></span>
                                                                </div>
                                                            </div>
														</div>
													</div>
												</div>
												<div class="row-fluid">
													<div class="span12">
														<div class="span5">
															<div class="control-group">
                                                                <label for="kd_unit_apt" class="control-label">Unit Apotek</label>
                                                                <div class="controls with-tooltip">
																	<?php if($this->session->userdata('kd_unit_apt')==$this->session->userdata('kd_unit_apt_gudang')) { ?>
																		<select id="kd_unit_apt" name="kd_unit_apt" class="input-large">
																			<option value="">--pilih--</option>
																			<?php
																			foreach ($unitapotek as $pab) {
																				# code...
																				if ($pab['kd_unit_apt']== $kd_unit_apt){
																					$cek = "selected=selected";
																				}
																				else {
																					$cek = "";
																				}
																			?>
																			<option value="<?php echo $pab['kd_unit_apt'] ?>" <?php echo $cek; ?>><?php echo $pab['nama_unit_apt'] ?></option>
																			<?php
																			}
																			?>
																		</select>
																	<?php } else { ?>
																		<input type="text" name="nama_unit_apt" id="nama_unit_apt" value="<?php if($unit=$this->mlaporanapt->ambilNamaUnit($this->session->userdata('kd_unit_apt'))) echo $unit; ?>" readonly class="span9 input-tooltip" data-original-title="nama unit" data-placement="bottom"/>
																		<input type="hidden" name="kd_unit_apt" id="kd_unit_apt" value="<?php echo $this->session->userdata('kd_unit_apt'); ?>" readonly class="span2 input-tooltip" data-original-title="kd unit apt " data-placement="bottom"/>
																	<?php } ?>
                                                                </div>
															</div>
														</div>
													</div>
												</div>
                                                <div class="control-group">
                                                    <label for="text1" class="control-label">&nbsp;</label>
                                                    <div class="controls with-tooltip">
                                                        <button class="btn btn-primary" type="submit"><i class="icon-search"></i> Cari</button>
                                                        <button class="btn " type="submit" name="reset" value="reset"><i class="icon-undo"></i> Reset</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TEXT INPUT FIELD-->                            
                            <!--Begin Datatables-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-move"></i></div>
                                            <h5></h5>
                                        </header>
                                        <div id="collapse4" class="body">
                                            <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                                                <thead>
                                                    <tr style="font-size:95% !important;">
                                                        <th style="text-align:center;">No</th>
                                                        <th style="text-align:center;">Kode</th>
                                                        <th style="text-align:center;">Nama Obat</th>
														<th style="text-align:center;">Satuan</th>
														<th style="text-align:center;">Stok</th>
														<th style="text-align:center;">HNA + PPN</th>
														<th style="text-align:center;">NILAI</th>
														<!--th>Jumlah</th-->														
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $no=1; $total=0;
                                                    foreach ($items as $item) {
                                                        $ppn=(10/100)*$item['harga_pokok'];
                                                        $hnappn=$item['harga_pokok']+$ppn;
                                                        //$nilai=$item['jml_stok']*$item['harga_pokok'];
                                                        $nilai=$item['jml_stok']*$item['harga_beli'];
                                                    ?>
                                                        <tr style="font-size:95% !important;">
                                                            <td style="text-align:center;"><?php echo $no; ?></td>
                                                            <td><?php echo $item['kd_obat'] ?></td>
                                                            <td><?php echo $item['nama_obat'] ?></td>
															<td style="text-align:center;"><?php echo $item['satuan_kecil'] ?></td>
															<td style="text-align:center;"><?php echo number_format($item['jml_stok'],1,'.',',') ?></td>
															<td style="text-align:right;"><?php echo number_format($item['harga_beli'],1,'.',',') ?></td>
															<td style="text-align:right;"><?php echo number_format($nilai,1,'.',',') ?></td>
                                                        </tr>                                                    
                                                    <?php
                                                    $no++;
													$total=$total+$nilai;
                                                    }
                                                    ?>
                                                </tbody>
												<tfoot>
													<tr>
														<th colspan="11" style="text-align:right;" class="header">Total Persediaan (Rp) : <input style="text-align:right;width:200px;font-size:95% !important;" type="text" class="input-medium cleared" id="totalpersediaan" value="<?php  echo number_format($total,1,'.',',');?>" disabled></th>
													</tr>
												</tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Datatables-->

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->




<script type="text/javascript">
	$('#dataTable').dataTable({
		"aaSorting": [[ 0, "asc" ]],
		"sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "Show _MENU_ entries"
		}
	});
	
	$('.with-tooltip').tooltip({
		selector: ".input-tooltip"
	});
</script>