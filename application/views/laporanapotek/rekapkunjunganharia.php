
            <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-edit"></i></div>
                                            <h5>LAPORAN REKAP KUNJUNGAN HARIAN </h5>
                                            <!-- .toolbar -->
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
                                                    <li><a target="" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>index.php/transapotek/laporanapt/rl1excelkunjunganharian/<?php echo $tahun ?>/<?php echo $bulan; ?>/<?php echo $kd_unit_apt; ?>"> <i class="icon-print"></i> Export to Excel</a></li>
													<li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.toolbar value="<-?php echo $periodeawal; ?>"-->
                                        </header>
                                        <div id="div-1" class="accordion-body collapse in body">
                                            <form class="form-horizontal" method="POST" action="<?php echo base_url() ?>index.php/transapotek/laporanapt/rekapkunjunganharian">
                                                <div class="row-fluid">
													<div class="span12">
														<div class="span6">
															<div class="control-group">
																<label for="periodeawal" class="control-label">Periode</label>
																<div class="controls with-tooltip">
                                                                    <select name="bulan" id="bulan">
                                                                        <option value='01' <?php if($bulan=="01")echo "selected=selected"; ?>>Januari</option>
                                                                        <option value='02' <?php if($bulan=="02")echo "selected=selected"; ?>>Februari</option>
                                                                        <option value='03' <?php if($bulan=="03")echo "selected=selected"; ?>>Maret</option>
                                                                        <option value='04' <?php if($bulan=="04")echo "selected=selected"; ?>>April</option>
                                                                        <option value='05' <?php if($bulan=="05")echo "selected=selected"; ?>>Mei</option>
                                                                        <option value='06' <?php if($bulan=="06")echo "selected=selected"; ?>>Juni</option>
                                                                        <option value='07' <?php if($bulan=="07")echo "selected=selected"; ?>>Juli</option>
                                                                        <option value='08' <?php if($bulan=="08")echo "selected=selected"; ?>>Agustus</option>
                                                                        <option value='09' <?php if($bulan=="09")echo "selected=selected"; ?>>September</option>
                                                                        <option value='10' <?php if($bulan=="10")echo "selected=selected"; ?>>Oktober</option>
                                                                        <option value='11' <?php if($bulan=="11")echo "selected=selected"; ?>>November</option>
                                                                        <option value='12' <?php if($bulan=="12")echo "selected=selected"; ?>>Desember</option>
                                                                    </select>
																	<input type="text" id="tahun" name="tahun" class="input-small input-tooltip" data-mask="9999"
																		   value="<?php echo $tahun; ?>" data-original-title="masukkan tahun" value="<?php echo $tahun; ?>" data-placement="bottom"/>
																</div>
															</div> 
														</div>
													</div>
												</div>
												<div class="row-fluid">
													<div class="span12">														
														<div class="span6">
                                                            <div class="control-group">
																<label for="kd_unit_apt" class="control-label">Unit Apotek</label>
																<div class="controls with-tooltip">
																	<input type="text" name="nama_unit_apt" id="nama_unit_apt" value="<?php if($unit=$this->mlaporanapt->ambilNamaUnit($this->session->userdata('kd_unit_apt'))) echo $unit; ?>" readonly class="span7 input-tooltip" data-original-title="nama unit" data-placement="bottom"/>
																	<input type="hidden" name="kd_unit_apt" id="kd_unit_apt" value="<?php echo $this->session->userdata('kd_unit_apt'); ?>" readonly class="span2 input-tooltip" data-original-title="kd unit apt " data-placement="bottom"/>
																</div>
															</div>
                                                        </div>
													</div>
												</div>
                                                <div class="control-group">
                                                    <label for="text1" class="control-label">&nbsp;</label>
                                                    <div class="controls with-tooltip">
                                                        <button class="btn btn-primary" type="submit"><i class="icon-search"></i> Cari</button>
                                                        <button class="btn " type="submit" name="reset" value="reset"><i class="icon-undo"></i> Reset</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TEXT INPUT FIELD-->                            
                            <!--Begin Datatables-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-move"></i></div>
                                            <h5></h5>
                                        </header>
                                        <div id="collapse4" class="body">
                                            <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                                                <thead>
                                                    <tr style="font-size:90% !important;" >
                                                        <th style="text-align:center;" rowspan="2">Bulan</th>
                                                        <th style="text-align:center;" rowspan="2">Tahun</th>
                                                        <th style="text-align:center;">Jenis Pasien</th>
                                                        <?php
                                                        $a_date = $tahun."-".$bulan."-01";
                                                        $date = new DateTime($a_date);
                                                        $date->modify('last day of this month');
                                                        $lastday=$date->format('d');                                                        
                                                        for ($x=1; $x<=$lastday;$x++) {
                                                            # code...
                                                            ?>
                                                            <th colspan="3"><?php echo $x; ?></th>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tr>
                                                    <tr>
                                                        <td>Shift</td>
                                                        <?php
                                                        $a_date = $tahun."-".$bulan."-01";
                                                        $date = new DateTime($a_date);
                                                        $date->modify('last day of this month');
                                                        $lastday=$date->format('d');                                                        
                                                        for ($x=1; $x<=$lastday;$x++) {
                                                            # code...
                                                            ?>
                                                            <th>1</th>
                                                            <th>2</th>
                                                            <th>3</th>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                    debugvar($items);
                                                foreach ($items as $item) {
                                                ?>
                                                <tr>
                                                <td><?php echo $item['bulan'] ?></td>
                                                <td><?php echo $item['tahun'] ?></td>
                                                <td><?php echo $item['customer'] ?></td>
                                                <?php
                                                for ($y=1; $y<=$lastday;$y++) {
                                                    # code...
                                                    ?>
                                                    <td><?php echo $item[$y.'_1']; ?></td>
                                                    <td><?php echo $item[$y.'_2']; ?></td>
                                                    <td><?php echo $item[$y.'_3']; ?></td>
                                                    <?php
                                                }
                                                ?>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Datatables-->

                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-move"></i></div>
                                            <h5></h5>
                                        </header>
                                        <div id="collapse4" class="body">
                                            <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                                                <thead>
                                                    <tr style="font-size:90% !important;" >
                                                        <th style="text-align:center;">Bulan</th>
                                                        <th style="text-align:center;">Tahun</th>
                                                        <th style="text-align:center;">Jenis Pasien</th>
                                                        <?php
                                                        $a_date = $tahun."-".$bulan."-01";
                                                        $date = new DateTime($a_date);
                                                        $date->modify('last day of this month');
                                                        $lastday=$date->format('d');                                                        
                                                        for ($x=1; $x<=$lastday;$x++) {
                                                            # code...
                                                            ?>
                                                            <th><?php echo $x; ?></th>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                   // debugvar($items);
                                                foreach ($items2 as $item2) {
                                                ?>
                                                <tr>
                                                <td><?php echo $item2['bulan'] ?></td>
                                                <td><?php echo $item2['tahun'] ?></td>
                                                <td><?php echo $item2['customer'] ?></td>
                                                <?php
                                                for ($y=1; $y<=$lastday;$y++) {
                                                    # code...
                                                    ?>
                                                    <td><?php echo $item2[$y]; ?></td>
                                                    <?php
                                                }
                                                ?>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->




<script type="text/javascript">
	
	$('.with-tooltip').tooltip({
		selector: ".input-tooltip"
	});
	
	$('#periodeawal').datepicker({
		format: 'dd-mm-yyyy'
	});
			
	$('#periodeakhir').datepicker({
		format: 'dd-mm-yyyy'
	});
	
</script>