<style type="text/css">
.fixed {
    position:fixed;
    top:0px !important;
    z-index:100;
    width: 1024px;    
}
.body1{
    opacity: 0.4;
    background-color: #000000;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$('#form').ajaxForm({
            beforeSubmit: function(a,f,o) {
                o.dataType = "json";
                $('div.error').removeClass('error');
                $('span.help-inline').html('');
                $('#progress').show();
                $('body').append('<div id="overlay1" style="position: fixed;height: 100%;width: 100%;z-index: 1000000;"></div>');
                $('body').addClass('body1');
                z=true;

                if(z==0)return false;
            },
            dataType:  'json',
            success: function(data) {
            //alert(data);
            if (typeof data == 'object' && data.nodeType)
            data = elementToString(data.documentElement, true);
            else if (typeof data == 'object')
            //data = objToString(data);
                if(parseInt(data.status)==1) //jika berhasil
                {
                    //apa yang terjadi jika berhasil
                    $('#progress').hide();
                    $('#overlay1').remove();
                    $('body').removeClass('body1');
                    $('#error').html('<div class="alert alert-success fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>Tutup Bulan Selesai</div>');

                    /*if(parseInt(data.cetak)>0){
                        //window.location.href='<?php echo base_url() ?>third-party/fpdf/penerimaanpenyetoran.php?tahun='+data.tahun+'&bulan='+data.bulan+'&status='+data.statuslap+'';
                        window.open('<?php echo base_url() ?>third-party/fpdf/mutasiobat.php?kd_unit_apt='+data.kd_unit_apt+'&bulan='+data.bulan+'&tahun='+data.tahun+'','_newtab');
                    } */                   
                }
                else if(parseInt(data.status)==0) //jika gagal
                {
                    //apa yang terjadi jika gagal
                    $('#progress').hide();
                    $('#overlay1').remove();
                    $('body').removeClass('body1');
                    $('#error').show();
                    $('#error').html('<div class="alert alert-success fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesan+'</div>');
                }

            }
        });
	});
</script>
			<div id="error"></div>
            <div id="overlay"></div>
            <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-edit"></i></div>
                                            <h5>PROSES MUTASI OBAT / ALKES</h5>
                                            <!-- .toolbar -->
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
                                                    <!--
                                                    <li><a class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>third-party/fpdf/bukukasumumpenerimaan.php?kd_unit_apt=<?php echo $kd_unit_apt ?>&bulan=<?php echo $bulan; ?>&tahun=<?php echo $tahun; ?>"> <i class="icon-print"></i> PDF</a></li>
                                                    -->
													<!--li><a target="" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>index.php/transapotek/laporanapt/exceltutupbuku/<?php echo $kd_unit_apt=$this->session->userdata('kd_unit_apt'); ?>/<?php echo $bulan; ?>/<?php echo $tahun; ?>"> <i class="icon-print"></i> Export to Excel</a></li-->
                                                    <li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.toolbar value="<-?php echo $periodeawal; ?>"-->
                                        </header>
                                        <div id="div-1" class="accordion-body collapse in body">
                                            <form id="form" class="form-horizontal" method="POST" action="<?php echo base_url() ?>index.php/transapotek/laporanapt/carimutasiobat2">                                                
												<div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="span7">
                                                            <div class="control-group">
                                                                <label for="bulan" class="control-label">Periode</label>
                                                                <div class="controls with-tooltip">
                                                                    <select name="bulan" id="bulan" class="input-medium">
                                                                        <option value="01" <?php if($bulan=='01') echo "selected=selected"; ?>>Januari</option>
                                                                        <option value="02" <?php if($bulan=='02') echo "selected=selected"; ?>>Februari</option>
                                                                        <option value="03" <?php if($bulan=='03') echo "selected=selected"; ?>>Maret</option>
                                                                        <option value="04" <?php if($bulan=='04') echo "selected=selected"; ?>>April</option>
                                                                        <option value="05" <?php if($bulan=='05') echo "selected=selected"; ?>>Mei</option>
                                                                        <option value="06" <?php if($bulan=='06') echo "selected=selected"; ?>>Juni</option>
                                                                        <option value="07" <?php if($bulan=='07') echo "selected=selected"; ?>>Juli</option>
                                                                        <option value="08" <?php if($bulan=='08') echo "selected=selected"; ?>>Agustus</option>
                                                                        <option value="09" <?php if($bulan=='09') echo "selected=selected"; ?>>September</option>
                                                                        <option value="10" <?php if($bulan=='10') echo "selected=selected"; ?>>Oktober</option>
                                                                        <option value="11" <?php if($bulan=='11') echo "selected=selected"; ?>>November</option>
                                                                        <option value="12" <?php if($bulan=='12') echo "selected=selected"; ?>>Desember</option>
                                                                    </select>
                                                                    <select name="tahun" id="tahun" class="input-small">
                                                                        <option value="<?php echo date('Y'); ?>" <?php if($tahun==date('Y')) echo "selected=selected"; ?>><?php echo date('Y'); ?></option>
                                                                        <option value="<?php echo date('Y')-1; ?>" <?php if($tahun==date('Y')-1) echo "selected=selected"; ?>><?php echo date('Y')-1; ?></option>
                                                                        <option value="<?php echo date('Y')-1; ?>" <?php if($tahun==date('Y')-2) echo "selected=selected"; ?>><?php echo date('Y')-2; ?></option>
                                                                        <option value="<?php echo date('Y')-1; ?>" <?php if($tahun==date('Y')-3) echo "selected=selected"; ?>><?php echo date('Y')-3; ?></option>
                                                                        <option value="<?php echo date('Y')-1; ?>" <?php if($tahun==date('Y')-4) echo "selected=selected"; ?>><?php echo date('Y')-4; ?></option>
                                                                        <option value="<?php echo date('Y')-1; ?>" <?php if($tahun==date('Y')-5) echo "selected=selected"; ?>><?php echo date('Y')-5; ?></option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>														
                                                    </div>	
														<div id="progress" style="display:none;"></div>
                                                </div>
                                                <div class="control-group">
                                                    <label for="text1" class="control-label">&nbsp;</label>
                                                    <div class="controls with-tooltip">
                                                        <button class="btn btn-primary" type="submit" name="submit" value="cari"><i class="icon-search"></i> Mutasi Obat</button>
                                                        <!--button class="btn" type="reset"><i class="icon-undo"></i> Reset</button-->
                                                        <!--button class="btn " type="submit" name="submit1" value="cetak"><i class="icon-print"></i> PDF</button-->
														
                                                    </div>
                                                </div>
                                            </form>
                                                    <table id="dataTable" class="table table-bordered responsive" style="position:relative;width:1200;">
                                                        <thead>
                                                            <tr style="font-size:80% !important;">
                                                                <th style="text-align:center;vertical-align:middle;">Tahun</th>                             
                                                                <th style="text-align:center;vertical-align:middle;">1</th>
                                                                <th style="text-align:center;vertical-align:middle;">2</th>
                                                                <th style="text-align:center;vertical-align:middle;">3</th>
                                                                <th style="text-align:center;vertical-align:middle;">4</th>
                                                                <th style="text-align:center;vertical-align:middle;">5</th>
                                                                <th style="text-align:center;vertical-align:middle;">6</th>
                                                                <th style="text-align:center;vertical-align:middle;">7</th>
                                                                <th style="text-align:center;vertical-align:middle;">8</th>
                                                                <th style="text-align:center;vertical-align:middle;">9</th>
                                                                <th style="text-align:center;vertical-align:middle;">10</th>
                                                                <th style="text-align:center;vertical-align:middle;">11</th>
                                                                <th style="text-align:center;vertical-align:middle;">12</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="listmutasi">
                                                            <?php
                                                            $no=1;
                                                            foreach ($items as $item) {
                                                            ?>
                                                                <tr style="font-size:80% !important;">
                                                                    <td style="width:25px !important;"><?php echo $item['tahun']; ?></td> 
                                                                    <td style="width:25px !important;"><?php if(!empty($item['1']))echo 'X'; ?></td> 
                                                                    <td style="width:25px !important;"><?php if(!empty($item['2']))echo 'X'; ?></td> 
                                                                    <td style="width:25px !important;"><?php if(!empty($item['3']))echo 'X'; ?></td> 
                                                                    <td style="width:25px !important;"><?php if(!empty( $item['4']))echo 'X'; ?></td> 
                                                                    <td style="width:25px !important;"><?php if(!empty($item['5']))echo 'X'; ?></td> 
                                                                    <td style="width:25px !important;"><?php if(!empty($item['6']))echo 'X'; ?></td> 
                                                                    <td style="width:25px !important;"><?php if(!empty($item['7']))echo 'X'; ?></td> 
                                                                    <td style="width:25px !important;"><?php if(!empty($item['8']))echo 'X'; ?></td> 
                                                                    <td style="width:25px !important;"><?php if(!empty($item['9']))echo 'X'; ?></td> 
                                                                    <td style="width:25px !important;"><?php if(!empty($item['10']))echo 'X'; ?></td> 
                                                                    <td style="width:25px !important;"><?php if(!empty($item['11']))echo 'X'; ?></td> 
                                                                    <td style="width:25px !important;"><?php if(!empty($item['12']))echo 'X'; ?></td> 
                                                                </tr>                                                    
                                                            <?php

                                                            $no++;
                                                            }                                                           
                                                            ?>
                                                        </tbody>

                                        </div>
                                    </div>                                    
                                </div>
								<div id="progress" style="display:none;"></div>
                            <!--Begin Datatables-->
                            <!--End Datatables-->                                
                            </div>
                            <!--END TEXT INPUT FIELD-->                            
                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->

			
<script type="text/javascript">
	$('.with-tooltip').tooltip({
		selector: ".input-tooltip"
	});
	
	
	
	var opts = {
      lines: 9, // The number of lines to draw
      length: 40, // The length of each line
      width: 9, // The line thickness
      radius: 0, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#000', // #rgb or #rrggbb
      speed: 1.4, // Rounds per second
      trail: 54, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: 'auto', // Top position relative to parent in px
      left: '470px' // Left position relative to parent in px
    };
    var target = document.getElementById('progress');
    var spinner = new Spinner(opts).spin(target); 
	
</script>