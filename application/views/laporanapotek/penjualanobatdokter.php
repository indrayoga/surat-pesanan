
            <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-edit"></i></div>
                                            <h5>LAPORAN PERESEPAN OBAT OLEH DOKTER </h5>
                                            <!-- .toolbar -->
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
                                                    <li><a target="" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>index.php/transapotek/laporanapt/rl1excelpenjualanobatbydokter/<?php echo $periodeawal ?>/<?php echo $periodeakhir; ?>/<?php echo $kd_unit_apt; ?>/<?php echo $kd_dokter; ?>/<?php echo $kd_pabrik; ?>"> <i class="icon-print"></i> Export to Excel</a></li>
                                                    <li><a target="_blank" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>third-party/fpdf/laporanpenjualanobatdokter.php?periodeawal=<?php echo $periodeawal ?>&periodeakhir=<?php echo $periodeakhir; ?>&kd_unit_apt=<?php echo $kd_unit_apt; ?>&kd_dokter=<?php echo $kd_dokter; ?>&kd_pabrik=<?php echo $kd_pabrik; ?>"> <i class="icon-print"></i> PDF</a></li>
													<li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.toolbar value="<-?php echo $periodeawal; ?>"-->
                                        </header>
                                        <div id="div-1" class="accordion-body collapse in body">
                                            <form class="form-horizontal" method="POST" action="<?php echo base_url() ?>index.php/transapotek/laporanapt/penjualanobatdokter">
                                                <div class="row-fluid">
													<div class="span12">
														<div class="span6">
															<div class="control-group">
																<label for="periodeawal" class="control-label">Tgl. Penjualan</label>
																<div class="controls with-tooltip">
																	<input type="text" id="periodeawal" name="periodeawal" class="input-small input-tooltip" data-mask="99-99-9999"
																		   value="<?php echo $periodeawal ?>" data-original-title="masukkan tanggal awal" value="<?php echo $periodeawal?>" data-placement="bottom"/>
																		   s/d
																	<input type="text" id="periodeakhir" name="periodeakhir" class="input-small input-tooltip" data-mask="99-99-9999"
																		   value="<?php echo $periodeakhir ?>" data-original-title="masukkan tanggal akhir" value="<?php echo $periodeakhir?>" data-placement="bottom"/>
																</div>
															</div> 
														</div>
													</div>
												</div>
												<div class="row-fluid">
													<div class="span12">														
														<div class="span6">
                                                            <div class="control-group">
																<label for="kd_unit_apt" class="control-label">Unit Apotek</label>
																<div class="controls with-tooltip">
																	<input type="text" name="nama_unit_apt" id="nama_unit_apt" value="<?php if($unit=$this->mlaporanapt->ambilNamaUnit($this->session->userdata('kd_unit_apt'))) echo $unit; ?>" readonly class="span7 input-tooltip" data-original-title="nama unit" data-placement="bottom"/>
																	<input type="hidden" name="kd_unit_apt" id="kd_unit_apt" value="<?php echo $this->session->userdata('kd_unit_apt'); ?>" readonly class="span2 input-tooltip" data-original-title="kd unit apt " data-placement="bottom"/>
																</div>
															</div>
                                                        </div>
													</div>
												</div>
												<div class="row-fluid">												
                                                    <div class="span12">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label for="kd_dokter" class="control-label">Dokter</label>
                                                                <div class="controls with-tooltip">
                                                                    <select name="kd_dokter" id="kd_dokter" class="input-medium">
                                                                        <option value="">Semua Dokter</option>
                                                                        <?php
                                                                        foreach ($datadokter as $dokter) {
                                                                        	# code...
                                                                        	if($dokter['kd_dokter']==$kd_dokter)$sel="selected=selected"; else $sel="";
                                                                        ?>
                                                                        <option value="<?php echo $dokter['kd_dokter'] ?>" <?php echo $sel; ?>><?php echo $dokter['nama_dokter'] ?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </select>                                                                                                                                       
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                                																							
                                                <div class="row-fluid">                                             
                                                    <div class="span12">
                                                        <div class="span6">
                                                            <div class="control-group">
                                                                <label for="kd_pabrik" class="control-label">Pabrik</label>
                                                                <div class="controls with-tooltip">
                                                                    <select name="kd_pabrik" id="kd_pabrik" class="input-medium">
                                                                        <option value="">Semua Pabrik</option>
                                                                        <?php
                                                                        foreach ($datapabrik as $pabrik) {
                                                                            # code...
                                                                            if($pabrik['kd_pabrik']==$kd_pabrik)$sel="selected=selected"; else $sel="";
                                                                        ?>
                                                                        <option value="<?php echo $pabrik['kd_pabrik'] ?>" <?php echo $sel; ?>><?php echo $pabrik['nama_pabrik'] ?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </select>                                                                                                                                       
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                                                                                                                          
                                                <div class="control-group">
                                                    <label for="text1" class="control-label">&nbsp;</label>
                                                    <div class="controls with-tooltip">
                                                        <button class="btn btn-primary" type="submit"><i class="icon-search"></i> Cari</button>
                                                        <button class="btn " type="submit" name="reset" value="reset"><i class="icon-undo"></i> Reset</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TEXT INPUT FIELD-->                            
                            <!--Begin Datatables-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-move"></i></div>
                                            <h5></h5>
                                        </header>
                                        <div id="collapse4" class="body">
                                            <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                                                <thead>
                                                    <tr style="font-size:90% !important;" >
                                                        <th style="text-align:center;">No</th>
                                                        <th style="text-align:center;">Nama Dokter</th>
                                                        <th style="text-align:center;">Kode Obat</th>
                                                        <th style="text-align:center;">Nama Obat</th>
                                                        <th style="text-align:center;">Pabrik</th>
                                                        <th style="text-align:center;">Total Qty</th>
                                                        <th style="text-align:center;">Total Harga</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $no=1;
                                                    $total=0;
                                                    foreach ($items as $item) {
                                                    ?>
                                                        <tr style="font-size:80% !important;">
                                                            <td style="text-align:center;"><?php echo $no; ?>&nbsp;</td>
                                                            <td style="text-align:left;"><?php echo $item['nama_dokter'] ?>&nbsp;</td>
                                                            <td><?php echo $item['kd_obat'] ?>&nbsp;</td>
                                                            <td><?php echo $item['nama_obat'] ?>&nbsp;</td>
                                                            <td><?php echo $item['nama_pabrik'] ?>&nbsp;</td>
                                                            <td style="text-align:right;">&nbsp;
                                                            <?php 
                                                                if($item['total']!=0)echo number_format($item['total'],0,',','.') 
                                                            ?>
                                                            </td>
                                                            <td style="text-align:right;">&nbsp;
                                                            <?php 
                                                                if($item['total_harga']!=0)echo number_format($item['total_harga'],2,',','.') 
                                                            ?>
                                                            </td>
                                                        </tr>                                                    
                                                    <?php
                                                    $total=$total + $item['total_harga'];
                                                    $no++;
                                                    }
                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="12" style="text-align:right;" class="header">Total Penjualan (Rp) : <input style="text-align:right;width:130px;font-size:95% !important;" type="text" class="input-medium cleared" id="totalpenjualan" value="<?php  echo number_format($total,2,',','.');?>" disabled></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Datatables-->

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->




<script type="text/javascript">
	$('#dataTable').dataTable({
		"bSort" : false,
		"sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "Show _MENU_ entries"
		}
	});
	
	$('.with-tooltip').tooltip({
		selector: ".input-tooltip"
	});
	
	$('#periodeawal').datepicker({
		format: 'dd-mm-yyyy'
	});
			
	$('#periodeakhir').datepicker({
		format: 'dd-mm-yyyy'
	});
	
</script>