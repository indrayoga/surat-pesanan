<script type="text/javascript">
$(document).ready(function() {
    $('#sum').click(function(){
        var summary=$('#summary').val();
        var periodeawal=$('#periodeawal').val();
        var periodeakhir=$('#periodeakhir').val();
        window.open(
        '<?php echo base_url() ?>/third-party/fpdf/laporansummarypenerimaanapotek.php?summary='+summary+'&periodeawal='+periodeawal+'&periodeakhir='+periodeakhir,
        '_blank' // <- This is what makes it open in a new window.
        );
    })

    $('#detail').click(function(){
        var summary=$('#summary').val();
        var periodeawal=$('#periodeawal').val();
        var periodeakhir=$('#periodeakhir').val();
        window.open(
        '<?php echo base_url() ?>/third-party/fpdf/laporandetailpenerimaanapotek.php?summary='+summary+'&periodeawal='+periodeawal+'&periodeakhir='+periodeakhir,
        '_blank' // <- This is what makes it open in a new window.
        );
    })
});
</script>
            <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-edit"></i></div>
                                            <h5>LAPORAN PENERIMAAN OBAT DAN ALKES </h5>
                                            <!-- .toolbar -->
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
													<li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.toolbar value="<-?php echo $periodeawal; ?>"-->
                                        </header>
                                        <div id="div-1" class="accordion-body collapse in body">
                                            <form class="form-horizontal" method="POST" action="#">
                                                <div class="row-fluid">
													<div class="span12">
														<div class="span4">
															<div class="control-group">
																<label for="periodeawal" class="control-label">Tgl. Penerimaan</label>
																<div class="controls with-tooltip">
																	<input type="text" id="periodeawal" name="periodeawal" class="input-small input-tooltip" data-mask="99-99-9999"
																		   value="<?php echo $periodeawal ?>" data-original-title="masukkan tanggal awal" value="<?php echo $periodeawal?>" data-placement="bottom"/>
																		   s/d
																	<input type="text" id="periodeakhir" name="periodeakhir" class="input-small input-tooltip" data-mask="99-99-9999"
																		   value="<?php echo $periodeakhir ?>" data-original-title="masukkan tanggal akhir" value="<?php echo $periodeakhir?>" data-placement="bottom"/>
																</div>
															</div> 
														</div>
														<div class="span6">
                                                            <div class="control-group">
																<label for="kd_unit_apt" class="control-label">Unit Apotek</label>
																<div class="controls with-tooltip">
																	<input type="text" name="nama_unit_apt" id="nama_unit_apt" value="<?php if($unit=$this->mlaporanapt->ambilNamaUnit($this->session->userdata('kd_unit_apt'))) echo $unit; ?>" readonly class="span7 input-tooltip" data-original-title="nama unit" data-placement="bottom"/>
																	<input type="hidden" name="kd_unit_apt" id="kd_unit_apt" value="<?php echo $this->session->userdata('kd_unit_apt'); ?>" readonly class="span2 input-tooltip" data-original-title="kd unit apt " data-placement="bottom"/>
																</div>
															</div>
                                                        </div>
													</div>
												</div>
												<div class="row-fluid">
													<div class="span12">
														<div class="span4">
                                                            <div class="control-group">
                                                                <label for="summary" class="control-label">Summary By</label>
                                                                <div class="controls with-tooltip">
																	<select id="summary" name="summary" class="input-large">
                                                                        <option value="">--pilih--</option>
                                                                        <option value="1">Pabrik</option>
                                                                        <option value="2">Supplier</option>
                                                                        <option value="3">Golongan</option>
                                                                        <option value="4">Kelas Terapi</option>
                                                                        <option value="5">Obat Generik</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
													</div>
												</div>                                              												
                                                <div class="control-group">
                                                    <label for="text1" class="control-label">&nbsp;</label>
                                                    <div class="controls with-tooltip">
                                                        <button class="btn btn-primary" type="button" id="sum" value="1"><i class="icon-search"></i> Summary</button>
                                                        <button class="btn btn-primary" type="button" id="detail" value="1"><i class="icon-search"></i> Detail</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!--END TEXT INPUT FIELD-->                            
                            <!--Begin Datatables-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-move"></i></div>
                                            <h5></h5>
                                        </header>
                                    </div>
                                </div>
                            </div>
                            <!--End Datatables-->

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->




<script type="text/javascript">
	$('#dataTable').dataTable({
		"aaSorting": [[ 0, "asc" ]],
		"sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "Show _MENU_ entries"
		}
	});
	
	$('.with-tooltip').tooltip({
		selector: ".input-tooltip"
	});
	
	$('#periodeawal').datepicker({
		format: 'dd-mm-yyyy'
	});
			
	$('#periodeakhir').datepicker({
		format: 'dd-mm-yyyy'
	});
	
	$('#tgl_tempo').datepicker({
		format: 'dd-mm-yyyy'
	});
</script>