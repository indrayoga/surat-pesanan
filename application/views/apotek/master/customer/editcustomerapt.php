<style type="text/css">
.fixed {
    position:fixed;
    top:0px !important;
    z-index:100;
    width: 1024px;    
}
.body1{
    opacity: 0.4;
    background-color: #000000;
}

</style>
<script type="text/javascript">
	function isNumberKey(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
		 
		return false;
		return true;
	}
	
    $(document).ready(function() {

            $('#form').ajaxForm({
                beforeSubmit: function(a,f,o) {
                    o.dataType = "json";
                    $('div.error').removeClass('error');
                    $('span.help-inline').html('');
                    $('#progress').show();
                    $('body').append('<div id="overlay1" style="position: fixed;height: 100%;width: 100%;z-index: 1000000;"></div>');
                    $('body').addClass('body1');
                    z=true;
                    $.ajax({
					//url: "<?php echo base_url(); ?>index.php/log_master/masterbarang/periksa",
                    url: "<?php echo base_url(); ?>index.php/masterapotek/aptcustomer/periksa",
                    type:"POST",
                    async: false,
                    data: $.param(a),
                    success: function(data){
                        //alert(data.status);
                        if(parseInt(data.status)==1){
                            z=data.status;
                            //alert('aa');
                            //alert($('input[name="harga"]').val());
                        }else if(parseInt(data.status)==0){
                            //alert('xxx');
                            $('#progress').hide();
                            $('body').removeClass('body1');
                            z=data.status;
                            for(yangerror=0;yangerror<=data.error;yangerror++){
                            //  alert(data.id[yangerror]);
                                $('#'+data.id[yangerror]).siblings('.help-inline').html('<p class="text-error">'+data.pesan[yangerror]+'</p>');
                                $('#error').html('<div class="alert alert-error fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>Terdapat beberapa kesalahan input silahkan cek inputan anda</div>');
                            }
                            $('#error').html('<div class="alert alert-error fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesanatas+'<br/>'+data.pesanlain+'</div>');
                            $('#overlay1').remove();
                            $('body').removeClass('body1');

                        }
                    },
                    dataType: 'json'
                    });

                    if(z==0)return false;
                },
                dataType:  'json',
                success: function(data) {
                //alert(data);
                if (typeof data == 'object' && data.nodeType)
                data = elementToString(data.documentElement, true);
                else if (typeof data == 'object')
                //data = objToString(data);
                    if(parseInt(data.status)==1) //jika berhasil
                    {
                        //apa yang terjadi jika berhasil
                        $('#progress').hide();
                        $('#overlay1').remove();
                        $('body').removeClass('body1');
                        $('#error').show();
                        $('#error').html('<div class="alert alert-success fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesan+'</div>');
                    }
                    else if(parseInt(data.status)==0) //jika gagal
                    {
                        //apa yang terjadi jika gagal
                        $('#progress').hide();
                        $('#overlay1').remove();
                        $('body').removeClass('body1');
                        $('body').removeClass('body1');
                        $('#error').html('<div class="alert alert-success fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesan+'</div>');
                    }

                }
            });       

    });
</script>
            <div id="error"></div>
            <div id="overlay"></div>
            <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-edit"></i></div>
                                            <h5>Edit Customer</h5>
                                            <!-- .toolbar -->
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
													<li><a href="<?php echo base_url() ?>index.php/masterapotek/aptcustomer/"> <i class="icon-list"></i> Daftar</a></li>
													<li><a href="<?php echo base_url() ?>index.php/masterapotek/aptcustomer/tambah"> <i class="icon-plus"></i> Tambah</a></li>
                                                    <!--<li class="dropdown">
                                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                                            <i class="icon-th-large"></i>
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="">q</a></li>
                                                            <li><a href="">a</a></li>
                                                            <li><a href="">b</a></li>
                                                        </ul>
                                                    </li>-->
                                                    <li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.toolbar -->
                                        </header>
                                        <div id="div-1" class="accordion-body collapse in body">
                                            <form class="form-horizontal" id="form" method="post" action="<?php echo base_url()?>index.php/masterapotek/aptcustomer/update">
                                                <input type="hidden" name="mode" value="edit">
                                                <div class="row-fluid">
                                                    <div class="span12">
														<div class="control-group">
															<label for="cust_code" class="control-label">Kode</label>
															<div class="controls with-tooltip">
																<input type="text" name="cust_code" id="cust_code" class="span1 input-tooltip"
																	value="<?php echo $items['cust_code'] ?>" readonly data-original-title="kd obat" data-placement="bottom"/>
																<span class="help-inline"></span>
															</div>
														</div>
                                                    </div>
                                                </div> 
												<div class="row-fluid">
													<div class="span12">														
														<div class="control-group">
															<label for="customer" class="control-label">Nama Customer</label>
															<div class="controls with-tooltip">
																<input type="text" name="customer" id="customer" class="span5 input-tooltip"
																	value="<?php echo $items['customer'] ?>" data-original-title="customer" data-placement="bottom"/>
																<span class="help-inline"></span>
															</div>
														</div>														
													</div>
												</div>
                                                <div class="row-fluid">
                                                    <div class="span12">                                                        
                                                        <div class="control-group">
                                                            <label for="tipe" class="control-label">Tipe</label>
                                                            <div class="controls with-tooltip">
                                                                <select name="tipe" class="input-medium">
                                                                    <option value="0" <?php if($items['type']==0)echo "selected=selected" ?>>UMUM</option>
                                                                    <option value="1" <?php if($items['type']==1)echo "selected=selected" ?>>Asuransi</option>
                                                                    <option value="2" <?php if($items['type']==2)echo "selected=selected" ?>>Perusahaan</option>
                                                                    <option value="3" <?php if($items['type']==3)echo "selected=selected" ?>>Sosial Dakwah</option>
                                                                    <option value="4" <?php if($items['type']==4)echo "selected=selected" ?>>BPJS</option>
                                                                    <option value="5" <?php if($items['type']==5)echo "selected=selected" ?>>MCU</option>
                                                                </select>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">                                                        
                                                        <div class="control-group">
                                                            <label for="contact" class="control-label">Kontak</label>
                                                            <div class="controls with-tooltip">
                                                                <input type="text" name="contact" id="contact" class="span5 input-tooltip"
                                                                    value="<?php echo $items['contact'] ?>" data-original-title="contact" data-placement="bottom"/>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">                                                        
                                                        <div class="control-group">
                                                            <label for="address" class="control-label">Alamat</label>
                                                            <div class="controls with-tooltip">
                                                                <input type="text" name="address" id="address" class="span5 input-tooltip"
                                                                    value="<?php echo $items['address'] ?>" data-original-title="address" data-placement="bottom"/>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">                                                        
                                                        <div class="control-group">
                                                            <label for="city" class="control-label">Kota</label>
                                                            <div class="controls with-tooltip">
                                                                <input type="text" name="city" id="city" class="span5 input-tooltip"
                                                                    value="<?php echo $items['city'] ?>" data-original-title="city" data-placement="bottom"/>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">                                                        
                                                        <div class="control-group">
                                                            <label for="state" class="control-label">Negara</label>
                                                            <div class="controls with-tooltip">
                                                                <input type="text" name="state" id="state" class="span5 input-tooltip"
                                                                    value="<?php echo $items['state'] ?>" data-original-title="state" data-placement="bottom"/>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">                                                        
                                                        <div class="control-group">
                                                            <label for="zip" class="control-label">Kode Pos</label>
                                                            <div class="controls with-tooltip">
                                                                <input type="text" name="zip" id="zip" class="span5 input-tooltip"
                                                                    value="<?php echo $items['zip'] ?>" data-original-title="zip" data-placement="bottom"/>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">                                                        
                                                        <div class="control-group">
                                                            <label for="phone1" class="control-label">Telp 1</label>
                                                            <div class="controls with-tooltip">
                                                                <input type="text" name="phone1" id="phone1" class="span5 input-tooltip"
                                                                    value="<?php echo $items['phone1'] ?>" data-original-title="phone1" data-placement="bottom"/>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">                                                        
                                                        <div class="control-group">
                                                            <label for="phone2" class="control-label">Telp 2</label>
                                                            <div class="controls with-tooltip">
                                                                <input type="text" name="phone2" id="phone2" class="span5 input-tooltip"
                                                                    value="<?php echo $items['phone2'] ?>" data-original-title="phone2" data-placement="bottom"/>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">                                                        
                                                        <div class="control-group">
                                                            <label for="fax" class="control-label">Fax</label>
                                                            <div class="controls with-tooltip">
                                                                <input type="text" name="fax" id="fax" class="span5 input-tooltip"
                                                                    value="<?php echo $items['fax'] ?>" data-original-title="fax" data-placement="bottom"/>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>
                                                </div>
                                                <?php
                                                $jm=$this->db->get_where('jasa_medis',array('cust_code'=>$items['cust_code'],'biaya'=>1))->row_array();
                                                ?>
                                                <div class="row-fluid">
                                                    <div class="span12">                                                        
                                                        <div class="control-group">
                                                            <label for="jm" class="control-label">JM</label>
                                                            <div class="controls with-tooltip">
                                                                <input type="text" name="jm" id="jm" class="span5 input-tooltip"
                                                                    value="<?php if(isset($jm['jumlah']))echo $jm['jumlah'] ?>" data-original-title="fax" data-placement="bottom"/>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>
                                                </div>
                                                <?php
                                                $kartu=$this->db->get_where('jasa_medis',array('cust_code'=>$items['cust_code'],'biaya'=>2))->row_array();
                                                ?>
                                                <div class="row-fluid">
                                                    <div class="span12">                                                        
                                                        <div class="control-group">
                                                            <label for="kartu" class="control-label">KARTU</label>
                                                            <div class="controls with-tooltip">
                                                                <input type="text" name="kartu" id="kartu" class="span5 input-tooltip"
                                                                    value="<?php if(isset($kartu['jumlah']))echo $kartu['jumlah'] ?>" data-original-title="fax" data-placement="bottom"/>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>
                                                </div>
                                                <?php
                                                $adm=$this->db->get_where('jasa_medis',array('cust_code'=>$items['cust_code'],'biaya'=>1))->row_array();
                                                ?>
                                                <div class="row-fluid">
                                                    <div class="span12">                                                        
                                                        <div class="control-group">
                                                            <label for="adm" class="control-label">ADM</label>
                                                            <div class="controls with-tooltip">
                                                                <input type="text" name="adm" id="adm" class="span5 input-tooltip"
                                                                    value="<?php if(isset($adm['jumlah']))echo $adm['jumlah'] ?>" data-original-title="fax" data-placement="bottom"/>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">                                                        
                                                        <div class="control-group">
                                                            <label for="account" class="control-label">Account</label>
                                                            <div class="controls with-tooltip">
                                                                <input type="text" name="account" id="account" class="span5 input-tooltip"
                                                                    value="<?php echo $items['account'] ?>" data-original-title="account" data-placement="bottom"/>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>
                                                </div>                                                
                                                <div class="control-group">
                                                    <label for="text1" class="control-label">&nbsp;</label>
                                                    <div class="controls with-tooltip">
                                                        <button class="btn btn-primary" type="submit"><i class="icon-ok"></i> Simpan</button>
                                                        <button class="btn " type="reset"><i class="icon-undo"></i> Reset</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <div id="progress" style="display:none;"></div>                                                                                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TEXT INPUT FIELD-->                            

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->
<script type="text/javascript">
    var opts = {
      lines: 9, // The number of lines to draw
      length: 40, // The length of each line
      width: 9, // The line thickness
      radius: 0, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#000', // #rgb or #rrggbb
      speed: 1.4, // Rounds per second
      trail: 54, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: 'auto', // Top position relative to parent in px
      left: '470px' // Left position relative to parent in px
    };
    var target = document.getElementById('progress');
    var spinner = new Spinner(opts).spin(target);    
</script>