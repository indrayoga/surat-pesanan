<style type="text/css">
.fixed {
    position:fixed;
    top:0px !important;
    z-index:100;
    width: 1024px;    
}
.body1{
    opacity: 0.4;
    background-color: #000000;
}

</style>
<script type="text/javascript">
	function isNumberKey(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
		 
		return false;
		return true;
	}
	
    $(document).ready(function() {
		//$('#harga_jual').trigger('change');
		$('#kd_golongan').trigger('change');
		$('#kd_kelas_terapi').trigger('change');
		<?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang')) { ?>
				$('#min_stok').focus();
		<?php } else { ?>
				$('#nama_obat').focus();
		<?php } ?>		
			$('#form').ajaxForm({
                beforeSubmit: function(a,f,o) {
                    o.dataType = "json";
                    $('div.error').removeClass('error');
                    $('span.help-inline').html('');
                    $('#progress').show();
                    $('body').append('<div id="overlay1" style="position: fixed;height: 100%;width: 100%;z-index: 1000000;"></div>');
                    $('body').addClass('body1');
                    z=true;
                    $.ajax({
                    url: "<?php echo base_url(); ?>index.php/masterapotek/obat/periksa",
                    type:"POST",
                    async: false,
                    data: $.param(a),
                    success: function(data){
                        //alert(data.status);
                        if(parseInt(data.status)==1){
                            z=data.status;
                            //alert('aa');
                            //alert($('input[name="harga"]').val());
                        }else if(parseInt(data.status)==0){
                            //alert('xxx');
                            $('#progress').hide();
                            $('body').removeClass('body1');
                            z=data.status;
                            for(yangerror=0;yangerror<=data.error;yangerror++){
                            //  alert(data.id[yangerror]);
                                $('#'+data.id[yangerror]).siblings('.help-inline').html('<p class="text-error">'+data.pesan[yangerror]+'</p>');
                                $('#error').html('<div class="alert alert-error fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>Terdapat beberapa kesalahan input silahkan cek inputan anda</div>');
                            }
                            $('#error').html('<div class="alert alert-error fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesanatas+'<br/>'+data.pesanlain+'</div>');
                            $('#overlay1').remove();
                            $('body').removeClass('body1');

                        }
                    },
                    dataType: 'json'
                    });

                    if(z==0)return false;
                },
                dataType:  'json',
                success: function(data) {
                //alert(data);
                if (typeof data == 'object' && data.nodeType)
                data = elementToString(data.documentElement, true);
                else if (typeof data == 'object')
                //data = objToString(data);
                    if(parseInt(data.status)==1) //jika berhasil
                    {
                        //apa yang terjadi jika berhasil
                        $('#progress').hide();
                        $('#overlay1').remove();
                        $('body').removeClass('body1');
                        $('#error').show();
                        $('#error').html('<div class="alert alert-success fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesan+'</div>');
                        $('#kd_obat').val(data.kd_obat);
						if(parseInt(data.posting)==3){
							 window.location.href='<?php echo base_url(); ?>index.php/masterapotek/obat/';
						}
                    }
                    else if(parseInt(data.status)==0) //jika gagal
                    {
                        //apa yang terjadi jika gagal
                        $('#progress').hide();
                        $('#overlay1').remove();
                        $('body').removeClass('body1');
                        $('body').removeClass('body1');
                        $('#error').html('<div class="alert alert-success fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesan+'</div>');
                    }

                }
            });       

    });
</script>
            <div id="error"></div>
            <div id="overlay"></div>
            <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
                            <div class="row-fluid">
								<form class="form-horizontal" id="form" method="post" action="<?php echo base_url()?>index.php/masterapotek/obat/simpan">
									<div class="row-fluid">
										<div class="span12">
											<div class="box">
												<header>
													<div class="icons"><i class="icon-edit"></i></div>
													<h5>ENTRY OBAT / ALKES</h5>
													<!-- .toolbar -->
													<div class="toolbar" style="height:auto;">
														<ul class="nav nav-tabs">
															<li><a href="<?php echo base_url() ?>index.php/masterapotek/obat/"> <i class="icon-list"></i> Daftar Obat</a></li>
															<?php if($this->session->userdata('kd_unit_apt')==$this->session->userdata('kd_unit_apt_gudang')){ ?>
																		<li><a href="<?php echo base_url() ?>index.php/masterapotek/obat/tambah"> <i class="icon-plus"></i> Tambah Obat</a></li>
															<?php } ?>
															<li>
																<a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
																	<i class="icon-chevron-up"></i>
																</a>
															</li>
														</ul>
													</div>
													<!-- /.toolbar -->
												</header>
												<div id="div-1" class="accordion-body collapse in body">
													<div class="row-fluid">
														<div class="span12">
															<div class="span6">
																<div class="control-group">
																	<label for="kd_obat" class="control-label">Kode</label>
																	<div class="controls with-tooltip">
																		<input <?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang'))echo "readonly";?> type="text" name="kd_obat" id="kd_obat" class="span5 input-tooltip"
																			value="" data-original-title="kd obat" data-placement="bottom" readonly/>
																		<span class="help-inline"></span>
																	</div>                                                                
																</div>
															</div>
														</div>
													</div>
													<div class="row-fluid">
														<div class="span12">
															<div class="span6">
																<div class="control-group">
																	<label for="nama_obat" class="control-label">Nama Obat / Alkes</label>
																	<div class="controls with-tooltip">
																		<input <?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang'))echo "readonly";?>  type="text" name="nama_obat" id="nama_obat" class="span11 input-tooltip"
																			value="" data-original-title="nama obat / alkes" data-placement="bottom"/>
																		<span class="help-inline"></span>
																	</div>
																</div>
															</div>														
														</div>
													</div>
													<div class="row-fluid">
														<div class="span12">
															<div class="span7">
																<div class="control-group">
																	<label for="kd_satuan_kecil" class="control-label">Pabrik Obat / Alkes</label>
																	<div class="controls with-tooltip">
																		<select <?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang'))echo "readonly";?> name="kd_pabrik" class="input-large">
																		<option value="">--pilih pabrik--</option>																
																			<?php
																			foreach ($datapabrik as $pab) {
																				# code...
																			?>
																			<option value="<?php echo $pab['kd_pabrik'] ?>"><?php echo $pab['nama_pabrik'] ?></option>
																			<?php
																			}
																			?>
																		</select>
																	</div>
																</div>                                                           
															</div>														
														</div>
													</div>
													<div class="row-fluid">
														<div class="span12">
															<div class="span6">
																<div class="control-group">
																	<label for="kd_golongan" class="control-label">Golongan Obat / Alkes</label>
																	<div class="controls with-tooltip">
																		<select <?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang'))echo "readonly";?> id="kd_golongan" name="kd_golongan" class="input-medium">
																		<option value="">--pilih golongan--</option>																
																			<?php
																			foreach ($datagolongan as $gol) {
																				# code...
																			?>
																			<option value="<?php echo $gol['kd_golongan'] ?>"><?php echo $gol['golongan'] ?></option>																			
																			<?php
																			}
																			?>
																		</select>
																		<span class="help-inline"></span>
																		Sub Golongan
																		<select <?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang'))echo "readonly";?> id="kd_sub" name="kd_sub" class="input-medium">
																		<option value="" gol="xxx">--pilih sub--</option>																
																			<?php
																			foreach ($datasub as $sub) {
																				# code...
																			?>
																			<option value="<?php echo $sub['kd_sub'] ?>" gol="<?php echo $sub['kd_golongan'] ?>"><?php echo $sub['sub_golongan'] ?></option>
																			
																			<?php
																			}
																			?>
																		</select>
																	</div>
																</div>
															</div>													
														</div>
													</div>
													<div class="row-fluid">
														<div class="span12">
															<div class="span6">
																<div class="control-group">
																	<label for="kd_kelas_terapi" class="control-label">Kelas Terapi</label>
																	<div class="controls with-tooltip">
																		<select <?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang'))echo "readonly";?> id="kd_kelas_terapi" name="kd_kelas_terapi" class="input-medium">
																		<option value="">--pilih Kelas Terapi--</option>																
																			<?php
																			foreach ($datakelasterapi as $kelasterapi) {
																				# code...
																			?>
																			<option value="<?php echo $kelasterapi['kd_kelas_terapi'] ?>"><?php echo $kelasterapi['nama_kelas_terapi'] ?></option>																			
																			<?php
																			}
																			?>
																		</select>
																		<span class="help-inline"></span>
																		Obat Generik
																		<select <?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang'))echo "readonly";?> id="kd_obat_generik" name="kd_obat_generik" class="input-medium">
																		<option value="" terapi="xxxx">--pilih Obat Generik--</option>																
																			<?php
																			foreach ($dataobatgenerik as $obatgenerik) {
																				# code...
																			?>
																			<option value="<?php echo $obatgenerik['kd_obat_generik'] ?>" terapi="<?php echo $obatgenerik['kd_kelas_terapi'] ?>"><?php echo $obatgenerik['nama_obat_generik'] ?></option>
																			
																			<?php
																			}
																			?>
																		</select>
																	</div>
																</div>
															</div>													
														</div>
													</div>													

													<div class="row-fluid">
														<div class="span12">
															<div class="control-group">
																<label for="ket_obat" class="control-label">Keterangan Obat</label>
																<div class="controls with-tooltip">
																	<input <?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang'))echo "readonly";?> type="text" name="ket_obat" id="ket_obat" class="span7 input-tooltip"
																		value="" data-original-title="keterangan obat" data-placement="bottom"/>
																	<input <?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang'))echo "readonly";?> type="checkbox" id="generic" name="generic" value="1" style="display:none;" />
																	&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Aktif &nbsp&nbsp<input <?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang'))echo "readonly";?> type="checkbox" id="is_aktif" name="is_aktif" value="1" />
																	<span class="help-inline"></span>
																</div>
															</div>																												
														</div>
													</div>
													<div class="row-fluid">
														<div class="span12">
															<div class="span5">
																<div class="control-group">
																	<label for="harga_dasar" class="control-label">Harga Dasar</label>
																	<div class="controls with-tooltip">
																		<input <?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang'))echo "readonly"?> align="right" type="text" name="harga_dasar" id="harga_dasar" class="span4 input-tooltip"
																			value="" data-original-title="harga dasar" data-placement="bottom"/>
																		&nbsp *belum termasuk pajak
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="row-fluid">
														<div class="span12">
															<div class="span5">
																<div class="control-group">
																	<label for="harga_beli" class="control-label">Harga Beli</label>
																	<div class="controls with-tooltip">
																		<input <?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang'))echo "readonly";?> align="right" type="text" name="harga_beli" id="harga_beli" class="span4 input-tooltip"
																			value="" data-original-title="harga beli" data-placement="bottom"/>
																		<span class="help-inline"></span>
																		&nbsp *termasuk pajak
																	</div>
																</div>
															</div>
															<!--div class="span5">
																<div class="control-group">
																	<label for="jml_stok" class="control-label">Jumlah Stok</label>
																	<div class="controls with-tooltip">
																		<input <-?php if($this->session->userdata('kd_unit_apt')!=$this->session->userdata('kd_unit_apt_gudang'))echo "readonly"; ?> align="right" type="text" name="jml_stok" id="jml_stok" class="span3 input-tooltip"
																			value="" data-original-title="jml stok" data-placement="bottom"/>
																		<span class="help-inline"></span>
																	</div>
																</div>
															</div-->
														</div>

														<div class="row-fluid">
															<div class="span12">
																<div class="box error">
																	<header>
																	<!-- .toolbar -->
																		<div class="toolbar" style="height:auto;float:left;">
																			<ul class="nav nav-tabs">
																				<li><button class="btn" type="button" id="tambahbaris"> <i class="icon-plus"></i> Tambah Satuan Obat</button></li>
																				<li><button class="btn" type="button" id="hapusbaris"> <i class="icon-remove"></i> Hapus Satuan Obat</button></li>
																			</ul>
																		</div>
																	<!-- /.toolbar -->
																	</header>
																	<div class="body collapse in" id="defaultTable">
																		<table class="table responsive">
																			<thead>
																				<tr style="font-size:80% !important;">
																					<th class="header">Urut</th>
																					<th class="header">Satuan</th>
																					<th class="header">Pembanding</th>
																				</tr>
																			</thead>
																			<tbody id="bodyinput">

																			</tbody>
																		</table>
																	</div>
																</div>
															</div>
														</div>

													</div>
													
	
													<!--div id="progress" style="display:none;"></div-->           
												</div>
											</div>
											
											<div class="row-fluid">
												<div class="span12">
													<div class="box error">
														<div id="div-2" class="accordion-body collapse in body">
																<div class="control-group">
																<label for="text2" class="control-label">&nbsp;</label>
																<div class="controls with-tooltip">
																	<!--button class="btn btn-primary" id="simpansetting" type="button"> <i class="icon-ok"></i> Simpan</button-->
																	<button class="btn btn-primary" type="submit"><i class="icon-ok"></i> Simpan Obat</button>
																	<button class="btn " type="reset"><i class="icon-undo"></i> Reset</button>
																</div>
															</div>
															<div id="progress" style="display:none;"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									
								</form>                                
                            </div>
                            <!--END TEXT INPUT FIELD-->                            

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->
<script type="text/javascript">
	$('#kd_golongan').change(function(){
	    //alert('xx');
	    var val=$(this).val();
	    //alert(val);
	    //$('#cust_code option[tipe!='+val+']').attr('hidden','hidden');
	    $('#kd_sub option[gol!='+val+']').attr('hidden','hidden');
	    $('#kd_sub option[gol!='+val+']').attr('disabled','disabled');
	    $('#kd_sub option[gol='+val+']').removeAttr('hidden');
	    $('#kd_sub option[gol='+val+']').removeAttr('disabled');
	    $('#kd_sub option[gol='+val+']').show();
	    $('#kd_sub option[gol=xxx]').show();
	    <?php //if(isset) ?>
	    $('#kd_sub option[gol=xxx]').prop('selected',true);
	});

	$('#kd_kelas_terapi').change(function(){
	    //alert('xx');
	    var val=$(this).val();
	    //alert(val);
	    //$('#cust_code option[tipe!='+val+']').attr('hidden','hidden');
	    $('#kd_obat_generik option[terapi!='+val+']').attr('hidden','hidden');
	    $('#kd_obat_generik option[terapi!='+val+']').attr('disabled','disabled');
	    $('#kd_obat_generik option[terapi='+val+']').removeAttr('hidden');
	    $('#kd_obat_generik option[terapi='+val+']').removeAttr('disabled');
	    $('#kd_obat_generik option[terapi='+val+']').show();
	    $('#kd_obat_generik option[terapi=xxxx]').show();
	    <?php //if(isset) ?>
	    $('#kd_obat_generik option[terapi=xxxx]').prop('selected',true);
	});
	
	$('#persen_harga_jual').change(function(){
		var persen_harga_jual=Number($(this).val());
		var harga_beli=Number($('#harga_beli').val());
		var margin=parseFloat((persen_harga_jual/100)*harga_beli);
		var hargajual=parseFloat(harga_beli+margin);
		$('#harga_jual').val(hargajual);
	});

	$('#harga_jual').change(function(){
		var harga_jual=Number($(this).val());
		var harga_beli=Number($('#harga_beli').val());
		//var margin=(persen_harga_jual/100)*harga_beli;
		//var persen_harga_jual=100*((harga_jual/harga_beli)-1);
		var margin=harga_jual-harga_beli;
		var persen_harga_jual=(100*margin)/harga_beli;
		$('#persen_harga_jual').val(persen_harga_jual);
	});

	$('.with-tooltip').tooltip({
        selector: ".input-tooltip"
    });
	
	$('#kd_kategori').change(function(){
		if($(this).val()=="CTK"){
			//$('#kd_sumber').text('Simpan Transfer');
			$('#kd_sumber').prop("disabled", false);
		}
		else {
			$('#kd_sumber').prop("disabled", true);
		}
	})
	
	$('#kd_golongan').change(function(){
		$.ajax({
			url: '<?php echo base_url() ?>index.php/masterapotek/obat/cekgolongan/',
			async:false,
			type:'get',
			data:{query:$('#kd_golongan').val()},
			success:function(data){
				var gol=data;
				if(gol=="OBAT"){
				//	$('#kd_sub').prop("disabled", false);
					$('#kd_sub').focus();
				}else {
				//	$('#kd_sub').prop("disabled", true);
					$('#kd_sub').focus();
				}
			},
			dataType:'json'                         
		});	 
    });
	
    var opts = {
      lines: 9, // The number of lines to draw
      length: 40, // The length of each line
      width: 9, // The line thickness
      radius: 0, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#000', // #rgb or #rrggbb
      speed: 1.4, // Rounds per second
      trail: 54, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: 'auto', // Top position relative to parent in px
      left: '470px' // Left position relative to parent in px
    };
    var target = document.getElementById('progress');
    var spinner = new Spinner(opts).spin(target);    


	$('#tambahbaris').click(function(){
		var urut=$('#bodyinput tr').length;
		urut++;
		var selectSatuan = "<select class='input-large barissatuan clear' name='kd_satuan[]'>"+
							"<?php
							foreach ($datakecil as $kecil) {
								# code...
								echo "<option value='".$kecil['kd_satuan_kecil']."'>".$kecil['satuan_kecil']."</option>";
							}
							?></select>"

		$('#bodyinput').append('<tr style="font-size:95% !important;">'+
                                    '<td><input style="text-align:right;" type="text" name="urut[]" value="'+urut+'" style="" class="input-small barisurut mousetrap cleared"></td>'+
                                    '<td>'+selectSatuan+'</td>'+
									'<td><input type="text" name="pembanding[]" value="1" class="input-small barispembanding cleared"></td>'+
                                '</tr>');
		
		
	}); //akhir function tambah baris

</script>