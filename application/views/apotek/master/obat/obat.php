<style type="text/css">
    .nonaktif{
        background-color: yellow !important;
    }
    .nonaktif td{
        background-color: yellow !important;
    }
</style>
<script>
$(document).ready(function() {

    $datapoli=$('#dataTable').dataTable( {
            "createdRow": function( row, data, dataIndex ) {
                if ( data[14] == 0 ) {
                  $(row).addClass( 'nonaktif' );
                }
          },
        "sPaginationType": "bootstrap",
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "<?php echo base_url() ?>index.php/masterapotek/obat/dataobat/<?php echo $nama_obat.'/'.$kd_satuan_kecil; ?>",
        //"sAjaxSource": "<?php echo base_url() ?>index.php/poli/ajaxdatapendaftaran/"+$('#kd_pasien').val()+"/"+$('#nama_pasien').val()+"/"+$('#periodeawal').val()+"/"+$('#periodeakhir').val()+"/"+$('#jns_kelamin').val(),
        "sServerMethod": "POST",
        "createdRow": function( row, data, dataIndex ) {
            console.log(data);
            if ( data[14] == 'Tidak Aktif' ) {
              $(row).addClass( 'nonaktif' );
            }
          }
        
    } );
    
    $('#btncari').click(function(){
        $("#dataTable").dataTable().fnDestroy();
        if($('#nama_obat').val()=="")nama_obat="NULL";else nama_obat=$('#nama_obat').val();
        if($('#kd_satuan_kecil').val()=="")kd_satuan_kecil="NULL";else kd_satuan_kecil=$('#kd_satuan_kecil').val();
        $datapoli=$('#dataTable').dataTable( {
            "sPaginationType": "bootstrap",
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url() ?>index.php/masterapotek/obat/dataobat/"+nama_obat+"/"+kd_satuan_kecil,
            "sServerMethod": "POST",
            "createdRow": function( row, data, dataIndex ) {
                console.log(data);
                if ( data[14] == 'Tidak Aktif' ) {
                    
                  $(row).addClass( 'nonaktif' );
                }
              }
            
        } );
        $("#dataTable").css('width','100%');
        return false;
    });

$('#dataTable').on( 'draw.dt', function () {
   // alert( 'Redraw occurred at: '+new Date().getTime() );
} );    

    //$('#dataTable').dataTable( {

    //} );

} );
</script>
            <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
                            <!--END TEXT INPUT FIELD-->                            
                            <!--Begin Datatables-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-move"></i></div>
                                            <h5>DAFTAR OBAT</h5>
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
                                                    <?php if($this->session->userdata('kd_unit_apt')==$this->session->userdata('kd_unit_apt_gudang')){ ?>
                                                                <li><a href="<?php echo base_url() ?>index.php/masterapotek/obat/cetakxls"> <i class="icon-plus"></i> Data Obat XLS</a></li>
                                                                <li><a href="<?php echo base_url() ?>index.php/masterapotek/obat/tambah"> <i class="icon-plus"></i> Tambah Obat</a></li>
                                                    <?php } ?>
                                                    <li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </header>
                                        <div id="collapse4" class="accordion-body collapse in body">
                                            <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Kode Obat</th>                                                        
														<th>Nama Obat</th>
                                                        <th>Pabrik</th>
                                                        <th>Satuan Obat</th>
                                                        <th>Gol</th>
                                                        <th>Sub Gol</th>
                                                        <th>Kelas Terapi</th>
                                                        <th>Komposisi</th>
                                                        <th>Harga Dasar</th>
                                                        <th>Harga Beli</th>
                                                        <th>Status</th>
                                                        <th>Pilihan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
												<!--	<?php
														//$no=1;
													//	foreach($items as $item){
														//debugvar($items);
													?>
													<tr>
														<td><?php //echo $item['kd_obat']; ?></TD>
														<td><?php //echo $item['nama_obat']; ?></td>
														<td><?php// echo $item['satuan_kecil']; ?></td>
														<td style="text-align:center;width:160px;">
															<!--?php
                                                            //if($this->mobat->isParent('apt_penjualan_detail','kd_obat',$item['kd_obat'])){
                                                            ?-->
                                                            
                                                            <!--?php
                                                           // }else{
                                                            ?-->
                                                          <!--  <a href="<?php // echo base_url() ?>index.php/masterapotek/obat/edit/<?php // echo $item['kd_obat'] ?>" class="btn"><i class="icon-edit"></i> Edit</a>
															<?php //if($this->session->userdata('kd_unit_apt')==$this->session->userdata('kd_unit_apt_gudang')){ ?>
																		<a href="#" onClick="xar_confirm('<?php // echo base_url() ?>index.php/masterapotek/obat/hapus/<?php // echo $item['kd_obat'] ?>','Apakah Anda ingin menghapus data ini?')" class="btn btn-danger"><i class="icon-remove"></i> Hapus</a>
															<?php // } ?>                                                            
															<!--?php
                                                            }
                                                            ?-->			
                                                      <!--  </td>
													</tr>-->
													<?php
															//$no++;
													//	} //tutup foreach
													?>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End Datatables-->

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->



<script type="text/javascript">
	$('#dataTable1').dataTable({
		"aaSorting": [[ 0, "asc" ]],
		"sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "Show _MENU_ entries"
		}
	});
	
    $('.with-tooltip').tooltip({
        selector: ".input-tooltip"
    });
</script>
