<?php 
    
    session_start();

    include 'WebClientPrint.php';
    use Neodynamic\SDK\Web\WebClientPrint;
    use Neodynamic\SDK\Web\Utils;
    use Neodynamic\SDK\Web\DefaultPrinter;
    use Neodynamic\SDK\Web\InstalledPrinter;
    use Neodynamic\SDK\Web\ClientPrintJob;

    $queryunitshift=$this->db->query('select * from unit_shift where kd_unit="APT"'); 
    $unitshift=$queryunitshift->row_array();

?>

<style>
#feedback { font-size: 1.4em; }
#listobat .ui-selecting, #listobat .ui-selecting { background: #FECA40; }
#listobat .ui-selected, #listobat .ui-selected { background: #F39814; color: white; }
#listobat, #listobat { list-style-type: none; margin: 0; padding: 0; width: 60%; }
#listobat li, #listobat li { margin: 3px; padding: 0.4em; font-size: 1.4em; height: 18px; }
#listobat6 .ui-selecting, #listobat6 .ui-selecting { background: #FECA40; }
#listobat6 .ui-selected, #listobat6 .ui-selected { background: #F39814; color: white; }
#listobat6, #listobat6 { list-style-type: none; margin: 0; padding: 0; width: 60%; }
#listobat6 li, #listobat6 li { margin: 3px; padding: 0.4em; font-size: 1.4em; height: 18px; }

#listdokter .ui-selecting, #listdokter .ui-selecting { background: #FECA40; }
#listdokter .ui-selected, #listdokter .ui-selected { background: #F39814; color: white; }
#listdokter { list-style-type: none; margin: 0; padding: 0; width: 60%; }
#listdokter li, #listdokter li { margin: 3px; padding: 0.4em; font-size: 1.4em; height: 18px; }

.warnatabel { background: #FECA40 !important; }

</style>
<style type="text/css">
.fixed {
    position:fixed;
    top:0px !important;
    z-index:100;
    width: 1024px;    
}
.body1{
    opacity: 0.4;
    background-color: #000000;
}
body.modal-open {
    overflow: hidden;
}

</style>
<!--style>
#feedback { font-size: 1.4em; }
#listdokter .ui-selecting, #listdokter .ui-selecting { background: #FECA40; }
#listdokter .ui-selected, #listdokter .ui-selected { background: #F39814; color: white; }
#listdokter, #listdokter { list-style-type: none; margin: 0; padding: 0; width: 60%; }
#listdokter li, #listdokter li { margin: 3px; padding: 0.4em; font-size: 1.4em; height: 18px; }
</style-->
  <script type = "text/javascript" >
    history.pushState(null, null, '');
    window.addEventListener('popstate', function(event) {
    history.pushState(null, null, '');
    });
    </script>
<script>

    function number_format(number, decimals, dec_point, thousands_sep) {
  //  discuss at: http://phpjs.org/functions/number_format/
  // original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: davook
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Theriault
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Michael White (http://getsprink.com)
  // bugfixed by: Benjamin Lupton
  // bugfixed by: Allan Jensen (http://www.winternet.no)
  // bugfixed by: Howard Yeend
  // bugfixed by: Diogo Resende
  // bugfixed by: Rival
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  //  revised by: Luke Smith (http://lucassmith.name)
  //    input by: Kheang Hok Chin (http://www.distantia.ca/)
  //    input by: Jay Klehr
  //    input by: Amir Habibi (http://www.residence-mixte.com/)
  //    input by: Amirouche
  //   example 1: number_format(1234.56);
  //   returns 1: '1,235'
  //   example 2: number_format(1234.56, 2, ',', ' ');
  //   returns 2: '1 234,56'
  //   example 3: number_format(1234.5678, 2, '.', '');
  //   returns 3: '1234.57'
  //   example 4: number_format(67, 2, ',', '.');
  //   returns 4: '67,00'
  //   example 5: number_format(1000);
  //   returns 5: '1,000'
  //   example 6: number_format(67.311, 2);
  //   returns 6: '67.31'
  //   example 7: number_format(1000.55, 1);
  //   returns 7: '1,000.6'
  //   example 8: number_format(67000, 5, ',', '.');
  //   returns 8: '67.000,00000'
  //   example 9: number_format(0.9, 0);
  //   returns 9: '1'
  //  example 10: number_format('1.20', 2);
  //  returns 10: '1.20'
  //  example 11: number_format('1.20', 4);
  //  returns 11: '1.2000'
  //  example 12: number_format('1.2000', 3);
  //  returns 12: '1.200'
  //  example 13: number_format('1 000,50', 2, '.', ' ');
  //  returns 13: '100 050.00'
  //  example 14: number_format(1e-8, 8, '.', '');
  //  returns 14: '0.00000001'

  number = (number + '')
    .replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}


$(function() {
    $( "#listobat" ).selectable({});
    $( "#listdokter" ).selectable({});
    $( "#listobat6" ).selectable({});
});

function SelectSelectableElement (selectableContainer, elementsToSelect)
{
    // add unselecting class to all elements in the styleboard canvas except the ones to select
    $(".ui-selected", selectableContainer).not(elementsToSelect).removeClass("ui-selected").addClass("ui-unselecting");
    
    // add ui-selecting class to the elements to select
    $(elementsToSelect).not(".ui-selected").addClass("ui-selected");

    // trigger the mouse stop event (this will select all .ui-selecting elements, and deselect all .ui-unselecting elements)
    selectableContainer.selectable('refresh');
    //selectableContainer.data("selectable")._mouseStop(null);
    //return false;
}
</script>
<!--script>
$(function() {
    $( "#listdokter" ).selectable({});
});

function SelectSelectableElement (selectableContainer, elementsToSelect)
{
    // add unselecting class to all elements in the styleboard canvas except the ones to select
    $(".ui-selected", selectableContainer).not(elementsToSelect).removeClass("ui-selected").addClass("ui-unselecting");
    
    // add ui-selecting class to the elements to select
    $(elementsToSelect).not(".ui-selected").addClass("ui-selected");

    // trigger the mouse stop event (this will select all .ui-selecting elements, and deselect all .ui-unselecting elements)
    selectableContainer.selectable('refresh');
    //selectableContainer.data("selectable")._mouseStop(null);
    //return false;
}
</script-->

<script src="<?php echo base_url(); ?>assets/js/mousetrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mousetrap-global-bind.min.js"></script> 
<script type="text/javascript">
  //Mousetrap.bindGlobal('ctrl+r', function() { window.location.href='<?php echo base_url() ?>index.php/transapotek/penjualan/tambahpenjualan'; return false;});
    Mousetrap.bindGlobal('f6', function() { 
        //window.location.href='<?php echo base_url() ?>index.php/transapotek/penjualan/tambahpenjualan'; return false;


window.open(
  '<?php echo base_url() ?>index.php/transapotek/penjualan/tambahpenjualan',
  '_newtab' // <- This is what makes it open in a new window.
);

    });
    
    Mousetrap.bindGlobal('f10', function() { 
        //$('#pembayaranform').modal("show");
       // if($('#pembayaranform').is(':visible')){
           // $('#terima').focus();
       // }
        //$('#cancelpenjualan').trigger('click');
        window.location.href='<?php echo base_url() ?>index.php/transapotek/penjualan/tambahpenjualan'
        return false;
    });
    
    Mousetrap.bindGlobal('f8', function() { 
        $('#pembayaranform').modal("show");
        if($('#pembayaranform').is(':visible')){
        var check = function(){
            if($('#pembayaranform').is(':visible')){
                // run when condition is met
                $('#terima').focus();
            }
            else {
                setTimeout(check, 1000); // check again in a second
            }
        }
        check();     
        }
        //$('#cancelpenjualan').trigger('click');
        //window.location.href='<?php echo base_url() ?>index.php/transapotek/penjualan/tambahpenjualan'
        return false;
    });
  
  /*Mousetrap.bind('f3', function() { 
    $('#transferapotek').trigger('click');
    return false;
  });*/
  
  Mousetrap.bindGlobal('ctrl+l', function() { 
    $('#pencarian').modal("show");
    return false;
  });
  
  Mousetrap.bindGlobal('ctrl+b', function() { 
    $('#tambahbaris').trigger('click');
    return false;
  });


    Mousetrap.bind('a', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'a'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('b', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'b'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('c', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'c'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('d', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'d'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('e', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'e'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('f', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'f'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('g', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'g'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('h', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'h'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('i', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'i'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('j', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'j'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('k', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'k'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('l', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'l'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('m', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'m'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('n', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'n'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('o', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'o'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('p', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'p'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('q', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'q'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('r', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'r'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('s', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'s'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('t', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'t'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('u', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'u'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('v', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'v'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('w', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'w'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('x', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'x'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('y', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'y'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('z', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+'z'); $('.focused .barisnamaobat').focus(); } return false; });
    Mousetrap.bind('space', function(e) { if($('#tesxx').is(':visible')){ $('.focused .barisnamaobat').val($('.focused .barisnamaobat').val()+' '); $('.focused .barisnamaobat').focus(); } return false; });
    //Mousetrap.bind('esc', function(e) { $('#tesxx').hide(); });
    //Mousetrap.bind('tab', function(e) { if($('#tesxx').is(':visible')){ $('#tesxx').hide(); } return false; });

    Mousetrap.bindGlobal(['esc','tab'], function() {

        $('#tesxx').hide();
        $( ".barisnamaobat" ).blur();
        setTimeout(function(){
            $( ".barisnamaobat" ).focus();
        }, 500);
    });

  Mousetrap.bindGlobal(['down','right'], function() {

        if($('#daftarobat').is(':visible')){    
            if($("#listobat .ui-selected").next().is('tr')){
        $('#modal-body-daftarobat').scrollTop($('#modal-body-daftarobat').scrollTop()+45);
        SelectSelectableElement($("#listobat"), $(".ui-selected").next('tr'));
            }else{
                return false;
            }
        }

        if($('#tesxx').is(':visible')){    
            if($("#listobat6 .ui-selected").next().is('tr')){
                //$('#modal-body-daftarobat').scrollTop($('#modal-body-daftarobat').scrollTop()+45);
                SelectSelectableElement($("#listobat6"), $(".ui-selected").next('tr'));
                $('.barisnamaobat').trigger('blur');
            }else{
                return false;
            }
        }   
        
        if($('#daftardokter').is(':visible')){    
            if($("#listdokter .ui-selected").next().is('tr')){
        $('#modal-body-daftardokter').scrollTop($('#modal-body-daftardokter').scrollTop()+45);
        SelectSelectableElement($("#listdokter"), $(".ui-selected").next('tr'));
            }else{
                return false;
            }
        }
    
    });
  
  Mousetrap.bindGlobal(['up','left'], function() {
        //$('#selectable').next('tr').trigger('click');

        if($('#daftarobat').is(':visible')){ 
            if($("#listobat .ui-selected").prev().is('tr')){
        $('#modal-body-daftarobat').scrollTop($('#modal-body-daftarobat').scrollTop()-45);
        SelectSelectableElement($("#listobat"), $(".ui-selected").prev('tr'));
            }else{
                return false;
            }
        }

        if($('#tesxx').is(':visible')){    
            if($("#listobat6 .ui-selected").prev().is('tr')){
                //$('#modal-body-daftarobat').scrollTop($('#modal-body-daftarobat').scrollTop()+45);
                SelectSelectableElement($("#listobat6"), $(".ui-selected").prev('tr'));
                $('.barisnamaobat').trigger('blur');
            }else{
                return false;
            }
        }       

    if($('#daftardokter').is(':visible')){ 
            if($("#listdokter .ui-selected").prev().is('tr')){
        $('#modal-body-daftardokter').scrollTop($('#modal-body-daftardokter').scrollTop()-45);
        SelectSelectableElement($("#listdokter"), $(".ui-selected").prev('tr'));
            }else{
                return false;
            }
        }
    

    });
  
    Mousetrap.bindGlobal('enter', function(e) { 
        //if (e.preventDefault) {
        //    e.preventDefault();
        //} else {
            // internet explorer
        //    e.returnValue = false;
        //}
        if($('#daftardokter').is(':visible')){
            $('#daftardokter').find('.ui-selected').find('.btn').trigger('click');
            return false;        
        }

        if($('#daftarobat').is(':visible')){
            $('#daftarobat').find('.ui-selected').find('.btn').trigger('click');
            return false;
        }

        if($('#tesxx').is(':visible')){
            $('#tesxx').find('.ui-selected').find('.btn').trigger('click');
            return false;
        }

        return false;
       // alert('x');
        //return false;
    });
  
  Mousetrap.bindGlobal('f2', function() {
        if($('#nama_pasien').is(':focus')){
            $.ajax({
                url: '<?php echo base_url() ?>index.php/transapotek/penjualan/ambilpasienbynama/',
                async:false,
                type:'get',                                                                 
                data:{query:$("#nama_pasien").val(),tes:$('.uniform:checked').val(),dor:$('#tgl_penjualan').val()},
                success:function(data){
                //typeahead.process(data)
                    $('#listpasien').empty();
                    $.each(data,function(i,l){
                        //alert(l.kd_unit_kerja1);
                       // $('#listpasien').append('<tr><td>'+l.kd_pasien+'</td><td>'+l.no_pendaftaran+'</td><td>'+l.nama_pasien+'</td><td>'+l.tgl_pendaftaran+'</td><td>'+l.nama_unit_kerja+'</td><td><a class="btn" onclick=\'pilihpasien("'+l.kd_pasien+'","'+l.no_pendaftaran+'","'+l.nama_pasien+'","'+l.cust_code+'","'+l.type+'","'+l.kd_dokter+'","'+l.dokter+'","'+l.tgl_pendaftaran+'","'+l.kd_unit_kerja+'")\'>Pilih</a></td></tr>');
                      $('#listpasien').append('<tr><td><input type="radio" class="barisradiopasien" name="barisradiopasien"></td><td>'+l.kd_pasien+'</td><td>'+l.no_pendaftaran+'</td><td>'+l.nama_pasien+'</td><td>'+l.customer+'</td><td>'+l.tgl_pendaftaran+'</td><td>'+l.nama_unit_kerja+'</td><td>'+l.dokter+'</td><td>'+l.tgl_lahir+'</td><td><a class="btn" onclick=\'pilihpasien("'+l.kd_pasien+'","'+l.no_pendaftaran+'","'+l.nama_pasien+'","'+l.cust_code+'","'+l.type+'","'+l.kd_dokter+'","'+l.dokter+'","'+l.tgl_pendaftaran+'","'+l.kd_unit_kerja+'")\'>Pilih</a></td></tr>');
                    }); 

                        $('.barisradiopasien').keyup(function(e){
                          if(e.keyCode == 13){
                            $(this).parent().siblings('td:last').find('.btn').trigger('click');
                          }
                        });

                        $('.barisradiopasien').focus(function(){
                          $(this).parent().siblings('td').addClass('warnatabel');
                        });

                        $('.barisradiopasien').blur(function(){
                          $(this).parent().siblings('td').removeClass('warnatabel');
                        });

                },
                dataType:'json'                         
            }); 
            $('#daftarpasien').modal("show");
            //$('#nama_pasien2').focus(); 
            $('#listpasien tr:first td:first').focus();
            var ex = document.getElementById('dataTable2');
            if ( ! $.fn.DataTable.fnIsDataTable( ex ) ) {
                $('#dataTable2').dataTable({
                    "sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
                    "sPaginationType": "bootstrap",
                    "oLanguage": {
                        "sLengthMenu": "Show _MENU_ entries"
                    }
                });
                var oTable = $('#dataTable2').dataTable();
                $('#nama_pasien1').keyup(function(e){
                    oTable.fnFilter( $(this).val() );
                    if(e.keyCode == 13){
                        //alert('xx')
                        return false;
                    }
                });
            };
            return false;
        }

        if($('#kd_dokter').is(':focus')){
            $.ajax({
                url: '<?php echo base_url() ?>index.php/transapotek/penjualan/ambildokterbykode/',
                async:false,
                type:'get',
                data:{query:$('#kd_dokter').val()},
                success:function(data){
                //typeahead.process(data)
                    $('#listdokter').empty();
                    $.each(data,function(i,l){
                        //alert(l);
                        $('#listdokter').append('<tr><td>'+l.kd_dokter+'</td><td>'+l.dokter+'</td><td><a class="btn" onclick=\'pilihdokter("'+l.kd_dokter+'","'+l.dokter+'")\'>Pilih</a></td></tr>');
                    });    
                },
                dataType:'json'                         
            }); 
            $('#daftardokter').modal("show");
            var ex = document.getElementById('dataTable3');
            if ( ! $.fn.DataTable.fnIsDataTable( ex ) ) {
                $('#dataTable3').dataTable({
                    "sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
                    "sPaginationType": "bootstrap",
                    "oLanguage": {
                        "sLengthMenu": "Show _MENU_ entries"
                    }
                });
                var oTable = $('#dataTable3').dataTable();
                $('#nama_dokter1').keyup(function(e){
                    oTable.fnFilter( $(this).val() );
                    if(e.keyCode == 13){
                        //alert('xx')
                        return false;
                    }
                });
            }

        } 

        if($('#nama_dokter').is(':focus')){

            $("#dataTable3").dataTable().fnDestroy();
            $('#dataTable3').dataTable( {
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "<?php echo base_url() ?>index.php/transapotek/penjualan/ambildokterbynama/",
                "sServerMethod": "POST",
                "fnServerParams": function ( aoData ) {
                    //aoData.push( { "query":""+$("#nama_dokter").val()+""} );
                    aoData.push( { "name": "query", "value":""+$("#nama_dokter").val()+""} );
                }
                
            } );
            $('#dataTable3').css('width','100%');
            //$("body").css("overflow", "hidden");
            $('#daftardokter').modal("show");
                var check = function(){
                    if($('#listdokter tr').length >0 && !$('#listdokter tr').hasClass('ui-selected')){
                        // run when condition is met
                        $('#listdokter tr:first').addClass('ui-selected');
                    }
                    else {
                        setTimeout(check, 1000); // check again in a second
                    }
                }
                check();     

            //$('#daftardokter').modal("show");

            return false;

        } 


  }); 
  
  Mousetrap.bindGlobal('ctrl+d', function() { 
    /*
        $.ajax({
      url: '<?php echo base_url() ?>index.php/transapotek/penjualan/ambildokterbynama/',
      async:false,
      type:'get',
      data:{query:$("#nama_dokter").val()},
      success:function(data){
      //typeahead.process(data)
        $('#listdokter').empty();
        $.each(data,function(i,l){
          //alert(l);
          $('#listdokter').append('<tr><td>'+l.kd_dokter+'</td><td>'+l.dokter+'</td><td><a class="btn" onclick=\'pilihdokter("'+l.kd_dokter+'","'+l.dokter+'")\'>Pilih</a></td></tr>');
        }); 
      },
      dataType:'json'                         
    });
    $('#daftardokter').modal("show");
    var ex = document.getElementById('dataTable3');
    if ( ! $.fn.DataTable.fnIsDataTable( ex ) ) {
      $('#dataTable3').dataTable({
        "sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
          "sLengthMenu": "Show _MENU_ entries"
        }
      });
      var oTable = $('#dataTable3').dataTable();
      $('#nama_dokter1').keyup(function(e){
        oTable.fnFilter( $(this).val() );
        if(e.keyCode == 13){
          //alert('xx')
          return false;
        }
      });
    };
    return false;
        */
  });
  
  Mousetrap.bindGlobal('f12', function() { 
    $('#simpan').trigger('click');
    return false;
  });
  
</script> 
<script type="text/javascript">
  var listkdobat=new Array();

    $(document).ready(function() {
        $('#admracik').change(function(){
          $('.barisjumlah').trigger('change');
        });
        <?php
        if(isset($_GET['cetak']) && $_GET['cetak']==1){
        ?>
        $('#btn-cetak').trigger('click');

        <?php
        }
        ?>
                $('#xxx1').hide();
                $('#xxx2').hide();
                $('#xxx3').hide();
    $('#nama_pasien').focus();
    $('#cust_code').trigger('change');
        $('#tipe').trigger('change');
        $('#nilaijasamedis').change(function(){
            if($('#nilaijasamedis').val()=='') $('#nilaijasamedis').val(0);
            if($('#jasa_medis').is(':checked')){
                $('#xxx1').show();
                $('#xxx2').show();
                $('#xxx3').show();
                totalpenjualan=0;
                jasa_medis=parseFloat($('#nilaijasamedis').val());
                jasa_adm=parseFloat($('#nilaibiayaadm').val());
                jasa_kartu=parseFloat($('#nilaibiayakartu').val());

                var transaksi=$('#total_transaksi').val();
                var sumtotal=$('#sumtotal').val();
                if(transaksi=='')transaksi=0;
                if(sumtotal=='')sumtotal=0;
                $('.barisjumlah').each(function(){
                        var val=$(this).val();
                        if(val=='')val=0;
                        totalpenjualan=totalpenjualan+parseFloat(val);
                });
                
                totalpenjualan=totalpenjualan+parseFloat(jasa_medis)+parseFloat(jasa_adm)+parseFloat(jasa_kartu);
              
               $('#jumlahapprove').val(totalpenjualan);
                          //$('#totalpenjualan').val(number_format(total1.toFixed(2),2,',','.'));

               $('#totalpenjualan').val(number_format(totalpenjualan.toFixed(2),2,',','.'));
               $('#total_transaksi').val(totalpenjualan.toFixed(2));
               $('#total_bayar').val(totalpenjualan.toFixed(2));
            }else{
                //alert('xx');
                totalpenjualan=0;
                jasa_medis=parseFloat($('#nilaijasamedis').val());
                jasa_adm=parseFloat($('#nilaibiayaadm').val());
                jasa_kartu=parseFloat($('#nilaibiayakartu').val());

                var transaksi=$('#total_transaksi').val();
                var sumtotal=$('#sumtotal').val();
                if(transaksi=='')transaksi=0;
                if(sumtotal=='')sumtotal=0;
                $('.barisjumlah').each(function(){
                        var val=$(this).val();
                        if(val=='')val=0;
                        totalpenjualan=totalpenjualan+parseFloat(val);
                });
                
                totalpenjualan=totalpenjualan;
               $('#jumlahapprove').val(totalpenjualan);
               //$('#totalpenjualan').val(totalpenjualan.toFixed(2));
               $('#totalpenjualan').val(number_format(totalpenjualan.toFixed(2),2,',','.'));

               $('#total_transaksi').val(totalpenjualan.toFixed(2));
               $('#total_bayar').val(totalpenjualan.toFixed(2));
            } 
            $('.barisjumlah').trigger('change');       
        });

        $('#nilaibiayaadm').change(function(){
            if($('#nilaibiayaadm').val()=='') $('#nilaibiayaadm').val(0);
            if($('#jasa_medis').is(':checked')){
                totalpenjualan=0;
                jasa_medis=parseFloat($('#nilaijasamedis').val());
                jasa_adm=parseFloat($('#nilaibiayaadm').val());
                jasa_kartu=parseFloat($('#nilaibiayakartu').val());

                var transaksi=$('#total_transaksi').val();
                var sumtotal=$('#sumtotal').val();
                if(transaksi=='')transaksi=0;
                if(sumtotal=='')sumtotal=0;
                $('.barisjumlah').each(function(){
                        var val=$(this).val();
                        if(val=='')val=0;
                        totalpenjualan=totalpenjualan+parseFloat(val);
                });
                
                totalpenjualan=totalpenjualan+parseFloat(jasa_medis)+parseFloat(jasa_adm)+parseFloat(jasa_kartu);
               $('#jumlahapprove').val(totalpenjualan);
               //$('#totalpenjualan').val(totalpenjualan.toFixed(2));
               $('#totalpenjualan').val(number_format(totalpenjualan.toFixed(2),2,',','.'));
               $('#total_transaksi').val(totalpenjualan.toFixed(2));
               $('#total_bayar').val(totalpenjualan.toFixed(2));
            }else{
                //alert('xx');
                totalpenjualan=0;
                jasa_medis=parseFloat($('#nilaijasamedis').val());
                jasa_adm=parseFloat($('#nilaibiayaadm').val());
                jasa_kartu=parseFloat($('#nilaibiayakartu').val());

                var transaksi=$('#total_transaksi').val();
                var sumtotal=$('#sumtotal').val();
                if(transaksi=='')transaksi=0;
                if(sumtotal=='')sumtotal=0;
                $('.barisjumlah').each(function(){
                        var val=$(this).val();
                        if(val=='')val=0;
                        totalpenjualan=totalpenjualan+parseFloat(val);
                });
                
                totalpenjualan=totalpenjualan;
               $('#jumlahapprove').val(totalpenjualan);
               //$('#totalpenjualan').val(totalpenjualan.toFixed(2));
               $('#totalpenjualan').val(number_format(totalpenjualan.toFixed(2),2,',','.'));
               $('#total_transaksi').val(totalpenjualan.toFixed(2));
               $('#total_bayar').val(totalpenjualan.toFixed(2));
            }  
            $('.barisjumlah').trigger('change');         
        });

        $('#nilaibiayakartu').change(function(){
            if($('#nilaibiayakartu').val()=='') $('#nilaibiayakartu').val(0);
            if($('#jasa_medis').is(':checked')){
                totalpenjualan=0;
                jasa_medis=parseFloat($('#nilaijasamedis').val());
                jasa_adm=parseFloat($('#nilaibiayaadm').val());
                jasa_kartu=parseFloat($('#nilaibiayakartu').val());

                var transaksi=$('#total_transaksi').val();
                var sumtotal=$('#sumtotal').val();
                if(transaksi=='')transaksi=0;
                if(sumtotal=='')sumtotal=0;
                $('.barisjumlah').each(function(){
                        var val=$(this).val();
                        if(val=='')val=0;
                        totalpenjualan=totalpenjualan+parseFloat(val);
                });
                
                totalpenjualan=totalpenjualan+parseFloat(jasa_medis)+parseFloat(jasa_adm)+parseFloat(jasa_kartu);
               $('#jumlahapprove').val(totalpenjualan);
               //$('#totalpenjualan').val(totalpenjualan.toFixed(2));
               $('#totalpenjualan').val(number_format(totalpenjualan.toFixed(2),2,',','.'));
               $('#total_transaksi').val(totalpenjualan.toFixed(2));
               $('#total_bayar').val(totalpenjualan.toFixed(2));
            }else{
                //alert('xx');
                totalpenjualan=0;
                jasa_medis=parseFloat($('#nilaijasamedis').val());
                jasa_adm=parseFloat($('#nilaibiayaadm').val());
                jasa_kartu=parseFloat($('#nilaibiayakartu').val());

                var transaksi=$('#total_transaksi').val();
                var sumtotal=$('#sumtotal').val();
                if(transaksi=='')transaksi=0;
                if(sumtotal=='')sumtotal=0;
                $('.barisjumlah').each(function(){
                        var val=$(this).val();
                        if(val=='')val=0;
                        totalpenjualan=totalpenjualan+parseFloat(val);
                });
                
                totalpenjualan=totalpenjualan;
               $('#jumlahapprove').val(totalpenjualan);
               //$('#totalpenjualan').val(totalpenjualan.toFixed(2));
               $('#totalpenjualan').val(number_format(totalpenjualan.toFixed(2),2,',','.'));
               $('#total_transaksi').val(totalpenjualan.toFixed(2));
               $('#total_bayar').val(totalpenjualan.toFixed(2));
            }        
            $('.barisjumlah').trigger('change');   
        });
        //$('#tipe').trigger('change');
        var totalpenjualan=0; var sisa=0;

            var jasa_medis=0;
            var jasa_adm=0;
            var jasa_kartu=0;
             //alert($('#jasa_medis').is(':checked'));
            $('#jasa_medis').click(function(){
                if($('#jasa_medis').is(':checked')){
                $('#xxx1').show();
                $('#xxx2').show();
                $('#xxx3').show();
                    totalpenjualan=0;
                    $('#nilaijasamedis').removeAttr('disabled');
                    $('#nilaibiayaadm').removeAttr('disabled');
                    $('#nilaibiayakartu').removeAttr('disabled');

                    jasa_medis=parseFloat($('#nilaijasamedis').val());
                    jasa_adm=parseFloat($('#nilaibiayaadm').val());
                    jasa_kartu=parseFloat($('#nilaibiayakartu').val());

                    var transaksi=$('#total_transaksi').val();
                    var sumtotal=$('#sumtotal').val();
                    if(transaksi=='')transaksi=0;
                    if(sumtotal=='')sumtotal=0;
                    $('.barisjumlah').each(function(){
                            var val=$(this).val();
                            if(val=='')val=0;
                            totalpenjualan=totalpenjualan+parseFloat(val);
                    });
                    
                    totalpenjualan=totalpenjualan+parseFloat(jasa_medis)+parseFloat(jasa_adm)+parseFloat(jasa_kartu);
                   $('#jumlahapprove').val(totalpenjualan);
                   //$('#totalpenjualan').val(totalpenjualan.toFixed(2));
               $('#totalpenjualan').val(number_format(totalpenjualan.toFixed(2),2,',','.'));
                   $('#total_transaksi').val(totalpenjualan.toFixed(2));
                   $('#total_bayar').val(totalpenjualan.toFixed(2));
                }else{
                $('#xxx1').hide();
                $('#xxx2').hide();
                $('#xxx3').hide();
                    //alert('xx');
                    $('#nilaijasamedis').attr('disabled','disabled');
                    $('#nilaibiayaadm').attr('disabled','disabled');
                    $('#nilaibiayakartu').attr('disabled','disabled');

                    totalpenjualan=0;
                    jasa_medis=parseFloat($('#nilaijasamedis').val());
                    jasa_adm=parseFloat($('#nilaibiayaadm').val());
                    jasa_kartu=parseFloat($('#nilaibiayakartu').val());

                    var transaksi=$('#total_transaksi').val();
                    var sumtotal=$('#sumtotal').val();
                    if(transaksi=='')transaksi=0;
                    if(sumtotal=='')sumtotal=0;
                    $('.barisjumlah').each(function(){
                            var val=$(this).val();
                            if(val=='')val=0;
                            totalpenjualan=totalpenjualan+parseFloat(val);
                    });
                    
                    totalpenjualan=totalpenjualan;
                   $('#jumlahapprove').val(totalpenjualan);
                   //$('#totalpenjualan').val(totalpenjualan.toFixed(2));
               $('#totalpenjualan').val(number_format(totalpenjualan.toFixed(2),2,',','.'));
                   $('#total_transaksi').val(totalpenjualan.toFixed(2));
                   $('#total_bayar').val(totalpenjualan.toFixed(2));
                }
                $('.barisjumlah').trigger('change');
            });

            if($('#jasa_medis').is(':checked')){
                totalpenjualan=0;
                $('#nilaijasamedis').removeAttr('disabled');
                $('#nilaibiayaadm').removeAttr('disabled');
                $('#nilaibiayakartu').removeAttr('disabled');
                jasa_medis=parseFloat($('#nilaijasamedis').val());
                jasa_adm=parseFloat($('#nilaibiayaadm').val());
                jasa_kartu=parseFloat($('#nilaibiayakartu').val());
            }

    var transaksi=$('#total_transaksi').val();
    var sumtotal=$('#sumtotal').val();
    if(transaksi=='')transaksi=0;
    if(sumtotal=='')sumtotal=0;
        $('.barisjumlah').each(function(){
                var val=$(this).val();
                if(val=='')val=0;
                totalpenjualan=totalpenjualan+parseFloat(val);
        });
        var admracik=$('#admracik').val();
        totalpenjualan=totalpenjualan+parseFloat(jasa_medis)+parseFloat(jasa_adm)+parseFloat(jasa_kartu)+parseFloat(admracik);
        $('#jumlahapprove').val(totalpenjualan);
        //$('#totalpenjualan').val(addCommas(totalpenjualan));
    //$('#totalpenjualan').val(totalpenjualan.toFixed(2));
               $('#totalpenjualan').val(number_format(totalpenjualan.toFixed(2),2,',','.'));
    var bayar=parseFloat(transaksi)-parseFloat(sumtotal);
    $('#sumtotal').val(sumtotal);
    $('#bayar').val(bayar);
    $('#bayar1').val(bayar);
    $('#sisa').val(sisa);
    
    $('#daftarpasien').on('shown.bs.modal', function () {
      // do something…
      $('.barisradiopasien:first').focus();
      //$('#nama_pasien2').focus();
    })
    

        $('#form').ajaxForm({
            beforeSubmit: function(a,f,o) {
                o.dataType = "json";
                $('div.error').removeClass('error');
                $('span.help-inline').html('');
                $('#progress').show();
                $('body').append('<div id="overlay1" style="position: fixed;height: 100%;width: 100%;z-index: 1000000;"></div>');
                $('body').addClass('body1');
                var urlnya="<?php echo base_url(); ?>index.php/transapotek/penjualan/periksapenjualan"; //buat validasi inputan
                //console.log($.param(a));
                //console.log($('#form').serialize());
                
                z=true;
                $('#simpan').prop('disabled',true);
                $.ajax({
                url: urlnya,
                type:"POST",
                async: false,
                data: $.param(a),
                success: function(data){
                    //alert(data.status);
                    if(parseInt(data.status)==1){
                        z=data.status;
                        //alert('aa');
                        //alert($('input[name="harga"]').val());
                    }else if(parseInt(data.status)==0){
                        //alert('xxx');
                        $('#progress').hide();
                        z=data.status;
                        for(yangerror=0;yangerror<=data.error;yangerror++){
                            $('#'+data.id[yangerror]).siblings('.help-inline').html('<p class="text-error">'+data.pesan[yangerror]+'</p>');
                            $('#'+data.id[yangerror]).parents('row-fluid').focus();
                            //$('#error').html('<div class="alert alert-error fade in"><button data-dismiss="alert" class="close" type="button"><i class="iconic-x"></i></button>Terdapat beberapa kesalahan input silahkan cek inputan anda</div>');                                 
                        }
                        $('#error').html('<div class="alert alert-error fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesanatas+'<br/>'+data.pesanlain+'</div>');

                        if(parseInt(data.clearform)==1){
                            //$('#form').resetForm();
                            $('input').live('keydown', function(e) {
                                if(e.keyCode == 13){
                                    return false;                                    
                                }
                            });

                            $('#form .cleared').clearFields();
                        }
                        $('#overlay1').remove();
                        $('body').removeClass('body1');
                    }
                  $('#simpan').prop('disabled',false);

                },
                dataType: 'json'
                });

                if(z==0)return false;
            },
            dataType:  'json',
            success: function(data) {
            //alert(data);
            if (typeof data == 'object' && data.nodeType)
            data = elementToString(data.documentElement, true);
            else if (typeof data == 'object')
            //data = objToString(data);
                $('#simpan').prop('disabled',false);
                if(parseInt(data.status)==1) //jika berhasil
                {
                    //apa yang terjadi jika berhasil
                    $('#progress').hide();
                    $('#overlay1').remove();
                    $('body').removeClass('body1');
                    $('#error').show();
                    $('#error').html('<div class="alert alert-success fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesan+'</div>');
                    $('#no_penjualan').val(data.no_penjualan); //baru
                    //$('#btn-cetak').removeAttr('disabled'); //baru
                    //$('#btn-cetak').attr('href','<?php echo base_url() ?>third-party/fpdf/buktipenerimaan.php?no_penerimaan='+data.no_penerimaan+'');
                   // $('#btn-tutuptrans').removeAttr('disabled'); //baru
          if(parseInt(data.keluar)>0){
                        window.location.href='<?php echo base_url(); ?>index.php/transapotek/penjualan';
                    }
                    if(parseInt(data.simpanbayar)>0){
                        window.location.href='<?php echo base_url(); ?>index.php/transapotek/penjualan/ubahpenjualan/'+data.no_penjualan+'?cetak=1';
                    }

                    $('#no_penjualan').val(data.no_penjualan);
                    //$('#btn-cetak').removeAttr('disabled');
                   // $('#btn-cetak').attr('href','<?php echo base_url() ?>third-party/fpdf/cetakbill.php?no_penjualan='+data.no_penjualan+'');
                  //  $('#btn-cetakkwitansi').removeAttr('disabled');
                    //$('#btn-tutuptrans').removeAttr('disabled');

                    if(parseInt(data.posting)==1 || parseInt(data.posting)==3){
                      //  $('#btn-tutuptrans').attr('value','bukatrans');
                       // $('#btn-tutuptrans').text('Buka Trans');
                        $('#btn-cetak').removeAttr('disabled');
                        //$('#btn-cetak').attr('href','<?php echo base_url() ?>third-party/fpdf/cetakbill.php?no_penjualan='+data.no_penjualan+'');
                        $('#btn-cetak').attr('onclick',"javascript:jsWebClientPrint.print('useDefaultPrinter=1&printerName=null&kd_user=<?php echo $this->session->userdata('kd_user') ?>&no_penjualan="+data.no_penjualan+"');");
                        $('#btn-cetakkwitansi').removeAttr('disabled');
                        //$('#btn-bayar').removeAttr('disabled');
                        window.location.href='<?php echo base_url(); ?>index.php/transapotek/penjualan/ubahpenjualan/'+data.no_penjualan+'?cetak=1';
                    }
                    if(parseInt(data.posting)==2){
                        //$('#btn-bayar').attr('disabled');                        
                  //      $('#btn-tutuptrans').attr('value','tutuptrans');
                   //     $('#btn-tutuptrans').text('Tutup Trans');
                        window.location.href='<?php echo base_url(); ?>index.php/transapotek/penjualan/ubahpenjualan/'+data.no_penjualan+'?cetak=1';
                    }
          if(parseInt(data.posting)==3){
                         window.location.href='<?php echo base_url(); ?>index.php/transapotek/penjualan/ubahpenjualan/'+data.no_penjualan+'?cetak=1';
                    }
          if(parseInt(data.simpanbayar)==1){
                         //window.location.href='<?php echo base_url(); ?>third-party/fpdf/cetakbill.php?no_penjualan='+data.no_penjualan;
            // window.open('<?php echo base_url(); ?>third-party/fpdf/cetakbill.php?no_penjualan='+data.no_penjualan,'_newtab');                  
                    }
                    //if(parseInt(data.cetak)>0){
                        //window.location.href='<?php echo base_url(); ?>index.php/loket/cetakregisterpasienxls/'+data.kd_pendaftaran;  
                        //window.open('<?php echo base_url(); ?>index.php/loket/cetakregisterpasienxls/'+data.kd_pendaftaran,'_newtab');                  
                        //window.location.href='<?php echo base_url(); ?>index.php/loket/';                       
                   // }else{
                       // window.location.href='<?php echo base_url(); ?>index.php/loket/';                       
                   // }
                    //$('#btn-cetak').trigger('click');
                }
                else if(parseInt(data.status)==0) //jika gagal
                {
                    //apa yang terjadi jika gagal
                    $('#progress').hide();
                    $('#overlay1').remove();
                    $('body').removeClass('body1');
                    $('#error').show();
                    $('#error').html('<div class="alert alert-success fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesan+'</div>');
                }

            }
        });       

    });


</script>
<style type="text/css">.datepicker{z-index:1151;}</style>
  <div id="error"></div>
    <div id="overlay"></div>


      <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
            <form class="form-horizontal"  id="form" method="POST" action="<?php echo base_url() ?>index.php/transapotek/penjualan/simpanpenjualan">
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header class="top" style="">
                                            <div class="icons"><i class="icon-edit"></i></div>
                                            <h5>PENJUALAN OBAT / ALKES </h5>
                                            <!-- .toolbar -->
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
                                                    <li><a class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>index.php/transapotek/penjualan/"> <i class="icon-list"></i> Daftar </a></li>
                                                    <!--li><a target="_blank" <-?php if(!$this->mpenjualan->isPosted($no_penjualan))echo "disabled"; ?> class="btn" id="btn-cetak" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<-?php if(!empty($no_penjualan)){ echo base_url() ?>third-party/fpdf/cetak.php?no_penjualan=<-?php echo $no_penjualan;} else echo '#'; ?>" <-?php if(empty($no_penjualan)){ ?>disabled<-?php } ?>> <i class="icon-print"></i> Bill</a></li-->
                          <li><button type="button" target="_blank" <?php if(!$this->mpenjualan->isSaved($no_penjualan))echo "disabled"; ?> class="btn" id="btn-cetak" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" onclick=""  <?php if(empty($no_penjualan)){ ?>disabled<?php } ?>> <i class="icon-print"></i>Cetak Bill</button></li>
                          <li><a target="_blank" <?php if(!$this->mpenjualan->isPosted($no_penjualan))echo "disabled"; ?> class="btn" id="btn-cetakkwitansi" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" data-toggle="modal" data-original-title="Cetak Kwitansi" data-placement="bottom" rel="tooltip" href="#cetakkwitansiform" <?php if(empty($no_penjualan)){ ?>disabled<?php } ?>> <i class="icon-print"></i> Kwitansi</a></li>
                          <li><button <?php if($this->mpenjualan->isPosted($no_penjualan))echo "disabled"; ?> class="btn" id="simpan" type="submit"  name="submit" value="simpan"> <i class="icon-save"></i> Simpan / (F12)</button></li>
                                                    <li><a target="_blank" class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>index.php/transapotek/penjualan/tambahpenjualan"> <i class="icon-plus"></i> Tambah / (F6)</a></li>
                                                    <li><a class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>index.php/transapotek/penjualan/tambahpenjualan"> <i class="icon-undo"></i>  Cancel / (F10) </a></li>
                                                    <!--<li><a class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" data-toggle="modal" data-original-title="Pencarian" data-placement="bottom" rel="tooltip" href="#pencarian"> <i class="icon-search"></i> Pencarian / (Ctrl+L)</a></li>
                                                    -->
                                                    <?php
                                                    if($unitshift['shift']==3){
                                                    ?>
                                                    <li><button <?php if($this->mpenjualan->isPosted1($no_penjualan)) {echo "disabled";} else { echo "enabled";} ?> class="btn" id="tampilkanformbayar" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" data-toggle="modal" data-original-title="Pembayaran" data-placement="bottom" rel="tooltip" href="#pembayaranform"> <i class="icon-ok"></i> Bayar / (F8)</button></li>
                                                    <?php
                                                    }
                                                    ?>
                          <li><button <?php if($this->mpenjualan->isPosted($no_penjualan)) {echo "disabled";} else { echo "enabled";} ?> class="btn" type="submit"  name="submit" value="transferapotek"> <i class="icon-ok"></i> Transfer Tagihan</button></li>
                          <li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.toolbar -->
                                        </header>
                                        <div id="div-1" class="accordion-body collapse in body">
                      <div class="row-fluid">
                        <div class="span12">
                          <div class="span6">
                            <div class="control-group">
                              <label for="no_penjualan" class="control-label">No. Transaksi </label>
                              <div class="controls with-tooltip">
                                <input type="text" name="no_penjualan" id="no_penjualan" value="<?php echo $no_penjualan; ?>" readonly class="span7 input-tooltip" data-original-title="no penjualan" data-placement="bottom"/>
                                <span class="help-inline"></span>
                                                                Resep  <input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> type="checkbox" id="resep" name="resep" value="1" <?php echo set_checkbox('resep','1',isset($itemtransaksi['resep'])&& $itemtransaksi['resep']=='1' ? true:false); ?> />
                              </div>
                            </div>
                          </div>
                          <div class="span6">
                            <div class="control-group">
                              <label for="tgl_penjualan" class="control-label">Tgl. Transaksi </label>
                              <div class="controls with-tooltip">
                                <input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> type="text" name="tgl_penjualan" id="tgl_penjualan" class="input-small input-tooltip cleared" data-original-title="tgl penjualan" data-mask="99-99-9999" value="<?php if(empty($itemtransaksi['tgl_penjualan']))echo date('d-m-Y'); else echo convertDate($itemtransaksi['tgl_penjualan']); ?>" data-placement="bottom"/>
                                <input type="hidden" id="tgl_penjualan2" name="tgl_penjualan2" value="<?php if(isset($itemtransaksi['tgl_penjualan']))echo convertDate($itemtransaksi['tgl_penjualan']); else echo date('d-m-Y'); ?>" class="span3 input-tooltip" data-original-title="tgl penjualan" data-placement="bottom"/>
                                <span class="help-inline"></span>
                                <input type="hidden" id="shiftapt" name="shiftapt" value="<?php if(isset($itemtransaksi['shiftapt']))echo $itemtransaksi['shiftapt'] ?>" class="span2 input-tooltip" data-original-title="shift" data-placement="bottom" readonly />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div> 
                                            <div class="row-fluid">
                        <div class="span12">
                          <div class="span6">
                            <div class="control-group">
                              <label for="nama_pasien" class="control-label">Pasien </label>
                              <div class="controls with-tooltip">
                                <input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> type="text" id="kd_pasien" name="kd_pasien" value="<?php if(isset($itemtransaksi['kd_pasien']))echo $itemtransaksi['kd_pasien'] ?>" readonly class="span4 input-tooltip" data-original-title="kd pasien" data-placement="bottom"/>
                                <input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> type="text" id="nama_pasien" tabindex="1" name="nama_pasien" value="<?php if(isset($itemtransaksi['nama_pasien']))echo $itemtransaksi['nama_pasien'] ?>" class="span8 input-tooltip" data-original-title="nama pasien" data-placement="bottom"/>
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp( List Pasien : F2 ) <span class="help-inline"></span>
                                <span class="help-inline"></span>                               
                              </div>
                            </div>
                          </div>
                          <div class="span6">
                            <div class="control-group">
                              <label for="tgl_penjualan" class="control-label">Keterangan </label>
                              <div class="controls with-tooltip">
                                <input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> type="text" name="keterangan" id="keterangan" class="input-large input-tooltip cleared" value="<?php if(isset($itemtransaksi['keterangan']))echo $itemtransaksi['keterangan'] ?>" data-placement="bottom"/>
                              </div>
                            </div>
                          </div>                          
                          <!--<div class="span6">
                            <div class="control-group">
                              <label for="adm_racik" class="control-label">Jasa Racik </label>
                              <div class="controls with-tooltip">
                                <input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> style="text-align:right;" type="text" id="adm_racik" name="adm_racik" value="<?php if(isset($itemtransaksi['adm_racik'])) echo number_format($itemtransaksi['adm_racik'],2,'.','') ?>" class="span4 input-tooltip" data-original-title="jasa racik" data-placement="bottom" readonly />
                                <span class="help-inline"></span>
                                Lunas <input type="checkbox" id="is_lunas" name="is_lunas" value="1" <?php echo set_checkbox('is_lunas','1',isset($itemtransaksi['is_lunas'])&& $itemtransaksi['is_lunas']=='1' ? true:false); ?> disabled />
                                 <input type="checkbox" id="tutup" name="tutup" style="display:none;" value="1" <?php echo set_checkbox('tutup','1',isset($itemtransaksi['tutup'])&& $itemtransaksi['tutup']=='1' ? true:false); ?> disabled />
                              </div>
                            </div>
                          </div>-->
                        </div>
                      </div>
                      <div class="row-fluid">
                        <div class="span12">
                          <div class="span6">
                            <div class="control-group">
                              <label for="cust_code" class="control-label">Jenis Pasien</label>
                              <div class="controls with-tooltip">
                                                                <select  <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly disabled"; ?> name="tipe" id="tipe" class="input-medium" tabindex="2">
                                                                    <option value="-">Pilih Tipe Pasien</option>
                                                                    <option value="0" <?php  if(isset($itemtransaksi["type"]) && $itemtransaksi['type']==0) echo "selected=selected"; else if(!isset($itemtransaksi["type"]) && isset($_COOKIE["tipe"]) && $_COOKIE["tipe"]==0) echo "selected=selected"; ?> >Tunai</option>
                                                                    <option value="1" <?php  if(isset($itemtransaksi["type"]) && $itemtransaksi['type']==1) echo "selected=selected"; else if(!isset($itemtransaksi["type"]) && isset($_COOKIE["tipe"]) && $_COOKIE["tipe"]==1) echo "selected=selected"; ?>>Asuransi</option>
                                                                    <option value="2" <?php  if(isset($itemtransaksi["type"]) && $itemtransaksi['type']==2) echo "selected=selected"; else if(!isset($itemtransaksi["type"]) && isset($_COOKIE["tipe"]) && $_COOKIE["tipe"]==2) echo "selected=selected"; ?>>Perusahaan</option>
                                                                    <option value="3" <?php  if(isset($itemtransaksi["type"]) && $itemtransaksi['type']==3) echo "selected=selected"; else if(!isset($itemtransaksi["type"]) && isset($_COOKIE["tipe"]) && $_COOKIE["tipe"]==3) echo "selected=selected"; ?>>Sosial Dakwah</option>
                                                                    <option value="4" <?php  if(isset($itemtransaksi["type"]) && $itemtransaksi['type']==4) echo "selected=selected"; else if(!isset($itemtransaksi["type"]) && isset($_COOKIE["tipe"]) && $_COOKIE["tipe"]==4) echo "selected=selected"; ?>>BPJS</option>
                                                                </select>
                                <select  <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly disabled"; ?> class="input-medium" tabindex="3" name="cust_code" id="cust_code">
                                <!--option value="">Pilih Jenis Pasien</option-->
                                
                                <?php
                                  foreach ($jenispasien as $jenis) {
                                    $select="";
                                    if(isset($itemtransaksi['cust_code'])){    
                                                                            if($itemtransaksi['cust_code']==$jenis['cust_code'])$select="selected=selected";else $select="";                                      
                                                                        }else if(isset($_COOKIE["cust_code"] )) {
                                                                            //debugvar($_COOKIE["cust_code"]);
                                                                            if($jenis['cust_code']==$_COOKIE["cust_code"])$select="selected=selected";else $select="";
                                                                        }
                                    else {
                                      if($jenis['cust_code']=="0")$select="selected=selected";else $select=""; 
                                                                        }
                                ?>
                                <option value="<?php if(!empty($jenis)) echo $jenis['cust_code'] ?>" <?php echo $select; ?> tipe="<?php if(!empty($jenis)) echo $jenis['type'] ?>"><?php echo $jenis['customer'] ?></option>
                                <?php
                                  }
                                ?>
                                </select>                               
                              </div>
                            </div>
                          </div>
                          <div class="span6">
                            <div class="control-group">
                              <label for="total_transaksi" class="control-label">Jumlah Transaksi </label>
                              <div class="controls with-tooltip">
                                <input  <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> style="text-align:right;" type="text" id="total_transaksi" name="total_transaksi" value="<?php if(isset($itemtransaksi['total_transaksi'])) echo number_format($itemtransaksi['total_transaksi'],2,'.','') ?>" class="span4 input-tooltip totaltransaksi cleared" data-original-title="jumlah transaksi" data-placement="bottom" readonly />
                                <input style="text-align:right;" type="hidden" id="jasabungkus" name="jasabungkus" value="<?php if(isset($itembungkus['setting']))echo $itembungkus['setting'] ?>" class="span4 input-tooltip jasabungkus cleared" data-original-title="jasa bungkus" data-placement="bottom"/>
                                <span class="help-inline"></span>                                 
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row-fluid">
                        <div class="span12">
                          <div class="span6">
                            <div class="control-group">
                              <label for="kd_dokter" class="control-label">Dokter</label>
                              <div class="controls with-tooltip">
                                <input  tabindex="4" <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> type="text" id="kd_dokter" name="kd_dokter" value="<?php if(isset($itemtransaksi['kd_dokter']))echo $itemtransaksi['kd_dokter'] ?>" class="span4 input-tooltip" data-original-title="kd dokter" data-placement="bottom"/>
                                <input  tabindex="5"  <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> type="text" id="nama_dokter" name="nama_dokter" value="<?php if(isset($itemtransaksi['dokter']))echo $itemtransaksi['dokter'] ?>" class="span8 input-tooltip" data-original-title="nama dokter" data-placement="bottom"/>
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp( List Dokter : F2 ) <span class="help-inline"></span>                               
                              </div>
                            </div>
                          </div>
                          <div class="span6">
                            <div class="control-group">
                              <label for="total_bayar" class="control-label">Biaya Racik </label>
                              <div class="controls with-tooltip" >
                              <input type="text" class="input-medium cleared" maxlength="6" <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?>  value="<?php if(isset($itemtransaksi['adm_racik'])) echo $itemtransaksi['adm_racik']; else echo 0; ?>"  name="admracik" id="admracik" style="text-align:right" >
                                <input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> style="text-align:right;" type="hidden" id="total_bayar" name="total_bayar" value="<?php if(isset($itemtransaksi['total_transaksi'])) echo number_format($itemtransaksi['total_transaksi'],2,'.','') ?>" class="span4 input-tooltip" data-original-title="total bayar" data-placement="bottom" readonly />
                                <input type="hidden" id="no_pendaftaran" name="no_pendaftaran" value="<?php 
                                if(isset($itemtransaksi['no_pendaftaran']) && !empty($itemtransaksi['no_pendaftaran'])){
                                  echo $itemtransaksi['no_pendaftaran']; 
                                }else{
                                  if(isset($itemtransaksi['no_register_rb']))echo $itemtransaksi['no_register_rb'];
                                }

                                ?>" class="span4 input-tooltip" data-original-title="no pendaftaran" data-placement="bottom"/>
                                <input type="hidden" name="tgl_pelayanan" id="tgl_pelayanan" value="<?php echo date('Y-m-d'); ?>" data-mask="9999-99-99" class="input-small">
                                <input type="hidden" name="jam_pelayanan" id="jam_pelayanan" value="<?php echo date('h:i:s'); ?>" data-mask="99:99:99" class="input-small"/>
                                <input type="hidden" id="jam_penjualan" name="jam_penjualan" value="<?php if(isset($itemtransaksi['jampenjualan']))echo $itemtransaksi['jampenjualan'] ?>" class="span4 input-tooltip" data-original-title="jam penjualan" data-placement="bottom"/>
                                <span class="help-inline"></span>                               
                              </div>
                            </div>
                          </div>
                        </div>
                          <div id="progress" style="display:none;"></div>
                      </div>                                            
                                        </div>
                                    </div>
                  
                  <div class="row-fluid">
                    <div class="span12">
                      <div class="box error">
                        <header>
                        <!-- .toolbar -->
                          <div class="toolbar" style="height:auto;float:left;">
                            <ul class="nav nav-tabs">
                              <li><button class="btn" type="button" id="tambahbaris"> <i class="icon-plus"></i> Tambah Obat (Ctrl+B)</button></li>
                              <li><button class="btn" type="button" id="hapusbaris"> <i class="icon-remove"></i> Hapus Obat</button></li>
                            </ul>
                          </div>
                        <!-- /.toolbar -->
                        </header>
                        <div class="body collapse in" id="defaultTable">
                          <table class="table responsive">
                            <thead>
                              <tr>
                                <th class="header">&nbsp;</th>
                                <th class="header" style="width:60px;">Kd.Obat</th>
                                <th class="header" style="width:180px;">Nama Obat</th>
                                <th class="header" style="width:90px;">Satuan</th>
                                <th class="header" style="width:120px;">Tgl.Expire</th>
                                <th class="header" style="width:150px;">Harga (Rp)</th>                               
                                <!--th class="header" style="width:60px;">Racik</th-->
                                                                <th class="header" style="width:50px;">Qty</th>
                                <th class="header" style="width:100px;">Jasa Resep</th>
                                <th class="header" style="text-align:right;width:120px;">Jumlah (Rp)</th>
                              </tr>
                            </thead>
                            <tbody id="bodyinput">
                              <?php
                                if(isset($itemsdetiltransaksi)){
                                  //$no=1;
                                  foreach ($itemsdetiltransaksi as $itemdetil){                                     
                                  ?>
                                    <tr>
                                      <td style="text-align:center;"><input type="checkbox" class="barisinput" /></td>
                                      <td><input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> type="text" name="kd_obat[]" value="<?php echo $itemdetil['kd_obat'] ?>" class="input-medium bariskodeobat cleared"></td>
                                      <td><input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> type="text" name="nama_obat[]" value="<?php echo $itemdetil['nama_obat'] ?>" style="width:300px !important;" class="input-large barisnamaobat cleared"></td>
                                      <td><input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> style="text-align:center;" type="text" name="satuan_kecil[]" value="<?php echo $itemdetil['satuan_kecil'] ?>" style="" class="input-small barissatuan cleared" disabled></td>
                                      <td><input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> style="text-align:center;" type="text" name="tgl_expire[]" value="<?php echo $itemdetil['tgl_expire'] ?>" class="input-small baristanggal cleared" readonly></td>
                                      <td><input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> style="text-align:right;" type="text" id="harga_jual" name="harga_jual[]" value="<?php echo number_format($itemdetil['harga_jual'],2,'.','') ?>" class="input-small barisharga cleared" readonly></td>                                                                           
                                                                            <td><input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> style="text-align:right;" type="text" id="qty" name="qty[]" maxlength="3" value="<?php echo $itemdetil['qty'] ?>" class="input-small barisqty cleared"></td>
                                      <td><input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> style="text-align:right;" type="text" id="adm_resep" name="adm_resep[]" value="<?php echo $itemdetil['adm_resep'] ?>" maxlength="6" class="input-small barisresep cleared"></td>
                                      <td style="text-align:right;"><input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "readonly"; ?> style="text-align:right;" type="text" name="total[]" value="<?php echo number_format($itemdetil['total'],2,'.','') ?>" class="input-medium barisjumlah cleared" readonly></td>
                                      <input type="hidden" name="jml_stok[]" value="<?php echo $itemdetil['jml_stok'] ?>" class="input-medium barisstok cleared">
                                      <input type="hidden" name="min_stok[]" value="<?php echo $itemdetil['min_stok'] ?>" class="input-mini barisminstok cleared">
                                                                            <input type="hidden" name="qty11[]" value="<?php echo $itemdetil['qty'] ?>" class="input-medium barisqty11 cleared">
                                      <td><input type="hidden" name="racikan1[]" value="" class="input-mini barisracik1 cleared"></td>
                                    </tr>
                                  <?php
                                    //$no++;
                                  }
                                }
                              ?>
                            </tbody>
                            <tfoot>
                                                            <tr>
                                                                <th colspan="10" style="text-align:right;" class="header">
                                                                    Jasa Medis : <input <?php if($this->mpenjualan->isPosted($no_penjualan))echo "disabled"; ?> type="checkbox" id="jasa_medis" name="jasa_medis" value="1" <?php echo set_checkbox('jasa_medis','1',isset($itemtransaksi['jasa_medis']) && $itemtransaksi['jasa_medis']>1 ? true:false); ?> />
                                                                </th>
                                                            </tr>
                                                            <tr id="xxx1">
                                                                <th colspan="10" style="text-align:right;" class="header">
                                                                    Jasa Medis : 
                                                                    <input type="text" class="input-medium cleared" maxlength="5" value="<?php if(isset($itemtransaksi['jasa_medis'])) echo $itemtransaksi['jasa_medis']; else echo $jasamedis; ?>" disabled name="nilaijasamedis" id="nilaijasamedis" style="text-align:right" >
                                                                </th>
                                                            </tr>
                                                            <tr id="xxx2">
                                                                <th colspan="10" style="text-align:right;" class="header">
                                                                    Biaya Adm : 
                                                                    <input type="text" class="input-medium cleared" maxlength="4" value="<?php if(isset($itemtransaksi['jasa_medis'])) echo $itemtransaksi['biaya_adm']; else  echo $biayaadm; ?>" disabled name="nilaibiayaadm" id="nilaibiayaadm" style="text-align:right" >
                                                                </th>
                                                            </tr>
                                                            <tr id="xxx3">
                                                                <th colspan="10" style="text-align:right;" class="header">
                                                                    Biaya Kartu : 
                                                                    <input type="text" class="input-medium cleared" maxlength="4" value="<?php if(isset($itemtransaksi['biaya_kartu'])) echo $itemtransaksi['biaya_kartu']; else  echo $biayakartu; ?>" disabled name="nilaibiayakartu" id="nilaibiayakartu" style="text-align:right" >
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th colspan="10" style="text-align:right;" class="header">
                                                                    Total (Rp) : 
                                                                    <input type="text" class="input-medium cleared" id="totalpenjualan" value="" style="text-align:right" disabled>
                                                                </th>
                                                            </tr>
                            </tfoot>
                          </table>                                                  
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row-fluid" style="position:absolute !important;z-index:1 !important;">
                    <!--div class="span12"-->
                      <div class="box error">
                        <header>
                          <div class="icons"><i class="icon-edit"></i></div>
                          <h5>Data Pembayaran </h5>
                          <div class="toolbar" style="height:auto;float:right;">
                            <ul class="nav nav-tabs">
                              <!--li><button class="btn" id="simpan" type="submit"  name="submit" value="simpan"> <i class="icon-save"></i> Simpan</button></li-->
                              <li><button <?php if($this->mpenjualan->cek($no_penjualan)) {echo "enabled";} else {echo "disabled";}?> class="btn" id="hapusbayar" type="button"> <i class="icon-remove"></i> Batal Transfer</button></li>
                            </ul>
                          </div>
                        </header>
                        <div class="body collapse in" id="defaultTable1">
                          <table class="table responsive">
                            <thead>
                              <tr>
                                <th class="header">&nbsp;</th>
                                <!--th>No</th-->
                                <th>Tanggal</th>
                                <th>Jenis Pembayaran</th>
                                <th class="span9">Jumlah (Rp)</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                                if(isset($itembayar)){
                                  $no=1;
                                  foreach ($itembayar as $item1){                                     
                                  ?>
                                    <tr>
                                      <!--td style="text-align:center;"><input type="checkbox" class="bariscekbok" /></td-->
                                      <td style="text-align:center;"><input type="hidden" class="bariscekbok" /></td>
                                      <!--td><-?php echo $no; ?></td-->
                                      <td><input type="text" name="tgl_bayar[]" value="<?php echo $item1['tgl_bayar'] ?>" class="input-small baristglbayar cleared" readonly></td>
                                      <td><input type="text" name="jenis_bayar[]" value="<?php echo $item1['jenis_bayar'] ?>" class="input-xxlarge barisjenis cleared" readonly></td>
                                      <td><input style="text-align:right;" type="text" name="totalbayar[]" value="<?php echo number_format($item1['total'],2,'.','') ?>" class="input-large baristotalbayar cleared" readonly></td>
                                    </tr>                                   
                                  <?php
                                    $no++;
                                  }
                                }
                              ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                      </div>
                    </div>
                                        <br/>
                                        <br/>
                                        <br/>
                  <!--/div-->
                  
                  <div aria-hidden="true" aria-labelledby="approveModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="pembayaranform" style="display: none;">
                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                      <h3 id="helpModalLabel"><i class="icon-external-link"></i> Pembayaran</h3>
                    </div>
                    <div class="modal-body" style="">
                      <div class="body" id="collapse4">
                        <div class="row-fluid">
                          <div class="span12">
                            <div class="control-group">
                              <label for="kd_jenis_bayar" class="control-label">Jenis Pembayaran</label>
                              <div class="controls with-tooltip">
                                <select class="input-medium cleared" name="kd_jenis_bayar" id="kd_jenis_bayar">
                                <?php
                                  foreach ($jenisbayar as $bayar) {
                                    $select="";
                                    if(isset($itemtransaksi['kd_jenis_bayar'])){    
                                                                            if($itemtransaksi['kd_jenis_bayar']==$bayar['kd_jenis_bayar'])$select="selected=selected";else $select="";
                                                                        }
                                ?>
                                <option value="<?php if(!empty($bayar)) echo $bayar['kd_jenis_bayar'] ?>" <?php echo $select; ?>><?php echo $bayar['jenis_bayar'] ?></option>
                                <?php
                                  }
                                ?>
                                </select>
                                <input type="hidden" name="tgl_bayar" id="tgl_bayar" class="input-small input-tooltip cleared" data-original-title="tgl bayar" data-mask="9999-99-99" value="<?php if(empty($tgl_bayar))echo date('Y-m-d'); else echo convertDate($tgl_bayar); ?>" data-placement="bottom"/>
                              </div>
                            </div>
                          </div>
                        </div>                        
                        <div class="control-group">
                          <label for="total_transaksi" class="control-label">Jumlah Transaksi</label>
                          <div class="controls with-tooltip">
                            <input type="text" style="text-align:right;" name="total_transaksi" id="total_transaksi" disabled class="input-large input-tooltip" value="<?php if(isset($itemtransaksi['total_transaksi']))echo number_format($itemtransaksi['total_transaksi'],2,'.','') ?>" data-original-title="total transaksi" data-placement="bottom"/>
                            <span class="help-inline"></span>
                          </div>
                        </div>
                        <div class="control-group" style="display:none;">
                          <label for="bayar" class="control-label">Bayar</label>
                          <div class="controls with-tooltip">
                            <input type="hidden" style="text-align:right;" name="bayar" id="bayar" class="input-large input-tooltip" data-original-title="bayar" data-placement="bottom"/>
                            <input type="hidden" name="bayar1" id="bayar1" class="input-medium input-tooltip" data-original-title="bayar1" data-placement="bottom"/>
                            <input type="hidden" name="sumtotal" id="sumtotal" value="<?php if(isset($itembayarform['totalsum'])) echo number_format($itembayarform['totalsum'],2,'.','') ?>" class="input-medium input-tooltip" data-original-title="sumtotal" data-placement="bottom"/>
                            <span class="help-inline"></span>
                          </div>
                        </div>
                        <div class="control-group" style="display:none">
                          <label for="sisa" class="control-label">Sisa</label>
                          <div class="controls with-tooltip">
                            <input type="hidden" style="text-align:right;" name="sisa" id="sisa" class="input-large input-tooltip" data-original-title="sisa" data-placement="bottom"/>
                            <span class="help-inline"></span>
                          </div>
                        </div>
                        <div class="control-group">
                          <label for="terima" class="control-label">Terima</label>
                          <div class="controls with-tooltip">
                            <input type="text" style="text-align:right;" name="terima" id="terima" class="input-large input-tooltip" data-original-title="terima" data-placement="bottom"/>
                            <span class="help-inline"></span>
                          </div>
                        </div>
                        <div class="control-group">
                          <label for="kembali" class="control-label">Kembali</label>
                          <div class="controls with-tooltip">
                            <input type="text" style="text-align:right;" name="kembali" id="kembali" class="input-large input-tooltip" data-original-title="kembali" data-placement="bottom"/>
                            <span class="help-inline"></span>
                          </div>
                        </div>
                        <div class="control-group">
                          <label for="catatan" class="control-label">&nbsp;</label>
                          <div class="controls with-tooltip">                             
                            <button class="btn btn-primary" type="submit" name="submit" value="simpanbayar" id="simpanbayar">Simpan Pembayaran</button>
                            <button aria-hidden="true" data-dismiss="modal" class="btn">Cancel</button>
                            <span class="help-inline"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button aria-hidden="true" data-dismiss="modal" class="btn">Close</button>
                    </div>
                  </div>
                  
                  <div aria-hidden="true" aria-labelledby="approveModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="cetakkwitansiform" style="display: none;">
                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                      <h3 id="helpModalLabel1"><i class="icon-external-link"></i> Cetak Kwitansi</h3>
                    </div>
                    <div class="modal-body" style="">
                      <div class="body" id="collapse4">                       
                        <div class="control-group">
                          <label for="terima1" class="control-label">Telah terima dari</label>
                          <div class="controls with-tooltip">
                            <input type="text" style="text-align:left;" name="terima1" id="terima1" class="input-xlarge input-tooltip" value="Tn./Ny." data-original-title="terima1" data-placement="bottom"/>                            
                            <span class="help-inline"></span>
                          </div>
                        </div>                                                
                        <div class="control-group">
                          <label for="terbilang" class="control-label">Terbilang</label>
                          <div class="controls with-tooltip">
                            <input type="text" style="text-align:left;" name="terbilang" id="terbilang" class="input-xlarge input-tooltip" value="<?php if(!empty($itemtransaksi))echo numericToString($itemtransaksi['total_transaksi']).'Rupiah' ?>" data-original-title="terbilang" data-placement="bottom"/>
                            <span class="help-inline"></span>
                          </div>
                        </div>
                        <div class="control-group">
                          <label for="untuk" class="control-label">Untuk</label>
                          <div class="controls with-tooltip">
                            <textarea id="untuk" name="untuk" cols="90" rows="3" class="input-large" style="width:270px">Untuk pembayaran biaya obat-obatan</textarea>
                            <span class="help-inline"></span>
                          </div>
                        </div>
                        <div class="control-group">
                          <label for="totalcetak" class="control-label">Total</label>
                          <div class="controls with-tooltip">
                            <input type="text" style="text-align:right;" name="totalcetak" id="totalcetak" class="input-large input-tooltip" value="<?php if(!empty($itemtransaksi)) echo 'Rp '.number_format($itemtransaksi['total_transaksi']) ?>" data-original-title="totalcetak" data-placement="bottom"/>
                            <span class="help-inline"></span>
                          </div>
                        </div>                        
                        <div class="control-group">
                          <label for="catatan" class="control-label">&nbsp;</label>
                          <div class="controls with-tooltip"> 
                            <button class="btn btn-primary" type="btn" name="btn-kwitansi" id="cetakkwitansi" >OK</button>
                            <button aria-hidden="true" data-dismiss="modal" class="btn">Cancel</button>
                            <span class="help-inline"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button aria-hidden="true" data-dismiss="modal" class="btn">Close</button>
                    </div>
                  </div>
                  
                  <div aria-hidden="true" aria-labelledby="approveModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="pencarian" style="display: none;width:77%;left:34% !important;">
                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                      <h3 id="helpModalLabel"><i class="icon-external-link"></i> Daftar Penjualan</h3>
                    </div>
                    <div class="modal-body" style="">
                      <div class="body" id="collapse4">
                        <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
                          <thead>
                            <tr>
                              <th>No Penjualan</th>
                              <th>Tanggal</th>
                              <th>Jenis Pasien</th>
                              <th>Nama</th>
                              <th>Total</th>
                              <!--<th>Approve</th>-->
                              <th>Pilihan</th>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button aria-hidden="true" data-dismiss="modal" class="btn">Close</button>
                    </div>
                  </div>
                  
                                </div>
                            </div>
                            <!--END TEXT INPUT FIELD-->                            
                            </form>

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->
<div style="display:none;position: absolute !important;right: 0;bottom: 0;z-index:99999999999 !important;" id="tesxx" class=" ui-widget-content draggable">
            <table id="dataTable6" class="table table-bordered " style="background:white !important;">
                <thead>
                    <tr>                        
                        <th style="text-align:center;">Kode Obat</th>
                        <th style="text-align:center;">Nama Obat</th>
                        <th style="text-align:center;">Satuan</th>
                        <th style="text-align:center;">Tgl. Expire</th>
                        <th style="text-align:center;">Harga Jual</th>
                        <th style="text-align:center;">Stok</th>
                        <th style="width:50px !important;">Pilihan</th>
                    </tr>
                </thead>
                <tbody id="listobat6">

                </tbody>
            </table>
</div>
<div aria-hidden="true" aria-labelledby="helpModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="daftarobat" style="display: none;width:70%;margin-left:-400px !important;"> 
    <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="helpModalLabel"><i class="icon-external-link"></i> Daftar Obat</h3>
    </div>
    <div class="modal-body" id="modal-body-daftarobat" style="height:340px;">
        <div class="body" id="collapse4">
            <table id="dataTable5" class="table table-bordered ">
                <thead>
                    <tr>            
                        <th style="text-align:center;">Kode Obat</th>
                        <th style="text-align:center;">Nama Obat</th>
            <th style="text-align:center;">Satuan</th>
            <th style="text-align:center;">Tgl. Expire</th>
            <th style="text-align:center;">Harga Jual</th>
            <th style="text-align:center;">Stok</th>
                        <th style="width:50px !important;">Pilihan</th>
                    </tr>
                </thead>
                <tbody id="listobat">

                </tbody>
            </table>
        </div>
    </div>
    <div class="modal-footer">
        <input type="text" id="nama_obat1" class="pull-left" autocomplete="off">
        <button aria-hidden="true" data-dismiss="modal" class="btn">Close</button>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="helpModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="daftarpasien" style="display: none;width:90%;margin-left:-600px !important;"> 
    <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="helpModalLabel"><i class="icon-external-link"></i> Daftar Pasien</h3>
    </div>
  <div class="row-fluid">
    <div class="span12">
      <div class="box">
        <header class="top" style="">
          <div class="icons"><i class="icon-edit"></i></div>
                    <h5>Asal Pasien</h5>
        </header>
        <div id="div-2" class="accordion-body collapse in body">
          <div class="row-fluid">
            <div class="span12">
              <div class="span6">
                <div class="control-group">                 
                  <label class="radio inline">
                    <input class="uniform" type="radio" name="asal" id="rj" value="1" checked />Rawat Jalan
                  </label>
                  <label class="radio inline">
                    <input class="uniform" type="radio" name="asal" id="rd" value="2" />Rawat Darurat
                  </label>
                  <label class="radio inline">
                    <input class="uniform" type="radio" name="asal" id="ri" value="3" />Rawat Inap
                  </label>            
                </div>
              </div>
              <div class="span6">
                <div class="control-group" align="right">
                  <input type="text" id="nama_pasien2" name="nama_pasien2" class="span8 input-tooltip" data-original-title="nama pasien" data-placement="bottom"/>
                  <input type="hidden" name="tgl_entry" id="tgl_entry" class="input-small input-tooltip cleared" data-original-title="tgl entry" data-mask="9999-99-99" value="<?php if(empty($tgl_entry))echo date('Y-m-d'); else echo $tgl_entry; ?>" data-placement="bottom"/>
                  <!--input type="submit" name="submitcari" id="submitcari" value="cari"/-->
                </div>                
              </div>              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <div class="modal-body" style="height:260px;">
        <div class="body" id="collapse4">
            <table id="dataTable4" class="table table-bordered table-condensed table-hover table-striped">
                <thead>
          <tr>  
            <th style="text-align:center;"></th>
            <th style="text-align:center;">No RM</th>
                        <th style="text-align:center;">No. Pendaftaran</th>
            <th style="text-align:center;">Nama</th>
            <th style="text-align:center;">Jns</th>
            <th style="text-align:center;">Tgl. Daftar</th>
            <th style="text-align:center;">Ruangan</th>
            <th style="text-align:center;">Dokter</th>
            <th style="text-align:center;">Tgl Lahir</th>
            <th style="text-align:center;" style="width:50px !important;">Pilihan</th>
                    </tr>         
                </thead>
                <tbody id="listpasien"> 
        
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal-footer">
        <input type="text" id="nama_pasien1" class="pull-left" autocomplete="off">    
    <button aria-hidden="true" data-dismiss="modal" class="btn">Close</button>    
    </div>
</div>


<div aria-hidden="true" aria-labelledby="helpModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="daftardokter" style="display: none;width:60%;margin-left:-400px !important;"> 
    <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h3 id="helpModalLabel"><i class="icon-external-link"></i> Daftar Dokter</h3>
    </div>
    <!--div class="modal-body" id="modal-body-daftardokter" style="height:340px;"-->
  <div class="modal-body" id="modal-body-daftardokter" style="height:340px;">
        <div class="body" id="collapse4">
            <table id="dataTable3" class="table table-bordered">
                <thead>
                    <tr>            
                        <th>Kode Dokter</th>
                        <th>Nama Dokter</th>
            <th style="width:50px !important;">Pilihan</th>
                    </tr>
                </thead>
                <tbody id="listdokter">

                </tbody>
            </table>
        </div>
    </div>
    <div class="modal-footer">
        <input type="text" id="nama_dokter1" class="pull-left" autocomplete="off">
        <button aria-hidden="true" data-dismiss="modal" class="btn">Close</button>
    </div>
</div>
      
<script type="text/javascript">

$('#tipe').change(function(){
    //alert('xx');
    var val=$(this).val();
    //alert(val);
    //$('#cust_code option[tipe!='+val+']').attr('hidden','hidden');
    $('#cust_code option[tipe!='+val+']').attr('hidden','hidden');
    $('#cust_code option[tipe='+val+']').removeAttr('hidden');
    $('#cust_code option[tipe='+val+']').show();
    <?php //if(isset) ?>
    $('#cust_code option[tipe='+val+']').prop('selected',true);
    <?php
        if(isset($itemtransaksi['cust_code'])){
    ?>
        $("#cust_code option[value=<?php echo $itemtransaksi['cust_code']; ?>]").prop('selected',true)
    <?php
        }else if(isset($_COOKIE["cust_code"] )) {
            //debugvar($_COOKIE["cust_code"]);
           // if($jenis['cust_code']==$_COOKIE['cust_code']){
    ?>
        $("#cust_code option[value=<?php echo $_COOKIE['cust_code']; ?>]").prop('selected',true)
    <?php
            //}
        }
    ?>
});

$('#jasa_medis').click(function(){
    $('.barisjumlah').trigger('change');
});

$('#tampilkanformbayar').click(function(){
        var check = function(){
            if($('#pembayaranform').is(':visible')){
                // run when condition is met
                $('#terima').focus();
            }
            else {
                setTimeout(check, 1000); // check again in a second
            }
        }
        check();     
        //if($('#pembayaranform').is(':visible')){
         //   $('#terima').focus();
       // }
});

            $('#btn-cetak').bind('click', function(e){
                    var r = confirm("Cetak Struk?");
                    //if($('#kd_pasien').val()=="")alert('Pilih pasien dulu');
                    if (r == true)
                      {
                            jsWebClientPrint.print('useDefaultPrinter=1&printerName=null&kd_user=<?php echo $this->session->userdata('id_user') ?>&no_penjualan=<?php echo $no_penjualan; ?>')
                            //jsWebClientPrint.print('useDefaultPrinter=1&printerName=null&kd_pasien='+$('#kd_pasien').val()+'');
                          /* $.ajax({
                                url: '<?php echo base_url() ?>index.php/reg/rwj/cetakstruk/'+$('#kd_pasien').val(),
                                type:"POST",
                                async: false,
                                success: function(data){

                                },
                                dataType: 'json'
                            });*/
                      }
                    else
                      {
                        
                      window.location.href="<?php echo base_url() ?>index.php/transapotek/penjualan/tambahpenjualan";           
                      return false;
                      }         
                      window.location.href="<?php echo base_url() ?>index.php/transapotek/penjualan/tambahpenjualan";           
                    //$('#cetakstruk').attr('href','<?php echo base_url() ?>index.php/reg/rwj/cetakstruk/'+data.kd_pasien);
                //    e.preventDefault();
            })

        $('#terima').keyup(function(e){
            if(e.keyCode == 13){
                console.log('xx')
                $('#kembali').focus();
                return false;
            }
        });


    $('.with-tooltip').tooltip({
        selector: ".input-tooltip"
    });
  
  $('#jam_pelayanan').timepicker({
        minuteStep: 1,
        showSeconds: true,
        showMeridian: false
    });
  
  $('#simpanbayar').click(function(){
        $('#pembayaranform').modal("hide");
    });
  
  $('#cetakkwitansi').click(function(){
        $('#cetakkwitansiform').modal("hide");
    });
  
   $('#tgl_penjualan').datepicker({
        format: 'dd-mm-yyyy'
    });
  
   $('#cust_code').change(function(){
      $.getJSON("<?php echo base_url() ?>index.php/transapotek/penjualan/jasamedis/"+$(this).val(),function(result){
        console.log(result);
        if(result!=""){
          $('#nilaijasamedis').val(result.jumlah);
          $('#nilaijasamedis').trigger('change');
        }
      });

      $.getJSON("<?php echo base_url() ?>index.php/transapotek/penjualan/adm/"+$(this).val(),function(result){
        console.log(result);
        if(result!=""){
          $('#nilaibiayaadm').val(result.jumlah);
          $('#nilaibiayaadm').trigger('change');
        }
      });

      $.getJSON("<?php echo base_url() ?>index.php/transapotek/penjualan/kartu/"+$(this).val(),function(result){
        console.log(result);
        if(result!=""){
          $('#nilaibiayakartu').val(result.jumlah);
          $('#nilaibiayakartu').trigger('change');
        }
      });

    })
    
    $('#tipe').change(function(){
        if($(this).val()=="0"){
            $('#tgl_penjualan').prop("disabled", true);         
        }
        else {
            $('#tgl_penjualan').prop("disabled", false);
        }
    })
    
  $('#kd_jenis_bayar').change(function(){
    if($(this).val()=="002"){
      $('#simpanbayar').text('Simpan Transfer');
    }
    else {
      $('#simpanbayar').text('Simpan Pembayaran');
    }
  })
  
  $('#cetakkwitansi').click(function(){
    var terima=$('#terima1').val();
    var terbilang=$('#terbilang').val();
    var untuk=$('#untuk').val();
        var totalcetak=$('#totalcetak').val();
        var nopenjualan=$('#no_penjualan').val();
    window.open('<?php echo base_url(); ?>/third-party/fpdf/cetakkwitansi.php?terima='+terima+'&terbilang='+terbilang+'&untuk='+untuk+'&totalcetak='+totalcetak+'&nopenjualan='+nopenjualan+'','_newtab');
    return false;
  });
  
  $('#bayar').change(function(){
    var val=$(this).val(); //ambil yg di bayar
    var bayar1=$('#bayar1').val(); 
    if(val=='')val=0;
    if(bayar1=='')bayar1=0;
        //var sisa=parseFloat(bayar1)-parseFloat(val);
        var sisa=parseFloat(val)-parseFloat(bayar1);
    $('#sisa').val(sisa.toFixed(2));
  });
  
  $('#terima').change(function(){
    var val=$(this).val(); //ambil terima
    var bayar=$('#bayar').val(); 
    if(val=='')val=0;
    if(bayar=='')bayar=0;
    var kembali=parseFloat(val)-parseFloat(bayar);
    $('#kembali').val(kembali.toFixed(2));
  });
  
  function pilihdokter(kd_dokter,dokter) {
    $('#kd_dokter').val(kd_dokter);
        $('#nama_dokter').val(dokter);
    $('#daftardokter').modal("hide");
        $('#kd_dokter').focus();

        return false;
        return false;
    }
  
  function pilihpasien(kd_pasien,no_pendaftaran,nama_pasien,cust_code,tipe,kd_dokter,dokter) {
    $('#kd_pasien').val(kd_pasien);
    $('#no_pendaftaran').val(no_pendaftaran);
        $('#nama_pasien').val(nama_pasien);
    $('#cust_code').val(cust_code);
    $('#tipe').val(tipe);
    $('#kd_dokter').val(kd_dokter);
    $('#nama_dokter').val(dokter);
    $('#daftarpasien').modal("hide");
        $('#cust_code').focus();
    }

  function pilihobat(kd_obat,nama_obat,satuan_kecil,tgl_expire,harga_jual,jml_stok,min_stok) {
        //listkdobat.push("['"+kd_obat+"','"+tgl_expire+"']");
        var list_kd_obat = $(".bariskodeobat").map(function() {
            return this.value;
        }).get();

        var list_tgl_expire = $(".baristanggal").map(function() {
            return this.value;
        }).get();
        //alert(values.join(","));
        console.log(list_tgl_expire);
        console.log(list_kd_obat);
        if (typeof list_kd_obat != 'undefined') {
          // ..
            var a = list_kd_obat.indexOf(kd_obat); 
            console.log(a);
            if(a!=-1){
                console.log(list_tgl_expire[a]);
               //console.log('xx');
                if(list_tgl_expire[a]==tgl_expire){
                $('#error').html('<div class="alert alert-error fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>Obat sudah di input</div>');
                throw 'Obat sudah di input';

                }
            }
        }        

    var batal=0;
    //$('.bariskodeobat').each(function(){
               // var val=$(this).val();
              //  var expire=$(this).parent().parent().find('.baristanggal').val();
              // console.log(expire);
              //  if(val==kd_obat && expire==tgl_expire){
               //     $('#error').html('<div class="alert alert-error fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>Obat sudah di input</div>');
               //    throw 'Obat sudah di input';
                    //console.log(val);
          //alert('Obat sudah di input');
          //batal=1;
        //}
        //});
    //if(batal)return false;
    
    $('.focused').find('.bariskodeobat').val(kd_obat);
        $('.focused').find('.barisnamaobat').val(nama_obat);
        $('.focused').find('.barissatuan').val(satuan_kecil);
        $('.focused').find('.baristanggal').val(tgl_expire);
    $('.focused').find('.barisharga').val(parseFloat(harga_jual).toFixed(2));
    $('.focused').find('.barisstok').val(jml_stok);
    $('.focused').find('.barisminstok').val(min_stok);
    $('#daftarobat').modal("hide");
        //$('.focused').find('input[name="qty[]"]').focus();
        //$('.barisresep').trigger('change');
        //$('.barisqty').trigger('change');
        //$('.barisjumlah').trigger('change');
        //$('.focused').find('input[name="racikan[]"]').focus();
        $('#tesxx').hide();

             // $('.barisjumlah').trigger('change');
        $('.focused').find('input[name="harga_jual[]"]').focus();
        $('.focused').find('input[name="qty[]"]').trigger('change');

        $('#tesxx').hide();


    return false;
    }

$(function() {
    $( "#tesxx" ).draggable();
});


$(function() {
  //  function position() {
 //   $( "#xxx" ).position({
 //       of: $("#tesxx"),
 //       my: "left top",
//        at: "left bottom",
 //       collision:"fit fit"
 //       });
//   }
 //   $( "#tesxx" ).css( "opacity", 0.5 );
   // $( "select, input" ).bind( "click keyup change", position );
    //$( "#parent" ).draggable({
    //    drag: position
    //});
 //  position();
});

        $('#tesxx').keyup(function(e){
            $('#tesxx').show();
        })

  
  $('#tambahbaris').click(function(){
    if($('.bariskodeobat').length>0){
            $('.barisstok').each(function(){
        var val=$(this).val(); //ambil stok
        var minstok=$('.focused').find('.barisminstok').val();
        var nama_obat=$('.focused').find('.barisnamaobat').val();
        //alert(minstok);
        if(parseFloat(minstok)==parseFloat(val)){
                    alert('Obat '+nama_obat+' telah mencapai batas minimum stok !');
                    //e.stopImmediatePropagation();
                    //return false;
                }       
            });

        }
    
    $('#bodyinput').append('<tr><td style="text-align:center;"><input type="checkbox" class="barisinput cleared" /></td>'+
                  '<td><input type="text" name="kd_obat[]"  value="" class="input-medium bariskodeobat cleared"></td>'+
                                    '<td><input type="text" name="nama_obat[]"  value="" autocomplete="off" style="width:300px !important;" class="input-large barisnamaobat cleared"></td>'+
                                    '<td><input type="text" name="satuan_kecil[]" value="" style="text-align:center;" class="input-small barissatuan cleared" readonly></td>'+
                  '<td><input type="text" name="tgl_expire[]" value="" style="text-align:center;" class="input-small baristanggal cleared" readonly ></td>'+
                  '<td><input type="text" name="harga_jual[]" value="" style="text-align:right;" class="input-small barisharga cleared" readonly ></td>'+                                   
                                    '<td><input type="text" name="qty[]"  maxlength="3" value="" style="text-align:right;" class="input-small barisqty cleared"></td>'+
                  '<td><input type="text" name="adm_resep[]"  maxlength="6" value="" style="text-align:right;" class="input-small barisresep cleared"></td>'+
                                    '<td style="text-align:right;"><input type="text"  style="text-align:right;" name="total[]" value="" class="input-medium barisjumlah cleared" readonly ></td>'+
                  '<input type="hidden" name="jml_stok[]"  value="" class="input-medium barisstok cleared">'+
                  '<input type="hidden" name="min_stok[]" value="" class="input-mini barisminstok cleared">'+
                                    '<input type="hidden" name="qty11[]" value="" class="input-medium barisqty11 cleared">'+
                  '<td><input type="hidden" name="racikan1[]" value="" class="input-mini barisracik1 cleared"></td>'+
                                '</tr>');
    
    $("#bodyinput tr:last input[name='nama_obat[]']").focus();


        $(function() {
            $( "#tesxx" ).draggable();
        });

    $('.barisjumlah').change(function(){ //di dlm function tambah baris
           var totalpenjualan=0;  var total1=0;
            var racik=$('#adm_racik').val();
            var admracik=$('#admracik').val();
            var jasa_medis=0;
            var jasa_adm=0;
            var jasa_kartu=0;
             //alert($('#jasa_medis').is(':checked'));
            if($('#jasa_medis').is(':checked')){
                jasa_medis=parseFloat($('#nilaijasamedis').val());
                jasa_adm=parseFloat($('#nilaibiayaadm').val());
                jasa_kartu=parseFloat($('#nilaibiayakartu').val());
            }else{
                jasa_medis=0;
                jasa_adm=0;
                jasa_kartu=0;
            }
            if(racik=='') racik=0;
            //if(jasa_medis=='') jasa_medis=0;
            $('.barisjumlah').each(function(){
                var val=$(this).val(); //ngambil jumlah
                if(val=='')val=0;
                    totalpenjualan=totalpenjualan+parseFloat(val); //buat totalin perbarisnya
                    //total1=parseFloat(racik)+parseFloat(totalpenjualan);
                    total1=parseFloat(totalpenjualan);
            });
            total1=parseFloat(jasa_adm)+parseFloat(jasa_kartu)+parseFloat(jasa_medis)+parseFloat(total1)+parseFloat(admracik);

           $('#jumlahapprove').val(total1);
           $('#totalpenjualan').val(number_format(total1.toFixed(2),2,',','.'));
           $('#total_transaksi').val(total1.toFixed(2));
           $('#total_bayar').val(total1.toFixed(2));

        });
    
    $('.barisqty').change(function(){  
      var val=$(this).val(); var total=0;
      var harga=$('.focused').find('.barisharga').val();
      var resep=$('.focused').find('.barisresep').val();
      if(val=='') val=0;
      if(harga=='')harga=0;
      if(resep=='')resep=0; 
      total=(parseFloat(val)*parseFloat(harga))+parseFloat(resep);
      $('.focused').find('.barisjumlah').val(total.toFixed(2)); 
      $('.barisjumlah').trigger('change');
    });

        $('.barisqty').keyup(function(e){
            if(e.keyCode == 13){
                //Mousetrap.trigger('ctrl+b');
                $("html, body").animate({
                 scrollTop:$("#pembayaranform").height()+50
                 },"slow");            
            }

        });

    $('.barisresep').change(function(){  
      var val=$(this).val(); var total=0;
      var harga=$('.focused').find('.barisharga').val();
      var qty=$('.focused').find('.barisqty').val();
      if(val=='')val=0;
      if(harga=='')harga=0;
      if(qty=='')qty=0;
      total=(parseFloat(qty)*parseFloat(harga))+parseFloat(val);
      $('.focused').find('.barisjumlah').val(total.toFixed(2)); 
      $('.barisjumlah').trigger('change');
    });
    
    $('.barisracik').change(function(){  
      var val=$(this).val(); //ngambil barisracik
      var total=0; 
      var harga=$('.focused').find('.barisharga').val();
      var qty=$('.focused').find('.barisqty').val();
      var jasabungkus=$('#jasabungkus').val();
      //var admracik=$('#adm_racik').val();
      if(val=='')val=0;
      if(harga=='')harga=0;
      if(qty=='')qty=0;
      if(jasabungkus=='')jasabungkus=0;
      var racik=parseFloat(val)*parseFloat(jasabungkus);
      total=parseFloat(qty)*parseFloat(harga);//+parseFloat(val);
      $('.focused').find('.barisjumlah').val(total.toFixed(2));
      $('.focused').find('.barisracik1').val(racik.toFixed(2));
      totaltransaksi();
      $('.barisjumlah').trigger('change');
    });
    
    $('.barisqty, .barisracik, .barisresep, .barisnamaobat, .bariskodeobat').click(function(){  
                $('#bodyinput tr').removeClass('focused'); //bwt ngilangin fokus sebelumnya,,klo misalnya mw update qty
                $(this).parent().parent('tr').addClass('focused'); //bwt ngilangin fokus sebelumnya,,klo misalnya mw update qty
    }); 
    

        $('.barisharga').keyup(function(e){
            if(e.keyCode == 13){
                $('.focused').find('.barisqty').focus();
                return false;
            }
        });

        $('.barisracik').keyup(function(e){
            if(e.keyCode == 13){
                $('.focused').find('.barisresep').focus();
                return false;
            }
        });

        $('.barisresep').keyup(function(e){
            if(e.keyCode == 13){ //klo enter di baris jumlah
                //alert('xx')
                $('#tambahbaris').trigger('click');
                throw 'stop';
                return false;
            }
        });

        $('.barisnamaobat').keyup(function(e){
            if(e.keyCode == 13){
                //$('.focused').find('.barisracik').focus();
                return false;
            }
        });

    $('.barisjumlah').keyup(function(e){ 
            if(e.keyCode == 13){ //klo enter di baris jumlah
                //alert('xx')
                $('#tambahbaris').trigger('click');
                return false;
            }
        });
    
    $('.barisqty').keyup(function(e){ 
            if(e.keyCode == 13){
                $('.focused').find('.barisresep').focus();
                return false;
            }
        });

    $('.bariskodeobat').keyup(function(e){

            if(e.keyCode == 13){
                //alert('xx')
                $('.bariskodeobat').parent().parent('tr').removeClass('focused');
                $(this).parent().parent('tr').addClass('focused');

                $("#dataTable5").dataTable().fnDestroy();
                $('#dataTable5').dataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": "<?php echo base_url() ?>index.php/transapotek/penjualan/ambildaftarobatbykode/",
          "sServerMethod": "POST",
                    "fnServerParams": function ( aoData ) {
                      aoData.push( { "name": "kd_obat", "value":""+$('.focused').find('.bariskodeobat').val()+""} );
                    }
                    
                } );
        $('#dataTable5').css('width','100%');
                $('#daftarobat').modal("show");
                    var check = function(){
                        if($('#listobat tr').length >0 && !$('#listobat tr').hasClass('ui-selected')){
                            // run when condition is met
                            $('#listobat tr:first').addClass('ui-selected');
                        }
                        else {
                            setTimeout(check, 1000); // check again in a second
                        }
                    }
                    check();     
            }
        });
    
    $('.barisnamaobat').keyup(function(e){


           // if(e.keyCode == 13){
                //alert('xx')
                $('.barisnamaobat').parent().parent('tr').removeClass('focused');
                $(this).parent().parent('tr').addClass('focused');

                $("#dataTable6").dataTable().fnDestroy();
                $('#dataTable6').dataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": "<?php echo base_url() ?>index.php/transapotek/penjualan/ambildaftarobatbynama/",
          "sServerMethod": "POST",
                    "fnServerParams": function ( aoData ) {
                      aoData.push( { "name": "nama_obat", "value":""+$('.focused').find('.barisnamaobat').val()+""} );
                    }
                    
                } );
        $('#dataTable6').css('width','100%');
                //$('#daftarobat').modal("show");
                    var check = function(){
                        if($('#listobat6 tr').length >0 && !$('#listobat6 tr').hasClass('ui-selected')){
                            // run when condition is met
                            $('#listobat6 tr:first').addClass('ui-selected');
                        }
                        else {
                            setTimeout(check, 1000); // check again in a second
                        }
                    }
                    check();     
            //}
            var xw=$(this);
            if(xw.val()!=""){
                $('#tesxx').show();
                
                function position() {
                    $( "#tesxx" ).position({
                        of: xw,
                        my: "left top",
                        at: "left bottom",
                        collision:"fit fit"
                        });
                }
                //$( "#tesxx" ).css( "opacity", 0.5 );
               // $( "select, input" ).bind( "click keyup change", position );
                //$( "#parent" ).draggable({
                //    drag: position
                //});
                position();

            }

        });
  }); //akhir function tambah baris
  
  $('#hapusbaris').click(function(){
         $('.barisinput:checked').parents('tr').remove();
      totaltransaksi();
           var totalpenjualan=0;  var total1=0;
            var racik=$('#adm_racik').val();
            var jasa_medis=0;
            var jasa_adm=0;
            var jasa_kartu=0;
             //alert($('#jasa_medis').is(':checked'));
            if($('#jasa_medis').is(':checked')){
                jasa_medis=parseFloat($('#nilaijasamedis').val());
                jasa_adm=parseFloat($('#nilaibiayaadm').val());
                jasa_kartu=parseFloat($('#nilaibiayakartu').val());
            }else{
                jasa_medis=0;
                jasa_adm=0;
                jasa_kartu=0;
            }
            if(racik=='') racik=0;
            //if(jasa_medis=='') jasa_medis=0;
            $('.barisjumlah').each(function(){
                var val=$(this).val(); //ngambil jumlah
                if(val=='')val=0;
                    totalpenjualan=totalpenjualan+parseFloat(val); //buat totalin perbarisnya
                    //total1=parseFloat(racik)+parseFloat(totalpenjualan);
                    total1=parseFloat(totalpenjualan);
            });
            total1=parseFloat(jasa_adm)+parseFloat(jasa_kartu)+parseFloat(jasa_medis)+parseFloat(total1);

           $('#jumlahapprove').val(total1);
           $('#totalpenjualan').val(number_format(total1.toFixed(2),2,',','.'));
           $('#total_transaksi').val(total1.toFixed(2));
           $('#total_bayar').val(total1.toFixed(2));    });
  
  $('#hapusbayar').click(function(){
    //$('.bariscekbok:checked').parents('tr').remove();
    $('.bariscekbok').parents('tr').remove();
    $.ajax({
      url: '<?php echo base_url() ?>index.php/transapotek/penjualan/hapuspembayaran/',
      async:false,
      type:'post',
      data:{query:$('#no_penjualan').val(),query1:$('#no_pendaftaran').val()},
      success:function(data){       
        alert('Pembayaran lama berhasil dihapus');
        location.reload();
      },
      dataType:'json'                         
    });  
    });
  
  $('input').live('keydown', function(e) {
            if(e.keyCode == 13){
                return false;                                    
            }
        });
  
  $(window).bind('scroll', function() {
        var p = $(window).scrollTop();
        var offset = $(window).offset();
        if(parseInt(p)>1){            
            $('.top').addClass('fixed');
        }else{
            $('.top').removeClass('fixed');
        }
        //if ($(window).scrollTop() + $(window).height() >= $('.top').height()) {
        //  $('.top').addClass('fixed');
        //}
    });

  $('.barisjumlah').change(function(){ //di dlm function tambah baris
           var totalpenjualan=0;  var total1=0;
            var racik=$('#adm_racik').val();
            var admracik=$('#admracik').val();
            var jasa_medis=0;
            var jasa_adm=0;
            var jasa_kartu=0;
             //alert($('#jasa_medis').is(':checked'));
            if($('#jasa_medis').is(':checked')){
                jasa_medis=parseFloat($('#nilaijasamedis').val());
                jasa_adm=parseFloat($('#nilaibiayaadm').val());
                jasa_kartu=parseFloat($('#nilaibiayakartu').val());
            }else{
                jasa_medis=0;
                jasa_adm=0;
                jasa_kartu=0;
            }
            if(racik=='') racik=0;
            //if(jasa_medis=='') jasa_medis=0;
            $('.barisjumlah').each(function(){
                var val=$(this).val(); //ngambil jumlah
                if(val=='')val=0;
                    totalpenjualan=totalpenjualan+parseFloat(val); //buat totalin perbarisnya
                    //total1=parseFloat(racik)+parseFloat(totalpenjualan);
                    total1=parseFloat(totalpenjualan);
            });
            total1=parseFloat(jasa_adm)+parseFloat(jasa_kartu)+parseFloat(jasa_medis)+parseFloat(total1)+parseFloat(admracik);

           $('#jumlahapprove').val(total1);
           $('#totalpenjualan').val(number_format(total1.toFixed(2),2,',','.'));
           $('#total_transaksi').val(total1.toFixed(2));
           $('#total_bayar').val(total1.toFixed(2));    });
  
  $('.barisjumlah').keyup(function(e){ 
    if(e.keyCode == 13){ //klo enter di baris jumlah
      //alert('xx')
      $('#tambahbaris').trigger('click');
      return false;
    }
  });
  
  $('.barisqty').keyup(function(e){ 
            if(e.keyCode == 13){ //klo enter di baris jumlah
                //alert('xx')
                $('#tambahbaris').trigger('click');
                return false;
            }
        });
    
  $('.barisqty').change(function(){  
    var val=$(this).val(); var total=0;
    var harga=$('.focused').find('.barisharga').val();
    var resep=$('.focused').find('.barisresep').val();
    if(val=='') val=0;
    if(harga=='')harga=0;
    if(resep=='')resep=0;
    total=(parseFloat(val)*parseFloat(harga))+parseFloat(resep);
    $('.focused').find('.barisjumlah').val(total.toFixed(2)); 
    $('.barisjumlah').trigger('change');
  });
  
  $('.barisresep').change(function(){  
    var val=$(this).val(); var total=0;
    var harga=$('.focused').find('.barisharga').val();
    var qty=$('.focused').find('.barisqty').val();
    if(val=='')val=0;
    if(harga=='')harga=0;
    if(qty=='')qty=0;
    total=(parseFloat(qty)*parseFloat(harga))+parseFloat(val);
    $('.focused').find('.barisjumlah').val(total.toFixed(2)); 
    $('.barisjumlah').trigger('change');
  });
  
  $('.barisracik').change(function(){  
    var val=$(this).val(); //ngambil barisracik
    var total=0; 
    var harga=$('.focused').find('.barisharga').val();
    var qty=$('.focused').find('.barisqty').val();
    var jasabungkus=$('#jasabungkus').val();
    //var admracik=$('#adm_racik').val();
    if(val=='')val=0;
    if(harga=='')harga=0;
    if(qty=='')qty=0;
    if(jasabungkus=='')jasabungkus=0;
    var racik=parseFloat(val)*parseFloat(jasabungkus);
    total=parseFloat(qty)*parseFloat(harga);//+parseFloat(val);
    $('.focused').find('.barisjumlah').val(total.toFixed(2));
    $('.focused').find('.barisracik1').val(racik);
    totaltransaksi();
    $('.barisjumlah').trigger('change');
  });
    
  $('.barisqty, .barisracik, .barisresep, .barisnamaobat, .bariskodeobat').click(function(){  
    $('#bodyinput tr').removeClass('focused'); //bwt ngilangin fokus sebelumnya,,klo misalnya mw update qty
    $(this).parent().parent('tr').addClass('focused'); //bwt ngilangin fokus sebelumnya,,klo misalnya mw update qty
  }); 
  
  $('.bariskodeobat').keyup(function(e){
    if(e.keyCode == 13){
      //alert('xx')
      $('.bariskodeobat').parent().parent('tr').removeClass('focused');
      $(this).parent().parent('tr').addClass('focused');

      $("#dataTable5").dataTable().fnDestroy();
      $('#dataTable5').dataTable( {
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "<?php echo base_url() ?>index.php/transapotek/penjualan/ambildaftarobatbykode/",
        "sServerMethod": "POST",
        "fnServerParams": function ( aoData ) {
          aoData.push( { "name": "kd_obat", "value":""+$('.focused').find('.bariskodeobat').val()+""} );
        }       
      } );
      $('#dataTable5').css('width','100%');
      $('#daftarobat').modal("show");
        var check = function(){
          if($('#listobat tr').length >0 && !$('#listobat tr').hasClass('ui-selected')){
            // run when condition is met
            $('#listobat tr:first').addClass('ui-selected');
          }
          else {
            setTimeout(check, 1000); // check again in a second
          }
        }
        check();     
    }
  });
  
  $('.barisnamaobat').keyup(function(e){
       // if(e.keyCode == 13){
            //alert('xx')
            $('.barisnamaobat').parent().parent('tr').removeClass('focused');
            $(this).parent().parent('tr').addClass('focused');

            $("#dataTable6").dataTable().fnDestroy();
            $('#dataTable6').dataTable( {
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "<?php echo base_url() ?>index.php/transapotek/penjualan/ambildaftarobatbynama/",
                "sServerMethod": "POST",
                "fnServerParams": function ( aoData ) {
                  aoData.push( { "name": "nama_obat", "value":""+$('.focused').find('.barisnamaobat').val()+""} );
                }
                
            } );
            $('#dataTable6').css('width','100%');
            //$('#daftarobat').modal("show");
                var check = function(){
                    if($('#listobat6 tr').length >0 && !$('#listobat6 tr').hasClass('ui-selected')){
                        // run when condition is met
                        $('#listobat6 tr:first').addClass('ui-selected');
                    }
                    else {
                        setTimeout(check, 1000); // check again in a second
                    }
                }
                check();     
        //}
        var xw=$(this);
        if(xw.val()!=""){
            $('#tesxx').show();
            
            function position() {
                $( "#tesxx" ).position({
                    of: xw,
                    my: "left top",
                    at: "left bottom",
                    collision:"fit fit"
                    });
            }
            //$( "#tesxx" ).css( "opacity", 0.5 );
           // $( "select, input" ).bind( "click keyup change", position );
            //$( "#parent" ).draggable({
            //    drag: position
            //});
            position();

        }
  });
  
  $('#nama_pasien1').keyup(function(e){
    //alert('fgf');
        /*
    if(e.keyCode == 13){

            $.ajax({
                url: '<?php echo base_url() ?>index.php/transapotek/penjualan/ambilpasienbynama/',
                async:false,
                type:'get',                                 
                data:{query:$(this).val(),tes:$('.uniform:checked').val(),dor:$('#tgl_entry').val()},
                success:function(data){
                //typeahead.process(data)
          $('#listpasien').empty();
          $.each(data,function(i,l){
            //alert(l.kd_unit_kerja1);
            $('#listpasien').append('<tr><td>'+l.kd_pasien+'</td><td>'+l.no_pendaftaran+'</td><td>'+l.nama_pasien+'</td><td>'+l.tgl_pendaftaran+'</td><td>'+l.nama_unit_kerja+'</td><td><a class="btn" onclick=\'pilihpasien("'+l.kd_pasien+'","'+l.no_pendaftaran+'","'+l.nama_pasien+'","'+l.cust_code+'","'+l.kd_dokter+'","'+l.dokter+'","'+l.tgl_pendaftaran+'","'+l.kd_unit_kerja+'")\'>Pilih</a></td></tr>');
          }); 
                },
                dataType:'json'                         
            }); 
            $('#daftarpasien').modal("show");
      $('#nama_pasien2').focus();
      //return false;
    }
    var ex = document.getElementById('dataTable4');
        if ( ! $.fn.DataTable.fnIsDataTable( ex ) ) {
            $('#dataTable4').dataTable({
                "sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
        "sPaginationType": "bootstrap",
                "oLanguage": {
          "sLengthMenu": "Show _MENU_ entries"
                }
            });
            var oTable = $('#dataTable4').dataTable();
            $('#nama_pasien1').keyup(function(e){
        oTable.fnFilter( $(this).val() );
                if(e.keyCode == 13){
                    //alert('xx')
                    return false;
                }
            });
        }
        */
  });

  $("#rd").click(function(){
    $('#nama_pasien2').trigger("keydown");
  })
  $("#rj").click(function(){
    $('#nama_pasien2').trigger("keydown");
  })
  $("#ri").click(function(){
    $('#nama_pasien2').trigger("keydown");
  })
  
  $('#nama_pasien2').keydown(function(e){
      //alert('fgf');
            $.ajax({
                url: '<?php echo base_url() ?>index.php/transapotek/penjualan/ambilpasienbynama/',
                async:false,
                type:'get',
        data:{query:$(this).val(),tes:$('.uniform:checked').val(),dor:$('#tgl_penjualan').val()},
                //data:{query:$(this).val(),tes:$('.uniform:checked').val()},
                success:function(data){
                //typeahead.process(data)
          $('#listpasien').empty();
          $.each(data,function(i,l){
            //alert(l.kd_unit_kerja1);
            //$('#listpasien').append('<tr><td>'+l.kd_pasien+'</td><td>'+l.no_pendaftaran+'</td><td>'+l.nama_pasien+'</td><td>'+l.tgl_pendaftaran+'</td><td>'+l.nama_unit_kerja+'</td><td><a class="btn" onclick=\'pilihpasien("'+l.kd_pasien+'","'+l.no_pendaftaran+'","'+l.nama_pasien+'","'+l.cust_code+'","'+l.type+'","'+l.kd_dokter+'","'+l.dokter+'","'+l.tgl_pendaftaran+'","'+l.kd_unit_kerja+'")\'>Pilih</a></td></tr>');
            $('#listpasien').append('<tr><td><input type="radio" class="barisradiopasien" name="barisradiopasien"></td><td>'+l.kd_pasien+'</td><td>'+l.no_pendaftaran+'</td><td>'+l.nama_pasien+'</td><td>'+l.customer+'</td><td>'+l.tgl_pendaftaran+'</td><td>'+l.nama_unit_kerja+'</td><td>'+l.dokter+'</td><td>'+l.tgl_lahir+'</td><td><a class="btn" onclick=\'pilihpasien("'+l.kd_pasien+'","'+l.no_pendaftaran+'","'+l.nama_pasien+'","'+l.cust_code+'","'+l.type+'","'+l.kd_dokter+'","'+l.dokter+'","'+l.tgl_pendaftaran+'","'+l.kd_unit_kerja+'")\'>Pilih</a></td></tr>');
          });   

                        $('.barisradiopasien').keyup(function(e){
                          if(e.keyCode == 13){
                            $(this).parent().siblings('td:last').find('.btn').trigger('click');
                          }
                        });

                        $('.barisradiopasien').focus(function(){
                          $(this).parent().siblings('td').addClass('warnatabel');
                        });

                        $('.barisradiopasien').blur(function(){
                          $(this).parent().siblings('td').removeClass('warnatabel');
                        });


                },
                dataType:'json'                         
            }); 
           // $('#daftarpasien').modal("show");
      //return false;

  });
  
  $('#kd_dokter').keyup(function(e){
        /*
    if(e.keyCode == 13){
            $.ajax({
                url: '<?php echo base_url() ?>index.php/transapotek/penjualan/ambildokterbykode/',
                async:false,
                type:'get',
                data:{query:$(this).val()},
                success:function(data){
                //typeahead.process(data)
          $('#listdokter').empty();
          $.each(data,function(i,l){
            //alert(l);
            $('#listdokter').append('<tr><td>'+l.kd_dokter+'</td><td>'+l.dokter+'</td><td><a class="btn" onclick=\'pilihdokter("'+l.kd_dokter+'","'+l.dokter+'")\'>Pilih</a></td></tr>');
          });    
                },
                dataType:'json'                         
            }); 
            $('#daftardokter').modal("show");
    }
    var ex = document.getElementById('dataTable3');
        if ( ! $.fn.DataTable.fnIsDataTable( ex ) ) {
            $('#dataTable3').dataTable({
                "sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
        "sPaginationType": "bootstrap",
                "oLanguage": {
          "sLengthMenu": "Show _MENU_ entries"
                }
            });
            var oTable = $('#dataTable3').dataTable();
            $('#nama_dokter1').keyup(function(e){
        oTable.fnFilter( $(this).val() );
                if(e.keyCode == 13){
                    //alert('xx')
                    return false;
                }
            });
        }
        */
  });
  
  $('#nama_dokter').keyup(function(e){
        if(e.keyCode == 13){
            $('#admracik').focus();         
        }

  });

  $('#admracik').keyup(function(e){
        if(e.keyCode == 13){
            Mousetrap.trigger('ctrl+b');
            $("html, body").animate({
             scrollTop:$("#totalpenjualan").height()+50
             },"slow");            
        }

  });

  
  function totaltransaksi(){
    var totalracik=0; var total1=0;
    $('.barisracik1').each(function(){
      var val=$(this).val();
      if(val=='')val=0;
            totalracik=totalracik+parseFloat(val);
        });
    var total=$('#totalpenjualan').val();
    total1=total1+totalracik;
       $('#total_transaksi').val(total1.toFixed(2));
     $('#total_bayar').val(total1.toFixed(2));
       $('#adm_racik').val(totalracik);
       $('#jumlah').val(total1.toFixed(2));
    }
  
  $('input[type="text"]').keydown(function(e){
        //get the next index of text input element
        var next_idx = $('input[type="text"]').index(this) + 1;
         
        //get number of text input element in a html document
        var tot_idx = $('body').find('input[type="text"]').length;
     
        //enter button in ASCII code
        if(e.keyCode == 13){
            //go to the next text input element
            if($(this).parent()[0]['nodeName']=='TD'){
                return false;
            }
            //$('input[type=text]:eq(' + next_idx + ')').focus();
            //$(this).next().focus();
            return false;


        }

    });
  
  var opts = {
      lines: 9, // The number of lines to draw
      length: 40, // The length of each line
      width: 9, // The line thickness
      radius: 0, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#000', // #rgb or #rrggbb
      speed: 1.4, // Rounds per second
      trail: 54, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: 'auto', // Top position relative to parent in px
      left: '470px' // Left position relative to parent in px
    };
    var target = document.getElementById('progress');
    var spinner = new Spinner(opts).spin(target); 

$(document).ready(function() {
    $("#daftarobat").on("show", function () {
      $("body").addClass("modal-open");

    }).on("hidden", function () {
        if($('#daftarobat').is(':visible')){
            $("body").addClass("modal-open");
        }else{
            $("body").removeClass("modal-open");
        }
    });

    $("#daftardokter").on("show", function () {
      $("body").addClass("modal-open");

    }).on("hidden", function () {
        if($('#daftardokter').is(':visible')){
            $("body").addClass("modal-open");
        }else{
            $("body").removeClass("modal-open");
            $('body').animate({
                scrollTop: $("#nama_dokter").height()
            });
        }
    });

}); 

/*$(document).ready(function() {
    $("#daftardokter").on("show", function () {
      $("body").addClass("modal-open");

    }).on("hidden", function () {
        if($('#daftardokter').is(':visible')){
            $("body").addClass("modal-open");
        }else{
            $("body").removeClass("modal-open");
        }
    });

});  */   

$(function(){
$('input,select').bind('keyup', function(eInner) {
if (eInner.keyCode == 13)
{
var el = null; // element to jump to
// if we have a tabindex, just jump to the next tabindex
var tabindex = $(this).attr('tabindex');
if (tabindex) {
tabindex ++;
if ($('[tabindex=' + tabindex + ']')) {
el = $('[tabindex=' + tabindex + ']');  
}
}
if (el == null) { // just take the next one
var i = $(":input").index(this)*1;
i++;
el = $(":input:eq(" + i + ")");
if (!el) el = null;
}
if (el == null || el.attr('type') == "submit") {
return true;    
}
 
if (el != null) {
el.focus(); 
}
return false;
}
}); 
 
});
</script>


    <script type="text/javascript">
        var wcppGetPrintersDelay_ms = 5000; //5 sec

        function wcpGetPrintersOnSuccess(){
            // Display client installed printers
            if(arguments[0].length > 0){
                var p=arguments[0].split("|");
                var options = '';
                for (var i = 0; i < p.length; i++) {
                    options += '<option>' + p[i] + '</option>';
                }
                $('#installedPrinters').css('visibility','visible');
                $('#installedPrinterName').html(options);
                $('#installedPrinterName').focus();
                $('#loadPrinters').hide();                                                        
            }else{
                alert("No printers are installed in your system.");
            }
        }

        function wcpGetPrintersOnFailure() {
            // Do something if printers cannot be got from the client
            alert("No printers are installed in your system.");
        }
    </script>

    <?php
    //Specify the ABSOLUTE URL to the php file that will create the ClientPrintJob object
    //In this case, this same page
    echo WebClientPrint::createScript(Utils::getRoot().'/billapotek.php')
    ?>
