<style type="text/css">
.fixed {
    position:fixed;
    top:0px !important;
    z-index:100;
    width: 1024px;    
}
.body1{
    opacity: 0.4;
    background-color: #000000;
}
body.modal-open {
    overflow: hidden;
}

</style>
<style>
#feedback { font-size: 1.4em; }
#listobat .ui-selecting, #listobat .ui-selecting { background: #FECA40; }
#listobat .ui-selected, #listobat .ui-selected { background: #F39814; color: white; }
#listobat, #listobat { list-style-type: none; margin: 0; padding: 0; width: 60%; }
#listobat li, #listobat li { margin: 3px; padding: 0.4em; font-size: 1.4em; height: 18px; }
</style>
<script>

    function number_format(number, decimals, dec_point, thousands_sep) {
  //  discuss at: http://phpjs.org/functions/number_format/
  // original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: davook
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Theriault
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Michael White (http://getsprink.com)
  // bugfixed by: Benjamin Lupton
  // bugfixed by: Allan Jensen (http://www.winternet.no)
  // bugfixed by: Howard Yeend
  // bugfixed by: Diogo Resende
  // bugfixed by: Rival
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  //  revised by: Luke Smith (http://lucassmith.name)
  //    input by: Kheang Hok Chin (http://www.distantia.ca/)
  //    input by: Jay Klehr
  //    input by: Amir Habibi (http://www.residence-mixte.com/)
  //    input by: Amirouche
  //   example 1: number_format(1234.56);
  //   returns 1: '1,235'
  //   example 2: number_format(1234.56, 2, ',', ' ');
  //   returns 2: '1 234,56'
  //   example 3: number_format(1234.5678, 2, '.', '');
  //   returns 3: '1234.57'
  //   example 4: number_format(67, 2, ',', '.');
  //   returns 4: '67,00'
  //   example 5: number_format(1000);
  //   returns 5: '1,000'
  //   example 6: number_format(67.311, 2);
  //   returns 6: '67.31'
  //   example 7: number_format(1000.55, 1);
  //   returns 7: '1,000.6'
  //   example 8: number_format(67000, 5, ',', '.');
  //   returns 8: '67.000,00000'
  //   example 9: number_format(0.9, 0);
  //   returns 9: '1'
  //  example 10: number_format('1.20', 2);
  //  returns 10: '1.20'
  //  example 11: number_format('1.20', 4);
  //  returns 11: '1.2000'
  //  example 12: number_format('1.2000', 3);
  //  returns 12: '1.200'
  //  example 13: number_format('1 000,50', 2, '.', ' ');
  //  returns 13: '100 050.00'
  //  example 14: number_format(1e-8, 8, '.', '');
  //  returns 14: '0.00000001'

  number = (number + '')
    .replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}



$(function() {
    $( "#listobat" ).selectable({});
});

function SelectSelectableElement (selectableContainer, elementsToSelect)
{
    // add unselecting class to all elements in the styleboard canvas except the ones to select
    $(".ui-selected", selectableContainer).not(elementsToSelect).removeClass("ui-selected").addClass("ui-unselecting");
    
    // add ui-selecting class to the elements to select
    $(elementsToSelect).not(".ui-selected").addClass("ui-selected");

    // trigger the mouse stop event (this will select all .ui-selecting elements, and deselect all .ui-unselecting elements)
    selectableContainer.selectable('refresh');
    //selectableContainer.data("selectable")._mouseStop(null);
    //return false;
}
</script>

<script src="<?php echo base_url(); ?>assets/js/mousetrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mousetrap-global-bind.min.js"></script>  
<script type="text/javascript">
	Mousetrap.bindGlobal('ctrl+r', function() { window.location.href='<?php echo base_url() ?>index.php/transapotek/aptpengajuan/tambahpengajuan'; return false;});
	//Mousetrap.bindGlobal('f4', function() { window.location.href='<?php echo base_url() ?>index.php/transapotek/aptpemesanan'; return false;});
	
	Mousetrap.bindGlobal('ctrl+s', function() { 
		$('#simpan').trigger('click');
		return false;
	});
	
	Mousetrap.bind(['up','left'], function() {
        //$('#selectable').next('tr').trigger('click');

        if($('#daftarobat').is(':visible')){ 
            if($("#listobat .ui-selected").prev().is('tr')){
				$('#modal-body-daftarobat').scrollTop($('#modal-body-daftarobat').scrollTop()-45);
				SelectSelectableElement($("#listobat"), $(".ui-selected").prev('tr'));
            }else{
                return false;
            }
        }
        $('.focused').prev('tr').find('.barisqtyb').focus().trigger('click');
    });

    Mousetrap.bind(['down','right'], function() {

        if($('#daftarobat').is(':visible')){    
            if($("#listobat .ui-selected").next().is('tr')){
				$('#modal-body-daftarobat').scrollTop($('#modal-body-daftarobat').scrollTop()+45);
				SelectSelectableElement($("#listobat"), $(".ui-selected").next('tr'));
            }else{
                return false;
            }
        }
        $('.focused').next('tr').find('.barisqtyb').focus().trigger('click');
    });

    Mousetrap.bind('enter', function(e) { 

        if($('#daftarobat').is(':visible')){
            $('.ui-selected').find('.btn').trigger('click');
            return false;
        }
        return false;
    });
	
	Mousetrap.bindGlobal('ctrl+b', function() { 
		$('#tambahbaris').trigger('click');
		return false;
	});
	
	Mousetrap.bindGlobal('ctrl+l', function() { 
		$('#pencarian').modal("show");
		return false;
	});
	
	Mousetrap.bindGlobal('ctrl+d', function() { 
		$.ajax({
			url: '<?php echo base_url() ?>index.php/transapotek/aptpengajuan/ambilsupplierbynama/',
			async:false,
			type:'get',
			data:{query:$("#nama").val()},
			success:function(data){
			//typeahead.process(data)
				$('#listsupplier').empty();
				$.each(data,function(i,l){
					//alert(l);
					$('#listsupplier').append('<tr><td>'+l.kd_supplier+'</td><td>'+l.nama+'</td><td>'+l.alamat+'</td><td><a class="btn" onclick=\'pilihsupplier("'+l.kd_supplier+'","'+l.nama+'")\'>Pilih</a></td></tr>');
				}); 
			},
			dataType:'json'                         
		});
		$('#daftarsupplier').modal("show");
		var ex = document.getElementById('dataTable5');
		if ( ! $.fn.DataTable.fnIsDataTable( ex ) ) {
			$('#dataTable5').dataTable({
				"sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "Show _MENU_ entries"
				}
			});
			var oTable = $('#dataTable5').dataTable();
			$('#nama1').keyup(function(e){
				oTable.fnFilter( $(this).val() );
				if(e.keyCode == 13){
					//alert('xx')
					return false;
				}
			});
		};
		return false;
	});
	
</script>

<script type="text/javascript">

    $(document).ready(function() {
		$('#kd_supplier').focus();
        var totalpengajuan=0;
        $('.barisjumlah').each(function(){
                var val=$(this).val();
                if(val=='')val=0;
                totalpengajuan=totalpengajuan+parseFloat(val);
        });
        $('#jumlahapprove').val(totalpengajuan);
       $('#totalpengajuan').val(number_format(totalpengajuan.toFixed(2),2,',','.'));

        $('#form').ajaxForm({
            beforeSubmit: function(a,f,o) {
                o.dataType = "json";
                $('div.error').removeClass('error');
                $('span.help-inline').html('');
                $('#progress').show();
                $('body').append('<div id="overlay1" style="position: fixed;height: 100%;width: 100%;z-index: 1000000;"></div>');
                $('body').addClass('body1');
                var urlnya="<?php echo base_url(); ?>index.php/transapotek/aptpengajuan/periksapengajuan"; //buat validasi inputan
                //console.log($.param(a));
                //console.log($('#form').serialize());
                z=true;
                $.ajax({
                url: urlnya,
                type:"POST",
                async: false,
                data: $.param(a),
                success: function(data){
                    //alert(data.status);
                    if(parseInt(data.status)==1){
                        z=data.status;
                        //alert('aa');
                        //alert($('input[name="harga"]').val());
                    }else if(parseInt(data.status)==0){
                        //alert('xxx');
                        $('#progress').hide();
                        z=data.status;
                        for(yangerror=0;yangerror<=data.error;yangerror++){
                            $('#'+data.id[yangerror]).siblings('.help-inline').html('<p class="text-error">'+data.pesan[yangerror]+'</p>');
                            $('#'+data.id[yangerror]).parents('row-fluid').focus();
                            //$('#error').html('<div class="alert alert-error fade in"><button data-dismiss="alert" class="close" type="button"><i class="iconic-x"></i></button>Terdapat beberapa kesalahan input silahkan cek inputan anda</div>');                                 
                        }
                        $('#error').html('<div class="alert alert-error fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesanatas+'<br/>'+data.pesanlain+'</div>');
                        if(parseInt(data.clearform)==1){
                            //$('#form').resetForm();
                            $('input').live('keydown', function(e) {
                                if(e.keyCode == 13){
                                    return false;                                    
                                }
                            });

                            $('#form .cleared').clearFields();
                        }
                        $('#overlay1').remove();
                        $('body').removeClass('body1');
                    }
                },
                dataType: 'json'
                });

                if(z==0)return false;
            },
            dataType:  'json',
            success: function(data) {
            //alert(data);
            if (typeof data == 'object' && data.nodeType)
            data = elementToString(data.documentElement, true);
            else if (typeof data == 'object')
            //data = objToString(data);
                if(parseInt(data.status)==1) //jika berhasil
                {
                    //apa yang terjadi jika berhasil
                    $('#progress').hide();
                    $('#overlay1').remove();
                    $('body').removeClass('body1');
                    $('#error').show();
                    $('#error').html('<div class="alert alert-success fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesan+'</div>');
                    $('#no_pengajuan').val(data.no_pengajuan);
                    $('#btn-cetak').removeAttr('disabled');
                    $('#btn-cetak').attr('href','<?php echo base_url() ?>third-party/fpdf/buktipengajuan.php?no_pengajuan='+data.no_pengajuan+'');
                    $('#btn-approve').removeAttr('disabled');
                    if(parseInt(data.keluar)>0){
                        window.location.href='<?php echo base_url(); ?>index.php/transapotek/aptpengajuan';
                    }
                    if(parseInt(data.posting)==3){
                       window.location.href='<?php echo base_url(); ?>index.php/transapotek/aptpengajuan/';
                    }  
                    if(parseInt(data.posting)==4){
                       window.location.href='<?php echo base_url(); ?>index.php/transapotek/aptpemesanan/';
                    }  
                }
                else if(parseInt(data.status)==0) //jika gagal
                {
                    //apa yang terjadi jika gagal
                    $('#progress').hide();
                    $('#overlay1').remove();
                    $('body').removeClass('body1');
                    $('#error').show();
                    $('#error').html('<div class="alert alert-success fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesan+'</div>');
                }

            }
        });       

    });


</script>
<style type="text/css">.datepicker{z-index:1151;}</style>
	<div id="error"></div>
    <div id="overlay"></div>


 			<!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
						<form class="form-horizontal"  id="form" method="POST" action="<?php echo base_url() ?>index.php/transapotek/aptpengajuan/simpanpengajuan">
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header class="top" style="">
                                            <div class="icons"><i class="icon-edit"></i></div>
                                            <h5>PENGAJUAN OBAT / ALKES</h5>
                                            <!-- .toolbar -->
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
                                                    <li><a class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>index.php/transapotek/aptpengajuan/"> <i class="icon-list"></i> Daftar </a></li>
													<!--li><a target="_blank" class="btn" id="btn-cetak" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php if(!empty($no_pengajuan)){ echo base_url() ?>third-party/fpdf/buktipengajuan.php?no_pengajuan=<?php echo $no_pengajuan;} else echo '#'; ?>" <?php if(empty($no_pengajuan)){ ?>disabled<?php } ?>> <i class="icon-print"></i> Cetak</a></li-->													                                                    
                                                    <li><a class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px 12px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" href="<?php echo base_url() ?>index.php/transapotek/aptpengajuan/tambahpengajuan"> <i class="icon-plus"></i> Tambah / (Ctrl+R)</a></li>
                                                    <li><a class="btn" style="border-style:solid;border-width:1px;line-height: 21px !important;padding: 4px;border-bottom:1px solid !important;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) #B3B3B3 !important;" data-toggle="modal" data-original-title="Pencarian" data-placement="bottom" rel="tooltip" href="#pencarian"> <i class="icon-search"></i> Pencarian / (Ctrl+L)</a></li>
                                                    <li><button <?php if($this->mpengajuan->isPosted($no_pengajuan)) {echo "disabled";} else {} ?> class="btn" id="simpan" type="submit"  name="submit" value="simpan"> <i class="icon-save"></i> Simpan / (Ctrl+S)</button></li>
                                                    <li><button class="btn" id="buatpesanan" type="submit" name="submit" value="buatpesanan" > <i class="icon-plus"></i> Buat Pesanan</button></li>


                                                    <li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.toolbar -->
                                        </header>
                                        <div id="div-1" class="accordion-body collapse in body">
											<div class="row-fluid">
												<div class="span12">
													<!--div class="span8"-->
														<div class="control-group">
															<label for="no_pengajuan" class="control-label">No. Pengajuan </label>
															<div class="controls with-tooltip">
																<input type="text" name="no_pengajuan" id="no_pengajuan" value="<?php echo $no_pengajuan; ?>" readonly class="span3 input-tooltip" data-original-title="no pengajuan" data-placement="bottom"/>
																<span class="help-inline"></span>
																Tgl. Pengajuan
																<input <?php if($this->mpengajuan->isPosted($no_pengajuan)) {echo "readonly";} else {if($kd_applogin==0) echo "readonly";} ?> type="text" name="tgl_pengajuan" id="tgl_pengajuan" class="input-small input-tooltip cleared" data-original-title="tgl pengajuan" data-mask="99-99-9999" value="<?php if(empty($itemtransaksi['tgl_pengajuan']))echo date('d-m-Y'); else echo convertDate($itemtransaksi['tgl_pengajuan']);  ?>" data-placement="bottom"/>
																<input type="hidden" name="jam_pengajuan" id="jam_pengajuan" value="<?php echo date('h:i:s'); ?>" data-mask="99:99:99" class="input-small"/>
																<input type="hidden" id="jam_pengajuan1" name="jam_pengajuan1" value="<?php if(isset($itemtransaksi['jam_pengajuan']))echo $itemtransaksi['jam_pengajuan'] ?>" class="input-small" data-original-title="jam pengajuan" data-placement="bottom"/>
																<!--input type="hidden" id="kd_applogin" name="kd_applogin" value="<-?php if(isset($applogin['kd_app']))echo $applogin['kd_app'] ?>" class="span3 input-tooltip" data-original-title="kd_applogin" data-placement="bottom"/-->
																<input type="hidden" id="kd_applogin" name="kd_applogin" value="<?php echo $kd_applogin; ?>" class="span1 input-tooltip" data-original-title="kd_applogin" data-placement="bottom"/>
															</div>
														</div>
													<!--/div-->													
												</div>
											</div>
											<!--div class="row-fluid">
												<div class="span12">
													<div class="control-group">
														<label for="tgl_pengajuan" class="control-label">Tgl. Pengajuan </label>
														<div class="controls with-tooltip">
															<input <-?php if($this->mpengajuan->isPosted($no_pengajuan))echo "readonly"; ?> type="text" name="tgl_pengajuan" id="tgl_pengajuan" class="input-small input-tooltip cleared" data-original-title="tgl pengajuan" data-mask="99-99-9999" value="<?php if(empty($itemtransaksi['tgl_pengajuan']))echo date('d-m-Y'); else echo convertDate($itemtransaksi['tgl_pengajuan']);  ?>" data-placement="bottom"/>
															<span class="help-inline"></span>
															<input type="hidden" name="jam_pengajuan" id="jam_pengajuan" value="<-?php echo date('h:i:s'); ?>" data-mask="99:99:99" class="input-small"/>
															<input type="hidden" id="jam_pengajuan1" name="jam_pengajuan1" value="<-?php if(isset($itemtransaksi['jam_pengajuan']))echo $itemtransaksi['jam_pengajuan'] ?>" class="input-small" data-original-title="jam pengajuan" data-placement="bottom"/>
														</div>
													</div>
												</div>
											</div-->
											<div class="row-fluid">
												<div class="span12">
													<!--div class="span8"-->
														<div class="control-group">
															<label for="jenis_sp" class="control-label">Jenis </label>
															<div class="controls with-tooltip">
                                <select name="jenis_sp" id="jenis_sp">
                                  <option value="1">SP Biasa</option>
                                  <option value="2">SP Prekursor</option>
                                  <option value="3">SP Narkotika</option>
                                  <option value="4">SP Psikotropika</option>
                                  <option value="5">SP Lain lain</option>
                                </select>
																<span class="help-inline"></span>																
															</div>
														</div>
													<!--/div-->													
												</div>
												 <div id="progress" style="display:none;"></div>
											</div>																						 																						
                      <div class="row-fluid">
                        <div class="span12">
                          <!--div class="span8"-->
                            <div class="control-group">
                              <label for="keterangan" class="control-label">Keterangan </label>
                              <div class="controls with-tooltip">
                                <input type="text" name="keterangan" id="keterangan" class="input-xxlarge">
                                <span class="help-inline"></span>                               
                              </div>
                            </div>
                          <!--/div-->                         
                        </div>
                         <div id="progress" style="display:none;"></div>
                      </div>                                                                        

                                        </div>
                                    </div>
									
									<div class="row-fluid">
										<div class="span12">
											<div class="box error">
												<header>
												<!-- .toolbar -->
													<div class="toolbar" style="height:auto;float:left;">
														<ul class="nav nav-tabs">
															<li><button <?php if($this->mpengajuan->isPosted($no_pengajuan)) {echo "disabled";} else {} ?> class="btn" type="button" id="tambahbaris"> <i class="icon-plus"></i> Tambah Obat (Ctrl+B)</button></li>
															<li><button <?php if($this->mpengajuan->isPosted($no_pengajuan)) {echo "disabled";} else {} ?> class="btn" type="button" id="hapusbaris"> <i class="icon-remove"></i> Hapus Obat</button></li>
														</ul>
													</div>
												<!-- /.toolbar -->
												</header>
												<div class="body collapse in" id="defaultTable">
													<table class="table responsive">
														<thead>
															<tr style="font-size:80% !important;">
                                <th class="header">&nbsp;</th>
                                <th class="header">Kode Obat</th>
																<th class="header">Nama Obat</th>
                                <th class="header">Distributor</th>
                                <th class="header">Qty Besar</th>                                                 
                                <th class="header">Satuan Besar</th>
                                <th class="header">Qty Kecil</th>                                                 
                                <th class="header">Satuan Kecil</th>
																<th class="header">Harga Beli</th>
																<th class="header">Jumlah </th>
															</tr>
														</thead>
														<tbody id="bodyinput">
															<?php
																if(isset($itemsdetiltransaksi)){
																	//$no=1;
																	foreach ($itemsdetiltransaksi as $itemdetil){																			
																	?>
																		<tr style="font-size:80% !important;">
                                      <td><input <?php if($this->mpengajuan->isPosted($no_pengajuan)) {echo "readonly";} else {} ?> type="checkbox" class="barisinput" /></td>                                                                            
																			
                                      <td><input <?php if($this->mpengajuan->isPosted($no_pengajuan)) {echo "readonly";} else {} ?> type="text" name="kd_obat[]" value="<?php echo $itemdetil['kd_obat'] ?>" class="input-small bariskodeobat cleared"></td>

																			<td><input <?php if($this->mpengajuan->isPosted($no_pengajuan)) {echo "readonly";} else {} ?> type="text" name="nama_obat[]" value="<?php echo $itemdetil['nama_obat'] ?>" style="font-size:90% !important;" class="input-xlarge barisnamaobat cleared"></td>
                                      
                                      <td>
                                        <select name="kd_supplier[]" class="input-medium barissupplier" >
                                          <option value="">--pilih distributor--</option>
                                          <?php
                                          foreach ($supplier as $sup) {
                                          # code...
                                          if($itemdetil['kd_supplier']==$sup['kd_supplier'])$sel="selected=selected";else $sel="";
                                          ?>
                                          <option value="<?php echo $sup['kd_supplier']; ?>" <?php echo $sel; ?>><?php echo $sup['nama']; ?></option>
                                          <?php
                                          }
                                          ?>
                                        </select>
                                      </td>                                      

                                      <td><input <?php if($this->mpengajuan->isPosted($no_pengajuan)) {echo "readonly";} else {} ?> style="text-align:right;" type="text" name="qty_besar[]" value="<?php echo $itemdetil['qty_besar'] ?>" style="font-size:90% !important;" class="input-small barisqtybesar mousetrap cleared"></td>
                                      
                                      <td>
                                        <select name="satuan_besar[]" class="input-small barissatuanbesar cleared">
                                        <?php
                                        $kd_satuan=explode(",", $itemdetil['kd_satuan']);
                                        $nm_satuan=explode(",", $itemdetil['nama_satuan']);
                                        foreach ($kd_satuan as $key => $value) {
                                          # code...
                                          if($value==$itemdetil['satuan_besar'])$selected="selected=selected"; else $selected="";
                                          echo "<option value='".$value."' ".$selected.">".$nm_satuan[$key]."</option>";
                                        }
                                        ?>
                                        </select>
                                      </td>
               
                                      <td><input <?php if($this->mpengajuan->isPosted($no_pengajuan)) {echo "readonly";} else {} ?> style="text-align:right;" type="text" name="qty_kecil[]" value="<?php echo $itemdetil['qty_kecil'] ?>" style="font-size:90% !important;" class="input-small barisqtykecil mousetrap cleared" readonly></td>
                                      
                                      <td><input <?php if($this->mpengajuan->isPosted($no_pengajuan)) {echo "readonly";} else {} ?> type="text" name="satuan_kecil[]" value="<?php echo $itemdetil['satuan_kecil'] ?>" class="input-small barissatuankecil cleared" readonly></td>
               
																			<td><input <?php if($this->mpengajuan->isPosted($no_pengajuan)) {echo "readonly";} else {} ?> style="text-align:right;" type="text" name="harga_beli[]" value="<?php echo number_format($itemdetil['harga_beli'],2,'.','') ?>" style="width:80px;font-size:90% !important" class="input-small barishargabeli cleared"></td>																			
																			<td style="text-align:right;">
                                          <input style="text-align:right;" <?php if($this->mpengajuan->isPosted($no_pengajuan)) {echo "readonly";} else {} ?> type="text" name="jumlah1[]" style="font-size:90% !important;" value="<?php echo number_format($itemdetil['jumlah'],2,',','.') ?>" class="input-medium barisjumlah1 cleared" readonly>                                                                  
                                          <input style="text-align:right;" <?php if($this->mpengajuan->isPosted($no_pengajuan)) {echo "readonly";} else {} ?> type="hidden" name="jumlah[]" style="font-size:90% !important;" value="<?php echo number_format($itemdetil['jumlah'],2,'.','') ?>" class="input-medium barisjumlah cleared" readonly>
                                      </td>                                                                            
																		</tr>
																	<?php
																		//$no++;
																	}
																}
															?>

														</tbody>
														<tfoot>
															<tr>
																<th colspan="10" style="text-align:right;" class="header">Total Pengajuan (Rp) : <input type="text" class="input-medium cleared" id="totalpengajuan" style="text-align:right" disabled></th>
															</tr>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
									</div>
									
									
                            <!--END TEXT INPUT FIELD-->                            
                                            </form>

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->

<div aria-hidden="true" aria-labelledby="helpModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="daftarsupplier" style="display: none;width:60%;margin-left:-400px !important;"> 
    <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">�</button>
        <h3 id="helpModalLabel"><i class="icon-external-link"></i> Daftar Distributor</h3>
    </div>
    <div class="modal-body" style="height:340px;">
        <div class="body" id="collapse4">
			<table id="dataTable5" class="table table-bordered ">
                <thead>
                    <tr>						
                        <th>Kode</th>
                        <th>Nama Distributor</th>
						<th>Alamat</th>
                        <th style="width:50px !important;">Pilihan</th>
                    </tr>
                </thead>
                <tbody id="listsupplier">

                </tbody>
            </table>
        </div>
    </div>
    <div class="modal-footer">
        <input type="text" id="nama1" class="pull-left" autocomplete="off">
        <button aria-hidden="true" data-dismiss="modal" class="btn">Close</button>
    </div>
</div>
			
<div aria-hidden="true" aria-labelledby="helpModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="daftarobat" style="display: none;width:70%;margin-left:-400px !important;"> 
    <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">�</button>
        <h3 id="helpModalLabel"><i class="icon-external-link"></i> Daftar Obat</h3>
    </div>
    <div class="modal-body" id="modal-body-daftarobat" style="height:300px;">
        <div class="body" id="collapse4">
            <table id="dataTable4" class="table table-bordered">
                <thead>
                    <tr>						
                        <th>Kode Obat</th>
                        <th>Nama Obat</th>
                        <th>Satuan Terbesar</th>
                        <th>Satuan terkecil</th>
						<!--th>Stok</th-->                     
                        <th style="width:50px !important;">Pilihan</th>
                    </tr>
                </thead>
                <tbody id="listobat">

                </tbody>
            </table>
        </div>
    </div>
    <div class="modal-footer">
        <input type="text" id="nama_obat" class="pull-left" autocomplete="off">
        <button aria-hidden="true" data-dismiss="modal" class="btn">Close</button>
    </div>
</div>
			
<script type="text/javascript">
    $('.with-tooltip').tooltip({
        selector: ".input-tooltip"
    });
	
	$('#tgl_pengajuan').datepicker({
        format: 'dd-mm-yyyy'
    });
	
	$('#tgl_tempo').datepicker({
        format: 'dd-mm-yyyy'
    });
	
	$('#jam_pengajuan').timepicker({
        minuteStep: 1,
        showSeconds: true,
        showMeridian: false
    });
	
	function pilihsupplier(kd_supplier,nama) {
		$('#kd_supplier').val(kd_supplier);
        $('#nama').val(nama);
		//$('#alamat').val(alamat);
        //$('.focused').find('#alamat').val(alamat);
        $('#daftarsupplier').modal("hide");
        $('#keterangan').focus();
    }

    function totaltransaksi(){
        var totalpengajuan=0; var total1=0;
        $('.barisjumlah').each(function(){
            var val=$(this).val(); 
            if(val=='')val=0;
            totalpengajuan=totalpengajuan+parseFloat(val);             
        });
       $('#jumlahapprove').val(totalpengajuan);
       $('#totalpengajuan').val(number_format(totalpengajuan.toFixed(2),2,',','.'));
    }
    
	//function pilihobat(kd_obat,nama_obat,satuan_kecil,pembanding,harga_beli) {
	function pilihobat(kd_obat1,nama_obat,satuan_besar,satuan_kecil,harga_beli,kd_supplier) {
        var x=0;
        //$('.bariskodeobat').each(function(){
         //   var val=$(this).val(); 
         //   if(val=='')val=0;
         //   if(kd_obat1==val){
               // x=1;
                //alert('obat ini sudah di input'); 
               // return false;
         //   }
        //});
        if(x){
            return false;
        }else{
            $('.focused').find('.bariskodeobat').val(kd_obat1);
            $('.focused').find('.barisnamaobat').val(nama_obat);
            $('.focused').find('.barissatuanbesar').val(satuan_besar);
            $('.focused').find('.barissatuankecil').val(satuan_kecil);
            $('.focused').find('.barissupplier').val(kd_supplier);
            $('.focused').find('.barisqtybesar').val(0);
            $('.focused').find('.barisqtykecil').val(0);
            $('.focused').find('.barishargabeli').val(parseFloat(harga_beli).toFixed(2));
  			   $('.barishargabeli').trigger('change');

            $.ajax({
                url: '<?php echo base_url() ?>index.php/transapotek/aptpengajuan/ambilsatuanobat/',
                async:false,
                type:'GET',
                data:{query:kd_obat1},
                success:function(data){
                  obatsatuan='<select name="satuan_besar[]" class="input-small barissatuanbesar cleared">';
                  $.each(data,function(i,l){

                    obatsatuan+='<option value="'+l.kd_satuan+'">'+l.satuan_kecil+'</option>';

                  });    
                  obatsatuan+='</select>';
                  $('.focused').find('.kolomsatuanbesar').html(obatsatuan);           

                },
                dataType:'json'                         
            });
           

  			   totaltransaksi();
            $('#daftarobat').modal("hide");
            $('.focused').find('.barishargabeli').trigger('change');
            $('.focused').find('input[name="qty_besar[]"]').focus();
            return false;
        }
		return false;
    }


    $('#bodyinput').on('change','.barisqtybesar', function(event) {
      event.preventDefault();
      /* Act on the event */
      kd_obat=$('.focused').find('.bariskodeobat').val();
      satuan_besar=$('.focused').find('.barissatuanbesar').val();
      qtybesar=parseInt($(this).val());
      $.ajax({
          url: '<?php echo base_url() ?>index.php/transapotek/aptpengajuan/ambildetilsatuanobat/',
          async:false,
          type:'GET',
          data:{query:kd_obat,satuan:satuan_besar},
          success:function(data){
            pembanding=parseInt(data.pembanding_terkecil);
            $('.focused').find('.barisqtykecil').val(qtybesar*pembanding);
            $('.focused').find('.barishargabeli').trigger('change');
          },
          dataType:'json'                         
      });      
    });

    $('#bodyinput').on('change','.barissatuanbesar', function(event) {
      event.preventDefault();
      /* Act on the event */
      kd_obat=$('.focused').find('.bariskodeobat').val();
      satuan_besar=$(this).val();
      qtybesar=parseInt($('.focused').find('.barisqtybesar').val());
      $.ajax({
          url: '<?php echo base_url() ?>index.php/transapotek/aptpengajuan/ambildetilsatuanobat/',
          async:false,
          type:'GET',
          data:{query:kd_obat,satuan:satuan_besar},
          success:function(data){
            pembanding=parseInt(data.pembanding_terkecil);
            $('.focused').find('.barisqtykecil').val(qtybesar*pembanding);
            $('.focused').find('.barishargabeli').trigger('change');
          },
          dataType:'json'                         
      });      
    });

    $('#bodyinput').on('change','.barissupplier', function(event) {
      event.preventDefault();
      /* Act on the event */
      kd_obat=$('.focused').find('.bariskodeobat').val();
      distributor=$('.focused').find('.barissupplier').val();

      $.ajax({
          url: '<?php echo base_url() ?>index.php/transapotek/aptpengajuan/ambilhargadistributor/',
          async:false,
          type:'GET',
          data:{query:kd_obat,distributor:distributor},
          success:function(data){
            if(data.kd_obat=='kosong'){
              alert('Belum ada transaksi penerimaan dari distributor ini, harga yang digunakan adalah harga terkecil');
            }
            $('.focused').find('.barishargabeli').val(data.harga_beli);
            $('.focused').find('.barishargabeli').trigger('change');
            $('.focused').find('.barishargabeli').focus();
          },
          dataType:'json'                         
      });      
    });

    $('#bodyinput').on('change','.barishargabeli', function(event) {

      event.preventDefault();
      var val=$(this).val(); //harga beli  
      var qtyk=$('.focused').find('.barisqtykecil').val();
      if(val=='')val=0;
      if(qtyk=='')qtyk=0;
      var total=parseFloat(val)*parseFloat(qtyk);
      $('.focused').find('.barisjumlah1').val(number_format(total.toFixed(2),2,',','.'));
      $('.focused').find('.barisjumlah').val(total.toFixed(2));
      totaltransaksi();

    });

	$('#tambahbaris').click(function(){
        if($('.bariskodeobat').length>0){
			$('.barisnamaobat').each(function(){
                var val=$(this).val(); 
                if(val==''){
                    alert('Nama obat tidak boleh kosong');
                    e.stopImmediatePropagation();
                    return false;
                }
            });

			$('.barishargabeli').each(function(){
                var val=$(this).val(); 
                if(val==''){
                    alert('Harga beli tidak boleh kosong');
                    e.stopImmediatePropagation();
                    return false;
                }
            });
        }

    var tampilsupplier="<select name=\"kd_supplier[]\" class=\"input-medium barissupplier\">"+
                "<option value=''>--pilih distributor--</option>"+
                                "<?php foreach ($supplier as $sup) { ?>"+
                                "<option value=<?php echo $sup['kd_supplier']; ?>><?php echo $sup['nama']; ?></option>"+
                                "<?php } ?>"+
                            "</select>";

		$('#bodyinput').append('<tr style="font-size:95% !important;">'+
                  '<td><input type="checkbox" class="barisinput cleared" /></td>'+
									'<td><input type="text" name="kd_obat[]" value="" class="input-small bariskodeobat cleared"></td>'+
									'<td><input type="text" name="nama_obat[]" value="" style="font-size:95% !important;" class="input-xlarge barisnamaobat cleared"></td>'+
                  '<td>'+tampilsupplier+'</td>'+
                  '<td><input style="text-align:right;" type="text" name="qty_besar[]" value="" style="font-size:95% !important;" class="input-small barisqtybesar mousetrap cleared"></td>'+
                  '<td class="kolomsatuanbesar"><input type="text" name="satuan_besar[]" value="" class="input-small barissatuanbesar cleared" readonly></td>'+
                  '<td><input style="text-align:right;" type="text" name="qty_kecil[]" value="" style="font-size:95% !important;" class="input-small barisqtykecil mousetrap cleared" readonly></td>'+
                  '<td><input type="text" name="satuan_kecil[]" value="" class="input-small barissatuankecil cleared" readonly></td>'+
									'<td><input style="text-align:right;" type="text" name="harga_beli[]" value="" style="width:80px;font-size:95% !important" class="input-small barishargabeli cleared"></td>'+
                  '<td style="text-align:right;"><input style="text-align:right;" style="font-size:95% !important;" type="text" name="jumlah1[]" value="" class="input-medium barisjumlah1 cleared" readonly></td>'+
                  '<td style="text-align:right;"><input style="text-align:right;" style="font-size:95% !important;" type="hidden" name="jumlah[]" value="" class="input-medium barisjumlah cleared" readonly></td>'+
                  '</tr>');
		
		$("#bodyinput tr:last input[name='kd_obat[]']").focus();
        $('#bodyinput tr').removeClass('focused'); 
        $("#bodyinput tr:last input[name='kd_obat[]']").parent().parent('tr').addClass('focused'); 
		
		$('.barisjumlah').change(function(){ 
            var totalpengajuan=0; 
			$('.barisjumlah').each(function(){
                var val=$(this).val();
                if(val=='')val=0;
                totalpengajuan=totalpengajuan+parseFloat(val);				
            });
           $('#jumlahapprove').val(totalpengajuan);
           $('#totalpengajuan').val(number_format(totalpengajuan.toFixed(2),2,',','.'));
        });
		
		
		$('.barisqtybesar, .barissupplier, .barisqtykecil, .barissatuanbesar, .barissatuankecil, .barishargabeli, .barisnamaobat, .baristanggal').click(function(){  
        $('#bodyinput tr').removeClass('focused'); 
        $(this).parent().parent('tr').addClass('focused'); 
        $(this).select();
		})
		
		$('.barishargabeli').change(function(){  
			var val=$(this).val(); //harga beli  
			var qtyk=$('.focused').find('.barisqtykecil').val();
			if(val=='')val=0;
			if(qtyk=='')qtyk=0;
			var total=parseFloat(val)*parseFloat(qtyk);
      $('.focused').find('.barisjumlah1').val(number_format(total.toFixed(2),2,',','.'));
      $('.focused').find('.barisjumlah').val(total.toFixed(2));
			totaltransaksi();
		})
        
        $('.barissupplier').keyup(function(e){  
            if(e.keyCode == 13){ 
                $('.focused').find('input[name="qtybesar[]"]').focus();
                return false;
            }
        });
        
        $('.barissatuanbesar').keyup(function(e){  
            if(e.keyCode == 13){ 
                $('.focused').find('input[name="hargabeli[]"]').focus();
                return false;
            }
        });
        
        $('.barisqtybesar').keyup(function(e){  
            if(e.keyCode == 13){ 
                if($(this).val()!=0)$('.focused').find('input[name="harga_beli[]"]').focus();
                return false;
            }
        });
        
        $('.barishargabeli').keyup(function(e){  
            if(e.keyCode == 13){ 
                $('#tambahbaris').trigger('click');
                return false;
            }
        });
        
		$('.barisnamaobat').keyup(function(e){
            if(e.keyCode == 13){
                //alert('xx')
                $('.barisnamaobat').parent().parent('tr').removeClass('focused');
                $(this).parent().parent('tr').addClass('focused');

                $("#dataTable4").dataTable().fnDestroy();
                $('#dataTable4').dataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": "<?php echo base_url() ?>index.php/transapotek/aptpengajuan/ambildaftarobatbynama/",
					"sServerMethod": "POST",
                    "fnServerParams": function ( aoData ) {
                      aoData.push( { "name": "nama_obat", "value":""+$('.focused').find('.barisnamaobat').val()+""} );
                    }
                    
                } );
				$('#dataTable4').css('width','100%');
                $('#daftarobat').modal("show");
                    var check = function(){
                        if($('#listobat tr').length >0 && !$('#listobat tr').hasClass('ui-selected')){
                            // run when condition is met
                            $('#listobat tr:first').addClass('ui-selected');
                        }
                        else {
                            setTimeout(check, 1000); // check again in a second
                        }
                    }
                    check();     
            }
        });
		
		$('.bariskodeobat').keyup(function(e){
            if(e.keyCode == 13){
                //alert('xx')
                $('.bariskodeobat').parent().parent('tr').removeClass('focused');
                $(this).parent().parent('tr').addClass('focused');

                $("#dataTable4").dataTable().fnDestroy();
                $('#dataTable4').dataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": "<?php echo base_url() ?>index.php/transapotek/aptpengajuan/ambildaftarobatbykode/",
          					"sServerMethod": "POST",
                    "fnServerParams": function ( aoData ) {
                      aoData.push( { "name": "kd_obat", "value":""+$('.focused').find('.bariskodeobat').val()+""} );
                    }
                    
                } );
				$('#dataTable4').css('width','100%');
                $('#daftarobat').modal("show");
                    var check = function(){
                        if($('#listobat tr').length >0 && !$('#listobat tr').hasClass('ui-selected')){
                            // run when condition is met
                            $('#listobat tr:first').addClass('ui-selected');
                        }
                        else {
                            setTimeout(check, 1000); // check again in a second
                        }
                    }
                    check();     
            }
        });
		
	}); //akhir function tambah baris
	
	$('#hapusbaris').click(function(){
         $('.barisinput:checked').parents('tr').remove();
            var totalpengajuan=0; var total1=0;
			
           $('.barisjumlah').each(function(){
                var val=$(this).val();
                if(val=='')val=0;
                totalpengajuan=totalpengajuan+parseFloat(val);				
            });
           $('#jumlahapprove').val(totalpengajuan);
           $('#totalpengajuan').val(number_format(totalpengajuan.toFixed(2),2,',','.'));
    });
	
	$('input').live('keydown', function(e) {
            if(e.keyCode == 13){
                return false;                                    
            }
        });
	
	$(window).bind('scroll', function() {
        var p = $(window).scrollTop();
        var offset = $(window).offset();
        if(parseInt(p)>1){            
            $('.top').addClass('fixed');
        }else{
            $('.top').removeClass('fixed');
        }
        //if ($(window).scrollTop() + $(window).height() >= $('.top').height()) {
        //  $('.top').addClass('fixed');
        //}
    });
	
        $('.barissatuan').keyup(function(e){  
            if(e.keyCode == 13){ 
                $('.focused').find('input[name="qty_box[]"]').focus();
                return false;
            }
        });
        
        $('.barisqtyb').keyup(function(e){  
            if(e.keyCode == 13){ 
                $('.focused').find('input[name="harga_beli[]"]').focus();
                return false;
            }
        });
        
        $('.barishargabeli').keyup(function(e){  
            if(e.keyCode == 13){ 
                $('#tambahbaris').trigger('click');
                return false;
            }
        });
	
		$('.barisjumlah').change(function(){ 
            var totalpengajuan=0; 
			$('.barisjumlah').each(function(){
                var val=$(this).val();
                if(val=='')val=0;
                totalpengajuan=totalpengajuan+parseFloat(val);				
            });
           $('#jumlahapprove').val(totalpengajuan);
           $('#totalpengajuan').val(number_format(totalpengajuan.toFixed(2),2,',','.'));
        });
		
		$('.barisqtyb').change(function(){ 
            var val=$(this).val();  //ngambil qty b            
            var pembanding=$('.focused').find('.barispembanding').val();
            var qtyk=parseFloat(val) * parseFloat(pembanding); //ngupdate qty k
			
			var hargabeli=$('.focused').find('.barishargabeli').val();
			if(val=='')val=0;
			if(hargabeli=='')hargabeli=0;
			var total=parseFloat(val)*parseFloat(hargabeli);
			
            $('.focused').find('.barisjumlah1').val(number_format(total.toFixed(2),2,',','.'));
			$('.focused').find('.barisjumlah').val(total.toFixed(2));			
            $('.focused').find('.barisqtyk').val(qtyk);
            //jumlahharga();
			totaltransaksi();
            $('.focused').find('input[name="harga_beli[]"]').focus();
        });
		
    $('.barisqtybesar, .barissupplier, .barisqtykecil, .barissatuanbesar, .barissatuankecil, .barishargabeli, .barisnamaobat, .baristanggal').click(function(){  
        $('#bodyinput tr').removeClass('focused'); 
        $(this).parent().parent('tr').addClass('focused'); 
        $(this).select();
    })
		
		$('.barishargabeli').change(function(){  
			var val=$(this).val(); //harga beli  
			var qtyk=$('.focused').find('.barisqtyk').val();
			if(val=='')val=0;
			if(qtyk=='')qtyk=0;
			var total=parseFloat(val)*parseFloat(qtyk);
            $('.focused').find('.barisjumlah1').val(number_format(total.toFixed(2),2,',','.'));
			$('.focused').find('.barisjumlah').val(total.toFixed(2));
			totaltransaksi();
		})
	
	$('.bariskodeobat').keydown(function(e){
		if(e.keyCode==13){
			$(this).focus();
			return false;
		}
    });
		
	$('.barisnamaobat').keyup(function(e){
            if(e.keyCode == 13){
                //alert('xx')
                $('.barisnamaobat').parent().parent('tr').removeClass('focused');
                $(this).parent().parent('tr').addClass('focused');

                $("#dataTable4").dataTable().fnDestroy();
                $('#dataTable4').dataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": "<?php echo base_url() ?>index.php/transapotek/aptpengajuan/ambildaftarobatbynama/",
					"sServerMethod": "POST",
                    "fnServerParams": function ( aoData ) {
                      aoData.push( { "name": "nama_obat", "value":""+$('.focused').find('.barisnamaobat').val()+""} );
                    }
                    
                } );
				$('#dataTable4').css('width','100%');
                $('#daftarobat').modal("show");
                    var check = function(){
                        if($('#listobat tr').length >0 && !$('#listobat tr').hasClass('ui-selected')){
                            // run when condition is met
                            $('#listobat tr:first').addClass('ui-selected');
                        }
                        else {
                            setTimeout(check, 1000); // check again in a second
                        }
                    }
                    check();     
            }
        });
		
		$('.bariskodeobat').keyup(function(e){
            if(e.keyCode == 13){
                //alert('xx')
                $('.bariskodeobat').parent().parent('tr').removeClass('focused');
                $(this).parent().parent('tr').addClass('focused');

                $("#dataTable4").dataTable().fnDestroy();
                $('#dataTable4').dataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": "<?php echo base_url() ?>index.php/transapotek/aptpengajuan/ambildaftarobatbykode/",
					"sServerMethod": "POST",
                    "fnServerParams": function ( aoData ) {
                      aoData.push( { "name": "kd_obat", "value":""+$('.focused').find('.bariskodeobat').val()+""} );
                    }
                    
                } );
				$('#dataTable4').css('width','100%');
                $('#daftarobat').modal("show");
                    var check = function(){
                        if($('#listobat tr').length >0 && !$('#listobat tr').hasClass('ui-selected')){
                            // run when condition is met
                            $('#listobat tr:first').addClass('ui-selected');
                        }
                        else {
                            setTimeout(check, 1000); // check again in a second
                        }
                    }
                    check();     
            }
        });
		
	$('#kd_supplier').keyup(function(e){
		if(e.keyCode == 13){
            $.ajax({
                url: '<?php echo base_url() ?>index.php/transapotek/aptpengajuan/ambilsupplierbykode/',
                async:false,
                type:'get',
                data:{query:$(this).val()},
                success:function(data){
                //typeahead.process(data)
					$('#listsupplier').empty();
					$.each(data,function(i,l){
						//alert(l);
						$('#listsupplier').append('<tr><td>'+l.kd_supplier+'</td><td>'+l.nama+'</td><td>'+l.alamat+'</td><td><a class="btn" onclick=\'pilihsupplier("'+l.kd_supplier+'","'+l.nama+'")\'>Pilih</a></td></tr>');
					});    
                },
                dataType:'json'                         
            }); 
            $('#daftarsupplier').modal("show");
		}
		var ex = document.getElementById('dataTable5');
        if ( ! $.fn.DataTable.fnIsDataTable( ex ) ) {
            $('#dataTable5').dataTable({
                "sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
				"sPaginationType": "bootstrap",
                "oLanguage": {
					"sLengthMenu": "Show _MENU_ entries"
                }
            });
            var oTable = $('#dataTable5').dataTable();
            $('#nama1').keyup(function(e){
				oTable.fnFilter( $(this).val() );
                if(e.keyCode == 13){
                    //alert('xx')
                    return false;
                }
            });
        }
	});
	
	$('#nama').keyup(function(e){
		if(e.keyCode == 13){

            $.ajax({
                url: '<?php echo base_url() ?>index.php/transapotek/aptpengajuan/ambilsupplierbynama/',
                async:false,
                type:'get',
                data:{query:$(this).val()},
                success:function(data){
                //typeahead.process(data)
					$('#listsupplier').empty();
					$.each(data,function(i,l){
						//alert(l);
						$('#listsupplier').append('<tr><td>'+l.kd_supplier+'</td><td>'+l.nama+'</td><td>'+l.alamat+'</td><td><a class="btn" onclick=\'pilihsupplier("'+l.kd_supplier+'","'+l.nama+'")\'>Pilih</a></td></tr>');
					});    
                },
                dataType:'json'                         
            }); 
            $('#daftarsupplier').modal("show");
		}
		var ex = document.getElementById('dataTable5');
        if ( ! $.fn.DataTable.fnIsDataTable( ex ) ) {
            $('#dataTable5').dataTable({
                "sDom": "<'pull-right'l>t<'row-fluid'<'span6'f><'span6'p>>",
				"sPaginationType": "bootstrap",
                "oLanguage": {
					"sLengthMenu": "Show _MENU_ entries"
                }
            });
            var oTable = $('#dataTable5').dataTable();
            $('#nama1').keyup(function(e){
				oTable.fnFilter( $(this).val() );
                if(e.keyCode == 13){
                    //alert('xx')
                    return false;
                }
            });
        }
	});
		
	$('input[type="text"]').keydown(function(e){
        //get the next index of text input element
        var next_idx = $('input[type="text"]').index(this) + 1;
         
        //get number of text input element in a html document
        var tot_idx = $('body').find('input[type="text"]').length;
     
        //enter button in ASCII code
        if(e.keyCode == 13){
            //go to the next text input element
            if($(this).parent()[0]['nodeName']=='TD'){
                return false;
            }
            //$('input[type=text]:eq(' + next_idx + ')').focus();
            //$(this).next().focus();
            return false;


        }

    });
	
	var opts = {
      lines: 9, // The number of lines to draw
      length: 40, // The length of each line
      width: 9, // The line thickness
      radius: 0, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#000', // #rgb or #rrggbb
      speed: 1.4, // Rounds per second
      trail: 54, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: 'auto', // Top position relative to parent in px
      left: '470px' // Left position relative to parent in px
    };
    var target = document.getElementById('progress');
    var spinner = new Spinner(opts).spin(target);

$(document).ready(function() {
    $("#daftarobat").on("show", function () {
      $("body").addClass("modal-open");

    }).on("hidden", function () {
        if($('#daftarobat').is(':visible')){
            $("body").addClass("modal-open");
        }else{
            $("body").removeClass("modal-open");
        }
    });

});	

</script>