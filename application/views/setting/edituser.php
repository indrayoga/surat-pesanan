<style type="text/css">
.fixed {
    position:fixed;
    top:0px !important;
    z-index:100;
    width: 1024px;    
}
.body1{
    opacity: 0.4;
    background-color: #000000;
}
</style>
<script type="text/javascript">

    $(document).ready(function() {
                $('#aplikasi').change(function(){
                    //var aplikasi=$(this).val();
                    //alert(aplikasi);
                    //if(aplikasi=='4'){
                       // $('#poli').show();
                   // }else{
                       // $('#poli').hide();
                   // }
                });

        $('#form').ajaxForm({
            beforeSubmit: function(a,f,o) {
                var selectedElmsIds = [];
                var selectedElms = $('#jstree').jstree("get_bottom_selected", true);
                x=0;
                $.each(selectedElms, function() {
                    if(this.id!=""){
                        selectedElmsIds.push(this.id);
                        a.push({name:'aksesuser['+x+']', value:this.id})
                        x++;
                    }
                });

                console.log(selectedElmsIds);
                o.dataType = "json";
                $('div.error').removeClass('error');
                $('span.help-inline').html('');
                $('#progress').show();
                $('body').append('<div id="overlay1" style="position: fixed;height: 100%;width: 100%;z-index: 1000000;"></div>');
                $('body').addClass('body1');
                var urlnya="<?php echo base_url(); ?>index.php/setting/periksauser";
                //console.log($.param(a));
                //console.log($('#form').serialize());
                z=true;
                $.ajax({
                url: urlnya,
                type:"POST",
                async: false,
                data: $.param(a),
                success: function(data){
                    //alert(data.status);
                    if(parseInt(data.status)==1){
                        z=data.status;
                        //alert('aa');
                        //alert($('input[name="harga"]').val());
                    }else if(parseInt(data.status)==0){
                        //alert('xxx');
                        $('#progress').hide();
                        z=data.status;
                        for(yangerror=0;yangerror<=data.error;yangerror++){
                            $('#'+data.id[yangerror]).siblings('.help-inline').html('<p class="text-error">'+data.pesan[yangerror]+'</p>');
                            $('#'+data.id[yangerror]).parents('row-fluid').focus();
                            //$('#error').html('<div class="alert alert-error fade in"><button data-dismiss="alert" class="close" type="button"><i class="iconic-x"></i></button>Terdapat beberapa kesalahan input silahkan cek inputan anda</div>');                                 
                        }
                        $('#error').html('<div class="alert alert-error fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesanatas+'<br/>'+data.pesanlain+'</div>');

                        $('#overlay1').remove();
                        $('body').removeClass('body1');
                    }
                },
                dataType: 'json'
                });

                if(z==0)return false;
            },
            dataType:  'json',
            success: function(data) {
            //alert(data);
            if (typeof data == 'object' && data.nodeType)
            data = elementToString(data.documentElement, true);
            else if (typeof data == 'object')
            //data = objToString(data);
                if(parseInt(data.status)==1) //jika berhasil
                {
                    //apa yang terjadi jika berhasil
                    $('#progress').hide();
                    $('#overlay1').remove();
                    $('body').removeClass('body1');
                    $('#error').show();
                    $('#error').html('<div class="alert alert-success fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesan+'</div>');
                    window.location.href="<?php echo base_url() ?>index.php/setting/user";    
                
                }
                else if(parseInt(data.status)==0) //jika gagal
                {
                    //apa yang terjadi jika gagal
                    $('#progress').hide();
                    $('#overlay1').remove();
                    $('body').removeClass('body1');
                    $('#error').show();
                    $('#error').html('<div class="alert alert-success fade in navbar navbar-fixed-top" style="margin-left:70px;margin-right:70px;"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+data.pesan+'</div>');
                    window.location.href="<?php echo base_url() ?>index.php/setting/user";    
                }

            }
        });       

    });


</script>
<style type="text/css">.datepicker{z-index:1151;}</style>
            <div id="error"></div>
            <div id="overlay"></div>
            <!-- #content -->
            <div id="content">
                <!-- .outer -->
                <div class="container-fluid outer">
                    <div class="row-fluid">
                        <!-- .inner -->
                        <div class="span12 inner">
                      <!--BEGIN INPUT TEXT FIELDS-->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="box">
                                        <header>
                                            <div class="icons"><i class="icon-edit"></i></div>
                                            <h5>Update User</h5>
                                            <!-- .toolbar -->
                                            <div class="toolbar" style="height:auto;">
                                                <ul class="nav nav-tabs">
                                                    <li>
                                                        <a class="accordion-toggle minimize-box" data-toggle="collapse" href="#div-1">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /.toolbar -->
                                        </header>
                                        <div id="div-1" class="accordion-body collapse in body">
                                            <form class="form-horizontal" id="form" method="post" action="<?php echo base_url() ?>index.php/setting/updateuser">
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="control-group">
                                                            <label for="kd_aplikasi" class="control-label">Aplikasi</label>
                                                            <div class="controls with-tooltip">
                                                                <select name="kd_aplikasi" id="kd_aplikasi" class="input-large" onchange="javascript:window.location.href='<?php echo base_url() ?>index.php/setting/edituser/<?php echo $id_user; ?>/'+this.value">
                                                                    <option value="">Pilih Aplikasi</option>
                                                                    <?php
                                                                    foreach ($dataaplikasi as $aplikasi) {
                                                                        # code...
                                                                        if($aplikasi['kd_aplikasi']==$app)$sel="selected=selected"; else $sel="";
                                                                    ?>
                                                                    <option value="<?php echo $aplikasi['kd_aplikasi'] ?>" <?php echo $sel; ?>><?php echo $aplikasi['nama_aplikasi'] ?></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <!--<div class="row-fluid" style="<?php if($app=='4')echo "display:block;";else echo "display:none"; ?>">
                                                    <div class="span12">
                                                        <div class="control-group">
                                                            <label for="poli" class="control-label">Poli</label>
                                                            <div class="controls with-tooltip">
                                                                <select name="poli" id="poli" class="input-large" onchange="javascript:window.location.href='<?php echo base_url() ?>index.php/setting/edituser/<?php echo $id_user; ?>/<?php echo $app; ?>/'+this.value">
                                                                    <option value="">Pilih Poliklinik</option>
                                                                        <?php
                                                                        $datapoli=$this->muser->ambilData('unit_kerja','parent=10');
                                                                        foreach ($datapoli as $poli) {
                                                                                if($poli['kd_unit_kerja']==$this->input->cookie('poli')){
                                                                                    $sel="selected=selected"; 
                                                                                }else if($poli['kd_unit_kerja']==$unit){
                                                                                   // debugvar($poli);
                                                                                    $sel="selected=selected"; 
                                                                                }else{
                                                                                  $sel="";  
                                                                                } 
                                                                        ?>
                                                                            <option value="<?php echo $poli['kd_unit_kerja'] ?>" <?php echo $sel; ?>><?php echo $poli['nama_unit_kerja'] ?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="row-fluid" style="<?php if($app=='7')echo "display:block;";else echo "display:none"; ?>">
                                                    <div class="span12">
                                                        <div class="control-group">
                                                            <label for="ruangan" class="control-label">Ruangan</label>
                                                            <div class="controls with-tooltip">
                                                                <select name="ruangan" id="ruangan" class="input-large" onchange="javascript:window.location.href='<?php echo base_url() ?>index.php/setting/edituser/<?php echo $id_user; ?>/<?php echo $app; ?>/'+this.value">
                                                                    <option value="">Pilih Ruangan</option>
                                                                        <?php
                                                                        $datapoli=$this->muser->ambilData('unit_kerja','parent=8');
                                                                        foreach ($datapoli as $poli) {
                                                                                if($poli['kd_unit_kerja']==$this->input->cookie('poli')){
                                                                                    $sel="selected=selected"; 
                                                                                }else if($poli['kd_unit_kerja']==$unit){
                                                                                   // debugvar($poli);
                                                                                    $sel="selected=selected"; 
                                                                                }else{
                                                                                  $sel="";  
                                                                                } 
                                                                        ?>
                                                                            <option value="<?php echo $poli['kd_unit_kerja'] ?>" <?php echo $sel; ?>><?php echo $poli['nama_unit_kerja'] ?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </div>-->
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="control-group">
                                                            <label for="username" class="control-label">Username</label>
                                                            <div class="controls with-tooltip">
                                                                <input type="hidden" name="id_user" value="<?php if(isset($item['id_user'])) echo $item['id_user']; ?>" id="" class="input-medium">
                                                                <input type="text" name="username" disabled value="<?php if(isset($item['username'])) echo $item['username']; ?>" id="username" class="input-medium">
                                                                <span class="help-inline"></span>
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12">
<div id="jstree">
  <ul>
  <?php
    $roots=$this->msetting->ambilData('menu','level=1 and kd_aplikasi="'.$app.'" ');
    foreach ($roots as $root) {
        $query=$this->db->query('select * from user_akses a join akses b on a.id_akses=b.id_akses where id_user="'.$id_user.'" and a.id_akses="'.$root['id_akses'].'" and b.aplikasi="'.$app.'" and b.aktif=1 ');
        $check=$query->row_array();
        # code...
    echo "<li ".( (!empty($root['id_akses'])) ? "id='".$root['id_akses']."'" : "" ) .">".( ($check) ? "<a href='#' class='jstree-clicked'>" : "" ).$root['menu']."</a>";
    ?>
    <?php
        $children1=$this->msetting->ambilData('menu','level=2 and parent="'.$root['id'].'" and kd_aplikasi="'.$app.'" ');
        if(!empty($children1)){
            echo "<ul>";
            foreach ($children1 as $child1) {
                $query=$this->db->query('select * from user_akses a join akses b on a.id_akses=b.id_akses where id_user="'.$id_user.'" and a.id_akses="'.$child1['id_akses'].'" and b.aplikasi="'.$app.'" and b.aktif=1 ');
                $check=$query->row_array();
                # code...
                echo "<li ".( (!empty($child1['id_akses'])) ? "id='".$child1['id_akses']."'" : "" ) .">".( ($check) ? "<a href='#' class='jstree-clicked'>" : "" ).$child1['menu']."</a>";
                    $children2=$this->msetting->ambilData('menu','level=3 and parent="'.$child1['id'].'" and kd_aplikasi="'.$app.'" ');
                    if(!empty($children2)){
                        echo "<ul>";
                        foreach ($children2 as $child2) {
                            $query=$this->db->query('select * from user_akses a join akses b on a.id_akses=b.id_akses where id_user="'.$id_user.'" and a.id_akses="'.$child2['id_akses'].'" and b.aplikasi="'.$app.'" and b.aktif=1 ');
                            $check=$query->row_array();
                            # code...
                            echo "<li ".( (!empty($child2['id_akses'])) ? "id='".$child2['id_akses']."'" : "" ) .">".( ($check) ? "<a href='#' class='jstree-clicked'>" : "" ).$child2['menu']."</a>";
                            $children3=$this->msetting->ambilData('menu','level=4 and parent="'.$child2['id'].'" and kd_aplikasi="'.$app.'" ');
                            if(!empty($children3)){
                                echo "<ul>";
                                foreach ($children3 as $child3) {
                                    $query=$this->db->query('select * from user_akses a join akses b on a.id_akses=b.id_akses where id_user="'.$id_user.'" and a.id_akses="'.$child3['id_akses'].'" and b.aplikasi="'.$app.'" and b.aktif=1 ');
                                    $check=$query->row_array();
                                    # code...
                                    echo "<li ".( (!empty($child3['id_akses'])) ? "id='".$child3['id_akses']."'" : "" ) .">".( ($check) ? "<a href='#' class='jstree-clicked'>" : "" ).$child3['menu']."</a>";
                                    $children4=$this->msetting->ambilData('menu','level=5 and parent="'.$child3['id'].'" and kd_aplikasi="'.$app.'" ');
                                    if(!empty($children4)){
                                        echo "<ul>";
                                        foreach ($children4 as $child4) {
                                            # code...
                                            $query=$this->db->query('select * from user_akses a join akses b on a.id_akses=b.id_akses where id_user="'.$id_user.'" and a.id_akses="'.$child4['id_akses'].'" and b.aplikasi="'.$app.'" and b.aktif=1 ');
                                            $check=$query->row_array();
                                            echo "<li ".( (!empty($child4['id_akses'])) ? "id='".$child4['id_akses']."'" : "" ).( ($check) ? "<a href='#' class='jstree-clicked'>" : "" ) .">".$child4['menu'];
                                        }
                                        echo "</ul>";
                                    }
                                    echo "</li>";
                                }
                                echo "</ul>";
                            } 
                            echo "</li>";         
                        }
                        echo "</ul>";
                    }  
                echo "</li>";            
            }
            echo "</ul>";
        }
    echo "</li>";
    }
  ?>
    <!--<li>Root node 1
      <ul>
        <li>Child node 1</li>
        <li><a href="#">Child node 2</a></li>
      </ul>
    </li>-->
  </ul>
</div>

                                                    </div>
                                                </div>       
                                                <div class="control-group">
                                                    <label for="text1" class="control-label">&nbsp;</label>
                                                    <div class="controls with-tooltip">
                                                        <button class="btn btn-primary" type="submit"><i class="icon-ok"></i> Simpan</button>
                                                        <button class="btn " type="reset"><i class="icon-undo"></i> Reset</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <div id="progress" style="display:none;"></div>                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TEXT INPUT FIELD-->                            

                            <hr>
                        </div>
                        <!-- /.inner -->
                    </div>
                    <!-- /.row-fluid -->
                </div>
                <!-- /.outer -->
            </div>
            <!-- /#content -->


<script type="text/javascript">

$('#jstree').jstree({
  "plugins" : [ "wholerow", "checkbox" ]
});

    $('#cbmenuall').click(function(){
        if($(this).checked){
            $('.cbmenu').prop( "checked", true );
        }else{
            $('.cbmenu').prop( "checked", false );
        }
    });
    $.configureBoxes(
      );

    $('#tanggal').datepicker({
        format: 'dd-mm-yyyy'
    });


    $('.with-tooltip').tooltip({
        selector: ".input-tooltip"
    });

    var opts = {
      lines: 9, // The number of lines to draw
      length: 40, // The length of each line
      width: 9, // The line thickness
      radius: 0, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#000', // #rgb or #rrggbb
      speed: 1.4, // Rounds per second
      trail: 54, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: 'auto', // Top position relative to parent in px
      left: '470px' // Left position relative to parent in px
    };
    var target = document.getElementById('progress');

</script>