            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <!-- #push do not remove -->
            <div id="push"></div>
            <!-- /#push -->
        </div>
        <!-- END WRAP -->

        <div class="clearfix"></div>

        <!-- BEGIN FOOTER -->

        <footer class="navbar-default navbar-fixed-bottom">
            <div id="footer">
                <p>2013 © Planet IT</p>
            </div>            
            <div class="span10 offset3">
            <form class="navbar-form">
            <input type="text" class="span2" value="<?php echo $this->session->userdata('nama_pegawai'); ?>" disabled>
            <?php 
                $unitshft=$this->session->userdata('unitshift');
                //debugvar($unitshft);
                $queryunitshift=$this->db->query('select * from unit_shift where kd_unit="'.$unitshft.'"'); 
                $unitshift=$queryunitshift->row_array();
            ?>
            <input type="text" class="span2" value="Loket <?php echo $this->session->userdata('loket'); ?>" disabled>
            <input type="text" class="span2" value="SHIFT <?php echo $unitshift['shift']; ?>" disabled>
            <?php 
                if($this->session->userdata('aplikasi')=='50'){
                    $queryunitapotek=$this->db->query('select * from apt_unit where kd_unit_apt="'.$this->session->userdata('kd_unit_apt').'"'); 
                    $unitapotek=$queryunitapotek->row_array();
                    ?>
                        <input type="text" class="span2" value="<?php echo $unitapotek['nama_unit_apt']; ?>" disabled>            
                    <?php
                }
                if($this->session->userdata('aplikasi')=='8'){
                    $queryunitapotek=$this->db->query('select * from log_lokasi where kd_lokasi="'.$this->session->userdata('kd_lokasi').'"'); 
                    $unitapotek=$queryunitapotek->row_array();
                    ?>
                        <input type="text" class="span2" value="<?php echo $unitapotek['lokasi']; ?>" disabled>            
                    <?php
                }
            ?>
            </form>        
            </div>
        </footer>        
<?php

   // include('/chat/samplea.php');
?>
        <!-- END FOOTER -->

        <!-- #helpModal -->
        <div id="helpModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="helpModalLabel"
             aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="helpModalLabel"><i class="icon-external-link"></i> Help</h3>
            </div>
            <div class="modal-body">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                    nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
                    anim id est laborum.
                </p>
            </div>
            <div class="modal-footer">

                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
        <!-- /#helpModal -->
    </body>
</html>
		
<script type="text/javascript">
$(document).ready(function(){
    if(($('.with-tooltip').siblings().attr('class')=='control-label wajib')){
       // console.log($('.with-tooltip').children().hasClass('input-tooltip'));
        $('.with-tooltip *').each(function(){
            if($(this).hasClass('input-tooltip') && $(this).parent().siblings().attr('class')=='control-label wajib' ){
                $(this).attr("data-original-title", $(this).attr("data-original-title") + " Wajib di Isi");
            }
        })
    }
});
</script>
<style type="text/css">
footer.navbar-default.navbar-fixed-bottom
 {
      background:#F5F5F5;
      color:black;
      padding:1em 0;
 }
 footer.navbar-default.navbar-fixed-bottom p
 {
      margin:0;
 }
</style>